﻿using System;
using System.Collections.Generic;
using System.Linq;
using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;

namespace Importer.P3_Aggregation.FactoriesSupportive
{

    public static class AtarRecordDtoComposer
    {
        public static AtarRecord[] CreateFor(string originatingActor, ActorMerit[] merits)
        {
            ActorMerit[] targetActorRelevantMerits = merits.Where(m => m.Actor.Equals(originatingActor)).ToArray();
            string[] targetActorRelevantStoryUids = targetActorRelevantMerits.Select(m => m.OriginUid).ToArray();

            List<ActorMerit> meritsOfActorsConnectedToActor = new List<ActorMerit>();
            foreach (string storyUid in targetActorRelevantStoryUids)
            {
                ActorMerit[] actorsInStory = merits.Where(m => m.OriginUid.Equals(storyUid)).Where(m => m.Actor.Equals(originatingActor) == false).ToArray();
                meritsOfActorsConnectedToActor.AddRange(actorsInStory);
            }

            string[] allConnectedActors = meritsOfActorsConnectedToActor.Select(m => m.Actor).Distinct().ToArray();
            List<AtarRecord> atars = new List<AtarRecord>();
            foreach (string relevantActor in allConnectedActors)
            {
                string[] relevantActorUids = merits.Where(m => m.Actor.Equals(relevantActor)).Select(m => m.OriginUid).ToArray();
                string[] originatingActorUids = merits.Where(m => m.Actor.Equals(originatingActor)).Select(m => m.OriginUid).ToArray();
                string[] uids = relevantActorUids.Where(uid => originatingActorUids.Contains(uid)).ToArray();
                atars.Add(new AtarRecord(originatingActor, relevantActor, uids.Length, uids));
            }

            AtarRecord[] presentableAtars = atars.OrderByDescending(a => a.Intensity).ThenBy(a => a.RelevantActor).ToArray();
            return presentableAtars;
        }
    }
}
