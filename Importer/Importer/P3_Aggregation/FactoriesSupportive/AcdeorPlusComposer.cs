﻿using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Importer.P3_Aggregation.FactoriesSupportive
{
    public static class AcdeorPlusComposer
    {
        public static AcdeorPlus[] CreateMany(AcdeorRecord[] acdeors, AggregatedStory[] storyMetas)
        {
            List<AcdeorPlus> aggregated = new List<AcdeorPlus>();
            foreach (AcdeorRecord acdeor in acdeors)
            {
                AggregatedStory storyMeta = storyMetas.Where(sm => sm.StoryUid.Equals(acdeor.OriginUid)).First();
                aggregated.Add(CreateSingle(acdeor, storyMeta));
            }

            return aggregated.OrderBy(am => am.EndDate).ToArray();
        }

        public static AcdeorPlus CreateSingle(AcdeorRecord acdeor, AggregatedStory storyMetadata)
        {
            return new AcdeorPlus
                (
                    originUid: acdeor.OriginUid,
                    actor: acdeor.Actor,
                    deed: acdeor.Deed,
                    endDate: storyMetadata.EndDate
                );
        }
    }
}
