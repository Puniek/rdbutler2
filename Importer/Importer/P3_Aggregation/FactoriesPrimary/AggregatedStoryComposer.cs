﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P3_Aggregation.Support;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    /// <summary>
    /// This is an AggregatedStoryFactory, but used from the Importer side. To make sure we have a name deduplication I call it a composer.
    /// </summary>
    public static class AggregatedStoryComposer
    {
        public static AggregatedStory[] CreateManyFromStories(ReadStory[] allStories, GlobalTime globalTime)
        {
            var storyMetadata = new List<AggregatedStory>();
            foreach (ReadStory story in allStories) { storyMetadata.Add(CreateSingleFromStory(story, allStories, globalTime)); }
            return storyMetadata.ToArray();
        }

        public static AggregatedStory CreateSingleFromStory(ReadStory story, ReadStory[] allStories, GlobalTime globalTime)
        {
            string storyUid = string.IsNullOrWhiteSpace(story.Uid) ? throw new ArgumentException("Story UID needs to exist") : story.Uid;
            string[] threads = story.Threads.Any() ? story.Threads : new []{ StoryDefaults.Threads };
            string[] motives = story.Motives.Any() ? story.Motives : Array.Empty<string>();
            string[] previousStories = story.PrevCampaignIds;
            string[] players = story.Players;
            string[] gms = story.Gms;

            int seqNo = -1;
            ReadStory[] ordered = allStories.OrderBy(s => s.Uid).ToArray();
            for (int i = 0; i < ordered.Length; i++) { if (ordered[i].Uid == storyUid) { seqNo = i + 1; break; } }

            (DateTime, DateTime) startEndDates = TimeCalculator.CalculateStartEndDates(story, allStories, globalTime);
            (string, string) formattedStartEndDates = (startEndDates.Item1.ToString("yyyy-MM-dd"), startEndDates.Item2.ToString("yyyy-MM-dd"));

            return new AggregatedStory(
                storyUid: storyUid,
                storyTitle: story.Title,
                threads: threads,
                motives: motives,
                startDate: formattedStartEndDates.Item1,
                endDate: formattedStartEndDates.Item2,
                seqNo: seqNo.ToString(),
                previousStories: previousStories,
                gms: story.Gms,
                players: story.Players,
                playerActors: story.ActorNames.Length >= story.Players.Length ? story.ActorNames[0..players.Length] : story.ActorNames,
                allActors: story.ActorNames,
                summary: story.Summary,
                body: story.Body
                );
        }

        public static AggregatedStory CreateWithDefaults(
            string storyUid = StoryDefaults.Uid,
            string storyTitle = StoryDefaults.Title,
            string[] threads = null,
            string[] motives = null,
            string startDate = null,
            string endDate = null,
            string seqNo = "1",
            string[] prevCampaignIds = null,
            string[] gms = null,
            string[] players = null,
            string[] playerActors = null,
            string[] allActors = null,
            string summary = StoryDefaults.Summary,
            string body = StoryDefaults.Body
            )
        {
            threads ??= new[] { StoryDefaults.Threads };
            motives ??= Array.Empty<string>();
            prevCampaignIds ??= StoryDefaults.PrevCampaignIds;
            startDate ??= new DateTime(1111, 11, 11).ToString("yyyy-MM-dd");
            endDate ??= new DateTime(1111, 11, 11).ToString("yyyy-MM-dd");
            gms ??= StoryDefaults.Gms;
            players ??= StoryDefaults.Players;
            playerActors ??= StoryDefaults.PlayerActors;
            allActors ??= StoryDefaults.AllActors;

            return new AggregatedStory(
                storyUid: storyUid,
                storyTitle: storyTitle,
                threads: threads,
                motives: motives,
                startDate: startDate,
                endDate: endDate,
                seqNo: seqNo,
                previousStories: prevCampaignIds,
                gms: gms,
                players: players,
                playerActors: playerActors,
                allActors: allActors,
                summary: summary,
                body: body
                );
        }
    }

}
