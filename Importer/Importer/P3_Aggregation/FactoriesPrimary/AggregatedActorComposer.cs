﻿using System;

using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P2_Extraction.Entities;
using Importer.P3_Aggregation.FactoriesSupportive;
using System.Collections.Generic;
using System.Linq;
using Commons.Tools;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    /// <summary>
    /// This is an AggregatedActorFactory, but used from the Importer side. To make sure we have a name deduplication I call it a composer.
    /// </summary>
    public static class AggregatedActorComposer
    {
        public static AggregatedActor[] CreateMany(AggregatedStory[] stories,
            ReadActor[] actorSheets,
            ActorMerit[] merits,
            ActorProgression[] actorProgressions,
            AggregatedActorFlashcard[] aggregatedActorFlashcards,
            IReadOnlyDictionary<string, string[]> storyToThreadsMap)
        {
            // With whom we have to work/
            string[] uniqueActorNames = ActorCandidateExtractor.GetUniqueActors(actorSheets, merits);

            // Aggregate information on every single one of them.
            List<AggregatedActor> aggregatedActors = new List<AggregatedActor>();
            foreach (string processedActorName in uniqueActorNames)
            {
                FlashcardRecord[] flashcards = aggregatedActorFlashcards.FirstOrDefault(x => x.ActorName == processedActorName)?.Records ?? Array.Empty<FlashcardRecord>();
                aggregatedActors.Add(CreateSingle(processedActorName, stories, actorSheets, merits, actorProgressions, flashcards, storyToThreadsMap));
            }
            return aggregatedActors.ToArray();
        }

        public static AggregatedActor CreateSingle(string processedActorName,
            AggregatedStory[] stories,
            ReadActor[] actors,
            ActorMerit[] allMerits,
            ActorProgression[] allProgressions,
            FlashcardRecord[] flashcardRecords, IReadOnlyDictionary<string, string[]> storyToThreadsMap)
        {
            ReadActor actorProfile = actors.Where(a => a.Name.Equals(processedActorName)).FirstOrDefault();                     //null if no Profile
            string[] storyIds = allMerits.Where(m => m.Actor.Equals(processedActorName)).Select(m => m.OriginUid).ToArray();   //empty if no Stories

            // Sensible defaults assuming actorProfile is null:
            string mechver = "9999";
            string uid = processedActorName.ToActorUid(mechver);
            string[] factions = new string[] { };
            string owner = "public";
            string body = string.Empty;

            if (actorProfile != null)
            {
                mechver = actorProfile.Mechver;
                uid = actorProfile.Uid;
                factions = actorProfile.Factions;
                owner = actorProfile.Owner;
                body = ExtractPureMarkdown(actorProfile.Body);
            }

            // Sensible defaults assuming there is no past for this Profile:
            AggregatedMerit[] aggregatedMerits = new AggregatedMerit[] { };
            AcdeorPlus[] aggregatedProgressions = new AcdeorPlus[] { };
            AtarRecord[] actorToActorRelations = new AtarRecord[] { };
            string[] threads = Array.Empty<string>();

            if (storyIds.Length > 0)
            {
                ActorMerit[] merits = allMerits.Where(m => m.Actor.Equals(processedActorName)).ToArray();
                AggregatedStory[] relevantStoriesToMerits = stories.Where(sm => merits.Any(m => m.OriginUid.Equals(sm.StoryUid))).ToArray();
                aggregatedMerits = AggregatedMeritComposer.CreateMany(merits, relevantStoriesToMerits);

                ActorProgression[] progressions = allProgressions.Where(m => m.Actor.Equals(processedActorName)).ToArray();
                AggregatedStory[] relevantMetasToProgressions = stories.Where(sm => progressions.Any(p => p.OriginUid.Equals(sm.StoryUid))).ToArray();
                aggregatedProgressions = AcdeorPlusComposer.CreateMany(progressions, relevantMetasToProgressions);

                actorToActorRelations = AtarRecordDtoComposer.CreateFor(processedActorName, allMerits);

                threads = storyIds.SelectMany(x => storyToThreadsMap.TryGetValue(x, out var values) ? values : Array.Empty<string>()).ToArray();
            }

            return new AggregatedActor(
                uid: uid,
                name: processedActorName,
                mechver: mechver,
                factions: factions,
                owner: owner,
                body: body,
                merits: aggregatedMerits,
                progressions: aggregatedProgressions,
                atarRelations: actorToActorRelations,
                flashcardRecords: flashcardRecords,
                threads: threads
            );
        }

        private static string ExtractPureMarkdown(string body) => body.Substring(body.IndexOf("##"));
    }

    public static class ActorCandidateExtractor
    {
        public static string[] GetUniqueActors(ReadActor[] actors, ActorMerit[] merits)
        {
            var actorProfileUids = actors.Select(a => a.Name).Distinct();
            var actorMeritUids = merits.Select(a => a.Actor).Distinct();
            return actorProfileUids.Union(actorMeritUids).ToArray();
        }
    }
}
