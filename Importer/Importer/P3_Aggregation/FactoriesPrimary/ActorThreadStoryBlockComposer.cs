﻿using Commons.CoreEntities;
using Importer.P3_Aggregation.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public class ActorThreadStoryBlockComposer
    {
        public static ActorThreadStoryBlock[] CreateManyFromManyActorsManyStories(
            AggregatedActor[] targetActors,
            AggregatedStory[] allStories,
            LocationRecord[] allLocationRecords
            )
        {
            AggregatedMerit[] allMerits = targetActors.SelectMany(s => s.Merits).ToArray();
            return targetActors.SelectMany(actor => CreateManyFromSingleActorManyStories(actor, allStories, allMerits, allLocationRecords)).ToArray();
        }

        public static ActorThreadStoryBlock[] CreateManyFromSingleActorManyStories(AggregatedActor actor, AggregatedStory[] allStories, AggregatedMerit[] allMerits, LocationRecord[] allLocationRecords)
        {
            string[] allRelevantStoryUids = StoryUidSelector.SelectAllRelevantToActor(actor.Merits, actor.Progressions);
            AggregatedStory[] relevantStories = allStories.Where(s => allRelevantStoryUids.Contains(s.StoryUid)).ToArray();

            return relevantStories.Select(story => CreateSingleFromSingleActorSingleStory(story, actor.Name, allMerits, actor.Merits, actor.Progressions)).ToArray();
        }

        public static ActorThreadStoryBlock CreateSingleFromSingleActorSingleStory(
            AggregatedStory story, 
            string actorName,
            AggregatedMerit[] allMerits,
            AggregatedMerit[] allMeritsForThisActor, 
            AcdeorPlus[] allProgressionsForThisActor
            )
        {
            string storyUid = story.StoryUid;
            string storyTitle = story.StoryTitle;
            string startDate = story.StartDate;
            string endDate = story.EndDate;
            string summary = story.Summary;

            string actorDeed;
            AggregatedMerit[] actorMeritsInThisStory = allMeritsForThisActor.Where(m => m.OriginUid == storyUid).ToArray();
            if (actorMeritsInThisStory.Length == 0) actorDeed = "AKTOR NIEOBECNY NA SESJI";
            else actorDeed = actorMeritsInThisStory.Select(m => m.Deed).FirstOrDefault();

            string[] actorProgressions = allProgressionsForThisActor.Where(p => p.OriginUid == storyUid).Select(p => p.Deed).ToArray();
            string[] actorsPresent = allMerits.Where(m => m.OriginUid == storyUid).Select(m => m.Actor).OrderBy(a => a).ToArray();

            return new ActorThreadStoryBlock(
                originatingActor: actorName,
                storyUid: storyUid,
                storyTitle: storyTitle,
                startDate: startDate,
                endDate: endDate,
                summary: summary,
                actorDeed: actorDeed,
                actorProgressions: actorProgressions,
                actorsPresent: actorsPresent
            );
        }
    }
}
