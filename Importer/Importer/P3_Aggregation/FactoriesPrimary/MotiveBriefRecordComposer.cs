﻿using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public static class MotiveBriefRecordComposer
    {
        public static MotiveBriefRecord[] CreateMany(AggregatedStory[] aggregatedStories, ReadMotive[] readMotives)
        {
            string[] allMotivesToConsider = aggregatedStories.SelectMany(s => s.Motives).Distinct().ToArray();

            List<MotiveBriefRecord> records = new List<MotiveBriefRecord>();
            foreach (var motiveId in allMotivesToConsider)
            {
                AggregatedStory[] relevantStories = aggregatedStories.Where(s => s.Motives.Contains(motiveId)).ToArray();
                ReadMotive linkedMotive = readMotives.Where(s => s.TagUid.Equals(motiveId)).FirstOrDefault();

                string startDate = relevantStories.Select(s => s.StartDate).Min();
                string endDate = relevantStories.Select(s => s.EndDate).Max();
                int storyCount = relevantStories.Count();
                string[] players = relevantStories.SelectMany(s => s.Players).Distinct().ToArray();
                string[] playerActors = relevantStories.SelectMany(s => s.PlayerActors).Distinct().ToArray();

                records.Add(new MotiveBriefRecord(
                    uid: motiveId,
                    name: linkedMotive.Name,
                    startDate: startDate,
                    endDate: endDate,
                    storyCount: storyCount,
                    players: players,
                    playerActors: playerActors,
                    shortDesc: linkedMotive != null ? linkedMotive.ShortDesc : string.Empty
                    ));
            }

            return records.ToArray();
        }

    }
}
