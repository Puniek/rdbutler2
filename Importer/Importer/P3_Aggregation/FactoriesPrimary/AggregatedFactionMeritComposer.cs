﻿using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public class AggregatedFactionMeritComposer
    {
        public static AggregatedFactionMerit[] Create(FactionMerit[] factionMerits, AggregatedStory[] aggregatedStories)
        {
            AggregatedFactionMerit[] aFMerits = factionMerits
                .Select(fm => CreateSingle(fm, aggregatedStories.Where(ags => ags.StoryUid == fm.OriginUid).FirstOrDefault()))
                .ToArray();

            return aFMerits;
        }

        public static AggregatedFactionMerit CreateSingle(FactionMerit merit, AggregatedStory aggregatedStory)
        {
            return new AggregatedFactionMerit(
                originUid: merit.OriginUid,
                actor: merit.Actor,
                deed: merit.Deed,
                threads: string.Join(", ", aggregatedStory.Threads),
                startDate: aggregatedStory.StartDate,
                endDate: aggregatedStory.EndDate
                );
        }
    }
}
