﻿using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public static class ActorBriefRecordComposer
    {
        public static ActorBriefRecord[] CreateMany(AggregatedActor[] aActors)
        {
            return aActors.Select(a => CreateSingle(a)).ToArray();
        }

        public static ActorBriefRecord CreateSingle(AggregatedActor aActor)
        {
            string name = aActor.Name;
            string mechver = aActor.Mechver;
            int intensity = aActor.Merits.Count();

            return new ActorBriefRecord(name: name, mechver: mechver, intensity: intensity, link: aActor.Uid);
        }
    }
}
