﻿using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.FactoriesPrimary
{
    public static class ThreadBriefRecordComposer
    {
        public static ThreadBriefRecord[] CreateMany(AggregatedStory[] aggregatedStories, ReadThread[] readThreads)
        {
            string[] allThreadsToConsider = aggregatedStories.SelectMany(s => s.Threads).Distinct().ToArray();
            
            List<ThreadBriefRecord> records = new List<ThreadBriefRecord>();
            foreach (var threadId in allThreadsToConsider)
            {
                AggregatedStory[] relevantStories = aggregatedStories.Where(s => s.Threads.Contains(threadId)).ToArray();
                ReadThread linkedThread = readThreads.Where(s => s.TagUid.Equals(threadId)).FirstOrDefault();

                string startDate = relevantStories.Select(s => s.StartDate).Min();
                string endDate = relevantStories.Select(s => s.EndDate).Max();
                int storyCount = relevantStories.Count();
                string[] players = relevantStories.SelectMany(s => s.Players).Distinct().ToArray();
                string[] playerActors = relevantStories.SelectMany(s => s.PlayerActors).Distinct().ToArray();
                string name = linkedThread == null ? string.Empty : linkedThread.Name;

                records.Add(new ThreadBriefRecord(
                    uid: threadId,
                    name: name,
                    startDate: startDate,
                    endDate: endDate,
                    storyCount: storyCount,
                    players: players,
                    playerActors: playerActors,
                    shortDesc: linkedThread != null ? linkedThread.ShortDesc : string.Empty
                    ));
            }

            return records.ToArray();
        }

    }
}
