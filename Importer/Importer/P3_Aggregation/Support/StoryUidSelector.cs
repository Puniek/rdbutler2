﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.Support
{
    public static class StoryUidSelector
    {
        /// <summary>
        /// Filter only relevant stories (extracting and unique-ing the crossection between Merits and Progression)
        /// </summary>
        public static string[] SelectAllRelevantToActor(AggregatedMerit[] actorMerits, AcdeorPlus[] actorProgressions)
        {
            string[] meritStoryUids = actorMerits.Select(s => s.OriginUid).ToArray();
            string[] progressionStoryUids = actorProgressions.Select(s => s.OriginUid).ToArray();
            string[] storyUids = meritStoryUids.Concat(progressionStoryUids).Distinct().ToArray();

            return storyUids;
        }
    }
}
