﻿using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P3_Aggregation.Support
{
    public static class TimeCalculator
    {
        /// <summary>
        /// Recursive function calculating actual dates from inputs.
        /// </summary>
        /// <param name="story">Story undergoing analysis</param>
        /// <param name="allStories">All existing stories</param>
        /// <returns>START date, END date.</returns>
        public static (DateTime startDate, DateTime endDate) CalculateStartEndDates(ReadStory story, ReadStory[] allStories, GlobalTime globalTime)
        {
            // This is our malformed date. If stuff goes wrong, we return this.
            DateTime malformedDate = new DateTime(1111, 11, 11);

            // Sanity check - malformed previous chronological ID
            if (story.PrevChronoIds.Length < 1)
                return (malformedDate, malformedDate);

            // Error check - negative story duration
            if (story.Time.Duration < 0)
                return (malformedDate, malformedDate);

            // Error check - both StartDate and Chronology are known. Only one should be filled.
            if (IsStartDatePrefilled(story) && IsChronologyPrefilled(story))
                return (malformedDate, malformedDate);

            // Standard cases:  we have the DATE record or we have the CHRONOLOGY record. Date > Chronology.
            if (IsStartDatePrefilled(story))
            {
                DateTime referenceDate = DateTime.ParseExact(story.Time.Date, "yyyyMMdd", CultureInfo.InvariantCulture);
                DateTime storyStart = referenceDate.AddDays(story.Time.Delay);
                DateTime storyEnd = storyStart.AddDays(story.Time.Duration);

                return (storyStart, storyEnd);
            }
            if (IsChronologyPrefilled(story))
            {
                DateTime referenceDate = globalTime[story.Time.Chronology.Trim()];
                DateTime storyStart = referenceDate.AddDays(story.Time.Delay);
                DateTime storyEnd = storyStart.AddDays(story.Time.Duration);

                return (storyStart, storyEnd);
            }

            // Every other case - we need to go deeper, into dark recursive past.
            ReadStory previous = null;
            try
            {
                previous = allStories.Where(s => s.Uid == story.PrevChronoIds[0]).First();
            }
            catch (Exception)
            {
                // This means a report points chronologically at something which does not exist. Malformed.
                return (malformedDate, malformedDate);
            }


            if (previous.Uid != story.Uid)
            {
                // This means we can move backwards in time; proper recursive case.
                DateTime referenceDate = CalculateStartEndDates(previous, allStories, globalTime).Item2;
                DateTime storyStart = referenceDate.AddDays(story.Time.Delay);
                DateTime storyEnd = storyStart.AddDays(story.Time.Duration);

                return (storyStart, storyEnd);
            }
            else
            {
                // This means we are the FIRST Story in the line. But neither Date nor Chronology is here.
                // The only sensible thing to do present malformed data, because the record is damaged.
                return (malformedDate, malformedDate);
            }

            throw new InvalidOperationException("Well, you should NEVER be able to get to this line of code of TimeRecord creation");
        }

        private static bool IsChronologyPrefilled(ReadStory story) => string.IsNullOrWhiteSpace(story.Time.Chronology) == false;
        private static bool IsStartDatePrefilled(ReadStory story) => string.IsNullOrWhiteSpace(story.Time.Date) == false;

    }
}
