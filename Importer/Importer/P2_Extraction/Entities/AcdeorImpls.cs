﻿namespace Importer.P2_Extraction.Entities
{
    public class ActorMerit : AcdeorRecord
    {
        public ActorMerit(string originatingStoryUid, string actor, string deed) :
            base(originatingStoryUid, actor, deed)
        {
        }
    }

    public class ActorProgression : AcdeorRecord
    {
        public ActorProgression(string originatingStoryUid, string actor, string change) :
            base(originatingStoryUid, actor, change)
        {
        }
    }

    public class FactionMerit : AcdeorRecord
    {
        public FactionMerit(string originatingStoryUid, string actor, string change) :
            base(originatingStoryUid, actor, change)
        {
        }
    }

}
