﻿using System;

namespace Importer.P2_Extraction.Entities
{
    /// <summary>
    /// Actor - Deed - Origin record. Actor who did X, actual Deed, Origin (f.ex. Story) of the AcDe.
    /// </summary>
    public class AcdeorRecord
    {
        public AcdeorRecord(string originUid, string actor, string deed)
        {
            OriginUid = originUid;
            Actor = actor;
            Deed = deed;
        }

        public string OriginUid { get; }
        public string Actor { get; }
        public string Deed { get; }

        public override int GetHashCode() => HashCode.Combine(OriginUid, Actor, Deed);
        public override bool Equals(object obj)
        {

            if (!(obj is AcdeorRecord other)) return false;

            if (OriginUid != other.OriginUid) return false;
            if (Actor != other.Actor) return false;
            if (Deed != other.Deed) return false;

            return true;
        }

        public override string ToString()
        {
            return $"Id: {OriginUid}, Actor: {Actor}, Deed: {Deed}";
        }
    }
}
