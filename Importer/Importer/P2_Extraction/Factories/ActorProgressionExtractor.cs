﻿using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using Importer.P2_Extraction.Entities;
using System.Collections.Generic;

namespace Importer.P2_Extraction.Extractors
{
    public static class ActorProgressionExtractor
    {
        public static ActorProgression[] CreateManyFromStories(ReadStory[] stories)
        {
            List<ActorProgression> progressions = new List<ActorProgression>();
            foreach (var story in stories) { progressions.AddRange(CreateManyFromStory(story)); }
            return progressions.ToArray();
        }

        public static ActorProgression[] CreateManyFromStory(ReadStory story)
        {
            List<ActorProgression> progressions = new List<ActorProgression>();
            string[] records = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(2, 3, "Progresja", story.Body);
            foreach (string record in records) { progressions.Add(CreateSingleFromRecord(story.Uid, record)); }
            return progressions.ToArray();
        }

        public static ActorProgression CreateSingleFromRecord(string originatingStoryUid, string record)
        {
            string[] components = SectionParser.ExtractComponentsFromAcdeorString(record);
            return new ActorProgression(originatingStoryUid: originatingStoryUid, actor: components[0], change: components[1]);
        }
    }
}
