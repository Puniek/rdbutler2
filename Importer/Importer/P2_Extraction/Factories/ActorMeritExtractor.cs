﻿using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using Importer.P2_Extraction.Entities;
using System.Collections.Generic;

namespace Importer.P2_Extraction.Extractors
{
    public static class ActorMeritExtractor
    {
        public static ActorMerit[] CreateManyFromStories(ReadStory[] stories)
        {
            List<ActorMerit> merits = new List<ActorMerit>();
            foreach (var story in stories) { merits.AddRange(CreateManyFromStory(story)); }
            return merits.ToArray();
        }

        public static ActorMerit[] CreateManyFromStory(ReadStory story)
        {
            string[] records = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(2, 3, "Zasługi", story.Body);

            List<ActorMerit> merits = new List<ActorMerit>();
            foreach (var record in records) { merits.Add(CreateSingleFromRecord(story.Uid, record)); }
            return merits.ToArray();
        }

        public static ActorMerit CreateSingleFromRecord(string originatingStoryUid, string record)
        {
            string[] components = SectionParser.ExtractComponentsFromAcdeorString(record);
            return new ActorMerit(originatingStoryUid: originatingStoryUid, actor: components[0], deed: components[1]);
        }
    }
}
