﻿using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using Importer.P2_Extraction.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P2_Extraction.Extractors
{
    public static class FactionMeritExtractor
    {
        public static FactionMerit[] CreateManyFromStories(ReadStory[] stories)
        {
            List<FactionMerit> merits = new List<FactionMerit>();
            foreach (var story in stories) { merits.AddRange(CreateManyFromStory(story)); }
            return merits.ToArray();
        }

        public static FactionMerit[] CreateManyFromStory(ReadStory story)
        {
            List<FactionMerit> merits = new List<FactionMerit>();
            string[] records = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(2, 3, "Frakcje", story.Body);
            foreach (string record in records) { merits.Add(CreateSingleFromRecord(story.Uid, record)); }
            return merits.ToArray();
        }

        public static FactionMerit CreateSingleFromRecord(string originatingStoryUid, string record)
        {
            string[] components = SectionParser.ExtractComponentsFromAcdeorString(record);
            return new FactionMerit(originatingStoryUid: originatingStoryUid, actor: components[0], change: components[1]);
        }
    }
}
