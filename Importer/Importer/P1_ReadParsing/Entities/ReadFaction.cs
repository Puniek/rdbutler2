﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Entities
{
    public class ReadFaction
    {
        public ReadFaction(string identifier, string name, string shortdesc, string body)
        {
            FactionId = identifier;
            Name = name;
            ShortDesc = shortdesc;
            Body = body;
        }

        public string FactionId { get; }
        public string Name { get; }
        public string ShortDesc { get; }
        public string Body { get; }


        public override string ToString()
        {
            return $"{FactionId}: {Name}, ShortDesc: {ShortDesc}";
        }

        public override int GetHashCode() => HashCode.Combine(
            FactionId, Name, ShortDesc);

        public override bool Equals(object obj)
        {
            if (obj is not ReadFaction other) return false;

            if (FactionId != other.FactionId) return false;
            if (Name != other.Name) return false;
            if (ShortDesc != other.ShortDesc) return false;

            return true;
        }
    }
}
