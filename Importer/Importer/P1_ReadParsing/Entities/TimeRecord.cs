﻿using System;
using System.Globalization;
using System.Linq;

namespace Importer.P1_ReadParsing.Entities
{
    /// <summary>
    /// A supporting entity; used to calculate dates and chronology later on.
    /// Therefore it is an early 'read' entity, not an independent one.
    /// </summary>
    public class TimeRecord
    {
        public TimeRecord(int duration, int delay, string date, string chronology)
        {
            Duration = duration;
            Delay = delay;
            Date = date;
            Chronology = chronology;
        }

        public int Duration { get; }
        public int Delay { get; }
        public string Date { get; }
        public string Chronology { get; }

        public override string ToString()
        {
            return $"Duration: {Duration} days, Delay: {Delay} days, Date: {Date}, Chronology: {Chronology}";
        }

        public override int GetHashCode() => HashCode.Combine(
            Duration, Delay, Date, Chronology);

        public override bool Equals(object obj)
        {
            if (obj is not TimeRecord other) return false;

            if (Duration != other.Duration) return false;
            if (Delay != other.Delay) return false;
            if (Date != other.Date) return false;
            if (Chronology != other.Chronology) return false;

            return true;
        }
    }
}