﻿using Commons.Tools;
using Importer.P1_ReadParsing.Parsers;
using System;
using System.Linq;

namespace Importer.P1_ReadParsing.Entities
{
    public class ReadStory
    {
        public ReadStory(
            string uid,
            string title,
            string[] threads,
            string[] motives,
            string[] prevChronoIds,
            string[] prevCampaignIds,
            string[] gms,
            string[] players,
            string summary,
            string location,
            TimeRecord timeRecord,
            string[] actorNames,
            string body
            )
        {
            Uid = uid;
            Title = title;
            Threads = threads;
            Motives = motives;
            PrevChronoIds = prevChronoIds;
            PrevCampaignIds = prevCampaignIds;
            Gms = gms;
            Players = players;
            Summary = summary;
            Location = location;
            Time = timeRecord;
            ActorNames = actorNames;
            Body = body;

        }

        public string Uid { get; }
        public string Title { get; }
        public string[] Threads { get; }
        public string[] Motives { get; }
        public string[] PrevChronoIds { get; }
        public string[] PrevCampaignIds { get; }
        public string[] Gms { get; }
        public string[] Players { get; }
        public string Summary { get; }
        public string Location { get; }
        public TimeRecord Time { get; }
        public string[] ActorNames { get; }
        public string Body { get; }


        public override string ToString()
        {
            return $"Id: {Uid}, Name: {Title}";
        }

        public override int GetHashCode() => HashCode.Combine(
            Uid, Title, string.Join(", ", Threads), PrevChronoIds, PrevCampaignIds,
            Location, Time);

        public override bool Equals(object obj)
        {
            if (obj is not ReadStory other) return false;

            if (Uid != other.Uid) return false;
            if (Title != other.Title) return false;
            if (Threads.SequenceEqual(other.Threads) == false) return false;
            if (Motives.SequenceEqual(other.Motives) == false) return false;
            if (PrevChronoIds.SequenceEqual(other.PrevChronoIds) == false) return false;
            if (PrevCampaignIds.SequenceEqual(other.PrevCampaignIds) == false) return false;
            if (Gms.SequenceEqual(other.Gms) == false) return false;
            if (Players.SequenceEqual(other.Players) == false) return false;
            if (Summary != other.Summary) return false;
            if (Location != other.Location) return false;
            if (Time.Equals(other.Time) == false) return false;
            //if (Body != other.Body) return false;     <-- this is commented out intentionally. We are NOT testing equality via Body. Design choice.
            return true;
        }
    }

    public record StoryDefaults
    {
        public const string Uid = "999999-default";
        public const string Title = "default";
        public const string Threads = "default";
        public static string[] PrevCampaignIds = new string[] { "999999-default" };
        public static string[] PrevChronoIds = new string[] { "999999-default" };
        public static string[] Gms = new string[] { "default" };
        public static string[] Players = new string[] { "default" };
        public const string Summary = "default";
        public const string Location = "1. Świat";
        public static TimeRecord Time = new TimeRecord(1, 0, "", "");
        public const string Body = "default";
        public static string[] ActorNames = new string[] { "default" };
        public static string[] PlayerActors = new string[] { "default" };
        public static string[] AllActors = new string[] { "default" };
    }
}