﻿using System;
using System.Linq;

namespace Importer.P1_ReadParsing.Entities
{
    public class ReadActor
    {
        public ReadActor(string identifier, string name, string mechver,
            string[] factions, string owner, string body)
        {
            Uid = identifier;
            Name = name;
            Mechver = mechver;
            Factions = factions;
            Owner = owner;
            Body = body;
        }

        public string Uid { get; }
        public string Name { get; }
        public string Mechver { get; }
        public string[] Factions { get; }
        public string Owner { get; }
        public string Body { get; }

        public override string ToString()
        {
            return $"Id: {Uid}, Name: {Name}, Mechver: {Mechver}, Owner: {Owner}";
        }

        public override int GetHashCode() => HashCode.Combine(
            Uid, Name, Mechver, Factions, Owner, Body);

        public override bool Equals(object obj)
        {
            if (!(obj is ReadActor other)) return false;

            if (Uid != other.Uid) return false;
            if (Name != other.Name) return false;
            if (Mechver != other.Mechver) return false;
            if (Factions.SequenceEqual(other.Factions) == false) return false;
            if (Owner != other.Owner) return false;

            // Body is NOT being compared with each other

            return true;
        }
    }
}