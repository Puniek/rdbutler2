﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Factories
{
    public static class ReadFactionFactory
    {
        public static ReadFaction[] CreateMany(FileRecord[] records)
        {
            var valid = records
                .Where(f => IdentifierParser.ExtractActorIdentifier(f.Path) != string.Empty);

            return valid.Select(f => CreateSingle(f))
                .ToArray();
        }

        public static ReadFaction CreateSingle(FileRecord record)
        {
            string identifier = IdentifierParser.ExtractActorIdentifier(record.Path);
            string name = MetadataSectionParser.ExtractSingleParamFromStory("name", record.Content);
            string shortdesc = MetadataSectionParser.ExtractSingleParamFromStory("shortdesc", record.Content);
            string body = record.Content;
            return new ReadFaction(identifier: identifier, name: name, shortdesc: shortdesc, body: body);
        }
    }
}
