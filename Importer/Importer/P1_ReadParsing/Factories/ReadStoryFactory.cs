﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Factories
{
    public static class ReadStoryFactory
    {
        public static ReadStory[] CreateMany(FileRecord[] records)
        {
            var valid = records
                .Where(f => IdentifierParser.ExtractStoryIdentifier(f.Path) != string.Empty)
                .Where(f => f.Path.Contains(".md"));

            return valid.Select(f => CreateSingle(f))
                .Where(f => f.Uid.Contains("_template") == false)  // no templates please
                .Where(f => f.Uid.Contains("INTERLUDIUM") == false)  // no other control info please
                .ToArray();
        }

        public static ReadStory CreateSingle(FileRecord record)
        {
            string uid = IdentifierParser.ExtractStoryIdentifier(record.Path);
            string title = MetadataSectionParser.ExtractSingleParamFromStory("title", record.Content);
            string[] threads = MetadataSectionParser.ExtractManyParamsFromStory("threads", record.Content);
            string[] motives = MetadataSectionParser.ExtractManyParamsFromStory("motives", record.Content);
            string[] prevCampaignUids = SectionParser.ExtractPrevCampaignUidsFromStory(record.Content);
            string summary = SectionParser.ExtractSummaryFromStory(record.Content);
            string location = SectionParser.ExtractLocationFromStory(record.Content);
            TimeRecord time = SectionParser.ExtractTimeRecordFromStory(record.Content);
            string[] players = MetadataSectionParser.ExtractManyParamsFromStory("players", record.Content);
            string[] gms = MetadataSectionParser.ExtractManyParamsFromStory("gm", record.Content);
            string[] prevChronoUids = SectionParser.ExtractPrevChronoUidsFromStory(record.Content);
            string[] actorNames = SectionParser.ExtractActorNamesFromStory(record.Content);
            string body = record.Content;

            return new ReadStory(
                uid: uid,
                title: title,
                threads: threads,
                motives: motives,
                prevChronoIds: prevChronoUids,
                prevCampaignIds: prevCampaignUids,
                gms: gms,
                players: players,
                summary: summary,
                location: location,
                timeRecord: time,
                actorNames: actorNames,
                body: body
                );
        }

        public static ReadStory CreateWithDefaults(
            string uid = StoryDefaults.Uid,
            string title = StoryDefaults.Title,
            string[] threads = null,
            string[] motives = null,
            string[] prevChronoIds = null,
            string[] prevCampaignIds = null,
            string[] gms = null,
            string[] players = null,
            string summary = StoryDefaults.Summary,
            string location = StoryDefaults.Location,
            TimeRecord time = null,
            string[] actorNames = null,
            string body = StoryDefaults.Body
            )
        {
            threads ??= new[] { StoryDefaults.Threads };
            motives ??= Array.Empty<string>();
            prevCampaignIds ??= StoryDefaults.PrevCampaignIds;
            prevChronoIds ??= StoryDefaults.PrevChronoIds;
            gms ??= StoryDefaults.Gms;
            players ??= StoryDefaults.Players;
            time ??= StoryDefaults.Time;
            actorNames ??= StoryDefaults.ActorNames;

            return new ReadStory(
                uid: uid,
                title: title,
                threads: threads,
                motives: motives,
                prevChronoIds: prevChronoIds,
                prevCampaignIds: prevCampaignIds,
                gms: gms,
                players: players,
                summary: summary,
                location: location,
                timeRecord: time,
                actorNames: actorNames,
                body: body
                );
        }
    }
}
