﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Factories
{
    public static class ReadStoryTagFactory
    {
        public static ReadStoryTag[] CreateMany(FileRecord[] fileRecords)
        {
            return fileRecords
                .Select(fileRecord => CreateSingle(fileRecord))
                .ToArray();
        }

        public static ReadStoryTag CreateSingle(FileRecord record)
        {
            string threadUid = IdentifierParser.ExtractStoryTagIdentifier(record.Path);
            string threadName = MetadataSectionParser.ExtractSingleParamFromStory("Nazwa", record.Content);
            string shortDesc = SectionParser.ExtractSectionFromStoryBody(2, 2, "Krótki opis", record.Content);
            string fullDesc = SectionParser.ExtractSectionFromStoryBody(2, 2, "Opis", record.Content);
            string spoilers = SectionParser.ExtractSectionFromStoryBody(2, 2, "Spoilers", record.Content);

            return new ReadStoryTag(tagUid: threadUid, name: threadName, shortDesc: shortDesc, fullDesc: fullDesc, spoilers: spoilers);
        }
    }
}
