﻿using Commons.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P1_ReadParsing.Parsers
{
    public class MetadataSectionParser
    {
        const string metadataHeader = "Metadane";

        public static string ExtractSingleParamFromStory(string parameter, string content)
        {
            string section = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, metadataHeader), content);

            string recordString = TextParserEngine.SafelyExtractSingleCapturedElementFromSingleLine(
                RegexFacade.ExtractParamFromMetadataSection(parameter), section).Trim(',', ' ', '\"');

            return recordString;
        }

        public static string[] ExtractManyParamsFromStory(string parameter, string content)
        {
            string section = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, metadataHeader), content);

            string recordString = TextParserEngine.SafelyExtractSingleCapturedElementFromSingleLine(
                RegexFacade.ExtractParamFromMetadataSection(parameter), section);

            string[] threads = recordString.Split(", ");
            return threads.Select(x => x.Trim(',', ' ', '\"')).Where(x => x != string.Empty).Distinct().ToArray();
        }
    }
}
