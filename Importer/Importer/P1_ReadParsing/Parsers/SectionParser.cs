﻿using System;
using System.Collections.Generic;

using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using System.Linq;
using System.Text.RegularExpressions;
using System.Reflection.Metadata;
using Importer.P3_Aggregation.Entities;
using Microsoft.Extensions.FileSystemGlobbing.Internal;
using Microsoft.Extensions.FileSystemGlobbing;

namespace Importer.P1_ReadParsing.Parsers
{
    public static class SectionParser
    {
        public static string[] ExtractSectionRecordsInActorDeedsFormatFromStoryBody(
            int headerDepth, int blockingheaderDepth, string headerName, string content) =>
            ExtractSectionRecordsFromStoryBody(headerDepth, blockingheaderDepth, headerName, content, RegexFacade.ProperActorDeedRecord(), true);

        private static string[] ExtractSectionRecordsFromStoryBody(
            int headerDepth, int blockingheaderDepth, string headerName, string content, string recordPattern, bool trim)
        {
            string section = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(headerDepth, blockingheaderDepth, headerName), content);

            var records = section.Split("\n")
                                 .Where(s => Regex.IsMatch(s, recordPattern))
                                 .Select(s => trim ? s.Trim() : s)
                                 .Where(s => !string.IsNullOrWhiteSpace(s));
            return records.ToArray();
        }

        public static string ExtractDeedsFromLocationRecord(string locationString)
        {
            if (Regex.IsMatch(locationString, RegexFacade.ProperLocationRecord()))
            {
                string result = Regex.Match(locationString, @".+?:(.+)$").Groups[1].Value;
                return result.Trim();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// If no section works, use this one - it safely extracts a section having specified parameters.
        /// </summary>
        public static string ExtractSectionFromStoryBody(
            int headerDepth, int blockingheaderDepth, string headerName, string content)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(headerDepth, blockingheaderDepth, headerName), content);
        }

        /// <summary>
        /// Acdeor = Actor - Deed - Origin. This one assumes ActeorRecord is crystallized in markdown list.
        /// </summary>
        public static string[] ExtractComponentsFromAcdeorString(string record)
        {
            string actorPattern = @"\* (.+?):.*$";
            string deedPattern = @"\* .+?:(.*)$";

            var actor = new Regex(actorPattern, RegexOptions.Multiline | RegexOptions.Singleline).Match(record);
            var deed = new Regex(deedPattern, RegexOptions.Multiline | RegexOptions.Singleline).Match(record);

            return new[] { actor.Groups[1].Value.Trim(), deed.Groups[1].Value.Trim() };
        }

        /// <summary>
        /// Every Story has a set of Actors. And we want to extract primary IDs - names - for use in StoryInfo in the future.
        /// </summary>
        public static string[] ExtractActorNamesFromStory(string content)
        {
            string[] records = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(2, 3, "Zasługi", content);
            return records.Select(r => SectionParser.ExtractComponentsFromAcdeorString(r)[0]).ToArray();
        }

        /// <summary>
        /// By definition there can be multiple threads per Story, so a Story can follow multiple other Stories.
        /// </summary>
        public static string[] ExtractPrevCampaignUidsFromStory(string content)
        {
            string continuity = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Kontynuacja"), content);

            string campaign = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(3, 3, "Kampanijna"), continuity);

            MatchCollection matches = new Regex(RegexFacade.ExtractTargetsFromLink(),
                RegexOptions.Multiline | RegexOptions.Singleline).Matches(campaign);

            return matches.Select(m => m.Groups[1].Value).ToArray();
        }

        public static string[] ExtractPrevChronoUidsFromStory(string content)
        {
            string continuity = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Kontynuacja"), content);

            string campaign = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(3, 3, "Chronologiczna"), continuity);

            MatchCollection matches = new Regex(RegexFacade.ExtractTargetsFromLink(),
                RegexOptions.Multiline | RegexOptions.Singleline).Matches(campaign);

            return matches.Select(m => m.Groups[1].Value).ToArray();
        }

        public static TimeRecord ExtractTimeRecordFromStory(string content)
        {
            string section = TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Czas"), content);

            string delay = Regex.Match(section, @"Opóźnienie:? *(-?\d+)", RegexOptions.IgnoreCase).Groups[1].Value;
            string duration = Regex.Match(section, @"Dni:? *(\d+)", RegexOptions.IgnoreCase).Groups[1].Value;
            string date = Regex.Match(section, @"Data:? *(\d+)", RegexOptions.IgnoreCase).Groups[1].Value;
            string chronology = Regex.Match(section, @"Chronologia:? *([\w\s]+)$", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Multiline).Groups[1].Value;

            if (string.IsNullOrEmpty(delay)) delay = "0";
            if (string.IsNullOrEmpty(duration)) duration = "0";
            if (string.IsNullOrEmpty(date)) date = "";
            if (string.IsNullOrEmpty(chronology)) chronology = "";

            return new TimeRecord(int.Parse(duration), int.Parse(delay), date, chronology);
        }

        public static string ExtractFlashcardSectionFromStory(string content)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(3, 3, "Fiszki"), content);
        }

        public static string ExtractSummaryFromStory(string content)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Streszczenie"), content);
        }

        public static string ExtractLocationFromStory(string content)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractHeaderSection(2, 2, "Lokalizacje"), content);
        }

        public static string[] ExtractBulletBlocksFromSection(string flashcardSection)
        {
            string regex = RegexFacade.ExtractBulletBlocksWithFirstUppercaseAndForcedDotall();
            var matches = Regex.Matches(flashcardSection, regex, RegexOptions.Singleline | RegexOptions.Multiline);
            string[] blocks = matches.Select(m => m.Groups[1].Value.Trim()).ToArray();
            return blocks;
        }

        public static string ExtractActorNameFromFlashcardBlock(string block)
        {
            // seek only leading asterisk, name always starts from uppercase, capture only until colon or newline
            string pattern = $@"^\* ({RegexFacade.UPPERCASE_LETTER}.+?)[:|\n]";   
            var match = Regex.Match(block, pattern);
            return match.Groups[1].Value.Trim();
        }

        public static string[] ExtractBulletRecordsFromFlashcardBlockIncludingRecordAfterColon(string block)
        {
            /*
             * Data looks like this:
             * `* Emilia d'Infernia: first bullet record
             *    * second bullet record`
             */

            // We deal with two cases. First record and every other record.
            List<string> records = new List<string>();

            // First record will be after a name and will be everything after a colon. It is 100% optional.
            string firstRecordPattern = $@"^\* {RegexFacade.UPPERCASE_LETTER}.+?:(.+?)(?=\n|\r)";
            var firstRecordMatch = Regex.Match(block, firstRecordPattern);

            // now to format it properly we need to set it to 'standard 4-indentation record'
            if (firstRecordMatch.Success) records.Add(firstRecordMatch.Groups[1].Value.Trim());

            // Second record is everything with more depth (indentation) than 0
            // thus \s+ not \s* -> more than zero. And it cannot be multiline.
            string otherRecordsPattern = $@"[\t ]+\* (.+)";
            var matches = Regex.Matches(block, otherRecordsPattern);
            records.AddRange(matches.Select(m => m.Groups[1].Value.Trim()));

            return records.ToArray();
        }
    }
}
