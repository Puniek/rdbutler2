﻿using Commons.Tools;
using System;

namespace Importer.P1_ReadParsing.Parsers
{
    public static class IdentifierParser
    {
        public static string ExtractActorIdentifier(string path)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractIdentifierFromPath("4"), path);
        }

        public static string ExtractStoryIdentifier(string path)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                RegexFacade.ExtractIdentifierFromPath("6"), path);
        }

        public static string ExtractStoryTagIdentifier(string path)
        {
            return TextParserEngine.SafelyExtractSingleCapturedElement(
                @"([\w-]+).md", path);
        }
    }
}
