﻿using Commons.Config;
using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using System;
using System.Linq;

namespace Importer.P1_ReadParsing
{
    public class Source
    {
        AppConfigProvider _provider;

        public Source()
        {
            _provider = new AppConfigProvider();
        }

        public ReadActor[] ProvideActors()
        {
            string path = _provider.GetPathToActorFolder();
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            ReadActor[] actors = ReadActorFactory.CreateMany(fileRecords);
            return actors;
        }

        public ReadFaction[] ProvideFactions()
        {
            string path = _provider.GetPathToFactionFolder();
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            ReadFaction[] factions = ReadFactionFactory.CreateMany(fileRecords);
            return factions;
        }

        public GlobalTime ProvideTime()
        {
            string path = _provider.GetPathToStoryFolder();     // time is located in Story folder
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            GlobalTime globalTime = GlobalTimeFactory.CreateSingle(fileRecords.Where(fr => fr.Path.Contains("_kluczowe-daty.md")).First().Content);
            return globalTime;
        }

        public ReadStory[] ProvideStories()
        {
            string path = _provider.GetPathToStoryFolder();
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            ReadStory[] stories = ReadStoryFactory.CreateMany(fileRecords);
            return stories;
        }

        public ReadThread[] ProvideThreads()
        {
            string path = _provider.GetPathToStoryThreadFolder();
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            ReadStoryTag[] threads = ReadStoryTagFactory.CreateMany(fileRecords);
            return threads.Select(s => new ReadThread(s.TagUid, s.Name, s.ShortDesc, s.FullDesc, s.Spoilers)).ToArray();
        }

        public ReadMotive[] ProvideMotives()
        {
            string path = _provider.GetPathToStoryMotiveFolder();
            FileRecord[] fileRecords = FileOps.ReadFiles(path);
            ReadStoryTag[] motives = ReadStoryTagFactory.CreateMany(fileRecords);
            return motives.Select(s => new ReadMotive(s.TagUid, s.Name, s.ShortDesc, s.FullDesc, s.Spoilers)).ToArray();
        }
    }
}