﻿using Commons.CoreEntities;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories
{

    public class AtarRecordModelFactory
    {
        public static AtarRecordModel CreateFrom(AtarRecord atar)
        {
            return new AtarRecordModel
            {
                OriginatingActor = atar.TargetActor,
                RelevantActor = atar.RelevantActor,
                Intensity = atar.Intensity,
                StoryUids = string.Join(", ", atar.StoryUids)
            };
        }
    }
}
