using System.Linq;

using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories;

public static class ActorFlashcardModelFactory
{
    public static ActorFlashcardModel CreateFrom(AggregatedActorFlashcard flashcard)
    {
        return new ActorFlashcardModel
        {
            Uid = flashcard.Uid,
            Name = flashcard.ActorName,
            Records = flashcard.Records.Select(x => new FlashcardRecordModel
            {
                OriginStoryUid = x.OriginStoryUid,
                Body = x.Body,
            }).ToList()
        };
    }
}