﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public class LocationRecordModelFactory
    {
        public static LocationRecordModel CreateFrom(LocationRecord record)
        {
            return new LocationRecordModel
            {
                OriginatingStory = record.OriginatingStory,
                Actor = record.Name,
                Deed = record.Event,
                Path = record.Path,
                EndDate = record.EndDate
            };
        }
    }
}
