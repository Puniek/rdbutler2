﻿using Commons.Infrastructure.DbModels;
using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public class ReadThreadModelFactory
    {
        public static ReadThreadModel CreateFrom(ReadThread rt)
        {
            return new ReadThreadModel
            {
                ThreadUid = rt.TagUid,
                Name = rt.Name,
                ShortDesc = rt.ShortDesc,
                FullDesc = rt.FullDesc,
                Spoilers = rt.Spoilers,
            };
        }
    }
}
