﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class GeneralStoryBlockModelFactory
    {
        public static GeneralStoryBlockModel CreateFrom(GeneralStoryBlock storyBlock)
        {
            return new GeneralStoryBlockModel
            {
                StoryUid = storyBlock.StoryUid,
                StoryTitle = storyBlock.StoryTitle,
                StartDate = storyBlock.StartDate,
                EndDate = storyBlock.EndDate,
                Threads = string.Join(", ", storyBlock.Threads),
                Motives = string.Join(", ", storyBlock.Motives),
                Summary = storyBlock.Summary,
                AllActorsPresent = string.Join(", ", storyBlock.AllActorsPresent)
            };
        }
    }

}
