﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using Importer.P2_Extraction.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class ActorThreadStoryBlockModelFactory
    {
        public static ActorThreadStoryBlockModel CreateFrom(ActorThreadStoryBlock atStoryBlock)
        {
            return new ActorThreadStoryBlockModel
            {
                OriginatingActor = atStoryBlock.OriginatingActor,
                StoryUid = atStoryBlock.StoryUid,
                StoryTitle = atStoryBlock.StoryTitle,
                StartDate = atStoryBlock.StartDate,
                EndDate = atStoryBlock.EndDate,
                Summary = atStoryBlock.Summary,
                TargetActorDeed = atStoryBlock.TargetActorDeed,
                TargetActorProgressions = string.Join(", ", atStoryBlock.TargetActorProgressions),
                AllActorsPresent = string.Join(", ", atStoryBlock.AllActorsPresent)
            };
        }
    }
}
