﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class AggregatedProgressionModelFactory
    {
        public static AggregatedProgressionModel CreateFrom(AcdeorPlus progression)
        {
            return new AggregatedProgressionModel
            {
                Actor = progression.Actor,
                OriginUid = progression.OriginUid,
                Deed = progression.Deed,
                EndDate = progression.EndDate
            };
        }
    }
}
