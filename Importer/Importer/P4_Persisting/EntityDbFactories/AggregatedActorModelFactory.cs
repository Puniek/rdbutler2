﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories
{

    public class AggregatedActorModelFactory
    {
        public static EveryActorModel CreateFrom(AggregatedActor aa)
        {
            return new EveryActorModel
            {
                ActorUid = aa.Uid,
                Name = aa.Name,
                Mechver = aa.Mechver,
                Owner = aa.Owner,
                Body = aa.Body
            };
        }
    }
}
