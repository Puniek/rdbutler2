﻿using Commons.Infrastructure.DbModels;
using Importer.P1_ReadParsing.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class ReadModelModelFactory
    {
        public static ReadMotiveModel CreateFrom(ReadMotive rm)
        {
            return new ReadMotiveModel
            {
                MotiveUid = rm.TagUid,
                Name = rm.Name,
                ShortDesc = rm.ShortDesc,
                FullDesc = rm.FullDesc,
                Spoilers = rm.Spoilers,
            };
        }
    }
}
