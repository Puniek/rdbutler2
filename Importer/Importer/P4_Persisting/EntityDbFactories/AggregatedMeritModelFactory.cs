﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Commons.Infrastructure.DbModels;

namespace Importer.P4_Persisting.EntityDbFactories
{

    public static class AggregatedMeritModelFactory
    {
        public static AggregatedMeritModel CreateFrom(AggregatedMerit merit)
        {
            return new AggregatedMeritModel
            {
                Actor = merit.Actor,
                Threads = merit.Threads,
                StartDate = merit.StartDate,
                EndDate = merit.EndDate,
                Deed = merit.Deed,
                OriginUid = merit.OriginUid
            };

        }
    }
}
