﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public class FactionBriefRecordModelFactory
    {
        public static FactionBriefRecordModel CreateFrom(FactionBriefRecord record)
        {
            return new FactionBriefRecordModel
            {
                Name = record.Name,
                Link = record.Link,
                Intensity = record.Intensity,
                ShortDesc = record.ShortDesc
            };
        }
    }
}
