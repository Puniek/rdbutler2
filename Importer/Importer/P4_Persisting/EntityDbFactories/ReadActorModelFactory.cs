﻿using Commons.Infrastructure.DbModels;
using Importer.P1_ReadParsing.Entities;
using System.ComponentModel.DataAnnotations;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class ReadActorModelFactory
    {
        public static ReadActorModel CreateFrom(ReadActor actor)
        {
            return new ReadActorModel
            {
                Uid = actor.Uid,
                Mechver = actor.Mechver,
                Name = actor.Name,
                Owner = actor.Owner,
                Body = actor.Body
            };
        }
    }
}
