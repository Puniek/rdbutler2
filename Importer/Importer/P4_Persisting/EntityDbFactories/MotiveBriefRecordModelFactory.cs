﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Importer.P4_Persisting.EntityDbFactories
{
    public static class MotiveBriefRecordModelFactory
    {
        public static MotiveBriefRecordModel CreateFrom(MotiveBriefRecord mbr)
        {
            return new MotiveBriefRecordModel
            {
                MotiveUid = mbr.MotiveUid,
                Name= mbr.Name,
                StartDate = mbr.StartDate,
                EndDate = mbr.EndDate,
                StoryCount = mbr.StoryCount,
                Players = string.Join(", ", mbr.Players),
                PlayerActors = string.Join(", ", mbr.PlayerActors),
                ShortDesc = mbr.ShortDesc
            };
        }
    }
}
