﻿using Commons.Tools;
using NUnit.Framework;

using System.Linq;

using Importer.P1_ReadParsing.Entities;
using Importer.P2_Extraction.Entities;
using Importer.P2_Extraction.Extractors;
using Importer.P1_ReadParsing.Factories;

namespace ImporterTests.P2_Extraction
{
    class CreateActorProgression
    {
        [Test]
        public void Are_created_from_prepared_file()
        {
            // Given
            FileRecord record = FileOps.ReadFile(
                @"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);
            ReadStory[] stories = new ReadStory[] { story };

            // When
            ActorProgression[] progressions = ActorProgressionExtractor.CreateManyFromStories(stories);

            // Expected
            ActorProgression example = new ActorProgression(
                originatingStoryUid: story.Uid,
                actor: "Arnulf Poważny",
                change: "stracił trochę szacunku w oczach innych uczniów; myślą, że nie rozpoznał Pięknotki jako terminuski. Cóż.");
            int progressionCount = 5;

            // Then
            Assert.True(progressions.Length == progressionCount);
            Assert.True(progressions.Where(s => s.Equals(example)).First() is ActorProgression);
        }

        [Test]
        public void Is_created_from_prepared_storyId_progressionRecord()
        {
            // Given
            string storyId = "190113-chronmy-karoline-przed-uczniami";
            string record = "* Arnulf Poważny: stracił trochę szacunku w oczach innych uczniów; myślą, że nie rozpoznał Pięknotki jako terminuski. Cóż.";

            // When
            ActorProgression actual = ActorProgressionExtractor.CreateSingleFromRecord(originatingStoryUid: storyId, record: record);

            // Expected
            ActorProgression expected = new ActorProgression(
                originatingStoryUid: storyId,
                actor: "Arnulf Poważny",
                change: "stracił trochę szacunku w oczach innych uczniów; myślą, że nie rozpoznał Pięknotki jako terminuski. Cóż.");

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
