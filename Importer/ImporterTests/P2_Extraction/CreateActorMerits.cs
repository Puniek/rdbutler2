﻿using Commons.Tools;
using NUnit.Framework;
using System.Linq;
using Importer.P1_ReadParsing.Entities;
using Importer.P2_Extraction.Entities;
using Importer.P2_Extraction.Extractors;
using Importer.P1_ReadParsing.Factories;

namespace ImporterTests.P2_Extraction
{
    public class CreateActorMerits
    {
        [Test]
        public void Having_predefined_story_from_md_file_MeritFactory_generates_7_merits()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);

            // When
            ActorMerit[] merits = ActorMeritExtractor.CreateManyFromStories(new[] { story });

            // Expected
            ActorMerit example = new ActorMerit(
                "190113-chronmy-karoline-przed-uczniami",
                "Teresa Mieralit",
                "nauczycielka magii leczniczej i katalistka w Szkole Magów. Pomogła Lilianie."
                );

            int meritCount = 7;

            // Then
            Assert.True(merits.Length == meritCount);
            Assert.True(merits.Where(s => s.Equals(example)).First() is ActorMerit);

        }

        [Test]
        public void Having_proper_storyId_and_record_MeritFactory_creates_proper_single_merit()
        {
            // Given
            string storyId = "190113-chronmy-karoline-przed-uczniami";
            string record = "* Arnulf Poważny: dyrektor skłonny do osłony swoich uczniów nawet, jeśli coś zbroili.";

            // When
            ActorMerit actual = ActorMeritExtractor.CreateSingleFromRecord(originatingStoryUid: storyId, record: record);

            // Expected
            ActorMerit expected = new ActorMerit(
                originatingStoryUid: storyId,
                actor: "Arnulf Poważny",
                deed: "dyrektor skłonny do osłony swoich uczniów nawet, jeśli coś zbroili.");

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Having_storyId_and_record_with_no_deed_MeritFactory_creates_Merit_with_empty_deed()
        {
            // Given
            string storyId = "200509-rekin-z-aurum-i-fortifarma";
            string record = "* Pięknotka Diakon:";

            // When
            ActorMerit actual = ActorMeritExtractor.CreateSingleFromRecord(originatingStoryUid: storyId, record: record);

            // Expected
            ActorMerit expected = new ActorMerit(
                originatingStoryUid: storyId,
                actor: "Pięknotka Diakon",
                deed: "");

            // Then
            Assert.AreEqual(expected, actual);
        }

    }
}
