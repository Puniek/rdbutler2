﻿using Commons.Tools;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Linq;
using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using Importer.P1_ReadParsing.Factories;

namespace ImporterTests.P3_Aggregation.Create
{
    class CreateStoryInfo
    {
        [Test]
        public void Creation_uses_GlobalTime_as_reference_to_date()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\200930-lekarz-dla-elizy.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);
            GlobalTime globalTime = new GlobalTime() { { "Inwazja Noctis", DateTime.ParseExact("00800917", "yyyyMMdd", CultureInfo.InvariantCulture) } };

            // When
            AggregatedStory actual = AggregatedStoryComposer.CreateSingleFromStory(story, new ReadStory[] { story }, globalTime);

            // Expected
            AggregatedStory expected = new(
                storyUid: story.Uid,
                storyTitle: story.Title,
                threads: story.Threads,
                motives: story.Motives,
                startDate: "0080-12-17",
                endDate: "0080-12-21",
                seqNo: "1",
                previousStories: new string[] { "200930-lekarz-dla-elizy" },
                gms: new string[] { "żółw" },
                players: new string[] { "kić", "fox" },
                playerActors: new string[] { "Wanessa Pyszcz", "Karmina Alarel" },
                allActors: new string[] { "Wanessa Pyszcz", "Karmina Alarel", "Eliza Ira", "Szymon Szynek", "Autofort Imperatrix" },
                summary: "irrelevant",
                body: "irrelevant"
                );

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Creation_deals_well_with_multiple_previous_stories()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\200930-lekarz-dla-elizy.md");
            ReadStory story = ReadStoryFactory.CreateWithDefaults(
                prevCampaignIds: new string[] { "211201-one", "211202-two" }
                );
            GlobalTime globalTime = new GlobalTime() { { "Inwazja Noctis", DateTime.ParseExact("00800917", "yyyyMMdd", CultureInfo.InvariantCulture) } };

            // When
            AggregatedStory actual = AggregatedStoryComposer.CreateSingleFromStory(story, new ReadStory[] { story }, globalTime);

            // Expected
            AggregatedStory expected = AggregatedStoryComposer.CreateWithDefaults(prevCampaignIds: new string[] { "211201-one", "211202-two" });

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Creates_many_calculating_dates_from_stories_chained_chronologically()
        {
            // Given
            ReadStory first = ReadStoryFactory.CreateWithDefaults(
                uid: "180101-first",
                prevChronoIds: new[] { "180101-first" },
                prevCampaignIds: new string[] { "180101-first" },
                time: new TimeRecord(duration: 3, delay: 2, date: "00850101", chronology: ""));     // which means it ends at 850101+2+3 = 850106
            ReadStory second = ReadStoryFactory.CreateWithDefaults(
                uid: "180102-second",
                prevChronoIds: new[] { "180101-first" },
                prevCampaignIds: new string[] { "180101-first" },
                time: new TimeRecord(duration: 2, delay: 1, date: "", chronology: ""));             // which means it ends at 850106+1+2 = 850109

            // When
            AggregatedStory[] storyMetas = AggregatedStoryComposer.CreateManyFromStories(new[] { first, second }, new GlobalTime());

            // Then
            Assert.AreEqual("0085-01-03", storyMetas[0].StartDate);
            Assert.AreEqual("0085-01-06", storyMetas[0].EndDate);
            Assert.AreEqual("0085-01-07", storyMetas[1].StartDate);
            Assert.AreEqual("0085-01-09", storyMetas[1].EndDate);
        }

        [Test]
        public void Creates_single_calculating_dates_from_story_having_negative_delay()
        {
            // Given
            ReadStory first = ReadStoryFactory.CreateWithDefaults(
                uid: "180101-first",
                prevChronoIds: new[] { "180101-first" },
                prevCampaignIds: new string[] { "180101-first" },
                time: new TimeRecord(duration: 3, delay: -10, date: "00850111", chronology: ""));   // which means it ends at 850111-10+3 = 850104

            // When
            AggregatedStory[] storyMetas = AggregatedStoryComposer.CreateManyFromStories(new[] { first }, new GlobalTime());

            // Then
            Assert.AreEqual("0085-01-01", storyMetas[0].StartDate);
            Assert.AreEqual("0085-01-04", storyMetas[0].EndDate);
        }

        [Test]
        public void For_properly_formed_Story_selects_first_number_of_actors_as_player_actors()
        {
            // Given
            ReadStory first = ReadStoryFactory.CreateWithDefaults(
                players: new string[] { "p1", "p2" },
                actorNames: new string[] { "first", "second", "third", "fourth" }
                );

            // When
            AggregatedStory[] storyMetas = AggregatedStoryComposer.CreateManyFromStories(new[] { first }, new GlobalTime());

            // Then
            string[] actualActorNames = storyMetas[0].PlayerActors;
            string[] expectedActorNames = new string[] { "first", "second" };

            Assert.IsTrue(expectedActorNames.SequenceEqual(actualActorNames));

        }

        [Test]
        public void For_malformed_Story_having_less_actors_than_players_selects_all_actors_as_player_actors()
        {
            // Given
            ReadStory first = ReadStoryFactory.CreateWithDefaults(
                players: new string[] { "p1", "p2", "p3", "p4" },
                actorNames: new string[] { "first", "second" }
                );

            // When
            AggregatedStory[] storyMetas = AggregatedStoryComposer.CreateManyFromStories(new[] { first }, new GlobalTime());

            // Then
            string[] actualActorNames = storyMetas[0].PlayerActors;
            string[] expectedActorNames = new string[] { "first", "second" };

            Assert.IsTrue(expectedActorNames.SequenceEqual(actualActorNames));

        }
    }
}
