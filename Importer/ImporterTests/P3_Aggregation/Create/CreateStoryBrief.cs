﻿using Commons.CoreEntities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateStoryBrief
    {
        [Test]
        public void CreateSimpleBrief()
        {
            // Given

            string storyUid = "211112-current";
            string storyTitle = "Current Story";
            string[] threads = new[] { "thread1", "thread2" };
            string[] motives = Array.Empty<string>();
            string startDate = "00800917";
            string endDate = "00800924";
            string[] players = new string[] { "pit", "til" };

            AggregatedStory story = new AggregatedStory(
                storyUid: storyUid,
                storyTitle: storyTitle,
                threads: threads,
                motives: motives,
                startDate: startDate,
                endDate: endDate,
                seqNo: "3",
                previousStories: new string[] { "211111-previous" },
                gms: new string[] { "żółw" },
                players: new string[] { "pit", "til" },
                playerActors: new string[] { "Michael Firegiver", "Eltor Retop" },
                allActors: new string[] { "Michael Firegiver", "Eltor Retop", "Elena Xairiss" },
                summary: "text",
                body: "more text"
                );

            // When
            StoryBriefRecord actual = StoryBriefRecordComposer.CreateSingle(story);

            // Then
            StoryBriefRecord expected = new StoryBriefRecord(
                storyUid: storyUid,
                title: storyTitle,
                threads: string.Join(", ", threads),
                motives: string.Join(", ", motives),
                startDate: startDate,
                endDate: endDate,
                players: string.Join(", ", players)
                );

            Assert.AreEqual(expected, actual);
        }
    }
}
