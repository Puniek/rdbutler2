﻿using Commons.Tools;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P2_Extraction.Entities;
using Importer.P2_Extraction.Extractors;
using Importer.P3_Aggregation.FactoriesPrimary;
using Importer.P1_ReadParsing.Factories;
using Importer.P3_Aggregation.FactoriesSupportive;

namespace ImporterTests.P3_Aggregation.Create
{
    class CreateAtarRecords
    {
        [Test]
        public void For_single_originating_actor_name_builds_AtarRecords_for_all_Actors_in_Stories_except_themselves()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(@"TestData\200930-lekarz-dla-elizy.md");
            FileRecord record2 = FileOps.ReadFile(@"TestData\200923-magiczna-burza-w-raju.md");

            ReadStory[] stories = ReadStoryFactory.CreateMany(new FileRecord[] { record1, record2 });
            GlobalTime globalTime = new GlobalTime() { { "Inwazja Noctis", DateTime.ParseExact("00800917", "yyyyMMdd", CultureInfo.InvariantCulture) } };
            AggregatedStory[] storyMetadatas = AggregatedStoryComposer.CreateManyFromStories(stories, globalTime);
            ActorMerit[] merits = ActorMeritExtractor.CreateManyFromStories(stories);

            string originatingActor = "Eliza Ira";

            // Expected
            int atarCount = 14; // 14 other characters, none repeated twice

            // When
            AtarRecord[] atars = AtarRecordDtoComposer.CreateFor(originatingActor, merits);

            // Then
            Assert.AreEqual(atars.Length, atarCount);
            Assert.IsTrue(atars.Where(a => a.Equals("Eliza Ira")).ToArray().Length == 0);   // does not have self record
        }
    }
}
