﻿using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using Importer.P3_Aggregation.FactoriesSupportive;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateActorBrief
    {
        [Test]
        public void CreateSimpleBriefFromExtractedParams()
        {
            // Given
            string name = "Martyn Hiwasser";
            string mechver = "2210";
            string uid = "2210-martyn-hiwasser";

            //      2 merits --> intensity 2
            AggregatedMerit[] merits = new AggregatedMerit[]
            {
                new AggregatedMerit("230101-story-1", name, "Did something", "thread", "00850303", "00850307"),
                new AggregatedMerit("230101-story-2", name, "Did something else", "thread", "00850309", "00850311")
            };

            AggregatedActor actor = new AggregatedActor(
                uid: uid,
                name: name,
                mechver: mechver,
                factions: new string[] { "orbiter" },
                owner: "public",
                body: "NOT UNDER TEST",
                merits: merits,
                progressions: new AcdeorPlus[] { },
                atarRelations: new AtarRecord[] { },
                flashcardRecords: Array.Empty<FlashcardRecord>(),
                threads: Array.Empty<string>()
            );

            // When
            ActorBriefRecord actual = ActorBriefRecordComposer.CreateSingle(actor);

            // Then
            ActorBriefRecord expected = new ActorBriefRecord(name, mechver, 2, uid);

            Assert.AreEqual(expected, actual);
        }
    }
}
