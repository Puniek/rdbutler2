﻿using Commons.CoreEntities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonsTest.Data
{
    public class CreateLocationTree
    {
        [Test]
        public void Converts_simple_working_record_tree_to_tree()
        {
            // Given
            LocationRecord[] records = {
                new LocationRecord("230505-cats", "Świat", "irrelevant", "Świat|", "1111-11-11"),
                new LocationRecord("230505-cats", "Primus", "irrelevant", "Świat|Primus|", "1111-11-11"),
                new LocationRecord("230505-cats", "Astoria", "irrelevant", "Świat|Primus|Astoria|", "1111-11-11"),
                new LocationRecord("230505-cats", "Neikatis", "irrelevant", "Świat|Primus|Neikatis|", "1111-11-11"),
            };

            // When
            LocationTreeNode actual = LocationTreeComposer.ConvertToTree(records);

            // Then

            LocationTreeNode expected = new LocationTreeNode()
            {
                Title = "Świat",
                Path = "Świat|",
                Children = new List<LocationTreeNode>
                {
                    new LocationTreeNode
                    {
                        Title = "Primus",
                        Path = "Świat|Primus|",
                        Children = new List<LocationTreeNode>
                        {
                            new LocationTreeNode
                            {
                                Title = "Astoria",
                                Path = "Świat|Primus|Astoria|",
                                Children = new List<LocationTreeNode>()
                            },
                            new LocationTreeNode
                            {
                                Title = "Neikatis",
                                Path = "Świat|Primus|Neikatis|",
                                Children = new List<LocationTreeNode>()
                            }
                        }
                    }
                }
            };


            Assert.AreEqual(expected, actual);
        }
    }
}
