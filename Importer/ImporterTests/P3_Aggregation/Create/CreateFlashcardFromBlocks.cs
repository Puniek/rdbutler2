﻿using Importer.P3_Aggregation.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateFlashcardFromBlocks
    {
        [Test]
        public void For_simple_record_no_suffix_one_record()
        {
            // Given
            string givenBlock = @"* Emilia d'Infernia
    * ""żona"" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> ""Egzekucja MILItarnA"")";

            // When
            ReadFlashcard actual = AggregatedActorFlashcardComposer.CreateSingleReadFlashcardFromStory(givenBlock);

            // Then
            ReadFlashcard expected = new ReadFlashcard("Emilia d'Infernia", new string[] {
                @"""żona"" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> ""Egzekucja MILItarnA"")"
            });

            Assert.AreEqual(expected, actual);
        }
    }
}
