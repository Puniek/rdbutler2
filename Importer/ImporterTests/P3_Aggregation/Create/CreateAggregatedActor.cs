﻿using Commons.Tools;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P2_Extraction.Entities;
using Importer.P2_Extraction.Extractors;
using Importer.P3_Aggregation.FactoriesPrimary;
using Importer.P1_ReadParsing.Factories;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateAggregatedActor
    {
        [Test]
        public void From_a_set_of_Stories_and_ActorSheets_extracts_all_unique_actor_candidate_names_as_strings()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(@"TestData\200930-lekarz-dla-elizy.md");
            FileRecord record2 = FileOps.ReadFile(@"TestData\1901-atena-sowinska.md");
            FileRecord record3 = FileOps.ReadFile(@"TestData\200923-magiczna-burza-w-raju.md");

            ReadStory[] stories = ReadStoryFactory.CreateMany(new FileRecord[] { record1, record3 });
            ActorMerit[] merits = ActorMeritExtractor.CreateManyFromStories(stories);
            ReadActor[] actors = ReadActorFactory.CreateMany(new FileRecord[] { record2 });

            // When
            string[] uniqueActors = ActorCandidateExtractor.GetUniqueActors(actors, merits);

            // Expected
            int uniqueActorsCount = 16; // one extra Atena + one duplication of Eliza in Stories.
            string duplicatedActor = "Eliza Ira";
            string actorFromProfile = "Atena Sowińska";

            // Then
            Assert.AreEqual(uniqueActors.Length, uniqueActorsCount);
            Assert.IsTrue(uniqueActors.Where(a => a.Equals(duplicatedActor)).ToArray().Length == 1);
            Assert.IsTrue(uniqueActors.Where(a => a.Equals(actorFromProfile)).ToArray().Length == 1);
        }

        [Test]
        public void If_actor_has_no_corresponding_Stories_only_ActorSheet_then_only_ActorSheet_data_will_create_an_AggregatedActor()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(@"TestData\200930-lekarz-dla-elizy.md");
            FileRecord record2 = FileOps.ReadFile(@"TestData\1901-atena-sowinska.md");
            FileRecord record3 = FileOps.ReadFile(@"TestData\200923-magiczna-burza-w-raju.md");

            ReadStory[] stories = ReadStoryFactory.CreateMany(new FileRecord[] { record1, record3 });
            GlobalTime globalTime = new GlobalTime() { { "Inwazja Noctis", DateTime.ParseExact("00800917", "yyyyMMdd", CultureInfo.InvariantCulture) } };
            AggregatedStory[] aggregatedStories = AggregatedStoryComposer.CreateManyFromStories(stories, globalTime);

            // Atena is not present in those Stories --> not present in Merits, Progressions, Plans etc
            ActorMerit[] merits = ActorMeritExtractor.CreateManyFromStories(stories);
            ActorProgression[] actorProgressions = ActorProgressionExtractor.CreateManyFromStories(stories);

            // Atena has an ActorSheet which shall be used
            ReadActor[] actors = ReadActorFactory.CreateMany(new[] { record2 });

            string actorName = "Atena Sowińska";
            FlashcardRecord[] actorFlashcards = AggregatedActorFlashcardComposer.CreateManyFromManyStories(stories, actors).FirstOrDefault(x => x.ActorName == actorName)?.Records ?? Array.Empty<FlashcardRecord>();
            var emptyStoryToThreadsMap = new Dictionary<string, string[]>(); // when no stories - there will be no story->thread map

            // When
            AggregatedActor actual = AggregatedActorComposer.CreateSingle(actorName, aggregatedStories, actors, merits, actorProgressions, actorFlashcards, emptyStoryToThreadsMap);

            // Expected
            AggregatedActor expected = new AggregatedActor(
                uid: "1901-atena-sowinska",
                name: actorName,
                mechver: "1901",
                factions: new string[] { "sowińscy", "epirjon", "asd" },
                owner: "public",
                body: "NOT UNDER TEST",
                merits: new AggregatedMerit[] { },
                progressions: new AcdeorPlus[] { },
                atarRelations: new AtarRecord[] { },
                flashcardRecords: Array.Empty<FlashcardRecord>(),
                threads: Array.Empty<string>()
            );

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void If_actor_has_no_corresponding_ActorSheet_only_Stories_then_Merits_Progressions_etc_will_create_an_AggregatedActor()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(@"TestData\200930-lekarz-dla-elizy.md");
            FileRecord record2 = FileOps.ReadFile(@"TestData\1901-atena-sowinska.md");
            FileRecord record3 = FileOps.ReadFile(@"TestData\200923-magiczna-burza-w-raju.md");

            ReadStory[] stories = ReadStoryFactory.CreateMany(new FileRecord[] { record1, record3 });
            GlobalTime globalTime = new GlobalTime() { { "Inwazja Noctis", DateTime.ParseExact("00800917", "yyyyMMdd", CultureInfo.InvariantCulture) } };
            AggregatedStory[] aggregatedStories = AggregatedStoryComposer.CreateManyFromStories(stories, globalTime);

            // Elisa is present in those Stories --> has merits, progressions, plans, etc
            ActorMerit[] merits = ActorMeritExtractor.CreateManyFromStories(stories);
            ActorProgression[] actorProgressions = ActorProgressionExtractor.CreateManyFromStories(stories);

            // Elisa has no ActorSheet present
            ReadActor[] actors = ReadActorFactory.CreateMany(new[] { record2 });

            string actorName = "Eliza Ira";
            FlashcardRecord[] actorFlashcards = AggregatedActorFlashcardComposer.CreateManyFromManyStories(stories, actors).FirstOrDefault(x => x.ActorName == actorName)?.Records ?? Array.Empty<FlashcardRecord>();
            var storyToThreadsMap = new Dictionary<string, string[]>
            {
                ["200930-lekarz-dla-elizy"] = new[] { "arcymag-krysztalow" },
                ["200923-magiczna-burza-w-raju"] = new[] { "legenda-arianny" },
            };

            // When
            AggregatedActor actual = AggregatedActorComposer.CreateSingle(actorName, aggregatedStories, actors, merits, actorProgressions, actorFlashcards, storyToThreadsMap);

            // Expected
            AggregatedActor expected = new AggregatedActor(
                uid: "9999-eliza-ira",
                name: actorName,
                mechver: "9999",
                factions: new string[] { },
                owner: "public",
                body: "",
                merits: new AggregatedMerit[] {
                    new AggregatedMerit("200930-lekarz-dla-elizy", actorName, "jeszcze nie arcymagini. Na wpół sparaliżowana, niweluje Skażenie i wątpi w swoje decyzje o nie niszczeniu Astorii. Pierwszy raz użyła magii by zamaskować ścigacz kryształami.", "arcymag-krysztalow", "0080-12-17", "0080-12-21"),
                    new AggregatedMerit("200923-magiczna-burza-w-raju", actorName, "osłabiła burzę magiczną, która spowodowałaby dużo większe zniszczenia w Ruinie Trzeciego Raju, acz tak, by nikt nie wiedział. Poluje na kompetentnych arcymagów.", "legenda-arianny", "1111-11-11", "1111-11-11")
                },
                progressions: new AcdeorPlus[] {
                    new AcdeorPlus("200930-lekarz-dla-elizy", actorName, "dookoła niej jest bezpiecznie; anomalie się nie materializują. Jest neutralizatorem chorych energii.", "0080-12-21"),
                    new AcdeorPlus("200930-lekarz-dla-elizy", actorName, "silna zależność emocjonalna od Karminy i Wanessy. Nienawiść do siebie. Oraz - pragnienie ochrony noktian i zbudowanie dla nich bezpiecznego miejsca.", "0080-12-21"),
                },
                atarRelations: new AtarRecord[] { }, // THIS ONE IS NOT UNDER TEST YET
                flashcardRecords: Array.Empty<FlashcardRecord>(),
                threads: new[] { "arcymag-krysztalow", "legenda-arianny" }
            );

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
