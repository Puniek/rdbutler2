﻿using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateThreadBriefRecords
    {
        [Test]
        public void For_single_story_with_single_thread_without_read_will_extract_values_corresponding_to_this_story()
        {
            // Given
            string threadId = "thread-1";
            string startDate = "0112-01-01";
            string endDate = "0112-01-05";
            string[] players = new string[] { "varether", "tileander" };
            string[] playerActors = new string[] { "Michael Firegiver", "Eltor Retop" };

            AggregatedStory[] stories = new AggregatedStory[] { new AggregatedStory(
                storyUid: "230620-first",
                storyTitle: "First",
                threads: new string[] { threadId },
                motives: Array.Empty<string>(),
                startDate: startDate,
                endDate: endDate,
                seqNo: "5",
                previousStories: new string[] { "230620-first" },
                gms: new string[] {"żółw"},
                players: players,
                playerActors: playerActors,
                allActors: new string[] { "Michael Firegiver", "Eltor Retop", "Connor Longman" },
                summary: "something happened", body: string.Empty) };
            
            ReadThread[] readThreads = Array.Empty<ReadThread>();

            // When
            ThreadBriefRecord[] actual = ThreadBriefRecordComposer.CreateMany(stories, readThreads);

            // Then
            ThreadBriefRecord[] expected = new ThreadBriefRecord[]
            {
                new ThreadBriefRecord(threadId, string.Empty, startDate, endDate, stories.Count(), players, playerActors, string.Empty)
            };

            CollectionAssert.AreEquivalent(expected, actual);

        }

        [Test]
        public void For_single_story_with_three_threads_will_extract_all_with_similar_params()
        {
            // Given
            string[] threads = new string[] { "thread-1", "thread-2", "thread-3" };
            string startDate = "0112-01-01";
            string endDate = "0112-01-05";
            string[] players = new string[] { "varether", "tileander" };
            string[] playerActors = new string[] { "Michael Firegiver", "Eltor Retop" };

            AggregatedStory[] stories = new AggregatedStory[] { new AggregatedStory(
                storyUid: "230620-first",
                storyTitle: "First",
                threads: threads,
                motives: Array.Empty<string>(),
                startDate: startDate,
                endDate: endDate,
                seqNo: "5",
                previousStories: new string[] { "230620-first" },
                gms: new string[] {"żółw"},
                players: players,
                playerActors: playerActors,
                allActors: new string[] { "Michael Firegiver", "Eltor Retop", "Connor Longman" },
                summary: "something happened", body: string.Empty) };

            ReadThread[] readThreads = Array.Empty<ReadThread>();

            // When
            ThreadBriefRecord[] actual = ThreadBriefRecordComposer.CreateMany(stories, readThreads);

            // Then
            ThreadBriefRecord[] expected = new ThreadBriefRecord[]
            {
                new ThreadBriefRecord(threads[0], string.Empty, startDate, endDate, stories.Count(), players, playerActors, string.Empty),
                new ThreadBriefRecord(threads[1], string.Empty, startDate, endDate, stories.Count(), players, playerActors, string.Empty),
                new ThreadBriefRecord(threads[2], string.Empty, startDate, endDate, stories.Count(), players, playerActors, string.Empty)
            };

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void For_two_stories_with_same_thread_will_make_a_crossection_of_params()
        {
            // Given
            string[] threads = new string[] { "thread-1" };
            string minStartDate = "0112-01-01";
            string maxEndDate = "0112-02-02";
            string[] players = new string[] { "varether", "tileander" };
            string[] uniquePlayerActors = new string[] { "Michael Firegiver", "Eltor Retop", "Elena Xairiss" };

            AggregatedStory[] stories = new AggregatedStory[] { 
                new AggregatedStory(
                    storyUid: "230620-first",
                    storyTitle: "First",
                    threads: threads,
                    motives: Array.Empty<string>(),
                    startDate: minStartDate,
                    endDate: "0112-01-03",
                    seqNo: "5",
                    previousStories: new string[] { "230620-first" },
                    gms: new string[] {"żółw"},
                    players: players,
                    playerActors: new string[] { uniquePlayerActors[0], uniquePlayerActors[1]},
                    allActors: new string[] { "Michael Firegiver", "Eltor Retop", "Connor Longman" },
                    summary: "something happened", body: string.Empty),
                new AggregatedStory(
                    storyUid: "230621-second",
                    storyTitle: "Second",
                    threads: threads,
                    motives: Array.Empty<string>(),
                    startDate: "0112-01-05",
                    endDate: maxEndDate,
                    seqNo: "5",
                    previousStories: new string[] { "230620-first" },
                    gms: new string[] {"żółw"},
                    players: players,
                    playerActors: new string[] { uniquePlayerActors[1], uniquePlayerActors[2]},
                    allActors: new string[] { "Michael Firegiver", "Elena Xairiss", "Connor Longman" },
                    summary: "something else happened", body: string.Empty)
            };

            ReadThread[] readThreads = Array.Empty<ReadThread>();

            // When
            ThreadBriefRecord[] actual = ThreadBriefRecordComposer.CreateMany(stories, readThreads);

            // Then
            ThreadBriefRecord[] expected = new ThreadBriefRecord[]
            {
                new ThreadBriefRecord(threads[0], string.Empty, minStartDate, maxEndDate, stories.Count(), players, uniquePlayerActors, string.Empty),
            };

            CollectionAssert.AreEquivalent(expected, actual);
        }

        [Test]
        public void For_single_story_with_single_thread_with_read_thread_will_integrate_data_properly()
        {
            // Given
            string threadId = "thread-1";
            string startDate = "0112-01-01";
            string endDate = "0112-01-05";
            string[] players = new string[] { "varether", "tileander" };
            string[] playerActors = new string[] { "Michael Firegiver", "Eltor Retop" };
            string shortThreadDesc = "Short description";
            string threadName = "First thread";

            AggregatedStory[] stories = new AggregatedStory[] { new AggregatedStory(
                storyUid: "230620-first",
                storyTitle: "First",
                threads: new string[] { threadId },
                motives: Array.Empty<string>(),
                startDate: startDate,
                endDate: endDate,
                seqNo: "5",
                previousStories: new string[] { "230620-first" },
                gms: new string[] {"żółw"},
                players: players,
                playerActors: playerActors,
                allActors: new string[] { "Michael Firegiver", "Eltor Retop", "Connor Longman" },
                summary: "something happened", body: string.Empty) };

            ReadThread[] readThreads = new ReadThread[] {
                new ReadThread(threadId, threadName, shortThreadDesc, "Full description", "Spoilers")
            };

            // When
            ThreadBriefRecord[] actual = ThreadBriefRecordComposer.CreateMany(stories, readThreads);

            // Then
            ThreadBriefRecord[] expected = new ThreadBriefRecord[]
            {
                new ThreadBriefRecord(threadId, threadName, startDate, endDate, stories.Count(), players, playerActors, shortThreadDesc)
            };

            CollectionAssert.AreEquivalent(expected, actual);

        }
    }
}
