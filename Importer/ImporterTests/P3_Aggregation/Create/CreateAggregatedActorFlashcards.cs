﻿using Commons.Tools;
using NUnit.Framework;

using System.Collections.Generic;
using System.Linq;

using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using Importer.P1_ReadParsing.Factories;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateAggregatedActorFlashcards
    {

        [Test]
        public void Flashcard_should_not_contain_duplicated_records_when_data_contains_duplicates()
        {
            // Given
            FileRecord record1 = FileOps.ReadFile(@"TestData\230113-ros-adrienne-a-new-recruit.md");
            FileRecord record2 = FileOps.ReadFile(@"TestData\230117-ros-wiertloplyw-i-tai-mirris.md");

            ReadStory[] stories = ReadStoryFactory.CreateMany(new[] { record1, record2 });
            var actorsDict = new Dictionary<string, string>
            {
                ["Napoleon Myszogłów"] = "uid01",
                ["Tristan Andrait"] = "uid02",
                ["Niferus Sentriak"] = "uid03",
                ["Talia Irris"] = "uid04",
                ["Adrianna Kastir"] = "uid05",
                ["Iga Komczirp"] = "uid06",
                ["Szymon Wilczek"] = "uid07",
                ["Tola Araya"] = "uid08",
                ["Bartek Milkin"] = "uid09",
                ["Tymon Perszak"] = "uid10",
                ["Kasia Karmnik"] = "uid11",
                ["Maciej Koszar"] = "uid12",
                ["Cyprian Mnusz"] = "uid13",
            };
            ReadActor[] actors = actorsDict.Select(x => new ReadActor(x.Value, x.Key, null, null, null, null)).ToArray();

            // When
            AggregatedActorFlashcard[] actual = AggregatedActorFlashcardComposer.CreateManyFromManyStories(stories, actors);

            // Expected
            var expectedValues = new Dictionary<string, FlashcardRecord[]>
            {
                ["Napoleon Myszogłów"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(ENCAO:  +0--- |Prawi wszystkim morały;;;;Bardzo lubi być w środku uwagi| VALS: Universalism, Conformity| DRIVE: Urażony szacunek)"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "atarienin: \"Co zrobisz to dostaniesz\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: smerf ważniak"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"Jesteście w stanie osiągnąć coś lepszego niż macie, tylko musicie się wykazać i udowodnić, że można Wam ufać\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "SPEC: ?"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "SPEC: dyplomacja między ROS a stacją Orbitera"),
                },
                ["Tristan Andrait"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(ENCAO:  0+--+ |Nie toleruje spokoju i nudy; wściekły, na siłowni| VALS: Face >> Achievement| DRIVE: Wolność + Chwała Noctis)"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "dekadianin: \"jest tylko starcie i walka\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: twardy, nie okazuje słabości, gardzi mamlasami, cyniczny"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"Poświęć więcej energii na pracę a mniej na gadanie, ok?\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "SPEC: walka, mięsień"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "(ENCAO:  0+--0 |Agresywny i waleczny;; Bezpośredni;; Nigdy nie kończy walki| VALS: Face >> Achievement| DRIVE: Wolność + Chwała Noctis)"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "dekadianin: \"Noctis shall not fall - czas się uzbroić i walczyć na nowym polu\""),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "styl: twardy, nie okazuje słabości, gardzi mamlasami, cyniczny"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "SPEC: breacher, inżynieria materiałów, przebicie / penetracja"),
                },
                ["Niferus Sentriak"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(ENCAO:  -0++0 |Wszystko rozwiążę teorią;;Przedsiębiorczy i pomysłowa| VALS: Achievement, Face| DRIVE: Legacy of helping)"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "klarkartianin: \"przeszłość nie ma znaczenia, ważne co możemy zrobić\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: Chris Voss 'late night DJ voice'"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"Wszystko rozwiążemy, to kwestia czasu\""),
                },
                ["Talia Irris"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(ENCAO:  0--0+ |lubi jak się jej schlebia;;Spontaniczna;;Przesądna| VALS: Security >> Self-direction| DRIVE: Życie wśród przyjaciół)"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "savaranka: \"słabość jednego z nas to słabość nas wszystkich\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: nostalgiczna savaranka;"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"Nie bądź słabym ogniwem\"; TRAKTOWANA JAK MASKOTKA"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "SPEC: science / medical / space"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "(ENCAO:  0++++ |lubi dzieci;;Przesądna;;Wierzy w duchy;;Nie jest zazdrosna| VALS: Security >> Self-direction| DRIVE: Życie wśród przyjaciół)"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "core wound: nigdy już nie zobaczę swoich dzieci i swojej rodziny; nienawidzę być sama i samotna"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "core lie: ten statek to moja ostatnia szansa na namiastkę rodziny"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "styl: nostalgiczna, melancholijna, cicha savaranka lubiąca towarzystwo ludzi"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "SPEC: space / void operations"),
                },
                ["Adrianna Kastir"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "ENCAO:  +-0-0 |Beztroska i nieco naiwna;;Sarkastyczna i złośliwa | VALS: Universalism, Conformity | DRIVE: Rule of Cool"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "atarienka, OBSERWATORKA"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: gadatliwa, ma zawsze rację, chce pomóc"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"My, dziewczyny musimy trzymać się razem\", \"Nie byłoby fajnie gdyby...\", \"Razem osiągnięmy więcej!!!\""),
                },
                ["Iga Komczirp"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "ENCAO:  0+-0- |Trzeba ciężko pracować;;Łatwa do zniechęcenia| VALS: Power, Humility | DRIVE: Impress the family"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(kapitan), atarienka: \"dołączę do elity!\", 22 lata"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"ugryzłam za dużo i wszystko wyszło mi spod kontroli\" \"ok, jeśli się na to nie zgodzę to nie mam statku!\" \"nie stać mnie na dobrą załogę\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: jestem duża i poważna"),
                },
                ["Szymon Wilczek"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "ENCAO:  --+-0 |Bezkompromisowy;;Kontemplacyjny;;Demotywujący| VALS: Security, Family | DRIVE: Pieniądze dla Siostry"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(inżynier ORAZ szmugler) atarienin \"kto nie próbuje nigdy nie wygrywa\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"zawarłem pakt z diabłem dla ratowania siostry\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: narzeka ale jest twardy"),
                },
                ["Tola Araya"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "ENCAO:  0-+00 |Niemożliwa do ruszenia;;Przewidująca, trudny do zaskoczenia| VALS: Self-direction, Universalism | DRIVE: Save the hibernated, Loyalty"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(eks-marine), 64 lata"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"tyle w życiu zrobiłam rzeczy, że ZALEŻY mi na transporcie tych ludzi\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: na boku"),
                },
                ["Bartek Milkin"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(dzieciak z kotami)"),
                },
                ["Tymon Perszak"] = new[]
                {
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "ENCAO:  -00+- |Wycieńczony gdy musi się socjalizować;;Osoba starej daty | VALS: Stimulation >> Universalism | DRIVE: Red Queen Race"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "(pilot)"),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "\"ja już chcę przejść na emeryturę...\" (zawodzi powoli) \"gdy ja byłem młody to spotkałem TAKIE asteroidy...\""),
                    new FlashcardRecord("230113-ros-adrienne-a-new-recruit", "kadencja: tak tak tak, oczywiście, rozedrgany, starszy zagubiony człowiek"),
                },
                ["Kasia Karmnik"] = new[]
                {
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "(ENCAO:  +00-- |Zapominalska;;Z natury odrzuca wszystkie pomysły| VALS: Face, Family >> Power| DRIVE: Być jak idol (admirał Termia))"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "atarienka: \"Możesz polegać tylko na sobie\""),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "kadencja: irytująca młodsza siostra"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "\"To niesprawiedliwe, nie pasuję tu!\""),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "SPEC: advancer, technik"),
                },
                ["Maciej Koszar"] = new[]
                {
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "(ENCAO: -+0-0 |Ostrożny;;Apatyczny| VALS: Family, Power >> Face| DRIVE: Bezpieczeństwo dla wszystkich)"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "rola: inżynier"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "\"nie wiem co się dzieje i muszę zapewnić że nikomu nie stanie się krzywda\""),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "kadencja: trwożliwy, ale konkretny"),
                },
                ["Cyprian Mnusz"] = new[]
                {
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "ENCAO:  0-+-0 |Pracowity;;Strasznie narzeka| VALS: Hedonism, Family| DRIVE: Wykupić się z długów"),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "górnik / walka, faeril: tylko ludzkie formy, tylko \"nasi\""),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "\"Nienawidzę tych wszystkich noktiańskich gówien...\" \"Nienawidzę TAI...\""),
                    new FlashcardRecord("230117-ros-wiertloplyw-i-tai-mirris", "kadencja: pomocny, dobrą rękę poda, ale narzeka"),
                },
            };
            AggregatedActorFlashcard[] expected = expectedValues.Select(x => new AggregatedActorFlashcard(actorsDict[x.Key], x.Key, x.Value)).ToArray();

            // Then
            Assert.AreEqual(expected.Length, actual.Length);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i].Uid, actual[i].Uid);
                Assert.AreEqual(expected[i].ActorName, actual[i].ActorName);
                for (int j = 0; j < expected[i].Records.Length; j++) Assert.AreEqual(expected[i].Records[j], actual[i].Records[j], $"Error for: {expected[i].ActorName}");
                CollectionAssert.AreEquivalent(expected[i].Records, actual[i].Records);
            }
            CollectionAssert.AreEquivalent(expected, actual);
            string[] allBullets = actual.SelectMany(x => x.Records.Select(y => y.Body)).ToArray();
            Assert.AreEqual(allBullets.ToHashSet().Count, allBullets.Length); // no duplicates
        }
    }
}
