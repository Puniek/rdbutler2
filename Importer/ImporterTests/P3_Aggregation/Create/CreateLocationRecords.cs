﻿using Commons.Tools;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using Importer.P1_ReadParsing.Factories;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateLocationRecords
    {
        [Test]
        public void Makes_successful_Paths_from_location_strings_having_different_depths()
        {
            // Given
            string[] actorDepths = new string[]
            {
                "1. Świat",
                "    1. Primus",
                "        1. Astoria",
                "            1. Cieniaszczyt",
                "    1. Esuriit",
                "        1. Astoria",
                "            1. Pustogor",
                "    1. Salienor"
            };

            // When
            string[] actual = LocationRecordTools.CreatePathsFromActorDepths(actorDepths);

            // Expected
            string[] expected = new string[]
            {
                "Świat|",
                "Świat|Primus|",
                "Świat|Primus|Astoria|",
                "Świat|Primus|Astoria|Cieniaszczyt|",
                "Świat|Esuriit|",
                "Świat|Esuriit|Astoria|",
                "Świat|Esuriit|Astoria|Pustogor|",
                "Świat|Salienor|"
            };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Creates_LocationRecords_from_prepared_file()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);
            AggregatedStory storyInfo = AggregatedStoryComposer.CreateWithDefaults(storyUid: "190113-chronmy-karoline-przed-uczniami", endDate: "0123-11-11");   // date irrelevant

            // When
            LocationRecord[] records = LocationRecordComposer.CreateManyFromStories(new[] { story }, new[] { storyInfo });

            // Expected
            LocationRecord example = new LocationRecord(
                originatingStory: "190113-chronmy-karoline-przed-uczniami",
                locName: "Szkoła Magów",
                locEvent: "miejsce \"ponurej zbrodni\" - zarówno Liliana jak i Napoleon zainfekowani toksyną eksplodowali dwa pomieszczenia. Pięknotka badała temat berserku magów.",
                path: "Świat|Primus|Astoria|Szczeliniec|Powiat Pustogorski|Zaczęstwo|Szkoła Magów|",
                endDate: storyInfo.EndDate);

            int count = 7;

            // Then
            Assert.True(records.Length == count);
            Assert.True(records.Where(s => s.Equals(example)).First() is LocationRecord);

        }
    }
}
