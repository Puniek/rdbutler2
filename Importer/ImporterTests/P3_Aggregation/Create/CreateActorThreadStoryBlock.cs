﻿using Commons.CoreEntities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateActorThreadStoryBlock
    {
        [Test]
        public void Properly_creates_one_for_working_data()
        {
            // Given
            // Construct core info

            string targetPlayer = "Ann The Destroyer";
            string originUid = "230101-second";
            string[] threads = new string[] { "thread-1", "thread-2" };
            string[] motives = new string[] { };
            string startDate = "0112-01-02";
            string endDate = "0112-01-05";
            string deed = "Did awesome stuff";
            AggregatedMerit targetMerit = new AggregatedMerit(originUid: originUid, actor: targetPlayer, deed: deed, threads: string.Join(", ", threads), startDate: startDate, endDate: endDate);
            string[] progressions = { "progression 1", "progression 2" };
            string storyTitle = "Second Story";
            string summary = "This is a summary";

            // Construct inputs

            string[] actorsPresent = new string[] { targetPlayer, "Bart", "Celine", "Darkness" };

            AggregatedStory story = new AggregatedStory(
                storyUid: originUid,
                storyTitle: storyTitle,
                threads: threads,
                motives: motives,
                startDate: startDate,
                endDate: endDate,
                seqNo: "03",
                previousStories: new string[] { "231101-first" },
                gms: new string[] { "gm 1"},
                players: new string[] {"player 1", "player 2"},
                playerActors: new string[] { targetPlayer, "Bart"},
                allActors: actorsPresent,
                summary: summary,
                body: "This is a body"
                );

            AggregatedMerit[] allMerits = new AggregatedMerit[]
            {
                new AggregatedMerit(originUid, "Bart", "Did something", string.Join(", ", threads), startDate, endDate),
                new AggregatedMerit(originUid, "Celine", "Did something else", string.Join(", ", threads), startDate, endDate),
                targetMerit,
                new AggregatedMerit(originUid, "Darkness", "Did something dark", string.Join(", ", threads), startDate, endDate)
            };

            AggregatedMerit[] actorMerits = new AggregatedMerit[]
            {
                targetMerit
            };

            AcdeorPlus[] actorProgressions = new AcdeorPlus[]
            {
                new AcdeorPlus(originUid, targetPlayer, progressions[0], endDate),
                new AcdeorPlus(originUid, targetPlayer, progressions[1], endDate)
            };

            // When
            ActorThreadStoryBlock actual = ActorThreadStoryBlockComposer.CreateSingleFromSingleActorSingleStory(story, targetPlayer, allMerits, actorMerits, actorProgressions);

            // Then
            ActorThreadStoryBlock expected = new ActorThreadStoryBlock(
                originatingActor: targetPlayer, 
                storyUid: originUid, 
                storyTitle: storyTitle, 
                startDate: startDate, 
                endDate: endDate, 
                summary: summary, 
                actorDeed: deed, 
                actorProgressions: progressions, 
                actorsPresent: actorsPresent);

            Assert.AreEqual(expected, actual);
        }
    }
}
