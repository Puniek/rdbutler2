﻿using Commons.CoreEntities;
using Importer.P1_ReadParsing.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateFactionBrief
    {
        [Test]
        public void For_single_faction_and_single_corresponding_merit_will_properly_integrate_into_brief()
        {
            // Given
            ReadFaction[] readFactions = { new ReadFaction("nocne-niebo", "Nocne Niebo", "mafia Kajrata", "irrelevant")};

            AggregatedFactionMerit[] merits = {
                new AggregatedFactionMerit("230707-first", readFactions[0].Name, "good stuff 1", "thread", "0111-01-01", "0111-01-03"),
                new AggregatedFactionMerit("230707-first", "RANDOM DIFF RECORD", "irrelevant", "thread", "0111-01-01", "0111-01-03")
            };

            // When
            FactionBriefRecord[] actual = FactionBriefRecordComposer.CreateMany(merits, readFactions);

            // Then
            FactionBriefRecord[] expected = new FactionBriefRecord[] { 
                new FactionBriefRecord(readFactions[0].Name, 1, readFactions[0].FactionId, readFactions[0].ShortDesc)
            };

            Assert.AreEqual(expected[0], actual[0]);
        }

        [Test]
        public void For_many_factions_and_many_corresponding_merits_will_create_many_briefs()
        {
            // Given
            ReadFaction[] readFactions = { 
                new ReadFaction("nocne-niebo", "Nocne Niebo", "mafia Kajrata", "irrelevant"),
                new ReadFaction("wolny-usmiech", "Wolny Uśmiech", "mafia Grzymościa", "irrelevant")
            };

            AggregatedFactionMerit[] merits = {
                new AggregatedFactionMerit("230707-first", readFactions[0].Name, "good stuff 1.1", "thread", "0111-01-01", "0111-01-03"),
                new AggregatedFactionMerit("230707-first", readFactions[1].Name, "good stuff 1.2", "thread", "0111-01-01", "0111-01-03"),
                new AggregatedFactionMerit("230708-second", readFactions[1].Name, "good stuff 2.2", "thread", "0111-01-04", "0111-01-07"),
                new AggregatedFactionMerit("230709-third", readFactions[0].Name, "good stuff 2.1", "thread", "0111-01-08", "0111-01-10")
            };

            // When
            FactionBriefRecord[] actual = FactionBriefRecordComposer.CreateMany(merits, readFactions);

            // Then
            FactionBriefRecord[] expected = new FactionBriefRecord[] {
                new FactionBriefRecord(readFactions[0].Name, 2, readFactions[0].FactionId, readFactions[0].ShortDesc),
                new FactionBriefRecord(readFactions[1].Name, 2, readFactions[1].FactionId, readFactions[1].ShortDesc)
            };

            Assert.IsTrue(expected.SequenceEqual(actual));
        }
    }
}
