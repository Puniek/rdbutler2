﻿using Castle.Components.DictionaryAdapter.Xml;
using Commons.CoreEntities;
using Importer.P2_Extraction.Entities;
using Importer.P3_Aggregation.FactoriesPrimary;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P3_Aggregation.Create
{
    public class CreateAggregatedFactionMerits
    {
        [Test]
        public void For_single_story_and_factionMerit_aggregates_them_into_aggregatedActionMerit()
        {
            // Given
            AggregatedStory story = AggregatedStoryComposer.CreateWithDefaults(
                storyUid: "230606-first", 
                startDate: "0111-12-10", 
                endDate: "0111-12-12", 
                threads: new string[] {"thread 1", "thread 2"});

            FactionMerit factionMerit = new FactionMerit(
                originatingStoryUid: story.StoryUid, 
                actor: "Nocne Niebo", 
                change: "took over the city");

            // When
            AggregatedFactionMerit actual = AggregatedFactionMeritComposer.Create(new FactionMerit[] {factionMerit}, new AggregatedStory[] { story })[0];

            // Then
            AggregatedFactionMerit expected = new AggregatedFactionMerit(
                originUid: story.StoryUid, 
                actor: factionMerit.Actor, 
                deed: factionMerit.Deed, 
                threads: string.Join(", ", story.Threads), 
                startDate: story.StartDate, 
                endDate: story.EndDate);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void For_many_stories_and_many_factionMerits_properly_selects_those_relevant_only_to_aggregate()
        {
            // Given
            AggregatedStory[] stories = new AggregatedStory[] {
                AggregatedStoryComposer.CreateWithDefaults(storyUid: "230606-first-r", startDate: "0111-12-10", endDate: "0111-12-12", threads: new string[] { } ),
                AggregatedStoryComposer.CreateWithDefaults(storyUid: "230607-second-r", startDate: "0111-12-13", endDate: "0111-12-15", threads: new string[] { } ),
                AggregatedStoryComposer.CreateWithDefaults(storyUid: "230608-third-ir", startDate: "0111-12-17", endDate: "0111-12-20", threads: new string[] { } ),
            };

            FactionMerit[] merits = new FactionMerit[] {
                new FactionMerit(originatingStoryUid: "230606-first-r", actor: "Nocne Niebo", change: "record 1.1"),
                new FactionMerit(originatingStoryUid: "230606-first-r", actor: "Wolny Uśmiech", change: "record 2.1"),
                new FactionMerit(originatingStoryUid: "230606-first-r", actor: "Nocne Niebo", change: "record 1.2"),
                new FactionMerit(originatingStoryUid: "230607-second-r", actor: "Nocne Niebo", change: "record 1.3"),
            };

            // When
            AggregatedFactionMerit[] actual = AggregatedFactionMeritComposer.Create(merits, stories);

            // Then
            AggregatedFactionMerit[] expected = new AggregatedFactionMerit[]{
                new AggregatedFactionMerit("230606-first-r", "Nocne Niebo", "record 1.1", "", "0111-12-10", "0111-12-12"),
                new AggregatedFactionMerit("230606-first-r", "Wolny Uśmiech", "record 2.1", "", "0111-12-10", "0111-12-12"),
                new AggregatedFactionMerit("230606-first-r", "Nocne Niebo", "record 1.2", "", "0111-12-10", "0111-12-12"),
                new AggregatedFactionMerit("230607-second-r", "Nocne Niebo", "record 1.3", "", "0111-12-13", "0111-12-15")
            };

            Assert.IsTrue(expected.SequenceEqual(actual));
        }
    }
}
