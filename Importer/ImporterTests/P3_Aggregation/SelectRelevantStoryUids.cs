﻿using Commons.CoreEntities;
using Importer.P3_Aggregation.Support;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.ActorstoryThreadAsMkdn
{
    public class SelectRelevantStoryUids
    {
        [Test]
        public void AllRelevantToActor_selects_1_for_identical_merit_and_progression()
        {
            // Given

            AggregatedMerit[] merits = new AggregatedMerit[]
            {
                new AggregatedMerit("220401-first", "irrelevant", "irrelevant", "irrelevant", "999998", "999999")
            };

            AcdeorPlus[] progressions = new AcdeorPlus[]
            {
                new AcdeorPlus("220401-first", "irrelevant", "irrelevant", "999999")
            };

            // When
            string[] actual = StoryUidSelector.SelectAllRelevantToActor(merits, progressions);

            // Expected
            string[] expected = new string[] { "220401-first" };

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AllRelevantToActor_deals_with_no_progressions_selecting_unique_merits()
        {
            // Given

            AggregatedMerit[] merits = new AggregatedMerit[]
            {
                new AggregatedMerit("220401-first", "irrelevant", "irrelevant", "irrelevant", "999998", "999999"),
                new AggregatedMerit("220402-second", "irrelevant", "irrelevant", "irrelevant", "999998", "999999")
            };

            AcdeorPlus[] progressions = new AcdeorPlus[]
            {
            };

            // When
            string[] actual = StoryUidSelector.SelectAllRelevantToActor(merits, progressions);

            // Expected
            string[] expected = new string[] { "220401-first", "220402-second" };

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AllRelevantToActor_deals_with_no_merits_selecting_unique_progressions()
        {
            // Given

            AggregatedMerit[] merits = new AggregatedMerit[]
            {
            };

            AcdeorPlus[] progressions = new AcdeorPlus[]
            {
                new AcdeorPlus("220401-first", "irrelevant", "irrelevant", "999999"),
                new AcdeorPlus("220402-second", "irrelevant", "irrelevant", "999999")

            };

            // When
            string[] actual = StoryUidSelector.SelectAllRelevantToActor(merits, progressions);

            // Expected
            string[] expected = new string[] { "220401-first", "220402-second" };

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AllRelevantToActor_deduplicates_progressions_and_merits_picking_uniques_from_both()
        {
            // Given

            AggregatedMerit[] merits = new AggregatedMerit[]
            {
                new AggregatedMerit("220401-first", "irrelevant", "irrelevant", "irrelevant", "999998", "999999"),
                new AggregatedMerit("220402-second", "irrelevant", "irrelevant", "irrelevant", "999998", "999999"),
                new AggregatedMerit("220404-fourth", "irrelevant", "irrelevant", "irrelevant", "999998", "999999")
            };

            AcdeorPlus[] progressions = new AcdeorPlus[]
            {
                new AcdeorPlus("220401-first", "gained X", "irrelevant", "999999"),
                new AcdeorPlus("220401-first", "gained Y", "irrelevant", "999999"),
                new AcdeorPlus("220402-second", "irrelevant", "irrelevant", "999999"),
                new AcdeorPlus("220403-third", "gained X", "irrelevant", "999999"),
                new AcdeorPlus("220403-third", "gained Y", "irrelevant", "999999")

            };

            // When
            string[] actual = StoryUidSelector.SelectAllRelevantToActor(merits, progressions);

            // Expected
            string[] expected = new string[] { "220401-first", "220402-second", "220404-fourth", "220403-third" };

            // Then
            Assert.AreEqual(expected, actual);
        }

    }
}
