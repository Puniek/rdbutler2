﻿using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using Importer.P3_Aggregation.Support;
using NUnit.Framework;
using System;
using System.Globalization;

namespace ImporterTests.P3_Aggregation
{
    public class CalculateChronologicalTime
    {

        [Test]
        public void To_startDate_add_delay_and_duration_to_get_endDate__for_known_startDate()
        {
            // Given
            ReadStory story = ReadStoryFactory.CreateWithDefaults(
                uid: "180101-first",
                prevChronoIds: new[] { "180101-first" },
                time: new TimeRecord(duration: 3, delay: 2, date: "00850101", chronology: ""));                 // which means it ends at 850101+2+3 = 850106

            // When
            (DateTime startDate, DateTime endDate) = TimeCalculator.CalculateStartEndDates(story, new ReadStory[] { story }, new GlobalTime());

            // Then
            Assert.AreEqual(DateTime.ParseExact("00850103", "yyyyMMdd", CultureInfo.InvariantCulture), startDate);
            Assert.AreEqual(DateTime.ParseExact("00850106", "yyyyMMdd", CultureInfo.InvariantCulture), endDate);
        }

        [Test]
        public void To_startDate_add_delay_and_duration_to_get_endDate__for_known_Chronology()
        {
            // Given
            ReadStory story = ReadStoryFactory.CreateWithDefaults(
                uid: "180101-first",
                prevChronoIds: new[] { "180101-first" },
                time: new TimeRecord(duration: 3, delay: 2, date: "", chronology: "Przebudzenie Saitaera"));

            GlobalTime time = new GlobalTime() { { "Przebudzenie Saitaera", DateTime.ParseExact("00790521", "yyyyMMdd", CultureInfo.InvariantCulture) } };

            // When
            (DateTime startDate, DateTime endDate) = TimeCalculator.CalculateStartEndDates(story, new ReadStory[] { story }, time);

            // Then
            Assert.AreEqual(DateTime.ParseExact("00790523", "yyyyMMdd", CultureInfo.InvariantCulture), startDate);
            Assert.AreEqual(DateTime.ParseExact("00790526", "yyyyMMdd", CultureInfo.InvariantCulture), endDate);
        }

        [Test]
        public void For_both_known_date_and_chronology_return_errorenous_date_as_input_story_is_damaged()
        {
            // Given
            ReadStory story = ReadStoryFactory.CreateWithDefaults(
                uid: "180101-first",
                prevChronoIds: new[] { "180101-first" },
                time: new TimeRecord(duration: 3, delay: 2, date: "00850101", chronology: "Przebudzenie Saitaera"));

            GlobalTime time = new GlobalTime() { { "Przebudzenie Saitaera", DateTime.ParseExact("00790521", "yyyyMMdd", CultureInfo.InvariantCulture) } };

            // When
            (DateTime startDate, DateTime endDate) = TimeCalculator.CalculateStartEndDates(story, new ReadStory[] { story }, time);

            // Expected
            DateTime error = DateTime.ParseExact("11111111", "yyyyMMdd", CultureInfo.InvariantCulture);

            // Then
            Assert.AreEqual(error, startDate);
            Assert.AreEqual(error, endDate);
        }

        [Test]
        public void For_negative_delay_subtract_days_from_startDate()
        {
            // Given
            ReadStory story = ReadStoryFactory.CreateWithDefaults(
                uid: "180101-first",
                prevChronoIds: new[] { "180101-first" },
                time: new TimeRecord(duration: 3, delay: -10, date: "00850111", chronology: ""));               // which means it ends at 850111-10+3 = 850104

            // When
            (DateTime startDate, DateTime endDate) = TimeCalculator.CalculateStartEndDates(story, new ReadStory[] { story }, new GlobalTime());

            // Then
            Assert.AreEqual(DateTime.ParseExact("00850101", "yyyyMMdd", CultureInfo.InvariantCulture), startDate);
            Assert.AreEqual(DateTime.ParseExact("00850104", "yyyyMMdd", CultureInfo.InvariantCulture), endDate);
        }

        [Test]
        public void For_negative_duration_returns_errorenous_date()
        {
            // Given
            ReadStory story = ReadStoryFactory.CreateWithDefaults(
                uid: "180101-first",
                prevChronoIds: new[] { "180101-first" },
                time: new TimeRecord(duration: -13, delay: 2, date: "00850101", chronology: ""));

            // When
            (DateTime startDate, DateTime endDate) = TimeCalculator.CalculateStartEndDates(story, new ReadStory[] { story }, new GlobalTime());

            // Expected
            DateTime error = DateTime.ParseExact("11111111", "yyyyMMdd", CultureInfo.InvariantCulture);

            // Then
            Assert.AreEqual(error, startDate);
            Assert.AreEqual(error, endDate);
        }

        [Test]
        public void For_chained_stories_use_previous_EndDate_as_StartDate()
        {
            // Given
            ReadStory first = ReadStoryFactory.CreateWithDefaults(
                uid: "180101-first",
                prevChronoIds: new[] { "180101-first" },
                time: new TimeRecord(duration: 3, delay: 2, date: "00850101", chronology: ""));     // which means it ends at 850101+2+3 = 850106
            ReadStory second = ReadStoryFactory.CreateWithDefaults(
                uid: "180102-second",
                prevChronoIds: new[] { "180101-first" },
                time: new TimeRecord(duration: 2, delay: 1, date: "", chronology: ""));             // which means it ends at 850106+2+1 = 850109

            // When
            (DateTime startDate1, DateTime endDate1) = TimeCalculator.CalculateStartEndDates(first, new ReadStory[] { first, second }, new GlobalTime());
            (DateTime startDate2, DateTime endDate2) = TimeCalculator.CalculateStartEndDates(second, new ReadStory[] { first, second }, new GlobalTime());

            // Then
            Assert.AreEqual(DateTime.ParseExact("00850103", "yyyyMMdd", CultureInfo.InvariantCulture), startDate1);
            Assert.AreEqual(DateTime.ParseExact("00850106", "yyyyMMdd", CultureInfo.InvariantCulture), endDate1);
            Assert.AreEqual(DateTime.ParseExact("00850107", "yyyyMMdd", CultureInfo.InvariantCulture), startDate2);
            Assert.AreEqual(DateTime.ParseExact("00850109", "yyyyMMdd", CultureInfo.InvariantCulture), endDate2);
        }
    }
}
