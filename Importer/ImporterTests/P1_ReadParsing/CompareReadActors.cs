﻿using Importer.P1_ReadParsing.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing
{
    public class CompareReadActors
    {

        [Test]
        public void Two_ReadActors_are_equal_if_they_differ_by_body_only()
        {
            // Given
            ReadActor a1 = new ReadActor(
                identifier: "1901-atena-sowinska",
                name: "Atena Sowińska",
                mechver: "1901",
                factions: new[] { "sowińscy", "epirjon", "asd" },
                owner: "public",
                body: "irrelevant");

            ReadActor a2 = new ReadActor(
                identifier: "1901-atena-sowinska",
                name: "Atena Sowińska",
                mechver: "1901",
                factions: new[] { "sowińscy", "epirjon", "asd" },
                owner: "public",
                body: "COMPLETELY irrelevant");

            // When, Then
            Assert.AreEqual(a1, a2);
        }
    }
}
