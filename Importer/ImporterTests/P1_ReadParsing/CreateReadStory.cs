﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using NUnit.Framework;
using System;

namespace ImporterTests.P1_ReadParsing
{
    internal class CreateReadStory
    {
        [Test]
        public void ReadStory_is_created_from_proper_predefined_md_file()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\180623-krzyk-w-wyplywowie.md");

            // When
            ReadStory actual = ReadStoryFactory.CreateSingle(record);

            // Expected
            ReadStory expected = new(
                uid: "180623-krzyk-w-wyplywowie",
                title: "Krzyk w Wypływowie",
                threads: new[] { "nowa-kompania" },
                motives: Array.Empty<string>(),
                prevChronoIds: new[] { "180623-krzyk-w-wyplywowie" },
                prevCampaignIds: new[] { "180623-krzyk-w-wyplywowie" },
                gms: new[] { "żółw" },
                players: new[] { "arleta", "kić" },
                summary: "Dwie ochroniarki gwiazdy pop przybyły do Wypływowa, stron rodzinnych owej gwiazdy pop. Tam okazało się, że znikają ludzie do jakiejś dziwnej efemerydy w kształcie kamiennego skorpiona. Efemeryda okazała się ciut bardziej inteligentna niż zwykle, magitrownia trochę zbyt przyoszczędzała i ogólnie poszło w ruinę. Terminuski zwerbowały do pomocy jedynego katalistę w okolicy, wpakowały swojego zleceniodawcę w kłopoty i ruszyły ratować magitrownię przed skorpionem...",
                location: @"1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Trójząb
                        1. Powiat Jezierzy
                            1. Wypływowo
                                1. Osiedle Siłowiec: na obrzeżach Wypływowa i służące jako osiedle industrialne
                                    1. Magitrownia Selenin: nad ogromnym uskokiem energii, pod połączoną kontrolą AvianXPress i miasta, współzarządzana przez człowieka i JEDNEGO katalistę...
                                1. Osiedle Lotnicze: znajdujące się najdalej od centrum Wypływowa
                                    1. Montownia Awianów: gdzie są dwaj kataliści ale nie chcą pomóc magitrowni
                                1. Osiedle Domowe: gdzie znikali ludzie przez problemy w magitrowni; blisko centrum Wypływowa
                                    1. Hotel Bez Pyłu: który niestety spłonął w wyniku problemów magicznych Małgorzaty i Kaliny",
                timeRecord: new TimeRecord(3, 0, "00860312", string.Empty),
                actorNames: new[] { "Małgorzata Nowacka", "Kalina Rotmistrz" },
                body: string.Empty
                );

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}