﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using NUnit.Framework;
using System;
using System.Globalization;

namespace ImporterTests.P1_ReadParsing
{
    public class CreateGlobalTime
    {
        [Test]
        public void Is_created_from_proper_predefined_date_file()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\_kluczowe-daty.md");

            // Expected
            GlobalTime expected = new GlobalTime
            {
                { "Pęknięcie Rzeczywistości", DateTime.ParseExact("00010714", "yyyyMMdd", CultureInfo.InvariantCulture) },
                { "Przebudzenie Saitaera", DateTime.ParseExact("00790321", "yyyyMMdd", CultureInfo.InvariantCulture) },
                { "Inwazja Noctis", DateTime.ParseExact("00800917", "yyyyMMdd", CultureInfo.InvariantCulture) },
                { "Zmiażdżenie Inwazji Noctis", DateTime.ParseExact("00820704", "yyyyMMdd", CultureInfo.InvariantCulture) },
                { "Aktualna chronologia", DateTime.ParseExact("01090724", "yyyyMMdd", CultureInfo.InvariantCulture) }
            };

            // When
            GlobalTime actual = GlobalTimeFactory.CreateSingle(record.Content);

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
