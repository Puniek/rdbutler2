﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using NUnit.Framework;

namespace ImporterTests.P1_ReadParsing
{
    public class CreateReadActor
    {
        [Test]
        public void ReadActor_is_created_from_proper_predefined_md_file()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\1901-atena-sowinska.md");

            // When
            ReadActor actual = ReadActorFactory.CreateSingle(record);

            // Expected
            ReadActor expected = new ReadActor(
                identifier: "1901-atena-sowinska",
                name: "Atena Sowińska",
                mechver: "1901",
                factions: new[] { "sowińscy", "epirjon", "asd" },
                owner: "public",
                body: "irrelevant");

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}