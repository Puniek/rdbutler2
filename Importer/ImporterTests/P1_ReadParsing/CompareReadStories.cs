﻿using Importer.P1_ReadParsing.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImporterTests.P1_ReadParsing
{
    public class CompareReadStories
    {
        [Test]
        public void Two_ReadStories_are_equal_if_they_have_all_members_the_same_in_value()
        {
            // Given
            ReadStory s1 = new ReadStory(
                uid: "1",
                title: "1",
                threads: new[] { "current-story" },
                motives: new[] { "motive" },
                prevChronoIds: new[] { "1" },
                prevCampaignIds: new[] { "1" },
                gms: new[] { "1" },
                players: new[] { "1" },
                summary: "1",
                location: "1",
                timeRecord: new TimeRecord(2, 3, "00190308", ""),
                actorNames: new[] { "name" },
                body: "1"
                );

            ReadStory s2 = new ReadStory(
                uid: "1",
                title: "1",
                threads: new[] { "current-story" },
                motives: new[] { "motive" },
                prevChronoIds: new[] { "1" },
                prevCampaignIds: new[] { "1" },
                gms: new[] { "1" },
                players: new[] { "1" },
                summary: "1",
                location: "1",
                timeRecord: new TimeRecord(2, 3, "00190308", ""),
                actorNames: new[] { "name" },
                body: "1"
                );

            // When, Then
            Assert.AreEqual(s1, s2);
        }
    }
}
