﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing
{
    public class CreateReadStoryTag
    {
        [Test]
        public void Creating_single_properly_extracts_all_fields_from_well_formed_record()
        {
            // Given
            FileRecord record = new FileRecord(
                path: @"C:\path\to\threads\deprecated.md",
                content: @"
# Thread
## Metadane

* Nazwa: Deprecated

## Krótki opis

Legendy i opowieści, rzeczy które się wydarzyły ale nie są w 100% prawdą.

## Opis

""Legendy i opowieści"", rzeczy które się wydarzyły ale rzeczywistość się przesunęła. W jakiejś formie te rzeczy się stały...

## Spoilers

brak
");

            // When
            ReadStoryTag actual = ReadStoryTagFactory.CreateSingle(record);

            // Then
            ReadStoryTag expected = new ReadStoryTag(
                tagUid: "deprecated",
                name: "Deprecated",
                shortDesc: @"Legendy i opowieści, rzeczy które się wydarzyły ale nie są w 100% prawdą.",
                fullDesc: @"""Legendy i opowieści"", rzeczy które się wydarzyły ale rzeczywistość się przesunęła. W jakiejś formie te rzeczy się stały...",
                spoilers: "brak"
                );

            Assert.AreEqual(expected, actual );

        }

        [Test]
        public void Lack_of_spoilers_is_not_a_problem()
        {
            // Given
            FileRecord record = new FileRecord(
                path: @"C:\path\to\threads\deprecated.md",
                content: @"
# Thread
## Metadane

* Nazwa: Deprecated

## Krótki opis

Legendy i opowieści, rzeczy które się wydarzyły ale nie są w 100% prawdą.

## Opis

""Legendy i opowieści"", rzeczy które się wydarzyły ale rzeczywistość się przesunęła. W jakiejś formie te rzeczy się stały...
");

            // When
            ReadStoryTag actual = ReadStoryTagFactory.CreateSingle(record);

            // Then
            ReadStoryTag expected = new ReadStoryTag(
                tagUid: "deprecated",
                name: "Deprecated",
                shortDesc: @"Legendy i opowieści, rzeczy które się wydarzyły ale nie są w 100% prawdą.",
                fullDesc: @"""Legendy i opowieści"", rzeczy które się wydarzyły ale rzeczywistość się przesunęła. W jakiejś formie te rzeczy się stały...",
                spoilers: string.Empty
                );

            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void All_fields_may_be_absent_except_threadId()
        {
            // Given
            FileRecord record = new FileRecord(
                path: @"C:\path\to\threads\deprecated.md",
                content: @"");

            // When
            ReadStoryTag actual = ReadStoryTagFactory.CreateSingle(record);

            // Then
            ReadStoryTag expected = new ReadStoryTag(
                tagUid: "deprecated",
                name: string.Empty,
                shortDesc: string.Empty, 
                fullDesc: string.Empty,
                spoilers: string.Empty
                );

            Assert.AreEqual(expected, actual);

        }
    }
}
