﻿using Commons.Tools;
using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    class ExtractActorNamesFromStory
    {
        [Test]
        public void Extracts_one_actor_when_one_present()
        {
            // Given
            string storyContent = @"whatever

## Zasługi
ssdsdsd
* Present Actor: did irrelevant stuff
dasdasdas
";
            // When
            string[] actual = SectionParser.ExtractActorNamesFromStory(storyContent);

            // Then
            string[] expected = new string[] { "Present Actor" };

            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [Test]
        public void Extracts_no_actor_when_none_present()
        {
            // Given
            string storyContent = @"whatever

## Zasługi
ssdsdsd
dasdasdas
";
            // When
            string[] actual = SectionParser.ExtractActorNamesFromStory(storyContent);

            // Then
            string[] expected = new string[] { };

            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [Test]
        public void Extracts_many_actors_when_one_present()
        {
            // Given
            string storyContent = @"whatever

## Zasługi

* Present Actor: did irrelevant stuff
* Another Actor: did irrelevant stuff

";
            // When
            string[] actual = SectionParser.ExtractActorNamesFromStory(storyContent);

            // Then
            string[] expected = new string[] { "Present Actor", "Another Actor" };

            Assert.IsTrue(expected.SequenceEqual(actual));
        }
    }
}
