﻿using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    public class ExtractThreadsFromStory
    {

        [Test]
        public void Extracts_one_thread_into_array()
        {
            // Given
            var storyMetadata = @"
## Metadane

* title: ""Ten nawiedzany i ta ukryta""
* threads: rekiny-a-akademia
* gm: żółw
* players: kić, dzióbek

## Kontynuacja
                ";

            // When
            string[] actual = MetadataSectionParser.ExtractManyParamsFromStory("threads", storyMetadata);

            // Then
            string[] expected = { "rekiny-a-akademia" };
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_two_distinct_threads_into_array()
        {
            // Given
            var storyMetadata = @"
## Metadane

* title: ""Ten nawiedzany i ta ukryta""
* threads: rekiny-a-akademia, plaga-szczurów
* gm: żółw
* players: kić, dzióbek

## Kontynuacja
                ";

            // When
            string[] actual = MetadataSectionParser.ExtractManyParamsFromStory("threads", storyMetadata);

            // Then
            string[] expected = { "rekiny-a-akademia", "plaga-szczurów" };
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_two_distinct_threads_if_third_record_is_duplicated()
        {
            // Given
            var storyMetadata = @"
## Metadane

* title: ""Ten nawiedzany i ta ukryta""
* threads: rekiny-a-akademia, plaga-szczurów, rekiny-a-akademia
* gm: żółw
* players: kić, dzióbek

## Kontynuacja
                ";

            // When
            string[] actual = MetadataSectionParser.ExtractManyParamsFromStory("threads", storyMetadata);

            // Then
            string[] expected = { "rekiny-a-akademia", "plaga-szczurów" };
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_one_working_thread_even_if_record_is_very_bad()
        {
            // Given
            var storyMetadata = @"
## Metadane

* title: ""Ten nawiedzany i ta ukryta""
* threads: , , rekiny-a-akademia, 
* gm: żółw
* players: kić, dzióbek

## Kontynuacja
                ";

            // When
            string[] actual = MetadataSectionParser.ExtractManyParamsFromStory("threads", storyMetadata);

            // Then
            string[] expected = { "rekiny-a-akademia" };
            Assert.AreEqual(expected, actual);
        }

    }
}
