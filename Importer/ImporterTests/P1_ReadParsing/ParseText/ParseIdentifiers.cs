﻿using Commons.Tools;
using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    class ParseIdentifiers
    {

        [TestCase(@"181122-upiory-w-biurowcu.md", "181122-upiory-w-biurowcu")]
        [TestCase(@"\181122-upiory-w-biurowcu.md", "181122-upiory-w-biurowcu")]
        [TestCase(@"C:\RiggedDice\rpg\ez2050\opowiesci\181122-upiory-w-biurowcu.md", "181122-upiory-w-biurowcu")]
        public void Extracts_story_identifier_from_proper_story_filepath(string path, string expected)
        {
            // When
            string actual = IdentifierParser.ExtractStoryIdentifier(path);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [TestCase(@"C:\RiggedDice\rpg\ez2050\opowiesci\1811281-upiory-w-biurowcu.md", "")]
        [TestCase(@"C:\RiggedDice\rpg\ez2050\opowiesci\18112-upiory-w-biurowcu.md", "")]
        public void For_improper_story_filepath_returns_empty_string_as_story_identifier(string path, string expected)
        {
            // When
            string actual = IdentifierParser.ExtractStoryIdentifier(path);

            // Then
            Assert.AreEqual(expected, actual);
        }


        [TestCase(@"1811-atena-sowinska.md", "1811-atena-sowinska")]
        [TestCase(@"\1811-atena-sowinska.md", "1811-atena-sowinska")]
        [TestCase(@"C:\RiggedDice\rpg\ez2050\1811-atena-sowinska.md", "1811-atena-sowinska")]
        public void Extracts_actor_identifier_from_proper_actor_filepath(string path, string expected)
        {
            // When
            string actual = IdentifierParser.ExtractActorIdentifier(path);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [TestCase(@"C:\RiggedDice\rpg\ez2050\karty-postaci\18111-atena-sowinska.md", "")]
        [TestCase(@"C:\RiggedDice\rpg\ez2050\karty-postaci\181-atena-sowinska.md", "")]
        public void For_improper_actor_filepath_returns_empty_string_as_actor_identifier(string path, string expected)
        {
            // When
            string actual = IdentifierParser.ExtractActorIdentifier(path);

            // Then
            Assert.AreEqual(expected, actual);
        }

    }
}
