﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    class ParseTimeRecords
    {
        [Test]
        public void Creates_TimeRecord_having_all_fields_properly_set()
        {
            // Given
            string input = @"## Czas

* Opóźnienie: 5
* Dni: 1
* Data: 00190204
* Chronologia: Inwazja Noctis
";

            // When
            TimeRecord actual = SectionParser.ExtractTimeRecordFromStory(input);

            // Expected
            TimeRecord expected = new TimeRecord(1, 5, "00190204", "Inwazja Noctis");

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Creates_TimeRecord_having_all_fields_properly_set__with_negative_delay()
        {
            // Given
            string input = @"## Czas

* Opóźnienie: -5
* Dni: 1
* Data: 00190204
* Chronologia: Inwazja Noctis
";

            // When
            TimeRecord actual = SectionParser.ExtractTimeRecordFromStory(input);

            // Expected
            TimeRecord expected = new TimeRecord(1, -5, "00190204", "Inwazja Noctis");

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
