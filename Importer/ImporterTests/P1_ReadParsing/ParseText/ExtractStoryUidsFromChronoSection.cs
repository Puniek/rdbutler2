﻿using Commons.Tools;
using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    class ExtractStoryUidsFromChronoSection
    {
        [Test]
        public void Extracts_only_single_chrono_uid_from_proper_text_section()
        {
            // Given
            string input = @"
# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211102 - Campaign link](211102-campaign-link)

### Chronologiczna

* [211102 - Chronological link](211102-chronological-link)

## Plan sesji
";

            // When
            string[] actual = SectionParser.ExtractPrevChronoUidsFromStory(input);

            // Expected
            string[] expected = new string[] { "211102-chronological-link" };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_multiple_chrono_uids_from_proper_text_section()
        {
            // Given
            string input = @"
# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211102 - Campaign link 1](211102-campaign-link-1)
* [211103 - Campaign link 2](211102-campaign-link-2)

### Chronologiczna

* [211102 - Chronological link 1](211102-chronological-link-1)
* [211103 - Chronological link 2](211102-chronological-link-2)

## Plan sesji
";

            // When
            string[] actual = SectionParser.ExtractPrevChronoUidsFromStory(input);

            // Expected
            string[] expected = new string[] { "211102-chronological-link-1", "211102-chronological-link-2" };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_only_single_campaign_uid_from_proper_text_section()
        {
            // Given
            string input = @"
# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211102 - Campaign link](211102-campaign-link)

### Chronologiczna

* [211102 - Chronological link](211102-chronological-link)

## Plan sesji
";

            // When
            string[] actual = SectionParser.ExtractPrevCampaignUidsFromStory(input);

            // Expected
            string[] expected = new string[] { "211102-campaign-link" };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_multiple_campaign_uids_from_proper_text_section()
        {
            // Given
            string input = @"
# {{ page.title }}

## Kontynuacja

### Kampanijna

* [211102 - Campaign link 1](211102-campaign-link-1)
* [211103 - Campaign link 2](211102-campaign-link-2)

### Chronologiczna

* [211102 - Chronological link 1](211102-chronological-link-1)
* [211103 - Chronological link 2](211102-chronological-link-2)

## Plan sesji
";

            // When
            string[] actual = SectionParser.ExtractPrevCampaignUidsFromStory(input);

            // Expected
            string[] expected = new string[] { "211102-campaign-link-1", "211102-campaign-link-2" };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
