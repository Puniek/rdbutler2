﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    class ParseAcdeors
    {
        [Test]
        public void Extracts_FactionPlan_records_from_predefined_Story()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);

            string firstPhase = SectionParser.ExtractSectionFromStoryBody(2, 2, "Plany", story.Body);

            // When
            string[] actual = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(3, 3, "Frakcji", firstPhase);

            // Expected
            string[] expected =
            {
                "* Szkoła Magów: zapewnić jakieś bezpieczeństwo przed szaleństwami uczniów"
            };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_ActorPlan_records_from_predefined_Story()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);

            // When
            string[] actual = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(2, 3, "Plany", story.Body);

            // Expected
            string[] expected =
            {
                "* Adela Kirys: znaleźć jakiś sposób, by pokazać, że jest świetna",
                "* Arnulf Poważny: zabezpieczyć magów przed terminusami",
            };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_FactionProgression_records_from_predefined_Story()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);

            string firstPhase = SectionParser.ExtractSectionFromStoryBody(2, 2, "Progresja", story.Body);

            // When
            string[] actual = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(3, 3, "Frakcji", firstPhase);

            // Expected
            string[] expected =
            {
                "* Testowa Frakcja: czy ten rekord zadziała?"
            };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_ActorProgression_records_from_predefined_Story()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);

            // When
            string[] actual = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(2, 3, "Progresja", story.Body);

            // Expected
            string[] expected =
            {
                "* Pięknotka Diakon: Arnulf Poważny jej ufa i będzie z nią współpracował zanim wybierze innego terminusa.",
                "* Arnulf Poważny: będzie współpracował z Pięknotką jako terminuską zanim z jakimkolwiek innym terminusem.",
                "* Arnulf Poważny: stracił trochę szacunku w oczach innych uczniów; myślą, że nie rozpoznał Pięknotki jako terminuski. Cóż.",
                "* Adela Kirys: traci trochę pewności siebie - widzi, że mogła zostać pozwana za to co zrobiła sama z tym eliksirem dla Karoliny Erenit (bez sprawdzenia osobiście)",
                "* Liliana Bankierz: dostała całkowicie nieuzasadnioną opinię osoby zdolnej do walki z terminusem w power suicie bez niczego (przez erupcję energii w Pięknotkę)."
            };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_Merit_records_from_predefined_Story()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);

            // When
            string[] actual = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(2, 2, "Zasługi", story.Body);

            // Expected
            string[] expected =
            {
                "* Pięknotka Diakon: poszła do Szkoły Magów zająć się papierkową robotą, skończyła rozwiązując małą Plagę sprowadzoną przypadkowo przez Adelę - przy okazji poznała kilku uczniów.",
                "* Arnulf Poważny: dyrektor skłonny do osłony swoich uczniów nawet, jeśli coś zbroili. Będzie współpracował z Pięknotką, bo udowodniła, że zależy jej na dobru a nie prawie.",
                "* Napoleon Bankierz: chciał chronić Karolinę Erenit przed innymi magami, zdobył eliksir od Adeli ale źle wyspecyfikował. Zaraził się, zdemolował pokój i - na szczęście - dyrektor wszystko wyciszył.",
                "* Liliana Bankierz: chciała pomóc Karolinie tak jak Napoleon, ale wezwała terminusa. Też się zaraziła i zdemolowała pokój. Dostała niesłuszną reputację. Kiedyś Diakonka, ale ciało odrzuciła tą krew.",
                "* Teresa Mieralit: nauczycielka magii leczniczej i katalistka w Szkole Magów. Pomogła Lilianie.",
                "* Adela Kirys: stworzyła eliksir dla Karoliny, proszona przez Napoleona. Niestety, eliksir wszedł w interakcję z polem magicznym Karoliny (nie wiedziała że Karolina ma takie pole).",
                "* Karolina Erenit: była źle traktowana przez magów (nie wie o tym), więc Napoleon Bankierz próbował ją chronić eliksirem Adeli Kirys. Stała się wektorem toksyny z Trzęsawiska. Tymczasowo."
            };

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void For_section_without_records_in_parsed_section_extracts_an_empty_array()
        {
            // Given
            string input = @"## Progresja

* .

### Frakcji

* Myszy: szczury
";

            // When
            string[] actual = SectionParser.ExtractSectionRecordsInActorDeedsFormatFromStoryBody(2, 3, "Progresja", input);

            // Expected
            string[] expected = new string[0];

            // Then
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
