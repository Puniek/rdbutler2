﻿using Commons.Tools;
using Importer.P1_ReadParsing.Entities;
using Importer.P1_ReadParsing.Factories;
using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    class ParseLocationRecords
    {
        [TestCase(@"1. Świat", "")]
        [TestCase(@"1. Świat: pełen zła", "pełen zła")]
        [TestCase(@"    1. Primus", "")]
        [TestCase(@"    1. Primus: kraina snów: kot", "kraina snów: kot")]
        public void Extracts_deed_from_location_record_regardless_of_depth(string input, string expected)
        {
            // When
            string actual = SectionParser.ExtractDeedsFromLocationRecord(input);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Extracts_location_SECTION_from_predefined_Story()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\190113-chronmy-karoline-przed-uczniami.md");
            ReadStory story = ReadStoryFactory.CreateSingle(record);

            // Expected
            string expected =
@"1. Świat
    1. Primus
        1. Astoria
            1. Szczeliniec
                1. Powiat Pustogorski
                    1. Zaczęstwo
                        1. Szkoła Magów: miejsce ""ponurej zbrodni"" - zarówno Liliana jak i Napoleon zainfekowani toksyną eksplodowali dwa pomieszczenia. Pięknotka badała temat berserku magów.";

            // When
            string actual = SectionParser.ExtractLocationFromStory(story.Body);

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
