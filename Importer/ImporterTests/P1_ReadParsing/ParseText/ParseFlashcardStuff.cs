﻿using Importer.P1_ReadParsing.Parsers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImporterTests.P1_ReadParsing.ParseText
{
    public class ParseFlashcardStuff
    {
        [Test]
        public void For_story_which_has_Flashcard_section_extract_it_properly()
        {
            // Given - expected component embedded in larger part
            string expectedSection = @"#### 1. Infernia

* Bartłomiej Korkoran: wuj i twarda łapka rządząca Infernią (WG: ""System służy ludziom"") | faeril: ""Infernia służy Korkoranom jako awatar Bezimiennej Pani.""
    * (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd)
    * ""Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!""
* Emilia d'Infernia
    * ""żona"" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> ""Egzekucja MILItarnA"")
* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.

#### 2. Arkologia Nativis

* Rafał Kidiron
    * (ENCAO: 00+-0 |precyzyjny i zaplanowany;; DIMIR (UB);; Żywy wąż nie człowiek | Achievement, Power, Face | DRIVE: Duma i monument)
    * security lord i nieformalny dyktator arkologii
    * ""Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości.""
    * ""Tylko ci, którzy są przydatni mają miejsce w Nativis""";

            // Given - larger part
            string partOfStory = @$"

### Sukces graczy (when you win)

* ?

## Sesja właściwa

### Fiszki

{expectedSection}

### Scena Zero - impl

Trzy dni temu. Kidiron ma wiadomość do Eustachego. Kasia została...

";
            // When
            string actual = SectionParser.ExtractFlashcardSectionFromStory(partOfStory);

            // Then
            Assert.AreEqual(expectedSection, actual);

        }

        [Test]
        public void For_Flashcard_section_extract_all_Flashcard_blocks()
        {
            // Given - input data
            string flashcardSection = @"#### 1. Infernia

* Bartłomiej Korkoran: wuj i twarda łapka rządząca Infernią (WG: ""System służy ludziom"") | faeril: ""Infernia służy Korkoranom jako awatar Bezimiennej Pani.""
    * (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd)
    * ""Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!""
* Emilia d'Infernia
    * ""żona"" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> ""Egzekucja MILItarnA"")
* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.

#### 2. Arkologia Nativis

* Rafał Kidiron
    * (ENCAO: 00+-0 |precyzyjny i zaplanowany;; DIMIR (UB);; Żywy wąż nie człowiek | Achievement, Power, Face | DRIVE: Duma i monument)
    * security lord i nieformalny dyktator arkologii
    * ""Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości.""
    * ""Tylko ci, którzy są przydatni mają miejsce w Nativis""";

            // When
            string[] actual = SectionParser.ExtractBulletBlocksFromSection(flashcardSection);

            // Then
            // Expected - split into subcomponents
            string[] expected = new[]
            {
                @"* Bartłomiej Korkoran: wuj i twarda łapka rządząca Infernią (WG: ""System służy ludziom"") | faeril: ""Infernia służy Korkoranom jako awatar Bezimiennej Pani.""
    * (ENCAO: +-000 |Bezkompromisowy, nieustępliwy;;Ciekawski|Family, Benevolence, Self-direction > Achievement, Tradition, Humility| DRIVE: Inkwizytor: ujawnianie bolesnych prawd)
    * ""Nasza Infernia ma za zadanie dać nam wolność oraz pomagać innym!""",
                @"* Emilia d'Infernia
    * ""żona"" Bartłomieja. Żywy arsenał. Egzekutor Bartłomieja. (Emilia -> ""Egzekucja MILItarnA"")",
                @"* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.",
                @"* Rafał Kidiron
    * (ENCAO: 00+-0 |precyzyjny i zaplanowany;; DIMIR (UB);; Żywy wąż nie człowiek | Achievement, Power, Face | DRIVE: Duma i monument)
    * security lord i nieformalny dyktator arkologii
    * ""Nasza arkologia zaszła tak daleko i nie możemy pozwolić, by cokolwiek stanęło na drodzę jej absolutnej wielkości.""
    * ""Tylko ci, którzy są przydatni mają miejsce w Nativis"""

            };

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void For_typical_Flashcard_block_extract_name()
        {
            // Given
            string block = @"* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.""";

            // When
            string actual = SectionParser.ExtractActorNameFromFlashcardBlock(block);

            // Then
            Assert.AreEqual("Ardilla Korkoran", actual);

        }

        [Test]
        public void For_typical_Flashcard_block_extract_records()
        {
            // Given
            string block = @"* Ardilla Korkoran: faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox
    * badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze.";

            // When
            string[] actual = SectionParser.ExtractBulletRecordsFromFlashcardBlockIncludingRecordAfterColon(block);

            // Then
            string[] expected = new string[] {
            @"faerilka działająca jak drakolitka i przyszywana kuzynka Eustachego  <-- często Fox",
            @"badacz/odkrywca/scrapper. Zmodyfikowana tak, by łatwiej wchodzić w niedostępne miejsca, bardziej zwinność + orientacja w terenie + pewne umiejętności badawcze."
            };

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
