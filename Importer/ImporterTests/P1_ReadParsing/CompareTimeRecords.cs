﻿using Importer.P1_ReadParsing.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace ImporterTests.P1_ReadParsing
{
    public class CompareTimeRecords
    {
        [Test]
        public void Two_identical_TimeRecords_are_the_same()
        {
            // Given
            TimeRecord tr1 = new TimeRecord(5, 7, "00190308", "");
            TimeRecord tr2 = new TimeRecord(5, 7, "00190308", "");

            // When, Then
            Assert.AreEqual(tr1, tr2);
        }

        [TestCase(4, 7, "20190204", "")]
        [TestCase(5, 8, "20190204", "")]
        [TestCase(5, 7, "20190205", "")]
        [TestCase(5, 7, "20190204", "chronology")]
        public void Two_TimeRecords_which_differ_in_any_field_are_different(int p1, int p2, string p3, string p4)
        {
            // Given
            TimeRecord tr1 = new TimeRecord(5, 7, "20190204", "");
            TimeRecord tr2 = new TimeRecord(p1, p2, p3, p4);

            // When, Then
            Assert.AreNotEqual(tr1, tr2);
        }

    }
}
