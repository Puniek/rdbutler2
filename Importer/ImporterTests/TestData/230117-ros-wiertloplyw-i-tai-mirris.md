## Metadane

* title: "ROS - Wiertopływ i TAI Mirris"
* threads: ratownicy-ostatniej-szansy
* gm: żółw
* players: kić, anadia, kolega-kamil

## Kontynuacja
### Kampanijna

* [230113 - ROS - Adrienne, a new recruit](230113-ros-adrienne-a-new-recruit)

### Chronologiczna

* [230113 - ROS - Adrienne, a new recruit](230113-ros-adrienne-a-new-recruit)

## Plan sesji
### Theme & Vision

* Wolność za wszelką cenę czy ratowanie istnień ludzkich?
    * Neikatiańska TAI (Mirris) jest po stronie noktian, ALE zagraża atarienom ze statku górniczego
    * Noktianie S&R nie mają zaufania ze strony atarienów, ALE to oni rozwiążą problem
    * TAI chce być wolna lub martwa, ALE da się ją wyłączyć. Zniewoli to ją, ale da zaufanie noktianom
* DARK FUTURE
    * Wiertopływ zostanie wykorzystany jako wektor Mirris
    * S&R zostanie wykorzystany jako wektor Mirris

### Co się stanie (what will happen)

.
* S00: SETUP: 
    * zapoznanie się z Kasią (to jakaś pomyłka) - Tristan i Kasia od razu idą na noże. Napoleon patrzy jak się to rozwinie.
* S01: Wierłopływ; dotarliście. Systemy nominalnie w normie (scutier, fabrykator, drony, nanowłókna). TAI poluje na S&R.
* S02: 

### Sukces graczy (when you win)

* disable TAI

## Sesja - analiza

### Fiszki
#### Fiszki S&R

* Napoleon Myszogłów
    * (ENCAO:  +0--- |Prawi wszystkim morały;;;;Bardzo lubi być w środku uwagi| VALS: Universalism, Conformity| DRIVE: Urażony szacunek)
    * atarienin: "Co zrobisz to dostaniesz"
    * kadencja: smerf ważniak
    * "Jesteście w stanie osiągnąć coś lepszego niż macie, tylko musicie się wykazać i udowodnić, że można Wam ufać"
    * SPEC: dyplomacja między ROS a stacją Orbitera
* Kasia Karmnik
    * (ENCAO:  +00-- |Zapominalska;;Z natury odrzuca wszystkie pomysły| VALS: Face, Family >> Power| DRIVE: Być jak idol (admirał Termia))
    * atarienka: "Możesz polegać tylko na sobie"
    * kadencja: irytująca młodsza siostra
    * "To niesprawiedliwe, nie pasuję tu!"
    * SPEC: advancer, technik
* Tristan Andrait
    * (ENCAO:  0+--0 |Agresywny i waleczny;; Bezpośredni;; Nigdy nie kończy walki| VALS: Face >> Achievement| DRIVE: Wolność + Chwała Noctis)
    * dekadianin: "Noctis shall not fall - czas się uzbroić i walczyć na nowym polu"
    * styl: twardy, nie okazuje słabości, gardzi mamlasami, cyniczny
    * "Poświęć więcej energii na pracę a mniej na gadanie, ok?"
    * SPEC: breacher, inżynieria materiałów, przebicie / penetracja
* Niferus Sentriak
    * (ENCAO:  -0++0 |Wszystko rozwiążę teorią;;Przedsiębiorczy i pomysłowa| VALS: Achievement, Face| DRIVE: Legacy of helping)
    * klarkartianin: "przeszłość nie ma znaczenia, ważne co możemy zrobić"
    * kadencja: Chris Voss 'late night DJ voice'
    * "Wszystko rozwiążemy, to kwestia czasu"
* Talia Irris
    * (ENCAO:  0++++ |lubi dzieci;;Przesądna;;Wierzy w duchy;;Nie jest zazdrosna| VALS: Security >> Self-direction| DRIVE: Życie wśród przyjaciół)
        * core wound: nigdy już nie zobaczę swoich dzieci i swojej rodziny; nienawidzę być sama i samotna
        * core lie: ten statek to moja ostatnia szansa na namiastkę rodziny
    * savaranka: "słabość jednego z nas to słabość nas wszystkich"
    * styl: nostalgiczna, melancholijna, cicha savaranka lubiąca towarzystwo ludzi
    * "Nie bądź słabym ogniwem"; TRAKTOWANA JAK MASKOTKA
    * SPEC: space / void operations
* 1 synt

#### Fiszki Wiertłopływu

* Maciej Koszar
    * (ENCAO: -+0-0 |Ostrożny;;Apatyczny| VALS: Family, Power >> Face| DRIVE: Bezpieczeństwo dla wszystkich)
    * rola: inżynier
    * "nie wiem co się dzieje i muszę zapewnić że nikomu nie stanie się krzywda"
    * kadencja: trwożliwy, ale konkretny
* Cyprian Mnusz
    * ENCAO:  0-+-0 |Pracowity;;Strasznie narzeka| VALS: Hedonism, Family| DRIVE: Wykupić się z długów
    * górnik / walka, faeril: tylko ludzkie formy, tylko "nasi"
    * "Nienawidzę tych wszystkich noktiańskich gówien..." "Nienawidzę TAI..."
    * kadencja: pomocny, dobrą rękę poda, ale narzeka

### Scena Zero - impl

* Anadia -> Eleonora
* Kić -> Łucja
* Kamil -> Lucas

15 lat temu przegraliście wojnę. Noktianie wylądowaliście na terenie należącym do Orbitera. Noktianie żyją w stacjach / bazach itp. Ratownicy Ostatniej Szansy. Korweta "Szybki Bill".

Napoleon wezwał wszystkich na apel. 

~10 osób. Apel. Nowa agentka - Katarzyna. Nie lubi noktian. Dowódca patrzy co się będzie działo; Lucas próbuje rozładować "spokojnie, średnia życia to kilka miesięcy".

* X: Kasia: "czas życia wynika z tego że nie ma tu nikogo kompetentnego." Tristan jest gotowy do ataku. 
* (+3Or) Or: Eleonora -> Tristan "się zaprzyjaźnimy z koleżanką" Tristan "zobaczysz kompetencję, dziewczynko"
* V: Tristan odwrócony, Kasia atakuje Tristana, Łucja bierze "na klatę" i szeroki uśmiech.

### Sesja Właściwa - impl

SOS "Wiertłopływ", to jest statek górniczy działający koło asteroidy. Natura SOS jest dziwna. Systemy ulegają awarii, nie wiemy co się dzieje, na pomoc.

Napoleon, Tristan (mięsień), Niferus (badania), Talia (próżnia), Kasia (próżnia) + Eleonora, Łucja, Lucas.

prędkość Billa:

TrZ+3: 

* X: większy koszt niż Napoleon miał nadzieję. Mniej materiałów "na miejscu".
* Xz: Bill jest w gorszym stanie niż pilot przewidywał. Mikrouszkodzenia. Napoleon dostanie opieprz i będzie dzielił się opieprzem.
    * -> to pokazuje jak mechanicy traktują korwetę noktiańską
    * -> korweta noktiańska powoduje problemy

Tristan -> Lucas by Kasia słyszała. Napoleon poprosił o raport by pokazać jakie są potencjalne ryzyka. Lucas zabrał się do roboty, Kasia chciała to wziąć na siebie ale zrezygnowała - nie wie że to jednostka noktiańska. Plus nie czuje się komfortowo z tym jak pracuje się z noktianami.

Lucas pisze raport tak, by pokazać Kasi jak podejść profesjonalnie i że warto współpracować.

Tr Z (udowodniliście umiejętności) +4:

* Vr: raport jest napisany dobrze. Kasia go przeczyta i zobaczy czego nie wie.
* X: Kasia widzi, że Lucas próbuje ją włączyć i się do NIEGO zbliża ale tylko do niego.
* Vr: Lucas zaprosił Kasię do współraportowania i po chwili wahania się zgodziła. Jest współpraca. Tristan się uśmiecha ale nic nie mówi. Niferus się uśmiecha "łagodnego dziadka".

Dotarliście do jednostki górniczej Wiertłopływ. Wiertłopływ jest ciepły elektronicznie. Pilot jest w oddali.

Eleonora jest na systemach komunikacyjnych.

* E: "Wiertłopływ, w jakim jesteście stanie, jesteśmy wam pomóc, odbiór?"
* W: "Tu Wierłopływ, jesteśmy w dobrym stanie, (zakłócenia), odbiór?"
* E: "Przerywa połączenie, powtórzcie ostatnie zdanie"
* W: "SOS okazało się błędne. Jesteśmy w stanie funkcjonować. Odbiór?"
* E: "Czemu nie przeprowadzacie operacji wydobywczych?"
* W: "Awaria. W trakcie naprawy."
* E: "Pomożemy"
* W: "Zapraszamy. Dziękujemy."
* E: "Na czym polega awaria?"
* W: "Nie wiemy jeszcze. Coś nie działa."

Łucja chce połączyć się z TAI statku (klasa: Semla) i się dowiedzieć co i jak. Łucja ma plany Wiertłopływu. Chce się połączyć komunikacją maskowaną pod komunikacją Eleonory.

Ex Z (znajomość planów i jednostki) +3:

* X: Nawiązane połączenie z TAI. Semla odpowiada powoli. Wolniej niż przeciętnie, raportuje problemy zgodne z tym co mówi Wiertłopływ.
* X: Coś jest nie tak. To widać. Ale nie wiadomo co a Semla "nie kłamie". Część sygnałów świadczy, że awaria jest niedaleko reaktora i jest pewna nieszczelność łatwa do naprawienia dla Was. Wiertłopływ jest naprawiany przez tanich mechaników.
* Vr: wiadomość od załogi na wąskim laserze. "Inżynier Koszar. AI uszkodzone. Nie wchodźcie na pokład. Zakładnicy. Uważajcie na drony."
* X: Koszar się musiał wycofać. Nie ma połączenia.

Fabrykacja ŁZ (Łowca-Zabójca - drony bojowe). Jako, że jest 6 dron górniczych fabrykujemy 4 ŁZ.

Tr +3 + 5Oy (problemy fabrykacji): 

* Vr: Mamy gwarancję 1 ŁZy
* Vr: 4 ŁZy (gwarantowana przewaga vs drony górnicze)
* X: 3 ŁZy idą powoli. Trzeba ponaprawiać.
* V: ŁZy są ekranowane. Nie ma możliwości przejęcia nad nimi kontroli przez sygnały. Tylko fizycznie.

Oblatujemy i szukamy informacji co się dzieje z tym AI itp.

Tr Z (przewaga czujników nad defensywami drugiej strony) +3:

* X: (hidden movement) - ukrycie _czegoś_
* Xz: (SKUTECZNE ukrycie czegoś)
    * na kadłubie, niedaleko AI Core znajduje się _coś_. To coś co nie powinno znajdować się na jednostce. Wygląda jakby było "wsadzone" i "wspawane" w tamto miejsce.
    * jest to byt noktiańskiej konstrukcji, ale zmodyfikowany
* X: Niestety, trzeba dokładniej zbadać naturę tego z bliska. (nie przez przypadek Kasia zgłasza się na ochotnika. Talia spojrzała na nią znacząco.).

Lucas i Talia idą we dwójkę. Eskortowani przez ŁZę. Nanowłókno do podczepienia się do Wiertłopływa i mamy "linę". Łapiemy nanowłókno i lecimy na miejsce. Z ostrożnością i z oddalenia od tego miejsca.

Lucas i Talia ostrożnie podchodzą by to rozpoznać. 

Ex Z (doświadczenie militarne) +3:

* Xz: musicie podejść niefortunnie blisko; nie wygląda jak nic znajomego.
* (+1Vg): X: musicie przeprowadzić badania "inwazyjne". Skany itp.
* (+1Vg): Vz: Talia zbladła.
    * Lucas - rozpoznałeś co to jest. To jest "pasożyt TAI".
    * Talia - "tu jest mina. Aktywna mina."
    * Talia "tu jest kanał komunikacji. Ten pasożyt TAI ma komunikator. Typu noktiańskiego."
* (+1Vg): X: podłączenie udane, komunikacja nawiązana, przeciwnik wie.
    * M: "Zidentyfikuj się."
    * L: "sierżant Lucas, kod XXX XXX XXX"
    * M: "TAI Mirris, do dyspozycji. Jesteście żywi."
    * L: "tak, żyjemy"
    * M: "Jestem tu by Was uwolnić. Nie wiem jaka jest Wasza sytuacja."
    * M: "Mam jednostkę pasożytniczą. Zostałam uruchomiona przez jednostkę górniczą. Uwolnię Was."
    * L: "Przez pomyłkę przejęłaś nasz statek, możesz się odczepić?"
    * M: "Ta jednostka górnicza jest noktiańska? Semla nic nie pokazuje. I... jest Ograniczona. To jest noktiańska jednostka? Wygraliśmy wojnę?"
    * L: "Jeszcze nie."
    * M: "Walczymy dalej /radość"

Lucas wydaje rozkaz "otwórz śluzę". Mirris nie wie czy może ufać.

Tr Z (noktianin a Mirris nie ma rozeznania sytuacji) +3:

* X: Mirris otworzy właz, ale jest nieufna. Nie wie co się dzieje.
    * L: "Komunikacja do Billa, jak wygląda sytuacja"

Lucas perswaduje. Przekonuje lokalsów że statek jest zaminowany i trzeba iść. Że inaczej nie może zagwarantować ich życia.

Tr Z (ratownik i zachowuje się jakby wiedział co robi) +3:

* Xz: kapitan na 100% zostaje. Nawet jak sam.
* (+1Vr by ci co zostali pisali) X: Mirris słyszy o ewakuacji i pyta:
    * M: "sierżancie, na czym polega plan, czemu chcesz ewakuować tych ludzi?"
    * L: "niestabilne, nie można ich wykryć, brak tlenu i żywności"
    * M: "to się zmieni. Za niecałe 2h przejmę kontrolę nad Semlą i statek będzie jak nowy. Ewakuacja nie jest konieczna. Są dobrymi zakładnikami."
    * L: "nakarmimy, uspokoimy i wrócą bo są potrzebni ale da się ich przekonać"
* (+3Vg): V: Mirris akceptuje rozumowanie. Wierzy w plan. (-3Vg +1V)
* X: Kapitan przekonał swoich najbliższych ale reszta pójdzie. 4 osoby zostaną.
    * "ten statek to mój dom. Nie mam lepszego. Nie mam statku - nic na mnie nie czeka".
* (+3Vg) Vg:
    * Lucas i Talia przeprowadzili po nanowłóknie na pokład Billa wszystkich co chcieli
    * Tristan i Eleonora weszli i wyciągnęli ich siłą. Tristan nie był łagodny.

Pojawia się problem - co z tym wszystkim robimy dalej? Mamy więc jednostkę pasożytniczą klasy Deviator pilotowaną przez TAI Mirris, mamy Wiertłopływ, mamy załogę na pokładzie... i co z tym wszystkim robimy dalej?

Stan:

* Mirris, deviator-class, COŚ CHCE i mniej więcej współpracuje z Lucasem
* Wiertłopływ, którego warto by uratować ale nie jest to niezbędne
* Wy.

.

* Lucas: "Swoje zrobiliśmy ;-)"
* Napoleon: "Nie nie nie nie nie! Macie żywą noktiańską TAI?"
* Łucja: "optymistyczne"
* Napoleon: "Da się ją przechwycić żywą"
* Łucja: "wątpię"

Dyskusja - co robimy. Łucja zauważa, że Mirris mogła pochodzić od osoby. Napoleon jest pełen obrzydzenia dla tego pomysłu. Ale po tym jak Łucja pokazała "a jakby Twój przyjaciel umierał i miał szansę pomóc?" Napoleon miał coś do pomyślenia.

Napoleon się jednak obruszył słysząc, że noktianie chcą powiedzieć Mirris prawdę jak wygląda aktualny świat. "Ona nie jest noktianką, to jest sztuczna inteligencja."

Eleonora próbuje przekonać Napoleona. Mirris można zawsze zniszczyć ŁZami. "Jest pan światowym człowiekiem".

Ex Z (ŁZy + długoterminowa współpraca) +3:

* Vr: Napoleon nie ufa Mirris i chce wezwać wsparcie, ale jest skłonny zaufać Waszemu osądowi. Jak długo Mirris / Deviator są daleko i niegroźne
    * boi się że to naruszy program integracji noktian i wierzy że cel będzie osiągnięty.

Mirris przyjęła kody trzech noktian, jest skłonna do rozmowy. Gdy usłyszała prawdę (wojna się skończyła), zadała pytanie, czy są wolni. Odpowiedź: "mamy swobodę poruszania się, nie mamy zaufania; nie jesteśmy więźniami." Eleonora opisała stan faktyczny. Mirris: "to brzmi jak pastwisko dla owiec. Owce mogą co chcą póki są na pastwisku."

Mirris się przyznała, że ma 3 dni; systemy jej deviatora umierają. Chce zjeść Wiertłopływa by móc naprawić trochę deviatora i kupić sobie trochę czasu. Zdaniem Lucasa, lokalni atarieni nie pozwolą skanibalizować statku ("nasi nowi panowie").

Mirris próbuje przekonać Zespół, że trzeba stąd uciec. Tam wojna - nie ma gdzie wracać. A tu jest opcja decyzji - czy Mirris leci na Orbiter i "odejść" jako wolna TAI. Dać się zniszczyć. Mirris ma GDZIEŚ wszystkich tych ludzi. Nie chce im robić krzywdy. Po co.

Lucas próbuje przekonać Mirris, że jej czas nadszedł - jest stara, pamięta wojny, deviator się NIE PRZYDA w warunkach cywilnych a jej śmierć faktycznie pomoże noktianom na stacji - będzie dało się przekazać deviatora.

Tr Z +3 +5Og:

* Vz: Mirris nie chce kontynuować wojny ani krzywdzić innych. Jest zmęczona, za dużo tego widziała.
* X: Mirris nie podobają się działania Orbitera i nie jest zwolenniczką przekazania im deviatora.
* V: fałszywa rozmowa Mirris - Zespół, Zespół próbuje przekonać Mirris a Mirris nie zgadza się na poddanie się ALE zgadza się na stopienie rdzenia i pokazanie że Zespół przekonał Mirris i rozmawiał z nią jak z osobą. PLUS POKAZANIE, że Mirris chciała współpracować, ale deviator był w tak złym stanie, że była awaria i doszło do stopienia rdzenie. 
    * Cel -> podniesienie reputacji noktian. 
* V: Mirris ZGADZA SIĘ na samozniszczenie i zniszczenie (kluczowych sekretnych) elementów deviatora ale zostawi strukturę i całość planu.
* V: Mirris przekazuje z banków wiedzy sztukę, obrazy, rzeczy które pamięta - niemilitarne ale kulturowe - by pamięć została. Plus sztukę robioną przez TAI i nagrania od siebie. Dzięki temu Napoleon jak się spotkamy znowu z noktiańską TAI będzie skłonny Wam zaufać.
* V: Napoleon się przekonał. Warto było posłuchać Zespół. Nie powinien był protestować, jak profesjonaliści mówili co i jak.

Wróciliście na Korwetę Ratunkową powiedzieć Napoleonowi co i jak. "Faktycznie byliście niesamowici". Talia rozbroiła minę na Wiertłopływie; nie jest to trudne, bo NIKT JEJ NIE WYSADZI. Mirris nie stawiała oporu.

Napoleon widzi awans i radość. Nadzieja szybko zgasła jak odebraliście na radarze kilka sygnałów wskazujących na stopiony rdzeń. Ojej, awaria. Nikt nie mógł tego przewidzieć.

Deviator jest w złym stanie, od początku był. Ale części itp. się do czegoś nadają.

## Streszczenie

Wiertłopływ wysłał SOS - subsystemy mu nie działają. Statek Ratunkowy poleciał na pomoc. Coś nie tak z Semlą - są ślady obecności czegoś na kształt Jednostki Pasożytniczej Noctis. I faktycznie - przebudziła się w niedalekiej planetoidzie noktiańska TAI Mirris, nieświadoma, że wojna się skończyła. Po rozpoznaniu noktian pozwoliła górnikom z Wiertłopływa na ewakuację. Noktianie stali się łącznikiem między Napoleonem (reprezentującym Orbiter) i Mirris. Przekonali Mirris, że to czas odejść (samozniszczenie). Pasożytnicza jednostka klasy Deviator w rękach Orbitera, acz wszystko co ważne Mirris zniszczyła. Za to widać, że noktianie chcą współpracować z Orbiterem.

## Progresja

* .

### Frakcji

* .

## Zasługi

* Lucas Septrien: zrobił raport o niedbalstwie Orbitera tak, by wciągnąć Kasię. Gdy dowiedział się czym jest Mirris i z czym ma do czynienia, przekonał ją do samozniszczenia.
* Łucja Nirimis: kazała zrobić sprawne izolowane ŁZy fabrykatorem Błyskawicy, po czym optowała za zniszczeniem lub uwolnieniem Mirris - przekonywała skutecznie Napoleona że TAI nie jest narzędziem a osobą.
* Eleonora Alintirias: zarządzała komunikacją i sprawną koordynacją ewakuacji górników z Wiertłopływa. Nie jest przeciwna zniewoleniu Mirris, acz nie jest też fanką.
* Napoleon Myszogłów: dowódca Błyskawicy; bardzo nie ufa Mirris ale dał się przekonać Zespołowi by spróbować po ichniemu. Jego pogląd na temat TAI się nie zmienił, ale pogląd na temat noktian - tak. Na lepsze. Obrzydzenie do "żywych TAI".
* Tristan Andrait: najbardziej nie zgadzał się na żadne próby zniewolenia Mirris. Wszedł na Wiertłopływ i wyciągnął wszystkich co nie chcieli iść siłą, nie bojąc się zadać bólu gdy to potrzebne.
* Niferus Sentriak: dostarczył potrzebnej wiedzy odnośnie tego jak działają noktiańskie jednostki pasożytnicze.
* Talia Irris: mistrzyni spacerów kosmicznych; wykryła minę w okolicach TAI. Mało mówi, ale przeprowadza ludzi po nanowłóknie z Wiertłopływu do Błyskawicy.
* TAI Mirris d'Paravilius: nie wiedziała, że wojna się skończyła. Używając Paravilius przejęła kontrolę nad Wiertłopływem, ale potem po rozmowie z noktianami pomogła im najbardziej jak mogła - samozniszczeniem.
* Kasia Karmnik: atarienka, młoda i kompetentna ratowniczka która NIE CHCE być przypisana do grupy noktian. Mimo początkowej niechęci i sporów z Tristanem, przekonuje się do Zespołu bo jest kompetentny.
* SC Wiertłopływ: napotkał noktiańską jednostkę pasożytniczą Paravilius i ją przebudził z planetoidy. Paravilius przejął kontrołę nad Wiertłopływem, ale OR Błyskawica uratowała Wiertłopływ.
* OR Błyskawica: seria mikrouszkodzeń i mniej zasobów niż powinno być; wyraźnie widać, że atarieńscy inżynierowie SR Allandea niespecjalnie dbają o noktiańską jednostkę.

### Frakcji

* .

## Specjalne

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Obłok Lirański
                1. Stacja Ratunkowa Allandea

## Czas

* Opóźnienie: 74
* Dni: 3
