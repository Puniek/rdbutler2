## Metadane

* title: "Lekarz dla Elizy"
* threads: arcymag-krysztalow
* gm: żółw
* players: kić, fox

## Kontynuacja
### Kampanijna

* [200930 - Lekarz dla Elizy](200930-lekarz-dla-elizy)

### Chronologiczna

* [200930 - Lekarz dla Elizy](200930-lekarz-dla-elizy)

## Punkt zerowy

### Dark Past

Inwazja Noctis się nie udała, ale Saitaer nie żyje. Eliza wyciągnięta przez Karminę i Wanessę, uciekły ze sprzętem do jaskini w Kryształowej Pustyni.

### Opis sytuacji

Grupka noktian nie ma zielonego pojęcia na temat nowej rzeczywistości. Kryształowa Pustynia jest Skażona - jeszcze nie nazywa się w ten sposób. Noktianie są rozbici, Astorianie na nich polują. Last stand? Plus, teren jest Skażony a Noktianie nie mają pojęcia co to znaczy.

### Postacie

* Eliza Ira: jeszcze nie arcymag kryształów. Budzą się w niej moce, których Eliza nie rozumie i których się boi. Żałuje wszystkich swoich decyzji.
* Karmina Alarel: (Fox): elitarna gwardzistka i przyjaciółka Elizy. Osoby przez nią osłaniane zginęły w kosmosie; pomaga Elizie.
* Wanessa Pyszcz: (Kić): zwiadowca noktiański. Ma nadzieję, że da się wyprowadzić ten świat na coś sprawnego mimo tragedii.

## Punkt zero

brak

## Misja właściwa

Eliza Ira, naukowiec i mistrzyni kryształów Noktiańskiej Akademii Nauk umiera. A dokładniej - pokonuje ją jej własna energia magiczna i poczucie winy. Eliza nie spodziewała się, że gdy Astoria wygra to noktianie będą ścigani. Nie spodziewała się prawdziwych horrorów Saitaera. Nie spodziewała się co się stanie. Jej ciało jest na wpół sparaliżowane. Jest uratowana przez Karminę i Wanessę. Leży w jaskini a moc magiczna się przez nią przelewa - a Eliza nie ma pojęcia co to znaczy i co się dzieje.

Karmina nie pozwoli Elizie umrzeć. Daje jej polecenie - tu są noktianie. Nadal. Ona _jakoś_ potrafi zrobić coś z tymi kryształami. Potrafi sprawić, by kryształy zaczęły działać. Eliza jest w stanie zbudować dla noktian bezpieczną bazę. Tylko ona. Karminie nie pozwala Elizie wpaść w depresję i uciec w chorobę. Potrzebuje jej aktywnej - dla niej samej. A Eliza wątpi w swoje przeszłe decyzje i w ten świat który widzi dookoła.

* O: silna zależność emocjonalna od Karminy i Wanessy. Robi to DLA NICH.
* X: nienawiść do siebie. Eliza chce przestać istnieć. Nie powinna istnieć po tym co zrobiła, jak wszystkich rozczarowała.
* V: część nienawiści Eliza przekierowuje w pomoc noktianom. Ona ich w to wpakowała, ona jest im winna ratunek.
* V: Eliza wierzy w Wanessę i Karminę - "the best of us". Na pewno sobie poradzą. Razem odbudują w tym chorym miejscu enklawę noktiańską.

Nie jest to jednak ani łatwe ani bezpieczne by przedostać się do Ortus-Conticium z tego terenu (kiedyś będzie nazwany Kryształową Pustynią). Niechętnie, ale Karmina poprosiła Elizę o zamaskowanie ich ścigacza. Eliza nie wie jak to zrobić. Karmina wyjaśniła, że Eliza potrafi robić rzeczy z kryształami. Jest mistrzynią kryształów. Poradzi sobie z tym. I faktycznie, Eliza po raz pierwszy użyła swojej mocy by zrobić COŚ z kryształami świadomie. (VVV) - ścigacz jest niewykrywalny. Wanessa znalazła drogę, najlepszą i najbezpieczniejszą (VXXX:potwór) i Karmina z Wanessą ruszyły na misję: porwijmy astoriańskiego lekarza który pomoże Elizie. Zostawiły Elizę z misją - ma zbudować dla noktian miejsce i bezpieczeństwo.

Jednak po drodze ścigacz noktiański napadł potwór. Amalgamat kamiennego potwora ze straszliwą paszczą, dwoma silnikami z jakiegoś statku kosmicznego i niemałą ilością macek zrobionych z drutów na grzbiecie. Jakaś forma elementalnego potwora Skażonego mocą Saitaera. I to cholerstwo jest szybsze niż ścigacz; szczęśliwie, Karmina jest naprawdę dobrym pilotem. Wanessa wypatrzyła wąwóz gdzie powinno być bezpiecznie; Karmina tam skierowała pojazd (VXXV) - potwór zaplątał się w wąwozie, ale jest tam też niewielki oddział, 4 noktian, którzy schronili się przed zagrożeniem. I Karmina właśnie sprowadziła im na łeb elementalnego potwora!

Nie może tak być. Nie zostawią pobratymców noktian na pastwę potwora. Karmina wyrzuca Wanessę ze ścigacza i jedzie pod potwora, łapiąc jedną z macek i ciągnąc ją w dół by potwór musiał otworzyć pysk (VXV). Wanessa rzuca granatem ppanc prosto w otwartą paszczę. Co prawda potwór dał radę złapać jednego noktianina i odgryźć mu rękę, ale ten ostry manewr Karminy i Wanessy skutecznie unieszkodliwiły potwora. Gdy noktianie doszli do siebie, Karmina przejęła dowodzenie i wysłała ich do Elizy Iry. Tam mają nadzieję przetrwać a nie jest jeszcze super daleko.

Noktianie powiedzieli Zespołowi o poruczniku Szymonie Szynku, który najpewniej ma deathwish - ale nie zamierza przestać walczyć i zebrał kilkudziesięciu żołnierzy noktiańskich. Zespół zdecydował się odbić z trasy na Ortus-Conticium; ważniejsze jest zobaczenie jak radzą sobie pobratymcy z Noctis. W najgorszym wypadku - zrobi się z nich dywersję. W najlepszym - mają silniejszy oddział którym można zrobić dużo więcej.

Droga do Szynka była ciężka, ale udało im się dostać tam bardzo szybko (VVV). Zakłócenia krystaliczne, wzrosty krystaliczne pochodzące z magii Elizy na ścigaczu sprawiły, że powietrzne patrole astoriańskie uznały ścigacz nie za pojazd dla niebezpiecznych noktianek a za anomalię, potwora powstałego z fuzji mocy Saitaera i pojazdu noktiańskiego. Tak więc droga do Szynka była stosunkowo bezproblemowa.

Na miejscu Szynek powitał je w miarę ostrożnie. Po krótkiej rozmowie Zespół zrozumiał porucznika Szynka - jest na szczycie chain-of-command, nie ma do kogo się odwołać i nie rozumie świata i tego co tu się dzieje. Przed chwilą rzeka pożarła dwóch jego ludzi którzy nabierali wodę. Wszystko jest Skażone, złe, wrogie i niebezpieczne. Udało mu się uratować 32 osoby i 16 rannych, z których części nie jest w stanie ruszyć. Mają też sprzęt.

Ze środka jaskini gdzie są zakamuflowane siły Szynka dobiegł ich cichy krzyk. Wanessa poleciała pierwsza (VV) - półmechaniczny eks-noktianin, z ranami zasklepionymi przez kable który zaraża kolejną osobę tą samą przypadłością. Wanessa zrobiła Execute i odkryła, że to jest zaraźliwe i ostrzegła kogo się dało (V), ale nie zdążyła zatrzymać infekcji (XX) - zarażeni są wśród zdrowych. Karmina, Szynek i Wanessa spróbowali zebrać oddział i jakoś się utrzymać (VVOOOOXX) - niestety, bardzo szybko większość sił została zarażona. Szynek wydał rozkaz wycofania się; Karmina zaczęła strzelać do osób które były niepewne. Wanessa strzeliła czerwoną flarą w powietrze - niech astorianie przyjadą ich uratować (lub zabić).

W końcu udało się wycofać 6 osób włącznie z Szynkiem oraz 4 pojazdy. Obserwowali, jak astoriańskie pojazdy zestrzeliwują to, co kiedyś było tymczasową bazą Noctis. Wanessa naszkicowała uratowanym drogę do Elizy Iry; docelowo dotrą tam 4 osoby z Szynkiem i 3 pojazdy (VX)...

A Zespół ruszył do Ortus-Conticium. MUSZĄ zdobyć tego lekarza - nie mają pojęcia co tu się dzieje i nie mają jak się tego dowiedzieć bez tego...
Jeszcze jedna rzecz im się rzuciła w oczy - niezależnie od okoliczności, _dookoła Elizy Iry jest bezpiecznie_. Cokolwiek ona robi, te dziwne energie nie dotykają terenu dookoła niej.

Ścigacz Karminy i Wanessy dotarł do bardzo niebezpiecznej rzeki i nie wiadomo jak się przedostać. Tym razem kolej Wanessy na użycie magii - znalazła przejście do Skażonego miasta (ExM -> VVX). Dzięki temu mają bezpieczną drogę której nie znają astorianie, też powrotną. Jako, że anomalie też nie postrzegają ścigacza w 100% jako byt obcy dzięki kryształom Elizy, było tyci łatwiej. Karmina jednak musiała bardzo ciężko manewrować i kontrolować ścigacz w Skażonym Mieście (ExM) - jakkolwiek jej się udało, zauważyła, że ścigacz zaczął ożywać i budzić się do życia. Zaczął stawać się prawdziwą, anomaliczną istotą. To zmartwiło Wanessę i Karminę, ale nie mają wyboru - grasz tym co masz...

**Ortus-Conticium jest niewłaściwie żywym miastem**. Po śmierci Saitaera (właśnie w tym mieście) i po tym, jak w niego wbił się ciężki krążownik, miasto stanęło w fali anomalicznej i w płomieniach. Po Ortus-Conticium krążą patrole astoriańskie, 10 osób sztuka, w ciężkich servarach i z lekarzami - by ratować ludzi lub ich eksterminować. By ewakuować miasto. Na południu od Ortus-Conticium znajduje się Mobile Fortress klasy 'Imperatrix'. Tak więc - nie ma możliwości podjechania od południa, od strony Zjednoczenia Letyckiego.

Jako, że Imperatrix stanowi jedyne źródło lekarzy i broni astoriańskiej w okolicy, Zespół zdecydował się poczekać do rana. Wtedy jest największa szansa na osiągnięcie celu - wyizolowanie pojedynczego oddziału z którego da się porwać lekarza. Zespół nie był w stanie wytrzymać siły energii magicznej w okolicy; Karmina i Wanessa zapadły w sen.

Sen miały niesamowicie przyjemny - są w Noctis, w domu. Jak się obudziły - okazało się, że są JAK W NOCTIS. Jeden z budynków zaadaptował do nich. Ścigacz patrzy na nie, jest zmodyfikowany (dopakowany technologicznie). Co jest szczególnie _creepy_, są tu pamiątki, wspomnienia, echa noktian. Tych, którzy zginęli w krążowniku i na planecie. Wanessa poszukała i znalazła stealth suit - pancerz należący kiedyś do jej sierżanta. Przeszedł ją dreszcz, ale go założyła.

Ścigacz i stealth suit są silnie anomaliczne...

Karmina dojechała z Wanessą w odpowiednie miejsce. Wanessa wyszła ze ścigacza i poszła na łowy - znaleźć odpowiedniego lekarza. (V): udało jej się znaleźć jeden z oddziałów, nieco bardziej wyizolowany. (V): ściągnęła więc serię stworów, by te zbliżyły się do astoriańskiego oddziału i zmusiły ich do wejścia do jednego z budynków i przejścia w tryb oblężenia. O: stealth suit jest jak druga skóra dla Wanessy. Zanim astoriańskie posiłki zdążyły się pojawić (V) Wanessa wystawiła ich na atak; wślizgnęła się do budynku. (XXXO): niestety, nie udało jej się czysto porwać maga; została ciężko ranna łapiąc go i wyskakując z nim przez okno. Ściągnęła potworów dodatkowo i Skaziła magicznie budynek; obrócił się przeciw astorianom. Nikt nie przeżył - tylko Wanessa i lekarz...

...którzy wpadli prosto do ścigacza Karminy. (VV): skutecznie ich złapała, przechwyciła i uciekła z Ortus-Conticium zanim ktokolwiek się zorientował. Zero śladów, zero problemów, acz (OO): ścigacz zaczął anomalizować; Karmina z trudem doprowadziła go do Elizy i do bazy, z trudem kontrolowała jego coraz bardziej koszmarną naturę; tam na miejscu zostanie zestrzelony. Niestety (XXX): wszyscy astorianie mają hipernet, o czym nie wiedzą agenci Noktis. Lekarz nagle pojawił się w bazie Elizy w hipernecie, więc Astoria zna lokalizację nędznej bazy Elizy - i Astoria wie, że noktianie zniszczyli oddział tego lekarza...

## Streszczenie

Eliza Ira umiera w jaskini; jej nowe moce ją zabijają. Karmina i Wanessa przemierzają Skażone przez zniszczenie fizycznej formy Saitaera tereny prowadzące do Ortus-Conticium by porwać astoriańskiego medyka by jej pomógł. Udaje im się to, po drodze ratując noktiańskie siły porucznika Szynka (6 z 50 osób). Niestety, uległy silnemu Skażeniu w Ortus i zniszczyły astoriański oddział ewakuacyjny; przez to dostarczyły do astoriańskiego dowodzenia informacje, gdzie znajduje się baza Elizy.

## Progresja

* Eliza Ira: dookoła niej jest bezpiecznie; anomalie się nie materializują. Jest neutralizatorem chorych energii.
* Eliza Ira: silna zależność emocjonalna od Karminy i Wanessy. Nienawiść do siebie. Oraz - pragnienie ochrony noktian i zbudowanie dla nich bezpiecznego miejsca.

### Frakcji

* .

## Zasługi

* Wanessa Pyszcz: zwiadowca Noctis; manewrowała w skrajnie Skażonym Ortus-Conticium i porwała lekarza ściągając na astoriański oddział potwory. Ciężko Skażona.
* Karmina Alarel: elitarna gwardzistka Noctis, pilot i przyjaciółka Elizy; doskonale pilotuje ścigacz, pokonała elementalnego potwora Skażonego ixionem i opanowała Elizę.
* Eliza Ira: jeszcze nie arcymagini. Na wpół sparaliżowana, niweluje Skażenie i wątpi w swoje decyzje o nie niszczeniu Astorii. Pierwszy raz użyła magii by zamaskować ścigacz kryształami.
* Szymon Szynek: porucznik Noctis; zebrał grupę straceńców z Noctis i próbował ich osłonić przed nowym światem. Prawie stracił wszystkich, ale kilka osób dotarło do Elizy Iry.
* Autofort Imperatrix: pozycjonowana na południu Ortus-Conticium, źródło sił medycznych i działań mających uratować populację cywilną Ortus-Conticium.

## Plany

* .

### Frakcji

* .

## Lokalizacje

1. Świat
    1. Primus
        1. Sektor Astoriański
            1. Astoria
                1. Sojusz Letejski
                    1. Przelotyk
                        1. Ruiniec
                            1. Ortus-Conticium: miejsce śmierci Saitaera i upadku krążownika noktiańskiego; swarmowane przez oddziały astoriańskie próbujące ratować kogo się da. Hiperskażone.
                            1. Ortus-Conticium, okolice: Hiperskażone miejsce; z licznymi potworami anomalnymi. Patrolowane z powietrza przez siły astoriańskie. W południowej części znajduje się Superforteca Imperatrix.
                                1. Rzeka Tirellia: niesamowicie niebezpieczne wschodnie odnóże; Wanessa znalazła przez anomaliczny most drogę do Ortus-Conticium.
                            1. Pustynia Kryształowa: jeszcze nie jest tak nazwana; chwilowo, pustkowie silnie anomaliczne przez falę po śmierci Saitaera. Liczne elementalne i ixiońskie formy.
                                1. Kryształowa Forteca: jeszcze nie jest tak nazwana; niewielka baza gdzie Eliza Ira umiera. Miejsce odporne na anomalie.

## Czas

* Chronologia: Inwazja Noctis
* Opóźnienie: 91
* Dni: 4

## Inne

.
