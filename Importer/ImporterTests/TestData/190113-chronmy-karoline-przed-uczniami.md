## Metadane

* title:  "Chrońmy Karolinę przed uczniami"
* threads: nemesis-pieknotki
* gm: żółw
* players: kić

## Kontynuacja
### Kampanijna

* [190112 - Eksperymenty na wiłach](190112-eksperymenty-na-wilach)

### Chronologiczna

* [190112 - Eksperymenty na wiłach](190112-eksperymenty-na-wilach)

## Projektowanie sesji

### Pytania sesji

* Czy Pięknotka dotrze do źródła problemu - dziwne specyfiki od Adeli?
* Czy Napoleon Bankierz dostanie wpiernicz od dyrektora za użycie niewłaściwych narkotyków? (chroni Ignacego i Adelę)

### Struktura sesji: Frakcje

* Napoleon: PROTECTOR: bezpieczeństwo Karoliny, bezpieczeństwo Adeli, bezpieczeństwo swoje, znalezienie reszty środków, neutralizacja środków
* Adela: SCIENTIST: bezpieczeństwo swoje, antidotum
* Skażony Perfum: PLAGUE: zainfekować kolejną osobę, wprowadzić w berserk, skrzywdzić maga zła
* Arnulf: PROTECTOR: śledzić Pięknotkę, utrudnić śledztwo formalnie

### Hasła i uruchomienie

* Ż: (kontekst otoczenia) -
* Ż: (element niepasujący) -
* Ż: (przeszłość, kontekst) -

### Dark Future

1. ...

## Potencjalne pytania historyczne

* brak

## Punkt zerowy

### Postacie

### Opis sytuacji

.

## Misja właściwa

**Scena**: (20:45)

_Zaczęstwo, Szkoła magów_

Arnulf Poważny, bardzo poważny dyrektor, stoi przy Pięknotce i mówi jej, że jej obecność nie jest potrzebna. Może wracać do domu. Pięknotka by chciała, ale Karla jej nie pozwoliła. Karla wysłała Pięknotkę do szkoły magów, bo podobno doszło do poważnego wydarzenia - kandydat na terminusa poważnie zdemolował salę używając potężnych energii magicznych. No i ktoś to zgłosił. Potrzebne jest oficjalne pustogorskie śledztwo. Pięknotka WIE, że Karla po prostu chce ją oddalić od Saitaera i spraw z tym związanych na razie...

To, co Pięknotce rzuciło się w oczy - dewastatorem jest Napoleon Bankierz. Mag, który był ostatnio zafascynowany Ateną. Nie jest to typ który coś by zdemolował normalnie. Coś tu nie pasuje. A Arnulf próbuje Pięknotki się pozbyć. Pięknotka powiedziała, że nie może zostawić tej sprawy. Arnulf jej powiedział wyraźnie - to nie jest istotna sprawa. Czy podpadła komuś? Arnulf powiedział, że zależy mu na tym, żeby Pięknotka to zostawiła - narobi kłopotów dobremu magowi który chce zostać w przyszłości terminusem. Jest tam coś więcej, niż Arnulf chce powiedzieć. Zwłaszcza, że Pięknotka widzi różne wykresy u Arnulfa.

(Tp:6,3,4=S). Arnulf zdecydował się powiedzieć całą prawdę którą zna.

* Jest taka graczka w Supreme Missionforce, Karolina Erenit. Pięknotka facepalmowała. Karolina nie jest magiem, jest człowiekiem - ale jest wspomagana magią. I jest piękna. Ale ma chłopaka, też ludzkiego.
* Napoleon się w niej zabujał. Serio. Zrobił coś bardzo głupiego - użył na niej jakiejś mikstury czy eliksiru. Spowodowało to efekt uboczny i on wpadł w berserk. Ujawniło się to po czasie. Wtedy zdemolował salę.
* Arnulf szuka, kto skontaktował się z terminusami. Sprawy szkolne powinny zostać w szkole. Tyle wie. Nie wie, skąd Napoleon miał tą miksturę itp.
* W tej chwili Napoleon jest w odosobnieniu. Został przebadany i wyleczony.

Pięknotka sprawdziła na szybko kto wysłał anonimową informację do terminusów pustogorskich. Anonimowa my ass - ale nadawca z pewnością to wie. To niejaka Liliana Bankierz, szkoląca się na medyka (acz zupełnie, zdaniem Arnulfa, nie mająca do tego talentu). O dziwo, Liliana i Napoleon dobrze ze sobą żyją na co dzień.

Pięknotka potrzebuje jakiejś formy osłony, by nie było to super potężne dochodzenie terminuskie. Więc - dostarcza usługi związane z urodą. I zaczyna zamotać Arnulfa - chodzi o to, by nie mógł określić co ona robi i do czego dąży (Tp+2:11,3,3=SS). Fakt, on się nie zorientuje. Ale **wszyscy** uczniowie rozpoznają terminuskę. Przez co Arnulf traci w oczach uczniów jako naiwniak.

AKCJA: +zainfekować kolejną osobę (Lilianę)

Pięknotka załatwiła, by Liliana dostała kupon. Liliana pierwsza skorzystała z okazji. Pięknotka jak zobaczyła Lilianę to zrobiła TAKIE oczy - zobaczyła Diakonkę. Ale Liliana jest z krwi Bankierz. Jest nienaturalnie piękna. Jest piękniejsza niż Pięknotka była przed ewolucją! Pięknotka powiedziała jawnie, że jest zaskoczona. Liliana przyznała, że kiedyś była Diakonką - ale niestety krew Diakon nie była z nią kompatybilna, więc została przekształcona ponownie. Zawsze chciała być Diakonem, ale jest Bankierzem... Pięknotka ją przytuliła ze współczuciem. Liliana jest gorąca. Ma temperaturę? Ale nie widać... maskuje się iluzją.

(Tp:9,3,3=SS). Liliana zdjęła welon iluzji i Pięknotka widzi, że nadal jest piękna - ale już nie tak. SS: The corruption took hold - choroba ją też zmieniła. Pięknotka widzi, że Liliana jest naenergetyzowana energią magiczną; ta energia ją aż rozpiera. Liliana skutecznie maskowała i swój wygląd i tą energię. Pięknotka czuje niezdrową energię; niewłaściwy kolor. Pięknotka pod przykrywką zabiegów kosmetycznych zaczęła obniżać jej temperaturę i spytała czemu Liliana zgłosiła Napoleona do Pustogoru. Liliana powiedziała, że zbrodnię należy ukarać.

* Napoleon jest niewinny. On jest ofiarą, ale nie napastnikiem
* Ona też jest dziewczyną i rozumie jak to jest - więc chciała by osoby za "to" odpowiedzialne zapłaciły
* Liliana ma nadzieję że przyjdzie terminus i zrobi dobrą robotę

(Tp:9,3,3=S). Pięknotka zobaczyła, że Liliana straciła kontrolę nad swoją mocą magiczną. Zna to - to konkretne toksyny ze Zjawosztup. Nabawić się można ich na Trzęsawisku, oczywiście, albo je kupić na przykład. To, co ma Liliana jest wariacją na temat. Łatwe do uleczenia, ale daje ostrą paranoję oraz niezwykłe zwiększenie mocy magicznej, niekontrolowane. Chwilę potem Liliana wystrzeliła _force nova_ o ogromnej sile - zupełnie tak jak słyszała Pięknotka o Napoleonie (TrZ+2:12,3,4=SS). W wirze dewastacji i potężnych energii Pięknotka ogłuszyła Lilianę...

Pomieszczenie jest doszczętnie zniszczone i Skażone. Zaraz wpadła katalistka - Teresa i zatrzymała się jak wryta. Pięknotka. Tutaj. Z nieprzytomną, niezbyt ubraną (zniszczenia) Lilianą. Wezwanie lekarza i inne takie...

_Zaczęstwo, Szkoła magów, godzina później_ (21:42)

Pięknotka u Arnulfa "na dywaniku". Arnulf, zdenerwowany, spytał co tu się stało. Pięknotka powiedziała całą prawdę poza tym, że Liliana wezwała Pustogor. Więc nie powiedziała też tego, co Liliana jej powiedziała. Arnulf się zdziwił pochodzeniem toksyny - żaden z uczniów nie był na Trzęsawisku. Tak uważa, ale jest tego całkiem pewny, nie z ASD "Centurionem" ostrzeliwującym Trzęsawisko z orbity. "ASD Centurion" wypala ołtarz Saitaera jak tylko ten się odradza - wykorzystywany jest jako 'target practice'.

Napoleon był zbadany toksykologicznie, ale nic nie znaleziono. Po chwili do Pięknotki doszła nowa wiadomość - z Liliany zniknęła toksyna. Po jednym wybuchu wszystko jest w porządku. I zdaniem Pięknotki to zupełnie nie jest to jak to normalnie działa. To jest coś nowego, ktoś zmienił sposób działania tej toksyny. Normalny sposób podania toksyny to przez skórę.

Czas porozmawiać z Napoleonem...

_Zaczęstwo, Szkoła magów, izolatka_ (21:48)

Napoleon siedzi, zgnębiony. Spojrzał na Pięknotkę i się zdziwił. Poznał Pięknotkę - jest znajomą KOMENDANT Sowińskiej. Prawie jest jej adiutantką. Pięknotka się aż uśmiechnęła. Spytała Napoleona co się stało. Napoleon powiedział, że nie wie o co chodzi. Przyciśnięty, powiedział - Atena Sowińska nauczyła go pojęć takich jak lojalność i honor. Nie narazi nikogo innego na problemy. Pięknotka aż dostała dreszczy. On nie ma pojęcia co Atena by zrobiła...

Pięknotka zauważyła, że powinien nauczyć się czegoś innego od Ateny - szacunku dla prawa i konieczności ochrony a nie głupiego ukrywania faktów. Wysłała prośbę do Ateny - niech ona rozwiąże dla Pięknotki ten problem (1Z). Atena skomunikowała się z Napoleonem i w krótkich, żołnierskich słowach wyjaśniła co należy zrobić. Napoleon wszystko powiedział Pięknotce.

* Jest taka piękna dziewczyna, Karolina Erenit. Gra w Supreme Missionforce. (to Pięknotka już wie)
* Niektórzy magowie, uczniowie tej szkoły, stwierdzili, że przecież jest człowiekiem. I to Skażona magicznie. Można spokojnie rzucać na nią zaklęcia.
* On próbował ją chronić. Nie wie kim są ci uczniowie, więc poprosił znaną sobie biomantkę o eliksir, który da Karolinie opcje retaliacji. By Napoleon wiedział kto to.
* Z niewiadomych przyczyn, eliksir zadziałał nie tak jak powinien. Chyba. Bo przecież on został czymś zarażony. Nie wiedział o Lilianie - ale ona nawet z Karoliną nie rozmawiała.
* Źródłem eliksiru jest Adela Kirys. Była jedna porcja, dla Karoliny - i eliksir miał trwać tydzień.

Pięknotce to nie pasuje. Adela może jest średnia w biznesie, ale alchemikiem jest świetnym. To nie powinno tak się stać. Po chwili dopytała Napoleona co powiedział Adeli - powiedział jej, że to jest dla **człowieka**. I wszystko jasne - Adela może być najlepszą biomantką, ale jeśli myśli że tworzy coś dla człowieka a tworzy coś dla Skażonego magią człowieka, prawie viciniusa... to w tym momencie sprawa może się poważnie zmienić. I najpewniej to się stało...

Pięknotka przekazała temat dyrektorowi, z ciężkim facepalmem... po czym razem napisali taki raport, by nikt nie ucierpiał...

Wpływ:

* Ż: 7: 
* K: 3: 

Skuteczne Akcje: wirus1, wirus2.

**Epilog** (22:05)

* Arnulf i Szkoła Magów tak załatwili sprawę, by nikt nie miał problemów związanych z głupim Skażonym Eliksirem Adeli.
* Karla była relatywnie zadowolona. Pięknotka zrobiła jakieś nudne, niezbyt potrzebne zadanie z dala od Saitaera.
* Pięknotka była zadowolona - mogła komuś pomóc.
* Arnulf i Pięknotka zacieśnili znajomość. Arnulf traktuje Pięknotkę jako swoją terminuskę go-to.

### Wpływ na świat

| Kto           | Wolnych | Sumarycznie |
|---------------|---------|-------------|
| Żółw          |    7    |     7       |
| Kić           |    3    |     5       |

Czyli:

* (K): Adela traci trochę pewności siebie - widzi, że mogła zostać pozwana za to co zrobiła sama bez sprawdzenia osobiście (2)
* (Ż): .

## Streszczenie

Karla chciała pozbyć się Pięknotki jeszcze raz. Wysłała na papierkową robotę do Szkoły Magów w Zaczęstwie. Tam okazało się, że kandydat na terminusa (Napoleon) próbował uratować człowieka, Karolinę Erenit przed innymi uczniami szkoły magów. Niestety, eliksir który zamówił u Adeli miał efekty uboczne. Pięknotka gładko to rozwiązała i nikt nie miał problemów. Poza tym, że Karolina nadal nie jest chroniona.

## Progresja

* Pięknotka Diakon: Arnulf Poważny jej ufa i będzie z nią współpracował zanim wybierze innego terminusa.
* Arnulf Poważny: będzie współpracował z Pięknotką jako terminuską zanim z jakimkolwiek innym terminusem.
* Arnulf Poważny: stracił trochę szacunku w oczach innych uczniów; myślą, że nie rozpoznał Pięknotki jako terminuski. Cóż.
* Adela Kirys: traci trochę pewności siebie - widzi, że mogła zostać pozwana za to co zrobiła sama z tym eliksirem dla Karoliny Erenit (bez sprawdzenia osobiście)
* Liliana Bankierz: dostała całkowicie nieuzasadnioną opinię osoby zdolnej do walki z terminusem w power suicie bez niczego (przez erupcję energii w Pięknotkę).

### Frakcji

* Testowa Frakcja: czy ten rekord zadziała?

## Zasługi

* Pięknotka Diakon: poszła do Szkoły Magów zająć się papierkową robotą, skończyła rozwiązując małą Plagę sprowadzoną przypadkowo przez Adelę - przy okazji poznała kilku uczniów.
* Arnulf Poważny: dyrektor skłonny do osłony swoich uczniów nawet, jeśli coś zbroili. Będzie współpracował z Pięknotką, bo udowodniła, że zależy jej na dobru a nie prawie.
* Napoleon Bankierz: chciał chronić Karolinę Erenit przed innymi magami, zdobył eliksir od Adeli ale źle wyspecyfikował. Zaraził się, zdemolował pokój i - na szczęście - dyrektor wszystko wyciszył.
* Liliana Bankierz: chciała pomóc Karolinie tak jak Napoleon, ale wezwała terminusa. Też się zaraziła i zdemolowała pokój. Dostała niesłuszną reputację. Kiedyś Diakonka, ale ciało odrzuciła tą krew.
* Teresa Mieralit: nauczycielka magii leczniczej i katalistka w Szkole Magów. Pomogła Lilianie.
* Adela Kirys: stworzyła eliksir dla Karoliny, proszona przez Napoleona. Niestety, eliksir wszedł w interakcję z polem magicznym Karoliny (nie wiedziała że Karolina ma takie pole).
* Karolina Erenit: była źle traktowana przez magów (nie wie o tym), więc Napoleon Bankierz próbował ją chronić eliksirem Adeli Kirys. Stała się wektorem toksyny z Trzęsawiska. Tymczasowo.

## Plany

* Adela Kirys: znaleźć jakiś sposób, by pokazać, że jest świetna
* Arnulf Poważny: zabezpieczyć magów przed terminusami

### Frakcji

* Szkoła Magów: zapewnić jakieś bezpieczeństwo przed szaleństwami uczniów

## Lokalizacje

1. Świat
    1. Primus
        1. Astoria
            1. Szczeliniec
                1. Powiat Pustogorski
                    1. Zaczęstwo
                        1. Szkoła Magów: miejsce "ponurej zbrodni" - zarówno Liliana jak i Napoleon zainfekowani toksyną eksplodowali dwa pomieszczenia. Pięknotka badała temat berserku magów.

## Czas

* Opóźnienie: 5
* Dni: 1

## Narzędzia MG

### Budowa sesji

**SCENA:**: Nie aplikuje

### Omówienie celu

* nic

## Wykorzystana mechanika

1811
