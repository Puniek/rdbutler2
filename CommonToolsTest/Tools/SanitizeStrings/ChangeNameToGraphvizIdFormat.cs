﻿using Commons.Tools;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonsTest.Tools.SanitizeStrings
{
    public class ChangeNameToGraphvizIdFormat
    {

        [TestCase("Arena Kalaternijska", "Arena_Kalaternijska")]
        [TestCase("Arena-Kalaternijska", "Arena_Kalaternijska")]
        [TestCase("Arena, Kalaternijska", "Arena__Kalaternijska")]
        [TestCase("Arena; Kalaternijska", "Arena__Kalaternijska")]
        [TestCase("Świat|Multivirt System|", "Świat_Multivirt_System_")]
        [TestCase("211010-story", "A_211010_Story")]
        public void For_different_variants_of_name_change_it_to_proper_graphviz_identifier(string name, string expected)
        {
            //Given - in Test Cases
            //When 
            string actual = name.ToGraphvizIdFormat();

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
