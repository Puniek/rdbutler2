﻿using Commons.Tools;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CommonsTest.Tools.SanitizeStrings
{
    public class ReconstructActorNameFromApi
    {
        [TestCase("Pięknotka Diakon")]
        [TestCase("pięknotka diakon")]
        [TestCase("pięknotka+diakon")]
        [TestCase("pięknotka-diakon")]
        [TestCase("Pięknotka+Diakon")]
        public void For_different_variants_of_name_reconstruct_name_corresponding_to_profile_name_on_character_sheet(string apiString)
        {
            //Given - in Test Cases
            //When 
            string actual = apiString.ReconstructActorNameFromApi();

            //Then
            Assert.AreEqual("Pięknotka Diakon", actual);
        }
    }
}
