﻿using Commons.Tools;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonsTest.Tools.CorrectTextual
{
    public class ParseRdDateRangeCheck
    {
        [TestCase("0112-04-22", "0112-04-22")]
        [TestCase("0112-04", "0112-04-01")]
        [TestCase("0112-0", "0112-01-01")]
        [TestCase("0112", "0112-01-01")]
        [TestCase("112", "0112-01-01")]
        public void Dates_are_properly_consumed_and_corrected(string input, string expected)
        {
            // Given - test case.
            // When
            string actual = CorrectRdDate.CorrectTextual(input);

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
