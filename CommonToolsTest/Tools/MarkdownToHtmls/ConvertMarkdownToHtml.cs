﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Commons.Tools;

namespace CommonsTest.Tools.MarkdownToHtmls
{
    public class ConvertMarkdownToHtml
    {
        [Test]
        public void For_bolded_markdown_generate_strong_html_in_paragraph()
        {
            // Given
            string input = "Hello **world**.";
            string expected = "<p>Hello <strong>world</strong>.</p>\n";

            // When
            string actual = MarkdownToHtml.Convert(input);

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
