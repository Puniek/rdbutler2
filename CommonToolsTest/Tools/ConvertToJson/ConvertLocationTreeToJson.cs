﻿using Commons.CoreEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CommonsTest.Tools.ConvertToJson
{
    public class ConvertLocationTreeToJson
    {
        [Test]
        public void Converts_nested_locationTree()
        {
            // Given
            LocationTreeNode locationTree = new LocationTreeNode()
            {
                Title = "Świat",
                Path = "Świat|",
                Children = new List<LocationTreeNode>
                {
                    new LocationTreeNode
                    {
                        Title = "Primus",
                        Path = "Świat|Primus|",
                        Children = new List<LocationTreeNode>
                        {
                            new LocationTreeNode
                            {
                                Title = "Astoria",
                                Path = "Świat|Primus|Astoria|",
                                Children = new List<LocationTreeNode>()
                            },
                            new LocationTreeNode
                            {
                                Title = "Neikatis",
                                Path = "Świat|Primus|Neikatis|",
                                Children = new List<LocationTreeNode>()
                            }
                        }
                    }
                }
            };

            // When
            var actual = locationTree.ToJson();

            // Then
            string expected =
"""
{
  "Title": "Świat",
  "Path": "Świat|",
  "Children": [
    {
      "Title": "Primus",
      "Path": "Świat|Primus|",
      "Children": [
        {
          "Title": "Astoria",
          "Path": "Świat|Primus|Astoria|",
          "Children": []
        },
        {
          "Title": "Neikatis",
          "Path": "Świat|Primus|Neikatis|",
          "Children": []
        }
      ]
    }
  ]
}
""";
            Assert.AreEqual(expected.Trim(), actual.Trim());
        }
    }
}
