﻿using Commons.Tools;
using NUnit.Framework;

namespace CommonsTest.Tools.ParseText
{
    public class GetRegexFromFacade
    {

        [TestCase("4", @"\b(\d{4}-([\w-]+))\b")]
        [TestCase("6", @"\b(\d{6}-([\w-]+))\b")]
        public void For_given_nonzero_date_length_create_regex_extracting_numeric_identifier_from_path(string section, string expected)
        {
            // When
            string actual = RegexFacade.ExtractIdentifierFromPath(section);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [TestCase("title", @"---.+?^title:\s*""*(.+?)""*\s*$.*---")]
        public void For_title_create_regex_extracting_title_line_from_front_matter(string section, string expected)
        {
            // When
            string actual = RegexFacade.ExtractParamFromFrontMatter(section);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [TestCase(@"Progresja", 1, 3, @"^#{1}\sProgresja[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)")]
        [TestCase(@"progresja", 1, 3, @"^#{1}\sprogresja[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)")]
        [TestCase(@"opis", 2, 1, @"^#{2}\sopis[:|\s]*$(.+?)(\Z|^#{1,1}\s\w+)")]
        [TestCase(@"Zasługi", 1, 1, @"^#{1}\sZasługi[:|\s]*$(.+?)(\Z|^#{1,1}\s\w+)")]
        [TestCase(@"Szkoły magiczne", 3, 3, @"^#{3}\sSzkoły magiczne[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)")]
        [TestCase(@"Motywacje (do czego dąży)", 3, 3, @"^#{3}\sMotywacje \(do czego dąży\)[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)")]
        public void Create_regex_extracting_headered_section_from_story(string name, int headerDepth,
            int blockingHeaderDepth, string expected)
        {
            // When
            string actual = RegexFacade.ExtractHeaderSection(headerDepth, blockingHeaderDepth, name);

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
