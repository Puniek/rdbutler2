﻿using Commons.Tools;
using NUnit.Framework;

namespace CommonsTest.Tools.ParseText
{
    public class ExtractWithTextParserEngine
    {

        [Test]
        public void For_single_match_extract_the_first_value_only()
        {
            // Given
            var pattern = @"(\d+)";
            var text = @"Ala ma kota 15, 15, 16";

            // Expecting
            var expected = @"15";

            // When
            string actual = TextParserEngine.SafelyExtractSingleCapturedElement(pattern: pattern, text: text);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void For_no_matches_return_empty_string()
        {
            // Given

            var pattern = @"(\d+)";
            var text = @"Ala ma kota";

            // Expecting
            var expected = string.Empty;

            // When
            string actual = TextParserEngine.SafelyExtractSingleCapturedElement(pattern: pattern, text: text);

            // Then
            Assert.AreEqual(expected, actual);
        }

    }
}
