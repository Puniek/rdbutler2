﻿using Commons.CoreEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;

namespace CommonsTest.DbLoaders
{
    public class CreateActorBriefRecord
    {
        [Test]
        public void Creates_corresponding_one_for_valid_ActorInfoDataModel()
        {
            // Given
            ActorBriefRecordModel abrm = new()
            {
                Name = "Martyn Hiwasser",
                Intensity = 5,
                Link = "2109-martyn-hiwasser",
                Mechver = "2109"
            };

            // When
            ActorBriefRecord actual = ActorBriefRecordDbLoader.CreateSingle(abrm);

            // Expected
            ActorBriefRecord expected = new ActorBriefRecord(
                name: "Martyn Hiwasser",
                intensity: 5,
                link: "2109-martyn-hiwasser",
                mechver: "2109"
                );

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
