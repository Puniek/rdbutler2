﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonsTest.DbLoaders
{
    public class CreateAggregatedSingleLocationDetails
    {
        [Test]
        public void Having_one_record_and_one_block_will_properly_create_locationDetails()
        {
            // Given
            LocationRecord[] lrms = new LocationRecord[]
            {
                new LocationRecord(originatingStory: "230707-story", locName: "Primus", locEvent: "Something happened here", path: "Świat|Primus|", endDate: "0112-01-05")
            };

            GeneralStoryBlock[] blocks = new GeneralStoryBlock[]
            {
                new GeneralStoryBlock
                (
                    storyUid: lrms[0].OriginatingStory,
                    storyTitle: "First story",
                    startDate: "0112-01-03",
                    endDate: "0112-01-05",
                    threads: new string[] {"related-thread"},
                    motives: new string[] {"related-motive"},
                    summary: "Stuff happened",
                    actorsPresent: new string[] { "Arianna Verlen", "Viorika Verlen" }
                )
            };

            // When
            var actual = AggregatedSingleLocationDetailsDbLoader.CreateSingle(lrms, blocks);

            // Then
            AggregatedSingleLocationDetails expected = new AggregatedSingleLocationDetails
            (
                name: lrms[0].Name,
                path: lrms[0].Path,
                relatedStories: new string[] { "230707-story" },
                relatedThreads: new string[] { "related-thread" },
                allActorsPresent: new string[] { "Arianna Verlen", "Viorika Verlen" },
                firstStartDate: "0112-01-03",
                lastEndDate: "0112-01-05",
                usefulLocRecords: lrms,
                storyBlocks: blocks
            );

            Assert.AreEqual(expected, actual);

        }


        [Test]
        public void Having_many_records_and_many_blocks_will_properly_aggregate_data()
        {
            // Given
            LocationRecord[] lrms = new LocationRecord[]
            {
                new LocationRecord(originatingStory: "230707-story", locName: "Primus", locEvent: "Something happened here", path: "Świat|Primus|", endDate: "0112-01-05"),
                new LocationRecord(originatingStory: "230708-story-2", locName: "Primus", locEvent: "Something else happened here", path: "Świat|Primus|", endDate: "0112-01-10")
            };

            GeneralStoryBlock[] blocks = new GeneralStoryBlock[]
            {
                new GeneralStoryBlock
                (
                    storyUid: lrms[0].OriginatingStory,
                    storyTitle: "First story",
                    startDate: "0112-01-03",
                    endDate: "0112-01-05",
                    threads: new string[] {"related-thread"},
                    motives: new string[] {"related-motive"},
                    summary: "Stuff happened",
                    actorsPresent: new string[] { "Arianna Verlen", "Viorika Verlen" }
                ),
                new GeneralStoryBlock
                (
                    storyUid: lrms[1].OriginatingStory,
                    storyTitle: "Second story",
                    startDate: "0112-01-07",
                    endDate: "0112-01-10",
                    threads: new string[] {"related-thread-2"},
                    motives: new string[] {"related-motive-2"},
                    summary: "Different stuff happened",
                    actorsPresent: new string[] { "Arianna Verlen", "Elena Verlen" }
                )
            }.Reverse().ToArray();  // because we want them from newest to oldest

            // When
            var actual = AggregatedSingleLocationDetailsDbLoader.CreateSingle(lrms, blocks);

            // Then
            AggregatedSingleLocationDetails expected = new AggregatedSingleLocationDetails
            (
                name: lrms[0].Name,
                path: lrms[0].Path,
                relatedStories: new string[] { blocks[0].StoryUid, blocks[1].StoryUid },
                relatedThreads: new string[] { "related-thread", "related-thread-2" },
                allActorsPresent: new string[] { "Arianna Verlen", "Elena Verlen", "Viorika Verlen" },
                firstStartDate: "0112-01-03",
                lastEndDate: "0112-01-10",
                usefulLocRecords: lrms,
                storyBlocks: blocks
            );

            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void Records_without_events_will_not_be_added_to_locationDetails_but_its_data_will_be_used()
        {
            // Given
            LocationRecord[] lrms = new LocationRecord[]
            {
                new LocationRecord(originatingStory: "230707-story", locName: "Primus", locEvent: "Something happened here", path: "Świat|Primus|", endDate: "0112-01-05"),
                new LocationRecord(originatingStory: "230708-story-2", locName: "Primus", locEvent: "Something else happened here", path: "Świat|Primus|", endDate: "0112-01-10")
            };

            GeneralStoryBlock[] blocks = new GeneralStoryBlock[]
            {
                new GeneralStoryBlock
                (
                    storyUid: lrms[0].OriginatingStory,
                    storyTitle: "First story",
                    startDate: "0112-01-03",
                    endDate: "0112-01-05",
                    threads: new string[] {"related-thread"},
                    motives: new string[] {"related-motive"},
                    summary: "Stuff happened",
                    actorsPresent: new string[] { "Arianna Verlen", "Viorika Verlen" }
                ),
                new GeneralStoryBlock
                (
                    storyUid: lrms[1].OriginatingStory,
                    storyTitle: "Second story",
                    startDate: "0112-01-07",
                    endDate: "0112-01-10",
                    threads: new string[] {"related-thread-2"},
                    motives: new string[] {"related-motive-2"},
                    summary: "Different stuff happened",
                    actorsPresent: new string[] { "Arianna Verlen", "Elena Verlen" }
                ),
                new GeneralStoryBlock
                (
                    storyUid: "230709-story-3",
                    storyTitle: "Third story",
                    startDate: "0112-01-11",
                    endDate: "0112-01-15",
                    threads: new string[] {"related-thread-2"},
                    motives: new string[] {"related-motive-2"},
                    summary: "Yet another stuff happened",
                    actorsPresent: new string[] { "Arianna Verlen", "Viorika Verlen" }
                )
            }.Reverse().ToArray();  // because we want them from newest to oldest

            LocationRecord[] extendedLrms = lrms.Concat( new LocationRecord[]{
                new LocationRecord(originatingStory: "230709-story-3", locName: "Primus", locEvent: "", path: "Świat|Primus|", endDate: "0112-01-15")
            }).ToArray();

            // When
            var actual = AggregatedSingleLocationDetailsDbLoader.CreateSingle(extendedLrms, blocks);

            // Then
            AggregatedSingleLocationDetails expected = new AggregatedSingleLocationDetails
            (
                name: lrms[0].Name,
                path: lrms[0].Path,
                relatedStories: new string[] { blocks[0].StoryUid, blocks[1].StoryUid, blocks[2].StoryUid },
                relatedThreads: new string[] { "related-thread", "related-thread-2" },
                allActorsPresent: new string[] { "Arianna Verlen", "Elena Verlen", "Viorika Verlen" },
                firstStartDate: "0112-01-03",
                lastEndDate: "0112-01-15",
                usefulLocRecords: lrms,
                storyBlocks: blocks
            );

            Assert.AreEqual(expected, actual);

        }
    }
}
