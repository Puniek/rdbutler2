﻿using Commons.CoreEntities;
using NUnit.Framework;
using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;

namespace CommonsTest.DbLoaders
{
    public class CreateAggregatedStory
    {
        [Test]
        public void Creates_proper_AggregatedStory_for_one_previous_Story_in_chain()
        {
            // Given
            AggregatedStoryModel storyModel = new AggregatedStoryModel
            {
                Uid = 1,
                OriginatingStory = "211112-current",
                OriginatingStoryTitle = "Current",
                Threads = "campaign",
                Motives = "motive",
                StartDate = "00800917",
                EndDate = "00800924",
                PreviousStories = "211111-previous",
                SequenceNumber = "3",
                Gms = "żółw",
                Players = "pit, til",
                PlayerActors = "Michael Firegiver, Eltor Retop",
                AllActors = "Michael Firegiver, Eltor Retop, Elena Xairiss",
                Summary = "text",
                Body = "more text"
            };

            // When
            AggregatedStory actual = AggregatedStoryDbLoader.CreateSingle(storyModel);

            // Then
            AggregatedStory expected = new AggregatedStory(
                storyUid: "211112-current",
                storyTitle: "Current",
                threads: new[] { "campaign" },
                motives: new[] { "motive" },
                startDate: "00800917",
                endDate: "00800924",
                seqNo: "3",
                previousStories: new string[] { "211111-previous" },
                gms: new string[] { "żółw" },
                players: new string[] { "pit", "til" },
                playerActors: new string[] { "Michael Firegiver", "Eltor Retop" },
                allActors: new string[] { "Michael Firegiver", "Eltor Retop", "Elena Xairiss" },
                summary: "text",
                body: "more text"
                );

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Creates_proper_AggregatedStory_for_many_previous_Stories_in_chain()
        {
            // Given
            AggregatedStoryModel storyModel = new AggregatedStoryModel
            {
                Uid = 1,
                OriginatingStory = "211112-current",
                OriginatingStoryTitle = "Current",
                Threads = "campaign",
                Motives = "motive",
                StartDate = "00800917",
                EndDate = "00800924",
                PreviousStories = "211111-previous-1, 211110-previous-2",
                SequenceNumber = "3",
                Gms = "żółw",
                Players = "pit, til",
                PlayerActors = "Michael Firegiver, Eltor Retop",
                AllActors = "Michael Firegiver, Eltor Retop, Elena Xairiss",
                Summary = "text",
                Body = "more text"
            };

            // When
            AggregatedStory actual = AggregatedStoryDbLoader.CreateSingle(storyModel);

            // Then
            AggregatedStory expected = new AggregatedStory(
                storyUid: "211112-current",
                storyTitle: "Current",
                threads: new[] { "campaign" },
                motives: new[] { "motive" },
                startDate: "00800917",
                endDate: "00800924",
                seqNo: "3",
                previousStories: new string[] { "211111-previous-1", "211110-previous-2" },
                gms: new string[] { "żółw" },
                players: new string[] { "pit", "til" },
                playerActors: new string[] { "Michael Firegiver", "Eltor Retop" },
                allActors: new string[] { "Michael Firegiver", "Eltor Retop", "Elena Xairiss" },
                summary: "text",
                body: "more text"
                );

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Creates_proper_AggregatedStory_with_multiple_threads()
        {
            // Given
            AggregatedStoryModel storyModel = new AggregatedStoryModel
            {
                Uid = 1,
                OriginatingStory = "211112-current",
                OriginatingStoryTitle = "Current",
                Threads = "historia-eustachego, arkologia-nativis, infernia-jej-imieniem",
                Motives = "motive-1, motive-2",
                StartDate = "00800917",
                EndDate = "00800924",
                PreviousStories = "211111-previous",
                SequenceNumber = "3",
                Gms = "żółw",
                Players = "pit, til",
                PlayerActors = "Michael Firegiver, Eltor Retop",
                AllActors = "Michael Firegiver, Eltor Retop, Elena Xairiss",
                Summary = "text",
                Body = "more text"
            };

            // When
            AggregatedStory actual = AggregatedStoryDbLoader.CreateSingle(storyModel);

            // Then
            AggregatedStory expected = new AggregatedStory(
                storyUid: "211112-current",
                storyTitle: "Current",
                threads: new[] { "historia-eustachego", "arkologia-nativis", "infernia-jej-imieniem" },
                motives: new[] { "motive-1", "motive-2" },
                startDate: "00800917",
                endDate: "00800924",
                seqNo: "3",
                previousStories: new string[] { "211111-previous" },
                gms: new string[] { "żółw" },
                players: new string[] { "pit", "til" },
                playerActors: new string[] { "Michael Firegiver", "Eltor Retop" },
                allActors: new string[] { "Michael Firegiver", "Eltor Retop", "Elena Xairiss" },
                summary: "text",
                body: "more text"
                );

            Assert.AreEqual(expected, actual);

            // INFO: the AggregatedStory does not currently use StoryToThreadRelation table, it uses string.Split(", ") instead.
            // REASONING: rdbutler2/_Documentation/notes/230325-0005-threads.md
        }
    }
}
