﻿using Commons.CoreEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;

namespace CommonsTest.DbLoaders
{
    public class CreateAcdeorTypes
    {
        [Test]
        public void Creates_corresponding_Progression_for_valid_model()
        {
            // Given
            AggregatedProgressionModel am = new AggregatedProgressionModel
            {
                Actor = "Maria Naavas",
                OriginUid = "211017-random",
                Deed = "got much richer",
                EndDate = "00800924",
                Uid = 13
            };

            // When
            AcdeorPlus actual = AggregatedProgressionDbLoader.CreateSingle(am);

            // Expected
            AcdeorPlus expected = new AcdeorPlus(
                actor: "Maria Naavas",
                originUid: "211017-random",
                deed: "got much richer",
                endDate: "00800924"
                );

            // Then
            Assert.AreEqual(expected, actual);
        }

    }
}
