﻿using Commons.CoreEntities;
using NUnit.Framework;
using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;

namespace CommonsTest.DbLoaders
{
    public class CreateAggregatedMerit
    {
        [Test]
        public void Creates_corresponding_one_for_valid_AggregatedMeritModel()
        {
            // Given
            AggregatedMeritModel amm = new AggregatedMeritModel
            {
                OriginUid = "211017-random",
                Actor = "Maria Naavas",
                Deed = "smiled prettily",
                Threads = "something with cats",
                StartDate = "00800917",
                EndDate = "00800924",
                Uid = 13
            };

            // When
            AggregatedMerit actual = AggregatedMeritDbLoader.CreateSingle(amm);

            // Expected
            AggregatedMerit expected = new AggregatedMerit(
                originUid: "211017-random",
                actor: "Maria Naavas",
                deed: "smiled prettily",
                threads: "something with cats",
                startDate: "00800917",
                endDate: "00800924");

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
