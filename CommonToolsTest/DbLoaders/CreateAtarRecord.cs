﻿using Commons.CoreEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;

namespace CommonsTest.DbLoaders
{
    public class CreateAtarRecord
    {
        [Test]
        public void Creates_corresponding_one_for_valid_AtarRecordModel()
        {
            // Given
            AtarRecordModel arm = new AtarRecordModel
            {
                OriginatingActor = "Maria Naavas",
                RelevantActor = "Martyn Hiwasser",
                Intensity = 3,
                StoryUids = "211011-cats, 211012-dogs, 211013-snails",
                Uid = 13
            };

            // When
            AtarRecord actual = AtarRecordDbLoader.CreateSingle(arm);

            // Expected
            AtarRecord expected = new AtarRecord(
                originatingActor: "Maria Naavas",
                relevantActor: "Martyn Hiwasser",
                intensity: 3,
                storyUids: new string[] { "211011-cats", "211012-dogs", "211013-snails" });

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
