﻿using Commons.CoreEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;

namespace CommonsTest.DbLoaders
{
    public class CreateAggregatedActor
    {
        [Test]
        public void Creates_corresponding_one_for_valid_aggregated_various_components()
        {
            // Given
            EveryActorModel eam = new()
            {
                ActorUid = "2109-martyn-hiwasser",
                Body = "a man of integrity and virtue",
                Mechver = "2109",
                Name = "Martyn Hiwasser",
                Owner = "public",
                Uid = 13
            };

            AggregatedMeritModel[] amm = new AggregatedMeritModel[]
            {
                new AggregatedMeritModel() { Uid = 1, Actor = "Martyn Hiwasser", Deed = "meowed", Threads = "unknown", OriginUid = "211017-cats", StartDate = "00800917", EndDate = "00800920"},
                new AggregatedMeritModel() { Uid = 2, Actor = "Martyn Hiwasser", Deed = "hissed", Threads = "unknown", OriginUid = "211018-cats2", StartDate = "00800921", EndDate = "00800923"}
            };

            AggregatedProgressionModel[] aprm = new AggregatedProgressionModel[]
            {
                new AggregatedProgressionModel() { Uid = 1, Actor = "Martyn Hiwasser", Deed = "progressed", OriginUid = "211017-cats", EndDate = "00800920"},
                new AggregatedProgressionModel() { Uid = 2, Actor = "Martyn Hiwasser", Deed = "progressed well", OriginUid = "211018-cats2", EndDate = "00800923"}
            };

            AtarRecordModel[] atarm = new AtarRecordModel[]
            {
                new AtarRecordModel() { OriginatingActor = "Martyn Hiwasser", RelevantActor = "Maria Naavas", Intensity = 2, StoryUids = "", Uid = 5},
                new AtarRecordModel() { OriginatingActor = "Martyn Hiwasser", RelevantActor = "Klaudia Stryk", Intensity = 1, StoryUids = "", Uid = 7},
                new AtarRecordModel() { OriginatingActor = "Martyn Hiwasser", RelevantActor = "Eustachy Korkoran", Intensity = 1, StoryUids = "", Uid = 8}
            };

            ActorFlashcardModel[] flashcards = new[]
            {
                new ActorFlashcardModel
                {
                    Uid = eam.ActorUid,
                    Name = eam.Name,
                    Records = new List<FlashcardRecordModel>
                    {
                        new FlashcardRecordModel { Body = "First body", OriginStoryUid = "211017-cats", },
                        new FlashcardRecordModel { Body = "2nd body!!", OriginStoryUid = "211018-cats2", }
                    }
                }
            };
            var flashcardRecordModels = flashcards.SelectMany(x => x.Records).ToArray();

            // When
            AggregatedActor actual = AggregatedActorDbLoader.Create(eam, amm, aprm, atarm, flashcardRecordModels, new[] { "unknown" });

            // Expected
            AggregatedMerit[] merits = new AggregatedMerit[]
            {
                new AggregatedMerit(originUid: "211017-cats", actor: "Martyn Hiwasser", deed: "meowed", threads: "unknown", startDate: "00800917", endDate: "00800920"),
                new AggregatedMerit(originUid: "211018-cats2", actor: "Martyn Hiwasser", deed: "hissed", threads: "unknown", startDate: "00800921", endDate: "00800923")
            };

            AcdeorPlus[] progressions = new AcdeorPlus[]
            {
                new AcdeorPlus(originUid: "211017-cats", actor: "Martyn Hiwasser", deed: "progressed", endDate: "00800920"),
                new AcdeorPlus(originUid: "211018-cats2", actor: "Martyn Hiwasser", deed: "progressed well", endDate: "00800923")
            };

            AcdeorPlus[] plans = new AcdeorPlus[]
            {
                new AcdeorPlus(originUid: "211017-cats", actor: "Martyn Hiwasser", deed: "planned", endDate: "00800920"),
                new AcdeorPlus(originUid: "211018-cats2", actor: "Martyn Hiwasser", deed: "planned well", endDate: "00800923")
            };

            AtarRecord[] atarms = new AtarRecord[]
            {
                new AtarRecord(originatingActor: "Martyn Hiwasser", relevantActor: "Maria Naavas", intensity:2, storyUids: Array.Empty<string>()),
                new AtarRecord(originatingActor: "Martyn Hiwasser", relevantActor: "Klaudia Stryk", intensity:1, storyUids: Array.Empty<string>()),
                new AtarRecord(originatingActor: "Martyn Hiwasser", relevantActor: "Eustachy Korkoran", intensity:1, storyUids: Array.Empty<string>())
            };

            FlashcardRecord[] flashcardRecords =
            {
                new FlashcardRecord(originStoryUid: "211017-cats", body: "First body"),
                new FlashcardRecord(originStoryUid: "211018-cats2", body: "2nd body!!"),
            };

            AggregatedActor expected = new(
                uid: "2109-martyn-hiwasser",
                name: "Martyn Hiwasser",
                mechver: "2109",
                factions: Array.Empty<string>(),
                owner: "public",
                body: "a man of integrity and virtue",
                merits: merits,
                progressions: progressions,
                atarRelations: atarms,
                flashcardRecords: flashcardRecords,
                threads: new[] { "unknown" });

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
