﻿using Commons.CoreEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;

namespace CommonsTest.DbLoaders
{
    public class CreateLocationRecord
    {
        [Test]
        public void Creates_corresponding_one_for_valid_LocationRecordModel()
        {
            // Given
            LocationRecordModel lrm = new LocationRecordModel
            {
                Actor = "Anomalia Kolapsu",
                Deed = "where the problem appeared",
                OriginatingStory = "211017-something-with-cats",
                Path = "Świat|Primus|Sektor Astoriański|Obłok Lirański|Anomalia Kolapsu|",
                Uid = 13,
                EndDate = "0123-11-11"
            };

            // Expected
            LocationRecord expected = new LocationRecord(
                originatingStory: "211017-something-with-cats",
                locName: "Anomalia Kolapsu",
                locEvent: "where the problem appeared",
                path: "Świat|Primus|Sektor Astoriański|Obłok Lirański|Anomalia Kolapsu|",
                endDate: "0123-11-11");

            // When
            LocationRecord actual = LocationRecordDbLoader.CreateSingle(lrm);

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
