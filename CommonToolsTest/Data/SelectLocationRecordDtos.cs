﻿using Commons.CoreEntities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonsTest.Data
{
    public class SelectLocationRecordDtos
    {
        [Test]
        public void Selects_records_with_distinct_paths_only_for_various_records()
        {
            // Given
            LocationRecord[] records = new LocationRecord[]
            {
                new("211130-assasin", "Cat home", "nothing happened", "Świat|Something|Third|Cat home", "0123-11-11"),
                new("211130-assasin", "Cat home", "something happened", "Świat|Something|Third|Cat home", "0123-11-11"),  // this should not appear; damaged
                new("211201-cat", "Cat home", "something else happened", "Świat|Something|Third|Cat home", "0123-11-11"), // this should not appear; not unique
                new("211201-cat", "Cat empire", "something happened", "Świat|Something|Third|Cat empire", "0123-11-11")
            };

            // When
            LocationRecord[] actual = LocationRecordDtoOps.SelectRecordsWithDistinctPaths(records);

            // Then
            Assert.AreEqual(2, actual.Length);
            Assert.IsTrue(actual.Where(r => r.Path == "Świat|Something|Third|Cat home").ToArray().Length == 1);
            Assert.IsTrue(actual.Where(r => r.Path == "Świat|Something|Third|Cat empire").ToArray().Length == 1);
        }

        [Test]
        public void Selects_subset_of_records_only_those_from_a_set_of_storyUids()
        {
            // Given
            string[] storyUids = new string[] { "220101-first", "220101-second" };
            LocationRecord[] records = new LocationRecord[]
            {
                new("220101-first", "Cat home", "nothing happened", "Świat|Something|First|Cat home", "0123-11-11"),
                new("220101-second", "Cat home", "nothing happened", "Świat|Something|First|Cat home", "0123-11-11"),
                new("220101-third", "Cat empire", "nothing happened", "Świat|Something|First|Cat empire", "0123-11-11"),
                new("220101-first", "Cat empire", "nothing happened", "Świat|Something|First|Cat empire", "0123-11-11"),
                new("220101-fourth", "Cat paranoia", "nothing happened", "Świat|Something|First|Cat paranoia", "0123-11-11")
            };

            // When
            LocationRecord[] actual = LocationRecordDtoOps.SelectRecordsFilteringByStoryUids(storyUids, records);

            // Expected
            LocationRecord[] expected = new LocationRecord[]
            {
                new("220101-first", "Cat home", "nothing happened", "Świat|Something|First|Cat home", "0123-11-11"),
                new("220101-first", "Cat empire", "nothing happened", "Świat|Something|First|Cat empire", "0123-11-11"),
                new("220101-second", "Cat home", "nothing happened", "Świat|Something|First|Cat home", "0123-11-11")
            };

            // Then
            Assert.That(expected, Is.EquivalentTo(actual));
        }
    }
}
