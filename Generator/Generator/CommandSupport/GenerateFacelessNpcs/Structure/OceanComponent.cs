﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.Structure
{
    public class OceanComponent
    {
        public OceanComponent(string name)
        {
            Name = name;
            HighTraits = Array.Empty<string>();
            LowTraits = Array.Empty<string>();
        }

        public OceanComponent(string name, string[] highTraits, string[] lowTraits)
        {
            Name = name;
            HighTraits = highTraits;
            LowTraits = lowTraits;
        }

        public string Name { get; }
        public string[] HighTraits { get; set; }
        public string[] LowTraits { get; set; }

        public override string ToString()
        {
            return $"{Name}, highTraits.Count: {HighTraits.Count()}, lowTraits.Count: {LowTraits.Count()}";
        }

    }
}
