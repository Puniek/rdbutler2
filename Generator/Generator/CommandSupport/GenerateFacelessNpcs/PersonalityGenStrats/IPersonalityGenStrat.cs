﻿using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats
{
    public interface IPersonalityGenStrat
    {
        (string encao, string[] personalityTraits) SelectPersonality(
            int minTraitsPerCategory, int maxTraitsPerCategory,
            OceanComponent[] oceansWithAllTraits);
    }
}
