﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats.RandomPersonalityStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats
{
    public class PersonalityGenStrat : IPersonalityGenStrat
    {
        private IRng _rng;
        ILowZeroHighGenStrat _lzhStrat;

        public PersonalityGenStrat(IRng rng, ILowZeroHighGenStrat lzhStrat)
        {
            _rng = rng;
            _lzhStrat = lzhStrat;
        }

        public (string encao, string[] personalityTraits) SelectPersonality(
            int minTraitsPerCategory, int maxTraitsPerCategory,
            OceanComponent[] oceansWithAllTraits)
        {
            // Stage 1: select High, Neutral, Low
            (List<OceanComponent> low, List<OceanComponent> neutral, List<OceanComponent> high) =
                _lzhStrat.GenerateLZH(oceansWithAllTraits, _rng);

            // Stage 2: select all traits - only 'low' and 'high' can have traits.

            List<string> selectedTraits = new();
            AddTraitsToSelectedTraits(minTraitsPerCategory, maxTraitsPerCategory,
                low, true, selectedTraits);
            AddTraitsToSelectedTraits(minTraitsPerCategory, maxTraitsPerCategory,
                high, false, selectedTraits);

            // Stage 3: present the result

            string personality = "ENCAO:  " +
                GetOceanSigil("Extraversion", low, neutral, high) +
                GetOceanSigil("Neuroticism", low, neutral, high) +
                GetOceanSigil("Conscientiousness", low, neutral, high) +
                GetOceanSigil("Agreeableness", low, neutral, high) +
                GetOceanSigil("Openness", low, neutral, high);

            return (personality, selectedTraits.ToArray());
        }

        private string GetOceanSigil(string component, 
            List<OceanComponent> low, List<OceanComponent> neutral, List<OceanComponent> high)
        {
            OceanComponent[] isInHigh = high.Where(c => c.Name == component).ToArray();
            if (isInHigh.Length > 0) return "+";

            OceanComponent[] isInNtr = neutral.Where(c => c.Name == component).ToArray();
            if (isInNtr.Length > 0) return "0";

            OceanComponent[] isInLow = low.Where(c => c.Name == component).ToArray();
            if (isInLow.Length > 0) return "-";

            throw new ArgumentException("Impossible situation happened; probably component name is wrong: " + component);
        }

        private void AddTraitsToSelectedTraits(int minTraitsPerCategory, int maxTraitsPerCategory, 
            List<OceanComponent> selectedList, bool selectedLow, List<string> selectedTraits)
        {
            foreach (OceanComponent component in selectedList)
            {
                List<string> traitsCopy = selectedLow ? component.LowTraits.ToList() : component.HighTraits.ToList();

                int howManyTraits = _rng.Next(minTraitsPerCategory, maxTraitsPerCategory);
                for (int i = 0; i < howManyTraits; i++)
                {
                    int selectedTrait = _rng.Next(0, traitsCopy.Count - 1);
                    selectedTraits.Add(traitsCopy[selectedTrait]);
                    traitsCopy.RemoveAt(selectedTrait);
                }
            }
        }

    }
}
