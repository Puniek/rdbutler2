﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats.RandomPersonalityStrats
{
    public class LzhGenFlatRandom : ILowZeroHighGenStrat
    {
        public (List<OceanComponent>, List<OceanComponent>, List<OceanComponent>) GenerateLZH(
            OceanComponent[] oceansWithAllTraits, IRng rng)
        {
            List<OceanComponent> low = new List<OceanComponent>();
            List<OceanComponent> zero = new List<OceanComponent>();
            List<OceanComponent> high = new List<OceanComponent>();

            foreach (var c in oceansWithAllTraits)
            {
                int lowNtrHigh = rng.Next(0, 2);
                if (lowNtrHigh == 0) low.Add(c);
                if (lowNtrHigh == 1) zero.Add(c);
                if (lowNtrHigh == 2) high.Add(c);
            }

            return (low, zero, high);
        }
    }
}
