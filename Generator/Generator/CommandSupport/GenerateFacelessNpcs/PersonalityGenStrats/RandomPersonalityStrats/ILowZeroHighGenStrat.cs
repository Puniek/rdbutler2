﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats.RandomPersonalityStrats
{

    /// <summary>
    /// Responsible for generating high-zero-low. Something like: {low: E, N, zero: A, high: C, O}
    /// </summary>
    public interface ILowZeroHighGenStrat
    {
        /// <summary>
        /// Sequence: low, zero, high.
        /// </summary>
        /// <param name="oceansWithAllTraits">All Ocean components with potential traits. Traits are unused here.</param>
        /// <returns>Output: {low: E, N, zero: A, high: C, O}</returns>
        (List<OceanComponent>, List<OceanComponent>, List<OceanComponent>) GenerateLZH(OceanComponent[] oceansWithAllTraits, IRng rng);
    }
}
