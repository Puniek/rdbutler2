﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats.RandomPersonalityStrats
{
    public class LzhGenWithGuaranteedLZH : ILowZeroHighGenStrat
    {
        public (List<OceanComponent>, List<OceanComponent>, List<OceanComponent>) GenerateLZH(
            OceanComponent[] oceansWithAllTraits, IRng rng)
        {
            List<OceanComponent> componentsCopy = oceansWithAllTraits.Select(o => o).ToList();

            // We need one high, one low and one neutral. Others are random.
            List<OceanComponent> low = new List<OceanComponent>();
            List<OceanComponent> zero = new List<OceanComponent>();
            List<OceanComponent> high = new List<OceanComponent>();

            // assign three mandatory
            RandomlyAssignOceanComponentToProperListAndRemoveElementFromComponentsCopy(componentsCopy, low, rng);
            RandomlyAssignOceanComponentToProperListAndRemoveElementFromComponentsCopy(componentsCopy, zero, rng);
            RandomlyAssignOceanComponentToProperListAndRemoveElementFromComponentsCopy(componentsCopy, high, rng);

            // assign remaining OCEAN components
            foreach (var c in componentsCopy)
            {
                int lowNtrHigh = rng.Next(0, 2);
                if (lowNtrHigh == 0) low.Add(c);
                if (lowNtrHigh == 1) zero.Add(c);
                if (lowNtrHigh == 2) high.Add(c);
            }

            return (low, zero, high);
        }

        private void RandomlyAssignOceanComponentToProperListAndRemoveElementFromComponentsCopy(
            List<OceanComponent> componentsCopy, List<OceanComponent> selectedList, IRng rng)
        {
            int index = rng.Next(0, componentsCopy.Count -1);
            selectedList.Add(componentsCopy[index]);
            componentsCopy.RemoveAt(index);
        }
    }
}
