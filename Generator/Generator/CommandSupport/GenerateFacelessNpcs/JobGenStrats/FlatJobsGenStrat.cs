﻿using Commons.Tools.Rngs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.JobGenStrats
{
    public class FlatJobsGenStrat : IJobGenStrat
    {
        private IRng _rng;

        public FlatJobsGenStrat(IRng rng)
        {
            _rng = rng;
        }

        public string[] SelectJobs(int minJobs, int maxJobs, string[] possibleJobs)
        {
            string[] onlyJobs = possibleJobs.Select(j => j[7..]).ToArray();

            int howManyJobs = _rng.Next(minJobs, maxJobs);
            
            List<string> selectedJobs = new List<string>();
            List<string> possibleJobsCopy = onlyJobs.Select(j => j).ToList();
            for (int i = 0; i < howManyJobs; i++)
            {
                int whichJob = _rng.Next(1, possibleJobsCopy.Count - i);
                selectedJobs.Add(possibleJobsCopy[whichJob - 1]);
                possibleJobsCopy.RemoveAt(whichJob - 1);
            }

            return selectedJobs.ToArray();
        }
    }
}
