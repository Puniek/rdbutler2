﻿using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.Formatters
{
    public class NpcBriefFormatter
    {
        public string NpcToMkdn(FacelessNpc npc)
        {
            // name
            string faceless = $"\n### Faceless NPC: {npc.Name}\n\n";
            faceless += "* **Name**: " + npc.Name + "\n";

            faceless += FormatPersonality(npc.Encao, npc.PersonalityTraits);
            faceless += FormatValues(npc.PositiveValues, npc.AntiValues);
            faceless += FormatDrives(npc.PositiveDrives, npc.NegativeDrives);
            faceless += FormatJobs(npc.JobsSelected);

            return faceless + "\n\n";
        }

        public string FormatJobs(string[] jobsSelected)
        {
            // string expected = "    * Potencjalne prace:\n        * Operator maszyn\n        * Grzyboznawca\n";

            string jobs = "    * Potencjalne prace:\n";

            foreach (var job in jobsSelected)
            {
                jobs += "        * " + job + "\n";
            }

            return jobs;
        }

        public string FormatDrives(string[] positiveDrives, string[] negativeDrives)
        {
            string drives = "    * Silnik:\n";

            foreach (var drive in positiveDrives)
            {
                drives += "        * TAK: " + drive + "\n";
            }
            foreach (var drive in negativeDrives)
            {
                drives += "        * NIE: " + drive + "\n";
            }

            return drives;
        }

        public string FormatValues(string[] positiveValues, string[] antiValues)
        {
            // "    * Wartości:\n        * TAK: Hedonism, Stimulation NIE: Self-direction\n";
            string values = "    * Wartości:\n        * ";
            string[] positiveBrief = positiveValues.Select(s => Regex.Match(s, @"(.+?):").Groups[1].Value).ToArray();
            string[] antiBrief = antiValues.Select(s => Regex.Match(s, @"(.+?):").Groups[1].Value).ToArray();

            foreach(var single in positiveBrief)
            {
                values += $"TAK: {single}, ";
            }
            foreach (var single in antiBrief)
            {
                values += $"NIE: {single}, ";
            }

            return values[0..^2] + "\n";

        }

        public string FormatPersonality(string encao, string[] personalityTraits)
        {
            string personality = $"    * Ocean: {encao}\n";
            foreach (var trait in personalityTraits)
            {
                personality += "        * " + trait + "\n";
            }

            return personality;
        }

    }
}
