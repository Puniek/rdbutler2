﻿using Commons.Tools.Rngs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator.CommandSupport.GenerateFacelessNpcs.GeneratingStrategies
{
    public class FlatDistinctSelector : ISelector
    {
        private IRng _rng;

        public FlatDistinctSelector(IRng rng)
        {
            _rng = rng;
        }

        public (string[] selectedPositive, string[] selectedNegative) SelectPositiveAndNegative(
            int minAmountPositive, int maxAmountPositive,
            int minAmountNegative, int maxAmountNegative,
            string[] allPossibleRecords)
        {
            List<string> possibilitiesCopy = allPossibleRecords.Select(v => v).ToList();

            // Positive values
            int howManyPositive = _rng.Next(minAmountPositive, maxAmountPositive);
            (possibilitiesCopy, List<string> selectedPositive) = TakeSomeOutFromPool(possibilitiesCopy, howManyPositive);

            // Negative values can not duplicate positive traits
            int howManyNegative = _rng.Next(minAmountNegative, maxAmountNegative);

            (possibilitiesCopy, List<string> selectedNegative) = TakeSomeOutFromPool(possibilitiesCopy, howManyNegative);

            return (selectedPositive.ToArray(), selectedNegative.ToArray());
        }

        private (List<string>, List<string>) TakeSomeOutFromPool(List<string> poolOfPossibilities, int howManyToTakeOut)
        {
            List<string> takenOut = new();
            for (int i = 0; i < howManyToTakeOut; i++)
            {
                int whichValue = _rng.Next(1, poolOfPossibilities.Count) - 1;    //-1, because this rng(1, 2) gives 1 or 2 and we have index [0, n-1]
                takenOut.Add(poolOfPossibilities[whichValue]);
                poolOfPossibilities.RemoveAt(whichValue);
            }

            return (poolOfPossibilities, takenOut);
        }
    }
}
