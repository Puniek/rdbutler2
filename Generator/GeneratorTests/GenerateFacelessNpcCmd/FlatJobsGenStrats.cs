﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.JobGenStrats;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd
{

    public class FlatJobsGenStrats
    {
        [Test]
        public void Generates_Job_List_Without_Duplicates()
        {
            // Given
            string[] possibleJobs = new string[] { "111101 Parlamentarzysta", "112018 Rektor", "122102 Kierownik do spraw sprzedaży",
                "311934 Technik budowy fortepianów i pianin", "312202 Mistrz produkcji w przemyśle drzewnym", "413103 Operator edytorów tekstu"
            };

            var rngMock = new Mock<IRng>();
            rngMock.SetupSequence(_ => _.Next(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(3)
                .Returns(5)
                .Returns(5)
                .Returns(1);

            var strat = new FlatJobsGenStrat(rngMock.Object);

            // When
            string[] actual = strat.SelectJobs(2, 3, possibleJobs);

            // Then
            string[] expected = new string[] { "Mistrz produkcji w przemyśle drzewnym", "Operator edytorów tekstu", "Parlamentarzysta" };   // 5, 5, 1
            Assert.AreEqual(expected, actual);
        }
    }
}