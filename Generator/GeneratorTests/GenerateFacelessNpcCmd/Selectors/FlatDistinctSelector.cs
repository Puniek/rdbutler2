﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.GeneratingStrategies;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd.Selectors
{
    public class FlatDistinctSelector
    {

        [Test]
        public void Values__Lists_Work_For_Final_Index_Edge_Case()
        {
            // Given
            string[] possibleValues = new string[] { "Self-direction: autonomy, ability", "Stimulation: feel alive",
                "Hedonism: pleasure", "Achievement: build more", "Power: more influence", "Face: prestige", "Security: being safe" };

            var rngMock = new Mock<IRng>();
            rngMock.SetupSequence(_ => _.Next(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(2)     // how many values to generate
                .Returns(7)     // first max @max val: Security
                .Returns(6)     // second max @max val: Face
                .Returns(2)     // how many values to generate
                .Returns(5)     // first min @max val: Power
                .Returns(4);    // second min @max val: Achievement

            var strat = new Generator.CommandSupport.GenerateFacelessNpcs.GeneratingStrategies.FlatDistinctSelector(rngMock.Object);

            // When
            (string[] actualPositiveValues, string[] actualAntiValues) = strat.SelectPositiveAndNegative(2, 3, 2, 2, possibleValues);

            // Then
            string[] expectedPositiveValues = new string[] { "Security: being safe", "Face: prestige" };
            string[] expectedAntiValues = new string[] { "Power: more influence", "Achievement: build more" };

            Assert.AreEqual(expectedPositiveValues, actualPositiveValues);
            Assert.AreEqual(expectedAntiValues, actualAntiValues);

        }

        [Test]
        public void Values__Lists_Work_For_First_Index_Eliminating_Already_Selected_Values_Ensuring_No_Duplicates()
        {
            // Given
            string[] possibleValues = new string[] { "Self-direction: autonomy, ability", "Stimulation: feel alive",
                "Hedonism: pleasure", "Achievement: build more", "Power: more influence", "Face: prestige", "Security: being safe" };

            var rngMock = new Mock<IRng>();
            rngMock.SetupSequence(_ => _.Next(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(2)     // how many values to generate
                .Returns(1)     // first max @min val: Self-direction
                .Returns(1)     // second max @min val: Stimulation
                .Returns(2)     // how many values to generate
                .Returns(1)     // first min @min val: Hedonism
                .Returns(1);    // second min @min val: Achievement

            var strat = new Generator.CommandSupport.GenerateFacelessNpcs.GeneratingStrategies.FlatDistinctSelector(rngMock.Object);

            // When
            (string[] actualPositiveValues, string[] actualAntiValues) = strat.SelectPositiveAndNegative(2, 3, 2, 2, possibleValues);

            // Then
            string[] expectedPositiveValues = new string[] { "Self-direction: autonomy, ability", "Stimulation: feel alive" };
            string[] expectedAntiValues = new string[] { "Hedonism: pleasure", "Achievement: build more" };

            Assert.AreEqual(expectedPositiveValues, actualPositiveValues);
            Assert.AreEqual(expectedAntiValues, actualAntiValues);

        }
    }
}
