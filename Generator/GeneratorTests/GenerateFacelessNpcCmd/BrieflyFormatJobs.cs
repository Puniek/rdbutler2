﻿using Generator.CommandSupport.GenerateFacelessNpcs.Formatters;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd
{
    public class BrieflyFormatJobs
    {
        [Test]
        public void Displays_single_job()
        {
            // Given
            string[] jobs = new string[] { "Operator maszyn", "Grzyboznawca" };

            // When
            string actual = new NpcBriefFormatter().FormatJobs(jobs);

            // Then
            string expected = "    * Potencjalne prace:\n        * Operator maszyn\n        * Grzyboznawca\n";

            Assert.AreEqual(expected, actual);
        }
    }
}
