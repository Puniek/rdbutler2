﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.NameGenStrats;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd
{
    public class RouletteNameGenStrats
    {
        [Test]
        public void Generates_First_Male_Name_With_Similar_Probability()
        {
            // Given
            string[] maleNameFreq = new string[] { "ANTONI 9183", "JAKUB 8942", "SZYMON 8264" };

            var rngMock = new Mock<IRng>();
            rngMock.Setup(_ => _.YesOrNo()).Returns(true);
            rngMock.Setup(_ => _.Next(It.IsAny<int>(), It.IsAny<int>())).Returns(100);

            var strat = new RouletteNameGenStrat(rngMock.Object);

            // When
            string actual = strat.SelectName(true, maleNameFreq, Array.Empty<string>());

            // Then
            string expected = "Antoni";
            Assert.AreEqual(expected.ToLower(), actual.ToLower());
        }

        [Test]
        public void Generates_Second_Male_Name_With_Similar_Probability()
        {
            // Given
            string[] maleNameFreq = new string[] { "ANTONI 9183", "JAKUB 8942", "SZYMON 8264" };

            var rngMock = new Mock<IRng>();
            rngMock.Setup(_ => _.YesOrNo()).Returns(true);
            rngMock.Setup(_ => _.Next(It.IsAny<int>(), It.IsAny<int>())).Returns(12000);

            var strat = new RouletteNameGenStrat(rngMock.Object);

            // When
            string actual = strat.SelectName(true, maleNameFreq, Array.Empty<string>());

            // Then
            string expected = "Jakub";
            Assert.AreEqual(expected.ToLower(), actual.ToLower());
        }

        [Test]
        public void Generates_Third_Male_Name_At_Boundary()
        {
            // Given
            string[] maleNameFreq = new string[] { "ANTONI 100", "JAKUB 100", "SZYMON 100" };

            var rngMock = new Mock<IRng>();
            rngMock.Setup(_ => _.YesOrNo()).Returns(true);
            rngMock.Setup(_ => _.Next(It.IsAny<int>(), It.IsAny<int>())).Returns(599);

            var strat = new RouletteNameGenStrat(rngMock.Object);

            // When
            string actual = strat.SelectName(true, maleNameFreq, Array.Empty<string>());

            // Then
            string expected = "SZYMON";
            Assert.AreEqual(expected.ToLower(), actual.ToLower());
        }
    }
}
