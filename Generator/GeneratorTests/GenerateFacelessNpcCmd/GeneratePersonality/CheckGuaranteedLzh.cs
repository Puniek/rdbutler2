﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats.RandomPersonalityStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd.GeneratePersonality
{
    public class CheckGuaranteedLzh
    {
        [Test]
        public void For_rng_returning_zero_guarantees_specific_sequence_of_ENCAO_with_3L_1Z_and_1H()
        {
            // Given
            OceanComponent[] oceanComponents = new OceanComponent[]
            {
                new OceanComponent("Extraversion", new string[] {}, new string[] {}),
                new OceanComponent("Neuroticism", new string[] {}, new string[] {}),
                new OceanComponent("Conscientiousness", new string[] {}, new string[] {}),
                new OceanComponent("Agreeableness", new string[] {}, new string[] {}),
                new OceanComponent("Openness", new string[] {}, new string[] {})
            };

                // For generator always returning '0', as in, lowest possible value
            var rngMock = new Mock<IRng>();
            rngMock.SetupSequence(_ => _.Next(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(0);

            // When
            (var low, var zero, var high) = new LzhGenWithGuaranteedLZH().GenerateLZH(oceanComponents, rngMock.Object);

            // Then
            // We can sensibly expect to have {L: e, a, o, Z: n, H: c} as this is the sequence
            Assert.AreEqual("Extraversion", low.First().Name);
            Assert.AreEqual(3, low.Count);
            Assert.AreEqual("Neuroticism", zero.First().Name);
            Assert.AreEqual("Conscientiousness", high.First().Name);
        }
    }
}
