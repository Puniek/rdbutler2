﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats.RandomPersonalityStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd.GeneratePersonality
{
    public class E2EPersonalityGenStrat
    {
        [Test]
        public void Generates_Ocean_String_And_Trait_List_Without_Duplicates()
        {
            // Given
            OceanComponent[] oceanComponents = new OceanComponent[]
            {
                new OceanComponent("Extraversion",
                    new string[] { "h_ex1", "h_ex2", "h_ex3", "h_ex4" },
                    new string[] { "l_ex1", "l_ex2", "l_ex3", "l_ex4" })
                ,
                new OceanComponent("Neuroticism",
                    new string[] { "h_nr1", "h_nr2", "h_nr3", "h_nr4" },
                    new string[] { "l_nr1", "l_nr2", "l_nr3", "l_nr4" })
                ,
                new OceanComponent("Conscientiousness",
                    new string[] { "h_cs1", "h_cs2", "h_cs3", "h_cs4" },
                    new string[] { "l_cs1", "l_cs2", "l_cs3", "l_cs4" })
                ,
                new OceanComponent("Agreeableness",
                    new string[] { "h_ag1", "h_ag2", "h_ag3", "h_ag4" },
                    new string[] { "l_ag1", "l_ag2", "l_ag3", "l_ag4" })
                ,
                new OceanComponent("Openness",
                    new string[] { "h_op1", "h_op2", "h_op3", "h_op4" },
                    new string[] { "l_op1", "l_op2", "l_op3", "l_op4" })
                ,
            };

            var rngMock = new Mock<IRng>();
            rngMock.SetupSequence(_ => _.Next(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(4)     // 4 - low pick 'openness'
                .Returns(2)     // 2 - neutral pick 'conscientiousness'
                .Returns(2)     // 2 - high pick 'agreeableness'
                .Returns(0)     // 0 - extraversion is 'low'
                .Returns(2)     // 2 - neuroticism is 'high'
                .Returns(1)     // 1 - 1 trait from low openness
                .Returns(1)     // 1 - at index 1 ("l_op2")
                .Returns(0)     // 0 - 0 traits from low extraversion
                .Returns(2)     // 2 - 2 traits from high agreeableness
                .Returns(0)     // 0 - at index 0 ("h_ag1")
                .Returns(0)     // 0 - at index 0 ("h_ag2")
                .Returns(1)     // 1 - 1 trait from high neuroticism
                .Returns(0);    // 0 - at index 0 ("h_nr1")

            var strat = new PersonalityGenStrat(rngMock.Object, new LzhGenWithGuaranteedLZH());

            // When
            (string actualEncao, string[] actualTraits) = strat.SelectPersonality(0, 2, oceanComponents);

            // Then
            string expectedEncao = "ENCAO:  -+0+-";
            string[] expectedTraits = new string[] { "l_op2", "h_ag1", "h_ag2", "h_nr1" };

            Assert.AreEqual(expectedEncao, actualEncao);
            Assert.AreEqual(expectedTraits, actualTraits);
        }
    }
}
