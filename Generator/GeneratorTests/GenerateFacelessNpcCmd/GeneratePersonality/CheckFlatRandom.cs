﻿using Commons.Tools.Rngs;
using Generator.CommandSupport.GenerateFacelessNpcs.PersonalityGenStrats.RandomPersonalityStrats;
using Generator.CommandSupport.GenerateFacelessNpcs.Structure;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd.GeneratePersonality
{
    public class CheckFlatRandom
    {
        [Test]
        public void For_rng_returning_zero_guarantees_specific_sequence_of_ENCAO_with_5L()
        {
            // Given
            OceanComponent[] oceanComponents = new OceanComponent[]
            {
                new OceanComponent("Extraversion", new string[] {}, new string[] {}),
                new OceanComponent("Neuroticism", new string[] {}, new string[] {}),
                new OceanComponent("Conscientiousness", new string[] {}, new string[] {}),
                new OceanComponent("Agreeableness", new string[] {}, new string[] {}),
                new OceanComponent("Openness", new string[] {}, new string[] {})
            };

            // For generator always returning '0', as in, lowest possible value
            var rngMock = new Mock<IRng>();
            rngMock.SetupSequence(_ => _.Next(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(0);

            // When
            (var low, var zero, var high) = new LzhGenFlatRandom().GenerateLZH(oceanComponents, rngMock.Object);

            // Then
            Assert.AreEqual("Extraversion", low.First().Name);
            Assert.AreEqual(5, low.Count);
            Assert.AreEqual(0, zero.Count);
            Assert.AreEqual(0, high.Count);
        }
    }
}
