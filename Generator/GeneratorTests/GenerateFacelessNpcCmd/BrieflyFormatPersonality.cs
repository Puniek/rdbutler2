﻿using Generator.CommandSupport.GenerateFacelessNpcs.Formatters;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorTests.GenerateFacelessNpcCmd
{
    public class BrieflyFormatPersonality
    {
        [Test]
        public void Displays_ENCAO_without_personality_traits()
        {
            // Given
            string encao = "ENCAO: +-00+";
            string[] traits = new string[] { };

            // When
            string actual = new NpcBriefFormatter().FormatPersonality(encao, traits);

            // Then
            string expected = "    * Ocean: ENCAO: +-00+\n";
            
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void Displays_ENCAO_with_many_personality_traits()
        {
            // Given
            string encao = "ENCAO: +-00+";
            string[] traits = new string[] { "loves life", "has good ideas" };

            // When
            string actual = new NpcBriefFormatter().FormatPersonality(encao, traits);

            // Then
            string expected = "    * Ocean: ENCAO: +-00+\n        * loves life\n        * has good ideas\n";

            Assert.AreEqual(expected, actual);

        }
    }
}
