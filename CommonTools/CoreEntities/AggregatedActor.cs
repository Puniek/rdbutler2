﻿using System;
using System.Linq;

namespace Commons.CoreEntities
{
    /// <summary>
    /// AggregatedActor represents EVERY information we have on a specific character which is present in the world.
    /// Its tidbits come from Story (Merits, who-knows-whom, location...), Profile (all info on profile)
    /// and every other place necessary. If that actor exists in the World, it shall be aggregated here.
    /// </summary>
    public class AggregatedActor
    {
        public AggregatedActor(string uid, string name, string mechver, string[] factions, string owner, string body,
            AggregatedMerit[] merits, AcdeorPlus[] progressions, AtarRecord[] atarRelations, FlashcardRecord[] flashcardRecords, string[] threads)
        {
            Uid = uid;
            Name = name;
            Mechver = mechver;
            Factions = factions;
            Owner = owner;
            Body = body;
            Merits = merits;
            Progressions = progressions;
            ActorToActorRelations = atarRelations;
            FlashcardRecords = flashcardRecords;
            Threads = threads;
        }

        public string Uid { get; }
        public string Name { get; }
        public string Mechver { get; }
        public string[] Factions { get; }
        public string Owner { get; }
        public string Body { get; }
        public AggregatedMerit[] Merits { get; }
        public AcdeorPlus[] Progressions { get; }
        public AtarRecord[] ActorToActorRelations { get; }
        public FlashcardRecord[] FlashcardRecords { get; }
        public string[] Threads { get; }

        public override string ToString() => $"{Name}|{Mechver}: merits: {Merits.Length}";

        public override int GetHashCode() => HashCode.Combine(HashCode.Combine(Uid, Name, Mechver, Factions, Owner, Merits, Progressions), HashCode.Combine(FlashcardRecords, Threads));
        public override bool Equals(object obj)
        {
            // For sanity's sake I am **NOT** comparing Body strings. Too large, too irritating while testing and not relevant enough.
            // For another sanity's sake Actor-to-Actor relations are not under Equals. If it was, I would never be able to make this object manually for tests.

            if (obj is not AggregatedActor other) return false;

            if (Uid != other.Uid) return false;
            if (Name != other.Name) return false;
            if (Mechver != other.Mechver) return false;
            if (Factions.SequenceEqual(other.Factions) == false) return false;
            if (Owner != other.Owner) return false;
            if (Merits.SequenceEqual(other.Merits) == false) return false;
            if (Progressions.SequenceEqual(other.Progressions) == false) return false;
            if (FlashcardRecords.SequenceEqual(other.FlashcardRecords) == false) return false;
            if (Threads.SequenceEqual(other.Threads) == false) return false;

            return true;
        }
    }
}
