﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    /// <summary>
    /// This thing represents a singular Story point for an Actor.
    /// It contains all the information an Actor needs about a Story.
    /// In other words - it is a 'Story from this Actor's point of view'.
    /// </summary>
    public class ActorThreadStoryBlock
    {
        public ActorThreadStoryBlock(string originatingActor, string storyUid, string storyTitle, string startDate, string endDate, string summary,
            string actorDeed, string[] actorProgressions, string[] actorsPresent)
        {
            OriginatingActor = originatingActor;
            StoryUid = storyUid;
            StoryTitle = storyTitle;
            StartDate = startDate;
            EndDate = endDate;
            Summary = summary;
            TargetActorDeed = actorDeed;
            TargetActorProgressions = actorProgressions;
            AllActorsPresent = actorsPresent;
        }

        public string OriginatingActor { get; init; }
        public string StoryUid { get; init; }
        public string StoryTitle { get; init; }
        public string StartDate { get; init; }
        public string EndDate { get; init; }
        public string Summary { get; init; }
        public string TargetActorDeed { get; init; }
        public string[] TargetActorProgressions { get; init; }
        public string[] AllActorsPresent { get; init; }

        public override int GetHashCode() => HashCode.Combine(StoryUid, StoryTitle, StartDate, EndDate, Summary, TargetActorDeed);
        public override bool Equals(object obj)
        {
            if (obj is not ActorThreadStoryBlock other) return false;

            if (OriginatingActor != other.OriginatingActor) return false;
            if (StoryUid != other.StoryUid) return false;
            if (StoryTitle != other.StoryTitle) return false;
            if (StartDate != other.StartDate) return false;
            if (EndDate != other.EndDate) return false;
            if (Summary != other.Summary) return false;
            if (TargetActorDeed != other.TargetActorDeed) return false;
            if (Enumerable.SequenceEqual(TargetActorProgressions, other.TargetActorProgressions) == false) return false;
            if (Enumerable.SequenceEqual(AllActorsPresent, other.AllActorsPresent) == false) return false;

            return true;
        }

        public override string ToString()
        {
            return $"{OriginatingActor} - {StoryUid} - {StoryTitle} @ {StartDate} - {EndDate} with Deed: {TargetActorDeed} " +
                $"having {TargetActorProgressions.Length} Progressions and {AllActorsPresent.Length} Actors present.";
        }
    }
}
