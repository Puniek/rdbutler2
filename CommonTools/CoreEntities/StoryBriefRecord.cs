﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Commons.CoreEntities
{
    public class StoryBriefRecord
    {
        /// <summary>
        /// Represents a brief record of a story, containing essential information about the story.
        /// </summary>
        public StoryBriefRecord(string storyUid, string title, string threads, string motives, string startDate, string endDate, string players)
        {
            StoryUid = storyUid;
            Title = title;
            Threads = threads;
            Motives = motives;
            StartDate = startDate;
            EndDate = endDate;
            Players = players;
        }

        public string StoryUid { get; }
        public string Title { get; }
        public string Threads { get; }
        public string Motives { get; }
        public string StartDate { get; }
        public string EndDate { get; }
        public string Players { get; }

        public override int GetHashCode() => HashCode.Combine(StoryUid, Title, Threads, StartDate, EndDate);
        public override bool Equals(object obj)
        {
            if (obj is not StoryBriefRecord other) return false;

            if (StoryUid != other.StoryUid) return false;
            if (Title != other.Title) return false;
            if (Threads != other.Threads) return false;
            if (Motives != other.Motives) return false;
            if (StartDate != other.StartDate) return false;
            if (EndDate != other.EndDate) return false;
            if (Players != other.Players) return false;

            return true;
        }

        public override string ToString() => $"{StoryUid} at {StartDate} - {EndDate}, threads: {Threads}, motives: {Motives}, players: {Players}";
    }
}
