﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    public class AggregatedSingleLocationDetails
    {
        public AggregatedSingleLocationDetails(
            string name, 
            string path, 
            string[] relatedStories, 
            string[] relatedThreads,
            string[] allActorsPresent, 
            string firstStartDate, 
            string lastEndDate, 
            LocationRecord[] usefulLocRecords, 
            GeneralStoryBlock[] storyBlocks)
        {
            Name = name;
            Path = path;
            RelatedStories = relatedStories;
            RelatedThreads = relatedThreads;
            AllActorsPresent = allActorsPresent;
            FirstStartDate = firstStartDate;
            LastEndDate = lastEndDate;
            UsefulLocationRecords = usefulLocRecords;
            StoryBlocks = storyBlocks;
        }

        public string Name { get; }
        public string Path { get; }
        public string[] RelatedStories { get; }
        public string[] RelatedThreads { get; }
        public string[] AllActorsPresent { get; }
        public string FirstStartDate { get; }
        public string LastEndDate { get; }

        /// <summary>
        /// Those are the ones containing data when something actually happened (usually, Deed).
        /// </summary>
        public LocationRecord[] UsefulLocationRecords { get; }
        public GeneralStoryBlock[] StoryBlocks { get; }

        public override bool Equals(object obj)
        {
            if (obj is not AggregatedSingleLocationDetails other) return false;

            if (Name != other.Name) return false;
            if (Path != other.Path) return false;
            if (!RelatedStories.SequenceEqual(other.RelatedStories)) return false;
            if (!RelatedThreads.SequenceEqual(other.RelatedThreads)) return false;
            if (!AllActorsPresent.SequenceEqual(other.AllActorsPresent)) return false;
            if (FirstStartDate != other.FirstStartDate) return false;
            if (LastEndDate != other.LastEndDate) return false;
            if (!UsefulLocationRecords.SequenceEqual(other.UsefulLocationRecords)) return false;
            if (!StoryBlocks.SequenceEqual(other.StoryBlocks)) return false;
            return true;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Path, FirstStartDate, LastEndDate); // we don't need everything here, really

        public override string ToString()
        {
            return $"Name: {Name} @ {Path}, ({FirstStartDate} - {LastEndDate}), RelatedStories count: {RelatedStories.Length}, RelatedThreads count: {RelatedThreads.Length}, AllActorsPresent count: {AllActorsPresent.Length}, LocationRecords count: {UsefulLocationRecords.Length}, StoryBlocks count: {StoryBlocks.Length}";
        }
    }
}
