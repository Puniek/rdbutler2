﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commons.CoreEntities
{
    /// <summary>
    /// All information on LocationRecord we have or need.
    /// </summary>
    public class LocationRecord
    {
        public LocationRecord(
            string originatingStory,
            string locName,
            string locEvent,
            string path,
            string endDate)
        {
            OriginatingStory = originatingStory;
            Name = locName;
            Event = locEvent;
            Path = path;
            EndDate = endDate;
        }

        public string OriginatingStory { get; set; }
        public string Name { get; set; }
        public string Event { get; set; }
        public string Path { get; set; }
        public string EndDate { get; set; }

        public override int GetHashCode() => HashCode.Combine(OriginatingStory, Name, Event, Path);
        public override bool Equals(object obj)
        {

            if (obj is not LocationRecord other) return false;

            if (OriginatingStory != other.OriginatingStory) return false;
            if (Name != other.Name) return false;
            if (Event != other.Event) return false;
            if (Path != other.Path) return false;

            return true;
        }

        public override string ToString() => $"{Path} @ {OriginatingStory} : {Event}, end: {EndDate}";

    }

    public static class LocationRecordDtoOps
    {
        public static LocationRecord[] SelectRecordsFilteringByStoryUids(string[] storyUids, LocationRecord[] allLocationRecords)
        {
            List<LocationRecord> selected = new();

            foreach (string storyUid in storyUids)
            {
                LocationRecord[] subset = allLocationRecords.Where(l => l.OriginatingStory == storyUid).ToArray();
                selected.AddRange(subset);
            }

            return selected.ToArray();
        }

        public static LocationRecord[] SelectRecordsWithDistinctPaths(LocationRecord[] records)
        {
            return records.GroupBy(r => r.Path).Select(r => r.First()).Distinct().ToArray();
        }
    }
}
