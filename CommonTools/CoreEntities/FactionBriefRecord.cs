﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    public class FactionBriefRecord
    {
        public FactionBriefRecord(string name, int intensity, string link, string shortDesc)
        {
            Name = name;
            Intensity = intensity;
            Link = link;
            ShortDesc = shortDesc;
        }

        public string Name { get; }
        public int Intensity { get; }
        public string Link { get; }
        public string ShortDesc { get; }

        public override int GetHashCode() => HashCode.Combine(Name, Intensity, Link, ShortDesc);
        public override bool Equals(object obj)
        {
            if (obj is not FactionBriefRecord other) return false;

            if (Name != other.Name) return false;
            if (Intensity != other.Intensity) return false;
            if (Link != other.Link) return false;
            if (ShortDesc != other.ShortDesc) return false;

            return true;
        }

        public override string ToString() => $"{Name} with intensity: {Intensity} at link: {Link}";
    }
}
