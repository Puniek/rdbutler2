﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    public class MotiveBriefRecord
    {
        public MotiveBriefRecord(string uid, string name, string startDate, string endDate, int storyCount, string[] players, string[] playerActors, string shortDesc)
        {
            MotiveUid = uid;
            Name = name;
            StartDate = startDate;
            EndDate = endDate;
            StoryCount = storyCount;
            Players = players;
            PlayerActors = playerActors;
            ShortDesc = shortDesc;
        }

        public string MotiveUid { get; }
        public string Name { get; }
        public string StartDate { get; }
        public string EndDate { get; }
        public int StoryCount { get; }
        public string[] Players { get; }
        public string[] PlayerActors { get; }
        public string ShortDesc { get; }


        public override int GetHashCode() => HashCode.Combine(MotiveUid, StartDate, EndDate, StoryCount, string.Join(", ", Players), string.Join(", ", PlayerActors));
        public override bool Equals(object obj)
        {
            if (obj is not MotiveBriefRecord other) return false;

            if (MotiveUid != other.MotiveUid) return false;
            if (Name != other.Name) return false;
            if (StartDate != other.StartDate) return false;
            if (EndDate != other.EndDate) return false;
            if (StartDate != other.StartDate) return false;
            if (StoryCount != other.StoryCount) return false;
            if (Players.SequenceEqual(other.Players) == false) return false;
            if (PlayerActors.SequenceEqual(other.PlayerActors) == false) return false;

            return true;
        }

        public override string ToString()
        {
            return $"{MotiveUid} @ {StartDate} - {EndDate}, stories: {StoryCount} with #player actors: {PlayerActors.Count()} for #players: {Players.Count()}";
        }
    }
}
