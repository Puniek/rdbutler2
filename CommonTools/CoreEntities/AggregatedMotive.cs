﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    public class AggregatedMotive
    {
        public AggregatedMotive(string uid, string name, string shortDesc, string fullDesc, string spoilers, GeneralStoryBlock[] storyBlocks)
        {
            Uid = uid;
            Name = name;
            ShortDesc = shortDesc;
            FullDesc = fullDesc;
            Spoilers = spoilers;
            StoryBlocks = storyBlocks;
        }

        public string Uid { get; }
        public string Name { get; }
        public string ShortDesc { get; }
        public string FullDesc { get; }
        public string Spoilers { get; }
        public GeneralStoryBlock[] StoryBlocks { get; }

        public override int GetHashCode() => HashCode.Combine(Uid, Name, ShortDesc, FullDesc, Spoilers);
        public override bool Equals(object obj)
        {
            if (obj is not AggregatedMotive other) return false;

            if (Uid != other.Uid) return false;
            if (Name != other.Name) return false;
            if (ShortDesc != other.ShortDesc) return false;
            if (FullDesc != other.FullDesc) return false;
            if (Spoilers != other.Spoilers) return false;

            return true;
        }

        public override string ToString() => $"{Uid} : {Name}, ShortDesc: {ShortDesc}, StoryBlocks.Count: {StoryBlocks.Length}";
    }
}
