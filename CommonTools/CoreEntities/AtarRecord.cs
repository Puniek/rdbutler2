﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commons.CoreEntities
{
    /// <summary>
    /// Actor-to-Actor Relation Record
    /// </summary>
    public class AtarRecord
    {
        public AtarRecord(
            string originatingActor,
            string relevantActor,
            int intensity,
            string[] storyUids)
        {
            TargetActor = originatingActor;
            RelevantActor = relevantActor;
            Intensity = intensity;
            StoryUids = storyUids;
        }

        public string TargetActor { get; }
        public string RelevantActor { get; }
        public int Intensity { get; }
        public string[] StoryUids { get; }

        public override int GetHashCode() => HashCode.Combine(TargetActor, RelevantActor, Intensity);
        public override bool Equals(object obj)
        {

            if (obj is not AtarRecord other) return false;

            if (TargetActor != other.TargetActor) return false;
            if (RelevantActor != other.RelevantActor) return false;
            if (Intensity != other.Intensity) return false;
            // At this time we don't care for sequence equality. StoryUids are irrelevant until a bug appears.

            return true;
        }

        public override string ToString()
        {
            return $"|{TargetActor}| to |{RelevantActor}| intensity: {Intensity} At: {string.Join("|", StoryUids)}";
        }
    }
}
