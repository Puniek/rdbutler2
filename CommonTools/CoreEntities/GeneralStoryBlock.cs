﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.CoreEntities
{
    public class GeneralStoryBlock
    {
        public GeneralStoryBlock(string storyUid, string storyTitle, string startDate, string endDate, string[] threads, string[] motives, string summary, string[] actorsPresent)
        {
            StoryUid = storyUid;
            StoryTitle = storyTitle;
            StartDate = startDate;
            EndDate = endDate;
            Threads = threads;
            Motives = motives;
            Summary = summary;
            AllActorsPresent = actorsPresent;
        }

        public string StoryUid { get; }
        public string StoryTitle { get; }
        public string StartDate { get; }
        public string EndDate { get; }
        public string[] Threads { get; }
        public string[] Motives { get; }
        public string Summary { get; }
        public string[] AllActorsPresent { get; }

        public override bool Equals(object obj)
        {
            if (obj is not GeneralStoryBlock other) return false;

            if (StoryUid != other.StoryUid) return false;
            if (StoryTitle != other.StoryTitle) return false;
            if (StartDate != other.StartDate) return false;
            if (EndDate != other.EndDate) return false;
            if (Summary != other.Summary) return false;
            if (!AllActorsPresent.SequenceEqual(other.AllActorsPresent)) return false;
            if (!Threads.SequenceEqual(other.Threads)) return false;
            if (!Motives.SequenceEqual(other.Motives)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StoryUid, StoryTitle, StartDate, EndDate, Summary, string.Join(", ", AllActorsPresent), string.Join(", ", Threads), string.Join(", ", Motives));
        }

        public override string ToString()
        {
            return $"{StoryUid} @ {StartDate} - {EndDate} , ActorCount: {AllActorsPresent.Length}, ThreadCount: {Threads.Length}, MotiveCount: {Motives.Length}";
        }
    }

}
