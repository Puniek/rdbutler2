﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Commons.Tools
{
    public static class FileOps
    {
        public static FileRecord[] ReadFiles(string directoryPath)
        {
            string[] filePaths = Directory.GetFiles(directoryPath);
            var fileContents = filePaths.Select(s => ReadFile(s));
            return fileContents.ToArray();
        }

        public static FileRecord ReadFile(string filePath)
        {
            string content = string.Empty;
            using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
            {
                content = reader.ReadToEnd();
            }
            return new FileRecord(path: filePath, content: content);
        }

        public static void SaveFile(string content, string filePath)
        {
            using (StreamWriter writer = new StreamWriter(path: filePath))
            {
                writer.Write(content);
            }
        }
    }

    /// <summary>
    /// FileRecord is a class representing a read file: identifier with a filePath.
    /// </summary>
    public class FileRecord
    {
        public FileRecord(string path, string content) { Path = path; Content = content; }

        public string Path { get; }
        public string Content { get; }
    }
}
