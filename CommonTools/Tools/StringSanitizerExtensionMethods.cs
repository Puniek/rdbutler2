﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Commons.Tools
{
    public static class StringSanitizerExtensionMethods
    {
        /// <summary>
        /// API does not usually accept spaces; by convention we are supposed to use '+'. 
        /// This method is supposed to get as good of a name as possible from API string.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string ReconstructActorNameFromApi(this string name)
        {
            TextInfo info = new CultureInfo("en-us").TextInfo;
            return info.ToTitleCase(name.Replace("+", " ").Replace("-", " "));
        }

        /// <summary>
        /// Actor or Faction can have Stories without having corresponding Profile. Therefore we need to be able to get it to the format of sorts.
        /// Generates ActorUid of type: `9999-widoczne-koty`
        /// </summary>
        public static string ToActorUid(this string actorName, string mechver = "9999") => mechver + "-" + actorName.Replace(" ", "-").ToLower();

        public static string ToGraphvizIdFormat(this string name)
        {
            // In Graphviz you can have an identifier starting from numbers, but the identifier can then only be a number
            // Therefore 211010_story becomes two nodes: 211010 and story. Therefore we need to prefix an identifier with a letter.
            // That way 211010_story -> A_211010_story which is a single identifier.
            string sanitized = name;
            if (char.IsDigit(name[0]))
                sanitized = "A_" + name;

            // Replace all symbols which Graphviz would consider to be 'special' with something easy to use.
            TextInfo info = new CultureInfo("en-us").TextInfo;
            return info.ToTitleCase(sanitized
                .Replace(" ", "_")
                .Replace("-", "_")
                .Replace(",", "_")
                .Replace(";", "_")
                .Replace("|", "_")
                );
        }

        public static string EscapeToGraphvizHtml(this string text)
        {
            return text
                .Replace("\"", "&quot;")
                .Replace("\n", "<br/>").Replace("\\n", "<br/>");
        }
    }
}
