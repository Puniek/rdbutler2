﻿using System;
using System.Text.RegularExpressions;

namespace Commons.Tools
{
    public static class CorrectRdDate
    {
        /// <summary>
        /// This method corrects damaged and incomplete dates in order to make their comparison possible.
        /// </summary>
        public static string CorrectTextual(string inputDate)
        {
            var parser = new Regex(@"(\d{2,4})-?(\d{2})?-?(\d{2})?");
            var match = parser.Match(inputDate);

            string year = match.Groups[1].Value.PadLeft(4, '0');
            string month = string.IsNullOrEmpty(match.Groups[2].Value) ? "01" : match.Groups[2].Value;
            string day = string.IsNullOrEmpty(match.Groups[3].Value) ? "01" : match.Groups[3].Value;

            return $"{year}-{month}-{day}";
        }
    }
}
