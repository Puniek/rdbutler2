﻿using Microsoft.Extensions.FileSystemGlobbing.Internal;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using static System.Net.Mime.MediaTypeNames;

namespace Commons.Tools
{
    public static class TextParserEngine
    {
            
        /// <summary>
        /// This approach means that if I ever get an empty match I get an empty collection. That way I don't have
        /// to deal with nulls, ever - thus string.Empty (no sensible default).
        /// </summary>
        public static string SafelyExtractSingleCapturedElement(string pattern, string text)
        {
            MatchCollection matches = new Regex(pattern, RegexOptions.Multiline | RegexOptions.Singleline)
                .Matches(text);

            string[] allResults = matches.Select(m => m.Groups[1].Value).ToArray();

            if (allResults.Length > 0) return allResults.First().Trim();
            else return string.Empty;
        }

        public static string SafelyExtractSingleCapturedElementFromSingleLine(string pattern, string text)
        {
            MatchCollection matches = new Regex(pattern)
                .Matches(text);

            string[] allResults = matches.Select(m => m.Groups[1].Value).ToArray();

            if (allResults.Length > 0) return allResults.First().Trim();
            else return string.Empty;
        }
    }

}
