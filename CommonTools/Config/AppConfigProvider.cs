﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Config
{
    /// <summary>
    /// This class parses the Settings.local file, built according to Settings.template file.
    /// Because of the stakeholders, I am not using standard C# .settings file.
    /// If there is no local file an exception shall be thrown.
    /// Look at localSettings.json, explanation in localSettings.template.
    /// </summary>
    public class AppConfigProvider
    {
        IConfigurationRoot _config;

        public AppConfigProvider()
        {
            try
            {
                _config = new ConfigurationBuilder()
                    .AddJsonFile("Config/localSettings.json", false, true)
                    .Build();
            } catch (Exception ex)
            {
                throw new InvalidOperationException("'Config/localSettings.json' is not working. Probably localSettings.json is malformed / not existing.", ex);
            }
            
        }

        

        public string GetPathToStoryFolder()
        {
            var path = _config["riggeddice-story-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }

        public string GetPathToActorFolder()
        {
            var path = _config["riggeddice-actor-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }

        public string GetPathToThreadOutputFolder()
        {
            var path = _config["riggeddice-thread-output-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }

        public string GetPathToActorOutputFolder()
        {
            var path = _config["riggeddice-actor-output-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }

        public string GetPathToGeneralOutputFolder()
        {
            var path = _config["riggeddice-output-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }
        public string GetPathToGeneratorInputFolder()
        {
            var path = _config["riggeddice-generator-input-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }

        /// <summary>
        /// By default, RiggedDice SQLite database file is located somewhere on HDD. But because of Docker configuration, we set the 
        /// 'default' as "app/db/riggeddice.db" to make it simple to use it in Docker.
        /// </summary>
        /// <returns></returns>
        public string GetPathToDatabaseFolder()
        {
            if (Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER") == "true")
                return _config["riggeddice-sqlite-db-docker-path"];
            return _config["riggeddice-sqlite-db-folder-path"];
        }

        /// <summary>
        /// By default, RiggedDice SQLite database file is located somewhere on HDD. But because of Docker configuration, we set the 
        /// 'default' as "app/db/riggeddice.db" to make it simple to use it in Docker.
        /// </summary>
        /// <returns></returns>
        public string GetPathToDatabaseFile()
        {
            return GetPathToDatabaseFolder() + "/" + _config["riggeddice-sqlite-db-file-name"];
        }

        public string GetPathToStoryThreadFolder()
        {
            var path = _config["riggeddice-thread-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }

        public string GetPathToStoryMotiveFolder()
        {
            var path = _config["riggeddice-motive-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }

        public string GetPathToFactionFolder()
        {
            var path = _config["riggeddice-faction-folder"];
            if (path == null)
                throw new InvalidOperationException("localSettings.json file @ Commons.Config is not set with local info. Please consult with localSettings.template.");

            return path;
        }
    }
}
