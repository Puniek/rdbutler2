﻿using Commons.Config;
using Commons.Infrastructure.DbModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Commons.Infrastructure
{
    public class RdContext : DbContext
    {
        public virtual DbSet<ReadActorModel> ReadActorModels { get; set; }
        public virtual DbSet<ReadStoryModel> ReadStoryModels { get; set; }
        public virtual DbSet<ReadFactionModel> ReadFactionModels { get; set; }
        public virtual DbSet<StoryToPlayerRelation> StoryToPlayerRelations { get; internal set; }
        public virtual DbSet<StoryToGmRelation> StoryToGmRelations { get; internal set; }
        public virtual DbSet<StoryToPrevChronoRelation> StoryToPrevChronoRelations { get; internal set; }
        public virtual DbSet<StoryToThreadRelation> StoryToThreadRelations { get; internal set; }
        public virtual DbSet<LocationRecordModel> LocationRecordModels { get; set; }
        public virtual DbSet<AggregatedStoryModel> AggregatedStoryModels { get; set; }
        public virtual DbSet<AtarRecordModel> AtarRecordModels { get; set; }
        public virtual DbSet<EveryActorModel> EveryActorModels { get; set; }
        public virtual DbSet<AggregatedProgressionModel> AggregatedProgressionModels { get; set; }
        public virtual DbSet<AggregatedMeritModel> AggregatedMeritModels { get; set; }
        public virtual DbSet<AggregatedFactionMeritModel> AggregatedFactionMeritModels { get; set; }
        public virtual DbSet<ActorToFactionModel> ActorToFactionModels { get; set; }
        public virtual DbSet<ActorBriefRecordModel> ActorBriefRecordModels { get; set; }
        public virtual DbSet<ActorFlashcardModel> ActorFlashcardModels { get; set; }
        public virtual DbSet<FlashcardRecordModel> FlashcardRecordModels { get; set; }
        public virtual DbSet<StoryBriefRecordModel> StoryBriefRecordModels { get; set; }
        public virtual DbSet<ActorThreadStoryBlockModel> ActorThreadStoryBlockModels { get; set; }
        public virtual DbSet<GeneralStoryBlockModel> GeneralStoryBlockModels { get; set; }
        public virtual DbSet<ReadThreadModel> ReadThreadModels { get; set; }
        public virtual DbSet<ReadMotiveModel> ReadMotiveModels { get; set; }
        public virtual DbSet<ThreadBriefRecordModel> ThreadBriefRecordModels { get; set; }
        public virtual DbSet<MotiveBriefRecordModel> MotiveBriefRecordModels { get; set; }
        public virtual DbSet<FactionBriefRecordModel> FactionBriefRecordModels { get; set; }
        public virtual DbSet<LocationTreeJsonModel> LocationTreeJsonModels { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Console.WriteLine($"DIAGNOSTICS: Data Source={new AppConfigProvider().GetPathToDatabaseFolder()}");
            optionsBuilder.UseSqlite($"Data Source={new AppConfigProvider().GetPathToDatabaseFile()}");
        }
    }
}
