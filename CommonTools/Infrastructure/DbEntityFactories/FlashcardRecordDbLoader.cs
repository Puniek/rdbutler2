using System.Linq;

using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories;

public static class FlashcardRecordDbLoader
{
    public static FlashcardRecord[] CreateMany(FlashcardRecordModel[] flashcardRecordModels) => flashcardRecordModels.Select(CreateSingle).ToArray();

    public static FlashcardRecord CreateSingle(FlashcardRecordModel flashcardRecordModel) =>
        new FlashcardRecord(originStoryUid: flashcardRecordModel.OriginStoryUid, body: flashcardRecordModel.Body);
}