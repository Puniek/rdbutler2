﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class ActorThreadStoryBlockDbLoader
    {
        public static ActorThreadStoryBlock[] CreateMany(ActorThreadStoryBlockModel[] models)
        {
            return models.Select(m => CreateSingle(m)).ToArray();
        }

        public static ActorThreadStoryBlock CreateSingle(ActorThreadStoryBlockModel model)
        {
            return new ActorThreadStoryBlock(
                originatingActor: model.OriginatingActor,
                storyUid: model.StoryUid,
                storyTitle: model.StoryTitle,
                startDate: model.StartDate,
                endDate: model.EndDate,
                summary: model.Summary,
                actorDeed: model.TargetActorDeed,
                actorProgressions: !string.IsNullOrEmpty(model.TargetActorProgressions) ? model.TargetActorProgressions.Split("\n") : new string[0],
                actorsPresent: model.AllActorsPresent.Split(", ")
                );
        }
    }   
}
