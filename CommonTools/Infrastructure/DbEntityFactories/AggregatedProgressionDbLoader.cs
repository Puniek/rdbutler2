﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AggregatedProgressionDbLoader
    {
        public static AcdeorPlus[] CreateMany(AggregatedProgressionModel[] plans)
        {
            return plans.Select(p => CreateSingle(p)).ToArray();
        }

        public static AcdeorPlus CreateSingle(AggregatedProgressionModel aModel)
        {
            return new AcdeorPlus(
                originUid: aModel.OriginUid,
                actor: aModel.Actor,
                deed: aModel.Deed,
                endDate: aModel.EndDate
                );
        }
    }
}
