﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class LocationRecordDbLoader
    {
        public static LocationRecord CreateSingle(LocationRecordModel r)
        {
            return new LocationRecord(
                originatingStory: r.OriginatingStory,
                locName: r.Actor,
                locEvent: r.Deed,
                path: r.Path,
                endDate: r.EndDate
                );
        }
    }
}
