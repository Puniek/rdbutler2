﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AggregatedMeritDbLoader
    {
        public static AggregatedMerit[] CreateMany(AggregatedMeritModel[] aMeritModels)
        {
            return aMeritModels.Select(m => CreateSingle(m)).ToArray();
        }

        public static AggregatedMerit CreateSingle(AggregatedMeritModel aMeritModel)
        {
            return new AggregatedMerit
            (
                originUid: aMeritModel.OriginUid,
                actor: aMeritModel.Actor,
                deed: aMeritModel.Deed,
                threads: aMeritModel.Threads,
                startDate: aMeritModel.StartDate,
                endDate: aMeritModel.EndDate
            );
        }
    }
}
