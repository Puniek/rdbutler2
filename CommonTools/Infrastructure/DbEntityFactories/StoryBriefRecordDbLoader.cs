﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class StoryBriefRecordDbLoader
    {
        public static StoryBriefRecord CreateSingle(StoryBriefRecordModel briefModel)
        {
            return new StoryBriefRecord(
                storyUid: briefModel.StoryUid,
                title: briefModel.Title,
                threads: briefModel.Threads,
                motives: briefModel.Motives,
                startDate: briefModel.StartDate,
                endDate: briefModel.EndDate,
                players: briefModel.Players);
        }
    }
}
