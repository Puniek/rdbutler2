﻿using Commons.CoreEntities;

using System.Linq;

using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AtarRecordDbLoader
    {
        public static AtarRecord[] CreateMany(AtarRecordModel[] atars)
        {
            AtarRecord[] records = atars.Select(a => CreateSingle(a)).ToArray();
            return records.OrderByDescending(r => r.Intensity).ThenBy(r => r.RelevantActor).ToArray();
        }

        public static AtarRecord CreateSingle(AtarRecordModel aRecord)
        {
            return new AtarRecord(
                originatingActor: aRecord.OriginatingActor,
                relevantActor: aRecord.RelevantActor,
                intensity: aRecord.Intensity,
                storyUids: aRecord.StoryUids.Split(",").Select(r => r.Trim()).ToArray()
                );
        }
    }
}
