﻿using Commons.CoreEntities;
using System;
using System.Linq;

using Commons.Infrastructure.DbModels;

namespace Commons.Infrastructure.DbEntityFactories
{
    public static class AggregatedActorDbLoader
    {
        public static AggregatedActor Create(EveryActorModel aActorModel,
            AggregatedMeritModel[] aMeritModels,
            AggregatedProgressionModel[] aProgressionModels,
            AtarRecordModel[] atarModels, FlashcardRecordModel[] flashcardRecordModels, string[] threads)
        {
            string uid = aActorModel.ActorUid;
            string name = aActorModel.Name;
            string mechver = aActorModel.Mechver;
            string owner = aActorModel.Owner;
            string body = aActorModel.Body;

            AggregatedMeritModel[] merits = aMeritModels.ToArray();
            AggregatedProgressionModel[] progressions = aProgressionModels.ToArray();
            AtarRecordModel[] atars = atarModels.ToArray();

            return new AggregatedActor(
                uid: uid,
                name: name,
                mechver: mechver,
                factions: Array.Empty<string>(),
                owner: owner,
                body: body,
                merits: AggregatedMeritDbLoader.CreateMany(merits).OrderBy(m => m.EndDate).ThenBy(m => m.OriginUid).ToArray(),
                progressions: AggregatedProgressionDbLoader.CreateMany(progressions).OrderBy(m => m.EndDate).ThenBy(m => m.OriginUid).ToArray(),
                atarRelations: AtarRecordDbLoader.CreateMany(atars),
                flashcardRecords: FlashcardRecordDbLoader.CreateMany(flashcardRecordModels),
                threads: threads
            );
        }

        public static AggregatedActor EmptyAndInvalid()
        {
            return new AggregatedActor(
                uid: "9999-INVALID-RECORD",
                name: "INVALID-RECORD",
                mechver: "9999",
                factions: Array.Empty<string>(),
                owner: string.Empty,
                body: string.Empty,
                merits: Array.Empty<AggregatedMerit>(),
                progressions: Array.Empty<AcdeorPlus>(),
                atarRelations: Array.Empty<AtarRecord>(),
                flashcardRecords: Array.Empty<FlashcardRecord>(),
                threads: Array.Empty<string>()
            );
        }
    }
}
