﻿using Commons.CoreEntities;
using Commons.Infrastructure.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbEntityFactories
{
    public class AggregatedThreadDbLoader
    {
        public static AggregatedThread CreateSingle(string threadUid, ReadThreadModel readThread, GeneralStoryBlockModel[] relevantStoryBlocks)
        {
            return new AggregatedThread(
                uid: threadUid,
                name: readThread != null ? readThread.Name : "",
                shortDesc: readThread != null ? readThread.ShortDesc : "",
                fullDesc: readThread != null ? readThread.FullDesc : "",
                spoilers: readThread != null ? readThread.Spoilers : "",
                storyBlocks: relevantStoryBlocks.Select(b => GeneralStoryBlockDbLoader.CreateSingle(b)).ToArray());
        }
    }
}
