﻿using Commons.CoreEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Commons.Infrastructure.Repos
{
    public interface IRepository
    {
        Task<AggregatedActor> GetActorByUid(string actorUid);
        Task<AggregatedActor[]> GetActorsByUids(string[] actorUids);
        Task<string> GetUniqueLocationRecordTree();
        Task<ActorBriefRecord[]> GetActorBriefs(Dictionary<string, string> filters);
        Task<FactionBriefRecord[]> GetFactionBriefs(Dictionary<string, string> filters);
        Task<AggregatedStory[]> GetAllAggregatedStories();
        Task<AggregatedMerit[]> GetAllAggregatedMerits();
        Task<AcdeorPlus[]> GetAllAggregatedProgressions();
        Task<StoryBriefRecord[]> GetStoryBriefs(Dictionary<string, string> filters);
        Task<AggregatedStory> GetStoryByUid(string storyUid);
        Task<ActorThreadStoryBlock[]> GetActorThreadStoryBlockByUid(string actorUid);
        Task<ThreadBriefRecord[]> GetThreadBriefs(Dictionary<string, string> filters);
        Task<MotiveBriefRecord[]> GetMotiveBriefs(Dictionary<string, string> filters);
        Task<AggregatedThread> GetThreadByUid(string threadUid);
        Task<AggregatedMotive> GetMotiveByUid(string motiveUid);
        Task<AggregatedSingleLocationDetails> GetSingleLocationDetails(string locationPath);
    }
}
