﻿using System;
using System.Collections.Generic;

using Commons.CoreEntities;

using System.Linq;
using System.Threading.Tasks;

using Commons.Infrastructure.DbEntityFactories;
using Commons.Infrastructure.DbModels;

using Microsoft.EntityFrameworkCore;
using Commons.Config;
using Commons.Tools;
using System.Threading;

namespace Commons.Infrastructure.Repos
{
    public class SqliteRepository : IRepository
    {
        public async Task<AggregatedActor> GetActorByUid(string actorUid) => (await GetActorsByUids(new[] { actorUid })).FirstOrDefault();

        public async Task<AggregatedActor[]> GetActorsByUids(string[] actorUids)
        {
            var actors = new List<AggregatedActor>();

            var loweredActorUids = actorUids.Select(x => x.ToLower()).ToHashSet();

            using (var db = new RdContext())
            {
                foreach (string loweredActorUid in loweredActorUids)
                {
                    EveryActorModel everyActorModel = await db.EveryActorModels.FirstOrDefaultAsync(a => a.ActorUid.ToLower() == loweredActorUid);
                    if (everyActorModel == null) return null;

                    string lowerName = everyActorModel.Name.ToLower();

                    AggregatedMeritModel[] aMeritModels = await db.AggregatedMeritModels.Where(m => m.Actor.ToLower() == lowerName).ToArrayAsync();
                    AggregatedProgressionModel[] aProgressionModels = await db.AggregatedProgressionModels.Where(m => m.Actor.ToLower() == lowerName).ToArrayAsync();
                    AtarRecordModel[] atarModels = await db.AtarRecordModels.Where(m => m.OriginatingActor.ToLower() == lowerName).ToArrayAsync();
                    FlashcardRecordModel[] flashcards = (await db.ActorFlashcardModels.Include(x => x.Records).Where(x => x.Name.ToLower() == lowerName).FirstOrDefaultAsync())?.Records.ToArray() ?? Array.Empty<FlashcardRecordModel>();
                    var relatedStoriesIds = flashcards.Select(x => x.OriginStoryUid).ToHashSet();
                    string[] threads = await db.StoryToThreadRelations.Where(x => relatedStoriesIds.Contains(x.StoryId)).Select(x => x.ThreadId).ToArrayAsync();
                    threads = threads.Distinct().ToArray();

                    actors.Add(AggregatedActorDbLoader.Create(everyActorModel, aMeritModels, aProgressionModels, atarModels, flashcards, threads));
                }
            }


            return actors.ToArray();
        }

        public async Task<ActorBriefRecord[]> GetActorBriefs(Dictionary<string, string> filters)
        {
            ActorBriefRecord[] briefs = null;

            using (var db = new RdContext())
            {
                IQueryable<ActorBriefRecordModel> query = db.ActorBriefRecordModels;

                if (filters.TryGetValue("minIntensity", out string intensity))
                {
                    int minIntensity = int.TryParse(intensity, out int temp) ? temp : 0;
                    query = query.Where(abrm => abrm.Intensity > minIntensity);
                }

                // In other words, ANYTHING related to dates. Dates, as usual, suck.
                if (filters.TryGetValue("startDate", out string startDate) || filters.TryGetValue("endDate", out string endDate))
                {
                    // If we have both startDate and endDate, use both to make a range.
                    if (filters.TryGetValue("startDate", out startDate) && filters.TryGetValue("endDate", out endDate))
                    {
                        string startFiltering = CorrectRdDate.CorrectTextual(startDate);
                        string endFiltering = CorrectRdDate.CorrectTextual(endDate);

                        query = query.Where(
                            abrm => db.AggregatedMeritModels
                                .Where(amm => amm.Actor == abrm.Name)
                                .Any(amm => string.Compare(amm.StartDate, startFiltering) >= 0 && string.Compare(amm.EndDate, endFiltering) <= 0)
                        );
                    }
                    // If we have only startDate, use it as a starting point
                    else if (filters.TryGetValue("startDate", out startDate) && !filters.TryGetValue("endDate", out endDate))
                    {
                        string startFiltering = CorrectRdDate.CorrectTextual(startDate);
                        query = query.Where(
                            abrm => db.AggregatedMeritModels
                                .Where(amm => amm.Actor == abrm.Name)
                                .Any(amm => string.Compare(amm.StartDate, startFiltering) >= 0)
                            );
                    }
                    // If we have only endDate, use it as ending point
                    else if (filters.TryGetValue("endDate", out endDate) && !filters.TryGetValue("startDate", out startDate))
                    {
                        string endFiltering = CorrectRdDate.CorrectTextual(endDate);

                        query = query.Where(
                            abrm => db.AggregatedMeritModels
                                .Where(amm => amm.Actor == abrm.Name)
                                .Any(amm => string.Compare(amm.EndDate, endFiltering) <= 0)
                            );
                    }
                }

                if (filters.TryGetValue("players", out string players))
                {
                    query = query.Where(
                        abrm => db.AggregatedStoryModels.Any(
                                asm => asm.AllActors.ToLower().Contains(abrm.Name.ToLower()) &&
                                asm.Players.ToLower().Contains(players.ToLower())
                            ));
                }
                if (filters.TryGetValue("gm", out string gm))
                {
                    query = query.Where(
                        abrm => db.AggregatedStoryModels.Any(
                                asm => asm.AllActors.ToLower().Contains(abrm.Name.ToLower()) &&
                                asm.Gms.ToLower().Contains(gm.ToLower())
                            ));
                }
                if (filters.TryGetValue("threads", out string threads))
                {
                    query = query.Where(
                        abrm => db.AggregatedStoryModels.Any(
                                asm => asm.AllActors.ToLower().Contains(abrm.Name.ToLower()) &&
                                asm.Threads.ToLower().Contains(threads.ToLower())
                            ));
                }
                if (filters.TryGetValue("motives", out string motives))
                {
                    query = query.Where(
                        abrm => db.AggregatedStoryModels.Any(
                                asm => asm.AllActors.ToLower().Contains(abrm.Name.ToLower()) &&
                                asm.Motives.ToLower().Contains(motives.ToLower())
                            ));
                }
                if (filters.TryGetValue("locations", out string location))
                {
                    var locationModels = db.LocationRecordModels
                        .Where(lrm => lrm.Actor.ToLower().Contains(location.ToLower()))
                        .Select(lrm => lrm.OriginatingStory);

                    var actorNames = db.AggregatedMeritModels
                        .Where(amm => locationModels.Contains(amm.OriginUid))
                        .Select(amm => amm.Actor);

                    query = query.Where(abrm => actorNames.Contains(abrm.Name));

                }
                if (filters.TryGetValue("actors", out string actor))
                {
                    query = query.Where(
                        abrm => db.AggregatedStoryModels.Any(
                                asm => asm.AllActors.ToLower().Contains(abrm.Name.ToLower()) &&
                                db.AggregatedMeritModels.Any(
                                    amm => amm.OriginUid == asm.OriginatingStory &&
                                    amm.Actor.ToLower().Contains(actor.ToLower())
                            )));
                }

                briefs = await query.Select(m => ActorBriefRecordDbLoader.CreateSingle(m)).ToArrayAsync();
            }

            return briefs;
        }

        public async Task<FactionBriefRecord[]> GetFactionBriefs(Dictionary<string, string> filters)
        {
            FactionBriefRecord[] briefs = null;

            using (var db = new RdContext())
            {
                briefs = await db.FactionBriefRecordModels.Select(m => FactionBriefRecordDbLoader.CreateSingle(m)).ToArrayAsync();
            }

            return briefs;
        }

        public async Task<AggregatedMerit[]> GetAllAggregatedMerits()
        {
            AggregatedMerit[] dtos = null;

            using (var db = new RdContext())
            {
                dtos = await db.AggregatedMeritModels.Select(m => AggregatedMeritDbLoader.CreateSingle(m)).ToArrayAsync();
            }

            return dtos;
        }

        public async Task<AcdeorPlus[]> GetAllAggregatedProgressions()
        {
            AcdeorPlus[] dtos = null;

            using (var db = new RdContext())
            {
                dtos = await db.AggregatedProgressionModels.Select(p => AggregatedProgressionDbLoader.CreateSingle(p)).ToArrayAsync();
            }

            return dtos;
        }

        public async Task<AggregatedStory[]> GetAllAggregatedStories()
        {
            AggregatedStory[] infos = null;

            using (var db = new RdContext())
            {
                infos = await db.AggregatedStoryModels.Select(s => AggregatedStoryDbLoader.CreateSingle(s)).ToArrayAsync();
            }

            return infos;
        }

        public async Task<StoryBriefRecord[]> GetStoryBriefs(Dictionary<string, string> filters)
        {
            StoryBriefRecord[] briefs = null;

            using (var db = new RdContext())
            {
                IQueryable<StoryBriefRecordModel> query = db.StoryBriefRecordModels;

                if (filters.TryGetValue("startDate", out string startDate))
                {
                    string startFiltering = CorrectRdDate.CorrectTextual(startDate);
                    query = query.Where(sbrm => string.Compare(sbrm.StartDate, startFiltering) >= 0);
                }
                if (filters.TryGetValue("endDate", out string endDate))
                {
                    string endFiltering = CorrectRdDate.CorrectTextual(endDate);
                    query = query.Where(sbrm => string.Compare(sbrm.EndDate, endFiltering) <= 0);
                }
                if (filters.TryGetValue("threads", out string threads))
                {
                    query = query.Where(sbrm => sbrm.Threads.ToLower().Contains(threads.ToLower()));
                }
                if (filters.TryGetValue("motives", out string motives))
                {
                    query = query.Where(sbrm => sbrm.Motives.ToLower().Contains(motives.ToLower()));
                }
                if (filters.TryGetValue("players", out string players))
                {
                    query = query.Where(sbrm => sbrm.Players.ToLower().Contains(players.ToLower()));
                }
                if (filters.TryGetValue("locations", out string location))
                {
                    // We are using ACTOR here not PATH because I don't want Cartesian Matrix Explosion. Look at data if you have any doubts.
                    query = query.Where(
                        sbrm => db.LocationRecordModels.Any(
                                lrm => lrm.OriginatingStory == sbrm.StoryUid && lrm.Actor.ToLower().Contains(location.ToLower())
                            ));
                }
                if (filters.TryGetValue("actors", out string actor))
                {
                    query = query.Where(
                        sbrm => db.AggregatedMeritModels.Any(
                                amm => amm.OriginUid == sbrm.StoryUid && amm.Actor.ToLower().Contains(actor.ToLower())
                            ));
                }
                if (filters.TryGetValue("gm", out string gm))
                {
                    query = query.Where(
                        sbrm => db.StoryToGmRelations.Any(
                                stgmr => stgmr.StoryId == sbrm.StoryUid && stgmr.GmId.ToLower().Contains(gm.ToLower())
                            ));
                }

                briefs = await query.Select(m => StoryBriefRecordDbLoader.CreateSingle(m)).ToArrayAsync();
            }

            return briefs;
        }

        public async Task<string> GetUniqueLocationRecordTree()
        {
            string locationTreeJSon = string.Empty;

            using (var db = new RdContext())
            {
                var locationTreeJsonModel = await db.LocationTreeJsonModels.FirstOrDefaultAsync();
                locationTreeJSon = locationTreeJsonModel.Tree;
            }

            return locationTreeJSon;
        }

        public async Task<AggregatedStory> GetStoryByUid(string storyUid)
        {
            var loweredUid = storyUid.ToLower();

            AggregatedStory story = null;

            using (var db = new RdContext())
            {
                AggregatedStoryModel model = await db.AggregatedStoryModels.FirstOrDefaultAsync(a => a.OriginatingStory.ToLower() == loweredUid);
                if (model != null)
                    story = AggregatedStoryDbLoader.CreateSingle(model);
            }

            return story;
        }

        public async Task<ActorThreadStoryBlock[]> GetActorThreadStoryBlockByUid(string actorUid)
        {
            ActorThreadStoryBlock[] blocks = Array.Empty<ActorThreadStoryBlock>();

            using (var db = new RdContext())
            {
                string aName = await db.EveryActorModels
                    .Where(a => a.ActorUid == actorUid)
                    .Select(a => a.Name)
                    .FirstOrDefaultAsync();

                ActorThreadStoryBlockModel[] models = await db.ActorThreadStoryBlockModels.Where(a => a.OriginatingActor.ToLower() == aName.ToLower()).ToArrayAsync();
                blocks = ActorThreadStoryBlockDbLoader.CreateMany(models);
            }

            return blocks;
        }

        public async Task<ThreadBriefRecord[]> GetThreadBriefs(Dictionary<string, string> filters)
        {
            ThreadBriefRecord[] records = Array.Empty<ThreadBriefRecord>();

            using (var db = new RdContext())
            {
                records = await db.ThreadBriefRecordModels.Select(r => ThreadBriefRecordDbLoader.CreateSingle(r)).ToArrayAsync();
            }

            return records;
        }

        public async Task<MotiveBriefRecord[]> GetMotiveBriefs(Dictionary<string, string> filters)
        {
            MotiveBriefRecord[] records = Array.Empty<MotiveBriefRecord>();

            using (var db = new RdContext())
            {
                records = await db.MotiveBriefRecordModels.Select(r => MotiveBriefRecordDbLoader.CreateSingle(r)).ToArrayAsync();
            }

            return records;
        }

        public async Task<AggregatedThread> GetThreadByUid(string threadUid)
        {
            AggregatedThread thread = null;

            using (var db = new RdContext())
            {
                ReadThreadModel readThread = await db.ReadThreadModels.Where(t => t.ThreadUid.ToLower().Contains(threadUid.ToLower())).FirstOrDefaultAsync();
                GeneralStoryBlockModel[] relevantStoryBlocks = await db.GeneralStoryBlockModels.Where(b => b.Threads.Contains(threadUid.ToLower())).ToArrayAsync();

                thread = AggregatedThreadDbLoader.CreateSingle(threadUid, readThread, relevantStoryBlocks.Reverse().ToArray());
            }

            return thread;
        }
        public async Task<AggregatedMotive> GetMotiveByUid(string motiveUid)
        {
            AggregatedMotive motive = null;

            using (var db = new RdContext())
            {
                ReadMotiveModel readMotive = await db.ReadMotiveModels.Where(m => m.MotiveUid.ToLower().Contains(motiveUid.ToLower())).FirstOrDefaultAsync();
                GeneralStoryBlockModel[] relevantStoryBlocks = await db.GeneralStoryBlockModels.Where(b => b.Motives.Contains(motiveUid.ToLower())).ToArrayAsync();

                motive = AggregatedMotiveDbLoader.CreateSingle(motiveUid, readMotive, relevantStoryBlocks.Reverse().ToArray());
            }

            return motive;
        }
        public async Task<AggregatedSingleLocationDetails> GetSingleLocationDetails(string locationPath)
        {
            AggregatedSingleLocationDetails details = null;

            using (var db = new RdContext())
            {
                LocationRecordModel[] lrms = await db.LocationRecordModels.Where(m => m.Path == locationPath).ToArrayAsync();
                string[] uniqueStoryUids = lrms.Select(l => l.OriginatingStory).Distinct().ToArray();

                GeneralStoryBlockModel[] blocks = await db.GeneralStoryBlockModels.Where(b => uniqueStoryUids.Contains(b.StoryUid)).ToArrayAsync();

                if(lrms.Length > 0) 
                {
                    details = AggregatedSingleLocationDetailsDbLoader.CreateSingle(
                        lrms.Select(m => LocationRecordDbLoader.CreateSingle(m)).ToArray(), 
                        blocks.Select(m => GeneralStoryBlockDbLoader.CreateSingle(m)).ToArray());
                }
            }

            return details;
        }
    }
}
