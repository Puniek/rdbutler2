﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{

    public class EveryActorModel
    {
        [Key]
        public int Uid { get; set; }
        public string ActorUid { get; set; }
        public string Name { get; set; }
        public string Mechver { get; set; }
        public string Owner { get; set; }
        public string Body { get; set; }
    }

}
