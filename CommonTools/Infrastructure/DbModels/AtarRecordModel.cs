﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{

    public class AtarRecordModel
    {
        [Key]
        public int Uid { get; set; }
        public string OriginatingActor { get; set; }
        public string RelevantActor { get; set; }
        public int Intensity { get; set; }
        public string StoryUids { get; set; }
    }

}
