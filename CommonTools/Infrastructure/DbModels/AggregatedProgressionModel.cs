﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{
    public class AggregatedProgressionModel
    {
        [Key]
        public int Uid { get; set; }
        public string Actor { get; set; }
        public string OriginUid { get; set; }
        public string Deed { get; set; }
        public string EndDate { get; set; }
    }

}
