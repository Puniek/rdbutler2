﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbModels
{
    public class ReadFactionModel
    {
        [Key]
        public int Id { get; set; }
        public string FactionId { get; set; }
        public string Name { get; set; }
        public string ShortDesc { get; set; }
        public string Body { get; set; }
    }
}
