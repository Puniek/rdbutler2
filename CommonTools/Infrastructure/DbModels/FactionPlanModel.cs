﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{
    public class FactionPlanModel
    {
        [Key]
        public int Uid { get; set; }
        public string OriginatingStory { get; set; }
        public string Actor { get; set; }
        public string Change { get; set; }
    }

}
