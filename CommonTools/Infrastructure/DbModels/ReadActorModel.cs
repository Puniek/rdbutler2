﻿using System.ComponentModel.DataAnnotations;

namespace Commons.Infrastructure.DbModels
{
    public class ReadActorModel
    {
        [Key]
        public string Uid { get; set; }
        public string Body { get; set; }
        public string Mechver { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
    }
}
