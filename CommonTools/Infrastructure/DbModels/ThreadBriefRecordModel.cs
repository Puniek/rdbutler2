﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbModels
{
    public class ThreadBriefRecordModel
    {
        [Key]
        public int Uid { get; set; }
        public string ThreadUid { get; set; }
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int StoryCount { get; set; }
        public string Players { get; set; }
        public string PlayerActors { get; set; }
        public string ShortDesc { get; set; }
    }
}
