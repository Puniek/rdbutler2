﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{
    public class AggregatedMeritModel
    {
        [Key]
        public int Uid { get; set; }
        public string Actor { get; set; }
        public string Threads { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Deed { get; set; }
        public string OriginUid { get; set; }
    }

}
