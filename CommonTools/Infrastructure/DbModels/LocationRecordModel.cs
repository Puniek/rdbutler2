﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{
    public class LocationRecordModel
    {
        [Key]
        public int Uid { get; set; }
        public string OriginatingStory { get; set; }
        public string Actor { get; set; }
        public string Deed { get; set; }
        public string Path { get; set; }
        public string EndDate { get; set; }
    }

}
