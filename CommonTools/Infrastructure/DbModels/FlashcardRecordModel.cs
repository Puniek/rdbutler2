using System.ComponentModel.DataAnnotations;

namespace Commons.Infrastructure.DbModels;

public class FlashcardRecordModel
{
    [Key]
    public int Id { get; set; }
    public string OriginStoryUid { get; set; }
    public string Body { get; set; }

    public ReadStoryModel Story { get; }
}