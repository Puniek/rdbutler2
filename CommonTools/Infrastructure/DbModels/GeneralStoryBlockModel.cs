﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbModels
{
    public class GeneralStoryBlockModel
    {
        [Key]
        public string StoryUid { get; set; }
        public string StoryTitle { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Threads { get; set; }
        public string Motives { get; set; }
        public string Summary { get; set; }
        public string AllActorsPresent { get; set; }
    }

}
