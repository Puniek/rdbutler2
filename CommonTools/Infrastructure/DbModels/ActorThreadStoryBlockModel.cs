﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Infrastructure.DbModels
{
    public class ActorThreadStoryBlockModel
    {
        [Key]
        public int Id { get; set; }
        public string OriginatingActor { get; set; }
        public string StoryUid { get; set; }
        public string StoryTitle { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Summary { get; set; }
        public string TargetActorDeed { get; set; }
        public string TargetActorProgressions { get; set; }
        public string AllActorsPresent { get; set; }
    }
}
