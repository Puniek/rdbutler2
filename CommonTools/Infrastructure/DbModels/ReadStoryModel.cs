﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{
    public class ReadStoryModel
    {
        [Key]
        public string Uid { get; set; }
        public string Title { get; set; }
        public string Threads { get; set; }
        public string Motives { get; set; }
        public string PrevCampUid { get; set; }
        public string Summary { get; set; }
        public string Location { get; set; }
        public int TimeDuration { get; set; }
        public int TimeDelay { get; set; }
        public string TimeDate { get; set; }
        public string Body { get; set; }
    }

}
