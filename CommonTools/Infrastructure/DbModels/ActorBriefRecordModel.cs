﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Commons.Infrastructure.DbModels
{
    public class ActorBriefRecordModel
    {
        [Key]
        public int Uid { get; set; }
        public string Name { get; set; }
        public string Mechver { get; set; }
        public string Link { get; set; }
        public int Intensity { get; set; }
    }
}
