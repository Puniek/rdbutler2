# Vision document

## High level vision

TODO.

## What works

* 201011: Primitives reading and DB insertion - Story, Actor
* 201011: Secondary primitives and DB insertion - GlobalTime (important dates)
* 201011: Calculated data
    * Location records (TODO: create a location MAP)
    * Chronology
    * Merits (TODO: integrate into 'aggregated profiles')

## What is to work

* Displayable Aggregates:
    * Create Profiles from Merits / Amend Profiles with Merits
    * Amend Stories with Chronology
    * Location map
    * Index: Stories, Profiles
    * Amend Profiles with Profile-to-Profile-Heatmap
    * Amend Profiles with Profile-to-Location-Heatmap
