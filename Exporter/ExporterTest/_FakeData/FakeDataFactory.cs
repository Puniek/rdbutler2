﻿using Commons.CoreEntities;
using System;

namespace ExporterTest._FakeData
{
    public static class FakeDataFactory
    {
        public static ActorBriefRecord SingleActor_MartynHiwasser_id_001__ActorBrief()
        {
            return new ActorBriefRecord("Martyn Hiwasser", "2109", 5, "2109-martyn-hiwasser");
        }

        public static AggregatedActor SingleActor_MartynHiwasser_id_001__AggregatedActor()
        {
            AggregatedMerit[] merits = new AggregatedMerit[]
            {
                new AggregatedMerit(originUid: "211017-cats", actor: "Martyn Hiwasser", deed: "meowed", threads: "unknown", startDate: "00800917", endDate: "00800920"),
                new AggregatedMerit(originUid: "211018-cats2", actor: "Martyn Hiwasser", deed: "hissed", threads: "unknown", startDate: "00800921", endDate: "00800923")
            };

            AcdeorPlus[] progressions = new AcdeorPlus[]
            {
                new AcdeorPlus(originUid: "211017-cats", actor: "Martyn Hiwasser", deed: "progressed", endDate: "00800920"),
                new AcdeorPlus(originUid: "211018-cats2", actor: "Martyn Hiwasser", deed: "progressed well", endDate: "00800923")
            };

            AcdeorPlus[] plans = new AcdeorPlus[]
            {
                new AcdeorPlus(originUid: "211017-cats", actor: "Martyn Hiwasser", deed: "planned", endDate: "00800920"),
                new AcdeorPlus(originUid: "211018-cats2", actor: "Martyn Hiwasser", deed: "planned well", endDate: "00800923")
            };

            AtarRecord[] atarms = new AtarRecord[]
            {
                new AtarRecord(originatingActor: "Martyn Hiwasser", relevantActor: "Maria Naavas", intensity:2, storyUids: new string[] { "211017-cats", "211018-cats2" }),
                new AtarRecord(originatingActor: "Martyn Hiwasser", relevantActor: "Klaudia Stryk", intensity:1, storyUids: new string[] { "211017-cats" }),
                new AtarRecord(originatingActor: "Martyn Hiwasser", relevantActor: "Eustachy Korkoran", intensity:1, storyUids: new string[] { "211017-cats" })
            };

            FlashcardRecord[] flashcardRecords =
            {
                new(originStoryUid: "211017-cats", body: "First body"),
                new(originStoryUid: "211018-cats2", body: "2nd body!!"),
            };

            AggregatedActor martynHiwasser = new(uid: "2109-martyn-hiwasser", name: "Martyn Hiwasser", mechver: "2109",
                factions: Array.Empty<string>(), owner: "public", body: "a man of integrity and virtue",
                merits: merits, progressions: progressions, plans: plans, atarRelations: atarms, flashcardRecords: flashcardRecords, threads: new[] { "thread1", "thread2" });

            return martynHiwasser;
        }

        public static AggregatedStory SingleAggregatedStory__211017_Cats()
        {
            return new AggregatedStory(
                storyUid: "211017-cats",
                storyTitle: "Cats",
                threads: new[] { "thread1", "thread2" },
                startDate: "00800917",
                endDate: "00800920",
                seqNo: "1",
                previousStories: new[] { "211017-cats" }, // self-referential
                gms: new[] { "kić" },
                players: new[] { "żółw" },
                playerActors: new[] { "Martyn Hiwasser" },
                allActors: new string[] { "Martyn Hiwasser" },
                summary: "There were cats and stuff.",
                body: "Body"
            );
        }

        public static (string, string) SingleActor_MartynHiwasser_id_001__MkdnString()
        {
            return (@"TODO", @"TODO");
        }
    }
}
