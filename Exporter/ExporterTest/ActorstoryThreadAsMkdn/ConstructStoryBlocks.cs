﻿using Commons.CoreEntities;
using Exporter.CommandSupport.ThresholdActorstoryAsMkdn.LocationComponent;
using ExporterTest._FakeData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.ActorstoryThreadAsMkdn
{
    public class ConstructStoryBlocks
    {
        [Test]
        public void When_only_one_actor_present_in_storyinfo()
        {
            // Given
            AggregatedStory story = FakeDataFactory.SingleAggregatedStory__211017_Cats();
            AggregatedActor actor = FakeDataFactory.SingleActor_MartynHiwasser_id_001__AggregatedActor();
            AggregatedMerit[] allMerits = actor.Merits;

            // When
            var actual = StoryBlockActorthreadViewFactory.CreateSingleStoryBlockForActorView(story, actor, allMerits);

            // Expected
            StoryBlockActorthreadView expected = new StoryBlockActorthreadView(
                storyUid: story.StoryUid,
                storyTitle: story.StoryTitle,
                startDate: story.StartDate,
                endDate: story.EndDate,
                summary: story.Summary, 
                actorDeed: actor.Merits.Where(m => m.OriginUid == story.StoryUid).FirstOrDefault().Deed,
                actorProgressions: actor.Progressions.Where(p => p.OriginUid == story.StoryUid).Select(p => p.Deed).ToArray(),
                actorsPresent: new[] { actor.Name });

            // Then
            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void When_many_people_present_in_storyinfo()
        {
            // Given
            AggregatedStory info = FakeDataFactory.SingleAggregatedStory__211017_Cats();
            AggregatedActor actor = FakeDataFactory.SingleActor_MartynHiwasser_id_001__AggregatedActor();

            List<AggregatedMerit> allMeritsBuilder = new();
            allMeritsBuilder.AddRange(actor.Merits);
            string threads = string.Join(", ", info.Threads); // todo: possibly change to array
            allMeritsBuilder.Add(new AggregatedMerit(
                originUid: info.StoryUid, deed: "irrelevant", threads: threads, startDate: info.StartDate, endDate: info.EndDate,  // irrelevant
                actor: "Klaudia Stryk"));
            allMeritsBuilder.Add(new AggregatedMerit(
                originUid: info.StoryUid, deed: "irrelevant", threads: threads, startDate: info.StartDate, endDate: info.EndDate,  // irrelevant
                actor: "Roland Sowiński"));
            AggregatedMerit[] allMerits = allMeritsBuilder.ToArray();

            // When
            var actual = StoryBlockActorthreadViewFactory.CreateSingleStoryBlockForActorView(info, actor, allMerits);

            // Expected
            StoryBlockActorthreadView expected = new StoryBlockActorthreadView(
                storyUid: info.StoryUid,
                storyTitle: info.StoryTitle,
                startDate: info.StartDate,
                endDate: info.EndDate,
                summary: info.Summary,
                actorDeed: actor.Merits.Where(m => m.OriginUid == info.StoryUid).FirstOrDefault().Deed,
                actorProgressions: actor.Progressions.Where(p => p.OriginUid == info.StoryUid).Select(p => p.Deed).ToArray(),
                actorsPresent: new[] { "Klaudia Stryk", actor.Name, "Roland Sowiński" });

            // Then
            Assert.AreEqual(expected, actual);
        }

    }
}
