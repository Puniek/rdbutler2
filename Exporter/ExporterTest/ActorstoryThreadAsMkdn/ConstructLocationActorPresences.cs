﻿using Commons.CoreEntities;
using Exporter.CommandSupport.ThresholdActorstoryAsMkdn.LocationComponent;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.ActorstoryThreadAsMkdn
{
    
    public class ConstructLocationActorPresences
    {
        [Test]
        public void Identifies_most_recent_location_and_sets_proper_intensity()
        {
            // Given
            LocationRecord[] records = new LocationRecord[]
            {
                new LocationRecord("220501-x", "Świat", "irrelevant", "Świat|", "0123-01-01"),
                new LocationRecord("220503-x", "Świat", "irrelevant", "Świat|", "0123-01-07"),
                new LocationRecord("220502-x", "Świat", "irrelevant", "Świat|", "0123-01-05"),
            };

            // When
            LocationActorPresence[] actual = LocationActorPresenceFactory.CreateVisitedWorldMap(records);

            // Expected
            LocationActorPresence[] expected = new LocationActorPresence[]
            {
                new LocationActorPresence("Świat", "Świat|", 3, "0123-01-07")
            };

            // Then
            Assert.That(actual, Is.EquivalentTo(expected));
        }

        [Test]
        public void Captures_simple_tree_with_different_locations()
        {
            // Given
            LocationRecord[] records = new LocationRecord[]
            {
                new LocationRecord("220502-x", "Primus", "irrelevant", "Świat|Primus|", "0123-01-05"),
                new LocationRecord("220501-x", "Świat", "irrelevant", "Świat|", "0123-01-01"),
                new LocationRecord("220503-x", "Świat", "irrelevant", "Świat|", "0123-01-07"),
                new LocationRecord("220502-x", "Świat", "irrelevant", "Świat|", "0123-01-05"),
                new LocationRecord("220501-x", "Primus", "irrelevant", "Świat|Primus|", "0123-01-01"),
                new LocationRecord("220503-x", "Esuriit", "irrelevant", "Świat|Esuriit|", "0123-01-07"),
                new LocationRecord("220505-x", "Świat", "irrelevant", "Świat|", "0123-01-11"),
                new LocationRecord("220505-x", "Primus", "irrelevant", "Świat|Primus|", "0123-01-11"),
                new LocationRecord("220505-x", "Home", "irrelevant", "Świat|Primus|Home|", "0123-01-11")
            };

            // When
            LocationActorPresence[] actual = LocationActorPresenceFactory.CreateVisitedWorldMap(records);

            // Expected
            LocationActorPresence[] expected = new LocationActorPresence[]
            {
                new LocationActorPresence("Świat", "Świat|", 4, "0123-01-11"),
                new LocationActorPresence("Esuriit", "Świat|Esuriit|", 1, "0123-01-07"),
                new LocationActorPresence("Primus", "Świat|Primus|", 3, "0123-01-11"),
                new LocationActorPresence("Home", "Świat|Primus|Home|", 1, "0123-01-11"),
                
            };

            // Then
            Assert.AreEqual(expected, actual);
        }
    }
}
