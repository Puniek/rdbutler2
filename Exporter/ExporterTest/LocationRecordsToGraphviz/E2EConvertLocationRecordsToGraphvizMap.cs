﻿using Commons.CoreEntities;
using Exporter.CommandSupport.WorldMapAsDotFile;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.LocationRecordsToGraphviz
{
    public class E2EConvertLocationRecordsToGraphvizMap
    {
        [Test]
        public void Converts_simple_one_parent_two_children_distinct_records()
        {
            // Given
            LocationRecord[] distinctRecords = new LocationRecord[]
            {
                new ("211130-story", "Świat", "nothing happened", "Świat|", "0123-11-11"),
                new ("211130-story", "Primus", "nothing happened", "Świat|Primus|", "0123-11-11"),
                new ("211130-story", "Multivirt System", "nothing happened", "Świat|Multivirt System|", "0123-11-11")
            };

            // Given
            LocationRecord[] allRecords = new LocationRecord[]
            {
                new ("211130-story", "Świat", "nothing happened", "Świat|", "0123-11-11"),
                new ("211130-story", "Primus", "nothing happened", "Świat|Primus|", "0123-11-11"),
                new ("211130-story", "Multivirt System", "nothing happened", "Świat|Multivirt System|", "0123-11-11")
            };

            // When
            string actual = new LocationRecordsToGraphvizMap().Convert(distinctRecords, allRecords);

            // Then
            string expected = @"graph world_map{rankdir = ""LR""
Świat_ [ color = ""2"", colorscheme = ""ylorrd9"", label = ""Świat"", penwidth = 5, tooltip = ""211130-story"" ]
Świat_Primus_ [ color = ""2"", colorscheme = ""ylorrd9"", label = ""Primus"", penwidth = 5, tooltip = ""211130-story"" ]
Świat_Multivirt_System_ [ color = ""2"", colorscheme = ""ylorrd9"", label = ""Multivirt System"", penwidth = 5, tooltip = ""211130-story"" ]
Świat_ -- Świat_Primus_ 
Świat_ -- Świat_Multivirt_System_ 
}".Replace("\r\n", "\n");

            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void Multiple_transitions_of_the_same_type_become_one_transition()
        {
            // Given
            LocationRecord[] distinctRecords = new LocationRecord[]
            {
                new ("211128-story1", "Świat", "", "Świat|", "0123-11-11"),
                new ("211128-story1", "Primus", "nothing happened", "Świat|Primus|", "0123-11-11"),
            };

            LocationRecord[] allRecords = new LocationRecord[]
            {
                new ("211128-story1", "Świat", "", "Świat|", "0123-11-11"),
                new ("211129-story2", "Świat", "", "Świat|", "0123-11-12"),
                new ("211130-story3", "Świat", "", "Świat|", "0123-11-13"),
                new ("211128-story1", "Primus", "nothing happened", "Świat|Primus|", "0123-11-11"),
                new ("211129-story2", "Primus", "nothing happened", "Świat|Primus|", "0123-11-12"),
                new ("211130-story3", "Primus", "nothing happened", "Świat|Primus|", "0123-11-13"),
            };

            // When
            string actual = new LocationRecordsToGraphvizMap().Convert(distinctRecords, allRecords);

            // Then
            string expected = @"graph world_map{rankdir = ""LR""
Świat_ [ color = ""3"", colorscheme = ""ylorrd9"", label = ""Świat"", penwidth = 5, tooltip = ""211128-story1, 211129-story2, 211130-story3"" ]
Świat_Primus_ [ color = ""3"", colorscheme = ""ylorrd9"", label = ""Primus"", penwidth = 5, tooltip = ""211128-story1, 211129-story2, 211130-story3"" ]
Świat_ -- Świat_Primus_ 
}".Replace("\r\n", "\n");

            Assert.AreEqual(expected, actual);
        }
    }
}
