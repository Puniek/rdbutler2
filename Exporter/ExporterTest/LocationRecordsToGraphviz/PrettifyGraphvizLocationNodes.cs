﻿using Exporter.CommandSupport.WorldMapAsDotFile;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csdot.Attributes.Types;

namespace ExporterTest.LocationRecordsToGraphviz
{
    public class PrettifyGraphvizLocationNodes
    {
        [TestCase(2, 2)]
        [TestCase(5, 3)]
        [TestCase(10, 4)]
        [TestCase(20, 5)]
        [TestCase(50, 6)]
        [TestCase(100, 7)]
        [TestCase(200, 8)]
        [TestCase(9999, 9)]
        [TestCase(0, 2)]
        public void According_to_rarity_of_use_node_has_different_color_according_to_ylorrd9_colorscheme(int arraySize, int colorIndex)
        {
            // Given
            LocationGraphvizNode node = new("under_test");
            node.Path = "World|Something|";
            node.Stories = new string[arraySize];

            // When
            LocationGraphvizNode actual = LocationNodeAugmenter.AddIntensityAsColorScheme(node);

            // Then

            LocationGraphvizNode expected = new("under_test");
            expected.Path = "World|Something|";
            expected.Stories = new string[arraySize];

            expected.Attribute.colorscheme.Value = "ylorrd9";
            expected.Attribute.color.Value = colorIndex.ToString();
            expected.Attribute.penwidth.Value = 5;

            string actualStr = actual.ElementToString();
            string expectedStr = expected.ElementToString();

            Assert.AreEqual(expectedStr, actualStr);
        }

        [Test]
        public void Its_tooltip_contains_the_list_of_stories_where_the_location_was_used()
        {
            LocationGraphvizNode node = new("under_test");
            node.Path = "World|Something|";
            node.Stories = new string[] { "211001-x1", "211002-x2", "211003-x3", "211004-x4" };

            // When
            LocationGraphvizNode actual = LocationNodeAugmenter.AddStoryListAsTooltip(node);

            // Then
            LocationGraphvizNode expected = new("under_test");
            expected.Path = "World|Something|";
            expected.Attribute.tooltip.Value = string.Join(", ", node.Stories);

            string actualStr = actual.ElementToString();
            string expectedStr = expected.ElementToString();

            Assert.AreEqual(expectedStr, actualStr);
        }
    }
}
