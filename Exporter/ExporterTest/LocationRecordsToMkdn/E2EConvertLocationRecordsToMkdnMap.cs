﻿using Commons.CoreEntities;
using Exporter.CommandSupport.WorldMapAsDotFile;
using Exporter.CommandSupport.WorldMapAsMkdn;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.LocationRecordsToMkdn
{
    public class E2EConvertLocationRecordsToMkdnMap
    {
        [Test]
        public void Converts_simple_one_parent_two_children_distinct_records()
        {
            // Given
            LocationRecord[] distinctRecords = new LocationRecord[]
            {
                new ("211130-story", "Świat", "nothing happened", "Świat|", "0123-11-11"),
                new ("211130-story", "Primus", "nothing happened", "Świat|Primus|", "0123-11-11" ),
                new ("211130-story", "Multivirt System", "nothing happened", "Świat|Multivirt System|", "0123-11-11")
            };

            // When
            string actual = new LocationRecordsToMkdnMap().Convert(distinctRecords);

            // Then
            string expected = @"1. Świat
    1. Multivirt System
    1. Primus";

            Assert.AreEqual(expected, actual);

        }

        [Test]
        public void Properly_deals_with_entities_with_same_name_and_different_paths()
        {
            // Given
            LocationRecord[] distinctRecords = new LocationRecord[]
            {
                new ("211130-story", "Świat", "nothing happened", "Świat|", "0123-11-11"),
                new ("211130-story", "Primus", "nothing happened", "Świat|Primus|", "0123-11-11"),
                new ("211130-story", "Astoria", "nothing happened", "Świat|Primus|Astoria|", "0123-11-11"),
                new ("211130-story", "Orbital Target", "nothing happened", "Świat|Primus|Astoria|Orbital Target|", "0123-11-11"),
                new ("211130-story", "Hit", "nothing happened", "Świat|Primus|Astoria|Orbital Target|Hit|", "0123-11-11"),
                new ("211130-story", "Neikatis", "nothing happened", "Świat|Primus|Neikatis|", "0123-11-11"),
                new ("211130-story", "Orbital Target", "nothing happened", "Świat|Primus|Neikatis|Orbital Target|", "0123-11-11"),
                new ("211130-story", "Miss", "nothing happened", "Świat|Primus|Neikatis|Orbital Target|Miss|", "0123-11-11"),
            };

            // When
            string actual = new LocationRecordsToMkdnMap().Convert(distinctRecords);

            // Then
            string expected = @"1. Świat
    1. Primus
        1. Astoria
            1. Orbital Target
                1. Hit
        1. Neikatis
            1. Orbital Target
                1. Miss";

            Assert.AreEqual(expected, actual);

        }
    }
}
