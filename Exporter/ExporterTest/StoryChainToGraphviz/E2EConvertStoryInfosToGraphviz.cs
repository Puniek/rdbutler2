﻿using Commons.CoreEntities;
using Exporter.CommandSupport.StoryChainAsDotFile;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.StoryChainToGraphviz
{
    public class E2EConvertStoryInfosToGraphviz
    {
        [Test]
        public void For_single_self_referencial_StoryInfo_renders_proper_node()
        {
            // Given
            AggregatedStory input = new AggregatedStory(
                storyUid: "211112-story",
                storyTitle: "Unused Title",
                threads: new[] { "campaign" },
                startDate: "00850110",
                endDate: "00850115",
                seqNo: "3",
                previousStories: new string[] { "211112-story" },
                gms: new string[] {"gamemaster"},
                players: new string[] { "player" },
                playerActors: new string[] {"actor"},
                allActors: new string[] { "actor" },
                summary: "Summary",
                body: "Body"
                );

            // When
            string actual = new StoryInfosToGraphviz().Convert(new AggregatedStory[] { input });

            // Then
            string expected = @"digraph story_chain{rankdir = ""LR""
A_211112_Story [ label = <<b><font point-size='20'>id: 211112-story</font></b><br/><br/>gm: gamemaster | players: player<br/>actors: actor<br/><br/>Summary<br/><br/>time: 00850110 - 00850115>, shape = ""rect"" ]
}".Replace("\r\n", "\n");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void For_two_StoryInfos_renders_proper_nodes_with_single_edge()
        {
            // Given
            AggregatedStory first = new AggregatedStory(
                storyUid: "211112-story",
                storyTitle: "Unused Title",
                threads: new[] { "campaign" },
                startDate: "00850110",
                endDate: "00850115",
                seqNo: "3",
                previousStories: new string[] { "211112-story" },
                gms: new string[] { "gamemaster" },
                players: new string[] { "player" },
                playerActors: new string[] { "actor" },
                allActors: new string[] { "actor" },
                summary: "Summary",
                body: "Body"
                );

            AggregatedStory second = new AggregatedStory(
                storyUid: "211113-story",
                storyTitle: "Unused Title",
                threads: new[] { "campaign" },
                startDate: "00850116",
                endDate: "00850120",
                seqNo: "4",
                previousStories: new string[] { "211112-story" },
                gms: new string[] { "gamemaster" },
                players: new string[] { "player" },
                playerActors: new string[] { "actor" },
                allActors: new string[] { "actor" },
                summary: "Summary",
                body: "Body"
                );

            // When
            string actual = new StoryInfosToGraphviz().Convert(new AggregatedStory[] { first, second });

            // Then
            string expected = @"digraph story_chain{rankdir = ""LR""
A_211112_Story [ label = <<b><font point-size='20'>id: 211112-story</font></b><br/><br/>gm: gamemaster | players: player<br/>actors: actor<br/><br/>Summary<br/><br/>time: 00850110 - 00850115>, shape = ""rect"" ]
A_211113_Story [ label = <<b><font point-size='20'>id: 211113-story</font></b><br/><br/>gm: gamemaster | players: player<br/>actors: actor<br/><br/>Summary<br/><br/>time: 00850116 - 00850120>, shape = ""rect"" ]
A_211112_Story -> A_211113_Story 
}".Replace("\r\n", "\n");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void For_StoryInfo_with_two_parents_renders_proper_node_with_two_edges()
        {
            // Given
            AggregatedStory firstParent = new AggregatedStory(
                storyUid: "211112-parent",
                storyTitle: "Unused Title",
                threads: new[] { "campaign" },
                startDate: "00850110",
                endDate: "00850115",
                seqNo: "3",
                previousStories: new string[] { "211112-parent" },
                gms: new string[] { "gamemaster" },
                players: new string[] { "player" },
                playerActors: new string[] { "actor" },
                allActors: new string[] { "actor" },
                summary: "Summary",
                body: "Body"
                );

            AggregatedStory secondParent = new AggregatedStory(
                storyUid: "211113-parent",
                storyTitle: "Unused Title",
                threads: new[] { "campaign" },
                startDate: "00850116",
                endDate: "00850120",
                seqNo: "4",
                previousStories: new string[] { "211113-parent" },
                gms: new string[] { "gamemaster" },
                players: new string[] { "player" },
                playerActors: new string[] { "actor" },
                allActors: new string[] { "actor" },
                summary: "Summary",
                body: "Body"
                );

            AggregatedStory child = new AggregatedStory(
                storyUid: "211114-child",
                storyTitle: "Unused Title",
                threads: new[] { "campaign" },
                startDate: "00850121",
                endDate: "00850125",
                seqNo: "5",
                previousStories: new string[] { "211112-parent", "211113-parent" },
                gms: new string[] { "gamemaster" },
                players: new string[] { "player" },
                playerActors: new string[] { "actor" },
                allActors: new string[] { "actor" },
                summary: "Summary",
                body: "Body"
                );

            // When
            string actual = new StoryInfosToGraphviz().Convert(new AggregatedStory[] { firstParent, secondParent, child });

            // Then
            string expected = @"digraph story_chain{rankdir = ""LR""
A_211112_Parent [ label = <<b><font point-size='20'>id: 211112-parent</font></b><br/><br/>gm: gamemaster | players: player<br/>actors: actor<br/><br/>Summary<br/><br/>time: 00850110 - 00850115>, shape = ""rect"" ]
A_211113_Parent [ label = <<b><font point-size='20'>id: 211113-parent</font></b><br/><br/>gm: gamemaster | players: player<br/>actors: actor<br/><br/>Summary<br/><br/>time: 00850116 - 00850120>, shape = ""rect"" ]
A_211114_Child [ label = <<b><font point-size='20'>id: 211114-child</font></b><br/><br/>gm: gamemaster | players: player<br/>actors: actor<br/><br/>Summary<br/><br/>time: 00850121 - 00850125>, shape = ""rect"" ]
A_211112_Parent -> A_211114_Child 
A_211113_Parent -> A_211114_Child 
}".Replace("\r\n", "\n");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void For_StoryInfo_with_damaged_Previous_render_it_as_it_was_self_referential()
        {
            // Given
            AggregatedStory input = new AggregatedStory(
                storyUid: "211112-story",
                storyTitle: "Unused Title",
                threads: new[] { "campaign" },
                startDate: "00850110",
                endDate: "00850115",
                seqNo: "3",
                previousStories: new string[] { "DAMAGED" },
                gms: new string[] { "gamemaster" },
                players: new string[] { "player" },
                playerActors: new string[] { "actor" },
                allActors: new string[] { "actor" },
                summary: "Summary",
                body: "Body"
                );

            // When
            string actual = new StoryInfosToGraphviz().Convert(new AggregatedStory[] { input });

            // Then
            string expected = @"digraph story_chain{rankdir = ""LR""
A_211112_Story [ label = <<b><font point-size='20'>id: 211112-story</font></b><br/><br/>gm: gamemaster | players: player<br/>actors: actor<br/><br/>Summary<br/><br/>time: 00850110 - 00850115>, shape = ""rect"" ]
}".Replace("\r\n", "\n");

            Assert.AreEqual(expected, actual);
        }
    }
}
