﻿using Exporter.CommandSupport.StoryChainAsDotFile;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.StoryChainToGraphviz
{
    class HtmlizeStoryLabel
    {
        [Test]
        public void For_node_having_quotes_determining_label_startend_replace_them_with_angle_brackets_for_html()
        {
            // Given
            string node = @"A_201125_Wyscig_Jako_Randka [ label = ""<b>id: 201125-wyscig-jako-randka</b><br/>gm: żółw | players: kić, fox<br/>actors: Arianna Verlen, Klaudia Stryk<br/>Infernia traci części i ekipę (...) ścigaczy na<br/>torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego.<br/>Przez Klaudię, naturalnie.<br/>time: 0111-02-08 - 0111-02-13"", shape = ""rect"" ]";

            // When
            string actual = ChangeNodeLabel.TextTypeToHtmlType(node);

            // Then
            string expected = @"A_201125_Wyscig_Jako_Randka [ label = <<b>id: 201125-wyscig-jako-randka</b><br/>gm: żółw | players: kić, fox<br/>actors: Arianna Verlen, Klaudia Stryk<br/>Infernia traci części i ekipę (...) ścigaczy na<br/>torze sfabrykowanym przez Klaudię i na anomalicznym Skażeniu Kontrolera Pierwszego.<br/>Przez Klaudię, naturalnie.<br/>time: 0111-02-08 - 0111-02-13>, shape = ""rect"" ]";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void For_node_having_quotes_determining_label_startend_and_quotes_inside_summary_replace_only_startend_with_angle_brackets_for_html()
        {
            // Given
            string node = @"A_211026_Koszt_Ratowania_Torszeckiego [ label = ""<b>id: 211026-koszt-ratowania-torszeckiego</b><br/>gm: żółw | players: anadia, kić<br/>actors: Marysia Sowińska, Paweł Szprotka<br/>Wiktor \""dostanie swoją zapłatę\"" od \""kogoś winnego\"". Marysia <br/>time: 0111-07-28 - 0111-08-01"", shape = ""rect"" ]";

            // When
            string actual = ChangeNodeLabel.TextTypeToHtmlType(node);

            // Then
            string expected = @"A_211026_Koszt_Ratowania_Torszeckiego [ label = <<b>id: 211026-koszt-ratowania-torszeckiego</b><br/>gm: żółw | players: anadia, kić<br/>actors: Marysia Sowińska, Paweł Szprotka<br/>Wiktor \""dostanie swoją zapłatę\"" od \""kogoś winnego\"". Marysia <br/>time: 0111-07-28 - 0111-08-01>, shape = ""rect"" ]";
            Assert.AreEqual(expected, actual);
        }
    }
}
