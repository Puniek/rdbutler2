﻿using Commons.CoreEntities;
using Exporter.CommandSupport.StoryChainAsDotFile;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.StoryChainToGraphviz
{
    public class MakeLabelForStoryInfoNode
    {
        [Test]
        public void For_simple_StoryInfo_make_a_label()
        {
            // Given
            AggregatedStory input = new AggregatedStory(
                storyUid: "211112-story",
                storyTitle: "Unused Title",
                threads: new[] { "campaign" },
                startDate: "00850110",
                endDate: "00850115",
                seqNo: "3",
                previousStories: new string[] { "211112-story" },
                gms: new string[] { "gamemaster" },
                players: new string[] { "player" },
                playerActors: new string[] { "actor" },
                allActors: new string[] { "actor" },
                summary: "Summary",
                body: "Body"
                );

            // When
            string actual = StoryInfoNodeLabelMaker.MakeFrom(input);

            // Then
            string expected = "<b><font point-size='20'>id: 211112-story</font></b><br/><br/>gm: gamemaster | players: player<br/>" +
                "actors: actor<br/><br/>Summary<br/><br/>time: 00850110 - 00850115";

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Splitting_short_string_into_chunks_does_nothing()
        {
            // Given
            string expected = "Too short string.";

            // When
            string actual = StoryInfoNodeLabelMaker.SplitTo80(expected);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Splitting_long_string_into_chunks_creates_newlined_strings_at_positions_more_or_less_multiple_of_80()
        {
            // Given
            string input = "This is a properly long string. This has a long index. We have length 80 at a x. Now this exceeds 80.";

            // When
            string actual = StoryInfoNodeLabelMaker.SplitTo80(input);

            // Then
            string expected = "This is a properly long string. This has a long index. We have length 80 at a\nx. Now this exceeds 80.";
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Splitting_very_long_string_into_chunks_creates_newlined_strings_at_positions_more_or_less_multiple_of_80()
        {
            // Given
            string input = "Satarail uderzył - zainfekował \"Owadem\" który wypełzł z Torszeckiego Samszara (który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować dzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę medyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest rozgrzeszony- nie był sobą. Ale polowanie na Torszeckiego czas zacząć.";

            // When
            string actual = StoryInfoNodeLabelMaker.SplitTo80(input);

            // Then
            string expected = "Satarail uderzył - zainfekował \"Owadem\" który wypełzł z Torszeckiego Samszara\n(który kiedyś ukradł somnibela Olgi). Zainfekowane Rekiny zaczęły dewastować\ndzielnicę. Karo skutecznie rozwala owady (i Lorenę), po czym Sensacjusza w glistę\nmedyczną. Marysia samodzielnie leci jako owad wyłączyć Samszara. Torszecki jest\nrozgrzeszony- nie był sobą. Ale polowanie na Torszeckiego czas zacząć.";
            Assert.AreEqual(expected, actual);
        }
    }
}
