﻿using Commons.CoreEntities;
using Exporter.CommandSupport.AggregatedActorAsMkdn;
using ExporterTest._FakeData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExporterTest.AggregatedActorToMkdn
{
    public class E2EConvertAggregatedActorToMarkdown
    {
        [Test]
        public void For_AggregatedActor_having_Stories_and_ActorSheet_creates_a_mkdn_with_identifier()
        {
            // NOTE: this is a primary test used to implement the Converter.
            // According to this, FakeDataFactory.SingleActor_MartynHiwasser_id_001__MkdnString is implemented **eventually**.

            // Given
            AggregatedActor actor = FakeDataFactory.SingleActor_MartynHiwasser_id_001__AggregatedActor();

            // When
            (string, string) actual = AggregatedActorToMarkdownConverter.Convert(actor);

            // Expected
            string name = actor.Name;
            string body = actor.Body;
            string singleMeritDeedText = actor.Merits[0].Deed;
            string singleProgressionDeedText = actor.Progressions[0].Deed;
            string singlePlanDeedText = actor.Plans[0].Deed;
            string singleAtarActor = actor.ActorToActorRelations[0].RelevantActor;

            // Then

            // NOTE: right now the mkdn format is unstable. I need to make sure stuff is there, but not how it exactly
            // looks like. So this test is suboptimal, but needs to be good enough.

            Assert.IsTrue(actual.Item2.Contains(name));
            Assert.IsTrue(actual.Item2.Contains(body));
            Assert.IsTrue(actual.Item2.Contains(singleMeritDeedText));
            Assert.IsTrue(actual.Item2.Contains(singleProgressionDeedText));
            Assert.IsTrue(actual.Item2.Contains(singlePlanDeedText));
            Assert.IsTrue(actual.Item2.Contains(singleAtarActor));
        }
    }
}
