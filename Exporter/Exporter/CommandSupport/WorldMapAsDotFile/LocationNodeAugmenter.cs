﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.WorldMapAsDotFile
{
    public static class LocationNodeAugmenter
    {
        /// <summary>
        /// This changes the node Attributes according to the documentation. The more this Node has related Stories - the colours will change.
        /// </summary>
        /// <param name="node"></param>
        public static LocationGraphvizNode AddIntensityAsColorScheme(LocationGraphvizNode node)
        {
            node.Attribute.colorscheme.Value = "ylorrd9";
            node.Attribute.penwidth.Value = 5;

            var colorValue = node.Stories.Length switch
            {
                < 3 => "2",
                < 6 => "3",
                < 11 => "4",
                < 21 => "5",
                < 51 => "6",
                < 101 => "7",
                < 251 => "8",
                _ => "9"
            };

            node.Attribute.color.Value = colorValue;            

            return node;
        }

        public static LocationGraphvizNode AddStoryListAsTooltip(LocationGraphvizNode node)
        {
            node.Attribute.tooltip.Value = string.Join(", ", node.Stories);
            return node;
        }

    }
}
