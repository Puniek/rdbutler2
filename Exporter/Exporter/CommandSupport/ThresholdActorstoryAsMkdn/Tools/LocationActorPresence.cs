﻿using Commons.CoreEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exporter.CommandSupport.ThresholdActorstoryAsMkdn.LocationComponent
{

    public record LocationActorPresence
    {
        public LocationActorPresence(string name, string path, int intensity, string lastVisitedDate)
        {
            Name = name;
            Path = path;
            Intensity = intensity;
            LastVisitedDate = lastVisitedDate;
        }

        public string Name { get; init; }
        public string Path { get; init; }
        public int Intensity { get; init; }
        public string LastVisitedDate { get; init; }

    }

    public static class LocationActorPresenceFactory
    {
        public static LocationActorPresence[] CreateVisitedWorldMap(LocationRecord[] relevantLocationRecords)
        {
            LocationRecord[] uniqueLocationRecords = LocationRecordDtoOps.SelectRecordsWithDistinctPaths(relevantLocationRecords);

            List<LocationActorPresence> presences = new ();
            foreach (LocationRecord rec in uniqueLocationRecords)
            {
                string name = rec.Name;
                string path = rec.Path;
                int intensity = relevantLocationRecords.Count(r => r.Path == rec.Path);
                string lastVisitedDate = relevantLocationRecords
                    .Where(r => r.Path == path)
                    .OrderByDescending(r => r.EndDate)
                    .FirstOrDefault()
                    .EndDate;

                presences.Add(new LocationActorPresence(name, path, intensity, lastVisitedDate));
            }

            return presences.OrderBy(p => p.Path).ToArray();

        }
    }

}
