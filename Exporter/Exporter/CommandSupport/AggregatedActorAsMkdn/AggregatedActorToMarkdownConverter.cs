﻿using Commons.CoreEntities;
using System;
using System.Linq;
using System.Text;

namespace Exporter.CommandSupport.AggregatedActorAsMkdn
{
    public class AggregatedActorToMarkdownConverter
    {
        /// <summary>
        /// Yes, eventually I should use some kind of templating engine here. But for now, the lazy / basic approach will suffice.
        /// </summary>
        public static (string, string) Convert(AggregatedActor aActor)
        {
            // We have three main parts: "Core" (ActorInfoDto part), "Read" (ActorSheet part), "Generated" (stuff generated from Stories).

            string corePart = CreateCorePart(aActor);
            string readPart = CreateReadPart(aActor);
            string generatedPart = CreateGeneratedPart(aActor);

            // If "Read" part contains the most important parts of "Core", we shall not put Core at start of the file.
            // Otherwise, we need to add the Core at start to make sure everything works properly.
            if (string.IsNullOrEmpty(readPart) == false && readPart.Contains(aActor.Name) && readPart.Contains("# {{ page.title }}"))
                corePart = string.Empty;

            // We need to link them together, separating by proper H1 headers in mkdn.
            string mkdn = CreateTextLayoutAndLinkPartsConditionally(corePart, readPart, generatedPart);

            return (aActor.Uid, mkdn);
        }

        private static string CreateTextLayoutAndLinkPartsConditionally(string corePart, string readPart, string generatedPart)
        {
            string mkdn;
            if (string.IsNullOrEmpty(corePart))
            {
                // This case means that core is present in a readPart already.
                mkdn = readPart + "\n\n# Generated: \n\n" + generatedPart;
            }
            else
            {
                // This case means that core is not present in a readPart.
                if (string.IsNullOrEmpty(readPart))
                {
                    // this case means readPart is null or empty; nothing to output
                    mkdn = corePart + "\n\n# Generated: \n\n" + generatedPart;
                }
                else
                {
                    // we have a sensible readPart so we can output it
                    mkdn = corePart + "\n\n# Read: \n\n" + readPart + "\n\n# Generated: \n\n" + generatedPart;
                }
            }

            return mkdn;
        }

        private static string CreateGeneratedPart(AggregatedActor aActor)
        {
            string flashcardsSection = CreateFlashcardSection(aActor.FlashcardRecords);
            string meritSection = CreateMeritSection(aActor.Merits);
            string progressionSection = CreateAcdeorSection(aActor.Progressions);
            string planSection = CreateAcdeorSection(aActor.Plans);
            string atarSection = CreateAtarSection(aActor.ActorToActorRelations);
            string threads = CreateThreadsSection(aActor.Threads);

            StringBuilder builder = new();
            bool hasRealtedThreads = string.IsNullOrEmpty(threads) == false;
            bool hasFlashcards = string.IsNullOrEmpty(flashcardsSection) == false;
            if (hasFlashcards || hasRealtedThreads)
            {
                builder.AppendLine("\n\n## Fiszki\n\n");
                if (hasFlashcards) builder.Append(flashcardsSection);
                if (hasRealtedThreads)
                {
                    builder.AppendLine("\n\n### Wątki\n\n");
                    builder.Append(threads);
                }
            }
            if (string.IsNullOrEmpty(meritSection) == false)
            {
                builder.AppendLine("\n\n## Dokonania\n\n");
                builder.AppendLine("| Opowieść | Dokonanie | Daty |");
                builder.AppendLine("| ---- | ---- | ---- |");
                builder.Append(meritSection);
            }
            if (string.IsNullOrEmpty(progressionSection) == false)
            {
                builder.AppendLine("\n\n## Progresja\n\n");
                builder.AppendLine("| Opowieść | Progresja | Końcowa data |");
                builder.AppendLine("| ---- | ---- | ---- |");
                builder.Append(progressionSection);
            }
            if (string.IsNullOrEmpty(planSection) == false)
            {
                builder.AppendLine("\n\n## Plany\n\n");
                builder.AppendLine("| Opowieść | Plan | Końcowa data |");
                builder.AppendLine("| ---- | ---- | ---- |");
                builder.Append(planSection);
            }
            if (string.IsNullOrEmpty(atarSection) == false)
            {
                builder.AppendLine("\n\n## Relacje Aktor - Aktor\n\n");
                builder.AppendLine("| Z kim | Intensywność | Opowieści |");
                builder.AppendLine("| ---- | ---- | ---- |");
                builder.Append(atarSection);
            }

            return builder.ToString();
        }

        private static string CreateThreadsSection(string[] threads) => string.Join(Environment.NewLine, threads.Select(x => x));

        private static string CreateFlashcardSection(FlashcardRecord[] flashcardRecords)
        {
            if (flashcardRecords.Length == 0) return string.Empty;

            string[] lines = flashcardRecords.Select(x => $"* {x.Body} | @ {x.OriginStoryUid}").ToArray();

            return string.Join(Environment.NewLine, lines);
        }

        private static string CreateAtarSection(AtarRecord[] actorToActorRelations)
        {
            if (actorToActorRelations.Length == 0) return string.Empty;

            var sorted = actorToActorRelations.OrderByDescending(r => r.Intensity).ThenBy(r => r.RelevantActor);

            string[] atarStrings = sorted.Select(r =>
                $"| {r.RelevantActor,-20} | {r.Intensity} | (({string.Join("; ", r.StoryUids)})) |"
            ).ToArray();

            return string.Join("\n", atarStrings);
        }

        private static string CreateAcdeorSection(AcdeorPlus[] acdeors)
        {
            if (acdeors.Length == 0) return string.Empty;

            var sorted = acdeors.OrderBy(a => a.EndDate);

            string[] acdeorsStrings = sorted.Select(m =>
                $"| {m.OriginUid,-35} | {m.Deed} | {m.EndDate}"
            ).ToArray();

            return string.Join("\n", acdeorsStrings);
        }

        private static string CreateMeritSection(AggregatedMerit[] merits)
        {
            if (merits.Length == 0) return string.Empty;

            var sorted = merits.OrderBy(a => a.EndDate);

            string[] meritStrings = sorted.Select(m =>
                $"| {m.OriginUid,-35} | {m.Deed} | {m.StartDate} - {m.EndDate} |"
            ).ToArray();

            return string.Join("\n", meritStrings);
        }

        private static string CreateReadPart(AggregatedActor aActor)
        {
            return aActor.Body;
        }

        private static string CreateCorePart(AggregatedActor aActor)
        {
            StringBuilder builder = new();
            builder.AppendLine("---");
            builder.AppendLine("categories: profile");
            builder.AppendLine("factions: " + string.Join(", ", aActor.Factions));
            builder.AppendLine("owner: " + aActor.Owner);
            builder.AppendLine("title: " + aActor.Name);
            builder.AppendLine("---");
            builder.AppendLine("");
            builder.AppendLine("# {{ page.title }}");

            return builder.ToString();
        }
    }
}
