﻿This application is designed to export things from the DB Importer imports to.
So we have a trifecta:

* Importer uses the text files as input and puts stuff into the database.
* WebAPI uses database as input and puts stuff as api to be consumed by a frontend.
* Exporter uses database as input and exports stuff to either Neo4j or output text or images.

What do we want to export?

1. AggregatedActor --> markdown, to export to 'output' on RiggedDice repo. Like old days.
2. Visualize the world map in (graphviz) as nodes with intensity, probably ;-).
3. Visualize the chronology and work with campaigns. So we know what-player-was-where and what-story-happened-when.

Those three above are a good start. When this is done, I can return to WebAPI / Blazor magic.
