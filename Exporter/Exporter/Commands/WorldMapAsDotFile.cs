﻿using Commons.CoreEntities;
using Commons.Infrastructure.Repos;
using Exporter.CommandSupport.WorldMapAsDotFile;
using Exporter.Infrastructure;

using System.Threading.Tasks;

namespace Exporter.Commands
{
    public class WorldMapAsDotFile
    {
        public async Task Execute(IRepository repository, IOutputFileSaver outputSaver)
        {
            // 1. Get all LocationRecords - needed to see the heatmap
            LocationRecord[] allRecords = await repository.GetLocationRecords();

            // 2. Get unique LocationRecords - needed to know what we are working on
            LocationRecord[] distinctRecords = LocationRecordDtoOps.SelectRecordsWithDistinctPaths(allRecords);

            // 3. Build graphviz text
            string graphviz = new LocationRecordsToGraphvizMap().Convert(distinctRecords, allRecords);

            // 4. Save the files on HDD in a path taken from the configuration.
            outputSaver.PersistWorldMapGraphviz(graphviz);

            // 5. dot worldMap.graphviz -Ooutput -Tsvg
        }
    }
}
