﻿using Commons.CoreEntities;
using Commons.Infrastructure.Repos;
using Exporter.CommandSupport.WorldMapAsMkdn;
using Exporter.Infrastructure;

using System.Threading.Tasks;

namespace Exporter.Commands
{
    public class WorldMapAsMkdn
    {
        public async Task Execute(IRepository repository, IOutputFileSaver outputSaver)
        {
            // 1. Get all LocationRecords - needed to see the heatmap
            LocationRecord[] allRecords = await repository.GetLocationRecords();

            // 2. Get unique LocationRecords - needed to know what we are working on
            LocationRecord[] distinctRecords = LocationRecordDtoOps.SelectRecordsWithDistinctPaths(allRecords);

            // 3. Build output markdown world map
            string markdown = new LocationRecordsToMkdnMap().Convert(distinctRecords);

            // 3. Save the files on HDD in a path taken from the configuration.
            outputSaver.PersistWorldMapMkdn(markdown);

            // 4. -> this is manual from cmdline. Convert the output to svg.
            // dot worldMap.graphviz -Ooutput -Tsvg
        }
    }
}
