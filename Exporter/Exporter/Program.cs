﻿using Commons.Infrastructure.Repos;
using Exporter.Commands;
using Exporter.Infrastructure;
using System;
using System.Threading.Tasks;

namespace Exporter
{
    class Program
    {
        static async Task Main(string[] args)
        {
            IRepository repository = new SqliteRepository();
            IOutputFileSaver saver = new RealOutputFileSaver();

            await new ActorstoryThreadAsMkdn().Execute(repository, saver);
            await new AggregatedActorAsMkdn().Execute(repository, saver);
            await new WorldMapAsDotFile().Execute(repository, saver);
            await new WorldMapAsMkdn().Execute(repository, saver);
            await new StoryChainAsDotFile().Execute(repository, saver);

            Console.WriteLine("Export successful.");
        }
    }
}
