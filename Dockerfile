FROM mcr.microsoft.com/dotnet/aspnet:latest AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:latest AS build
WORKDIR /src
COPY . .

RUN dotnet restore "Api/Api/Api.csproj"
RUN dotnet build "Api/Api/Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Api/Api/Api.csproj" -c Release -o /app/publish

COPY CommonTools/Config/localSettings.json /app/publish/Config/localSettings.json

FROM base AS final
WORKDIR /app

COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "Api.dll"]
