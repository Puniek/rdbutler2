## Idea

### World Map use cases

There are two main usecases.

1. Use the subset of the world map to create a location map during a rpg session
2. Look at the whole world map and see how often we play with it and where

Currently we support only second of those usecases. WorldMapToMd is supposed to support the first one only.

### How to do it

Basically, a mix between already existing WorldMapToDot and ActorToMkdn. Not much new stuff. But useful.

* Read LocationRecords from DB
* Select only unique LocationRecords
* render them to mkdn format
* Save on HDD

Most important - add proper tests to prove it works. Too much database stuff, need to add tests for primary usecase.

### Question

Do we duplicate 80% of StoryChainAsDotFile? Of course, yes. It is better to duplicate some code as a new Command than to make the original Command do too much - this approach is more extensible and allow me to diverge the code in the future.
