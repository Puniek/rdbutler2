## Idea

Move Actorthreads to database and then display them in UI.

### Problem

[Look at note 220616 - Actorstory](220616-actorstory)

Now it works on Gitlab-level, outputting stuff in Exporter. We want to do the same on API-level and then on UI-level.

Usecases (copying old ones):

* Actor: 'Izabela Zarantel'. I want to see her story from conception to death and to see what dates I have available for next Story.
* Actor: 'Izabela Zarantel'. I want to know if she had ever visited Pustogor or not and if so, WHEN.
* Actor: 'Izabela Zarantel'. I want to know during which Stories she met Martyn Hiwasser and what happened in those Stories.
* kluczowe zasoby z momentu chronologii? I mean, tak jak my mieliśmy key zasób działo zniszczenia, albo mamy pandorę, albo tą przynętę na kryptę. Żebym mogła sprawdzić, jakimi key zasobami dysponuje w danym momencie chronologii
    * z perspektywy TEJ POSTACI albo FRAKCJI TEJ POSTACI albo ODPOWIEDNIEJ POSTACI
* key npc - czy dostępne? Mam na myśli, informacje o stanie danej postaci w danym okresie czasu. Np. xx.xx.xxxx Martyn Hiwasser - szpital.
    * _<-- this one still not supported_
    * z perspektywy - określę dla mojej postaci datę i sprawdzę tamtą postać by określić jej stan
        * _<-- this one is supported XD_

### Solution

How do we build it?

* Backend
    * Create an endpoint in API which returns Actorthreads for a particular Actor
        1. Move Exporter's Actorthread generation to Importer
        2. Add Actorthread to database
        3. API endpoint + proper extraction
* UI
    * Create a component on SingleActor page displaying all Storythreads
        1. Find a proper component (Material UI Card?)
    * Link this stuff together
    * Make it 'visible or not' via checkbox

Scope:

1. Every Actor with at least 'Intensity 1' has at least 1 of those ActorThreads

Examples:

* reference: any Exporter output

### Possible expansion

1. None atm
