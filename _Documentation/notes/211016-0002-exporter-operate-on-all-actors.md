# Architecture Design Record
## Descriptor

* Number: 0002
* Title: Exporter shall use batch-sequential approach, reading everything from DB instead of one object.

## Context - Value neutral, describe forces at play

Concerns: OutputAggregatedActorAsMkdn

* There are many ways how to read stuff from DB, aggregate it and write it to markdown files. Which one to choose? Example using Actors:
    1. I can read **a single** Actor, read everything associated with that actor, aggregate it, make a mkdn file and save it. Then take a next one.
    2. I can read **all** Actors, read everything associated with all actors, aggregate it all, make a batch of mkdn files and save those as a batch.
* In both cases I will have to move on to "not-actor" later.
* Analysis of approaches
    * Approach (1) - much more HDD operations, somewhat slower overall and a bit slower to implement.
    * Approach (2) - much more memory intensive, will not scale above a number of records in the database. Also, much easier in terms of algorithm creation and testability.
* Currently Exporter is run at most 2 times / week.

## Decision - 1 sentence, “We will ....”

We will select approach (2) - batch processing of all relevant components. Decision is reversible and easy to refactor.

## Status - Proposed, Accepted, Deprecated, Superseded

Accepted, implemented.

## Consequences

For now, everything works perfectly. When the data starts being too large, refactor will be needed.
