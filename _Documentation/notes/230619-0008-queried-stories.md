## Idea

Queryable stories.

### Problem

At this point it is time to focus on a queryable Stories. Sometimes people who are not me (Fox or T) want to run a Story.

1. Which chronology to choose? Which characters were present between dates X and Y?
2. What story needs to be linked to a specific Thread? What happened in the Thread selected?
3. All stories which took place in Verlenland (f.ex.) in chronological order

### Current state:

* We use regexes, of course :-).
* Or SQLite DB explorer.
* Both of which only I use. Obviously.
    * And are not perfect in any shape or form, they require searching and correlating _things_.

### Solution
#### 1. What to build

Technical:

1. Create a new UI section in "Stories". Mockup below:
    1. ![Mockup](mats/230619/230619-01-story-ui-vision.png)
2. Make it Visible / Invisible (if Invisible, it is disabled)
3. When the button is clicked, send a query to a different endpoint (one with 'select * with criteria')

Scope:

1. locations (locationRecord contains "Verlenland")
    * shorthand: `l:`
2. actors (aggregatedMerit or aggregatedProgression contains "Kalina Rotmistrz")
    * shorthand: `a:`
3. threads (storyBriefRecordModel contains "nemesis")
    * shorthand: `th:`
4. startDate, endDate
    * shorthand: `sd:`, `ed:`
5. player, gm
    * shorthand: `p:`, `gm:`
5. operators: `AND`, `OR`, `NOT`
6. terminators: `;`

Examples:

* `actors: Arianna;`
    * return all Stories where actor "Arianna Verlen" was present
* `a: Arianna Verlen OR Elena Verlen;`
    * return all Stories where actors "Arianna Verlen" or "Elena Verlen" were present
* `l: Verlenland`
    * return all Stories where path "Świat|Primus|Sektor Astoriański|Astoria|Sojusz Letejski|Aurum|Verlenland|Hold Karaan|Barbakan Centralny|" contains "Verlenland". 
    * As there is only one query, terminator `;` is not needed
* `t: nemesis; a:Erwin`
    * return all Stories where Thread contains "nemesis" and Actor present contains "Erwin"
* `a: Arianna V; p: NOT fox`
    * return all Stories where Arianna was present, but Fox wasn't a player
* .
    * return all Stories, as no criteria are present
* `xxx: yyy`, `a: Arianna`
    * return no Stories, as query is invalid (no quotes or wrong query)


#### 2. How to build

1. Change API, adding new endpoint which properly supports all the above cases
2. Change UI, adding new hideable element
3. If Execute button is clicked, we send a query to new endpoint and display it in SimpleTable.

### Possible expansion

1. New filtering commands, probably
