## Idea

### Problem

Current state of the system slowly stops supporting everything we want from the system. We slowly need to have any type of UI which allows us to browse and see things.

### Current state:

* We use Exporter as a crutch - we generate:
    * World map (as markdown and as graphviz)
    * Metadata for Actors as Threads and as +gen to Profile

### Solution

1. What to do
    1. Create an API which can be publicly accessible as an API.
    2. Create a UI to browse and subselect the data.
2. Usecases to be supported
    1. Select ALL Actors
    2. Select a SUBSET of Actors
        1. "All Sharks in the system"
        2. "All Actors who have ever been in the city of Podwiert"
        3. "All Actors who have been in the city of Podwiert between THOSE dates"
    3. Select an Actor by id
        1. View all data we have
        2. View a Thread of an Actor (set of Stories in sequence), adding which Threads were those Stories assigned to
        3. View Actor - to - Actor relation map of an Actor, the Deeds etc. Standard 'generated' stuff
    4. View Stories
        1. View all Stories
            1. In creation order
            2. In chronological order
        2. View a subset of Stories
            1. Linked to thread_X in creation order
            2. Linked to thread_X in chronological order, displaying dates

...

### How to do it

1. Create an API and deploy it. Application name: "Api"
    1. Read stuff from the DB via EF
    2. Create endpoints
    3. Select the way to host it
    4. Deploy it via Docker
2. Create a UI and deploy it. Application name: "UiClient"
    1. Select a technology
        1. I wanted Blazor, but T told me she wants to dabble with it; therefore, React + potentially MaterialTable.
    2. Visualize endpoints, one at a time
    3. Select the way to host it
    4. Deploy it via Docker

### Possible expansion

1. This is an ongoing system. The more is done, the more capabilities we have. 
2. Worthy and hard expansions:
    1. WorldMap as "graphviz", but it is browsable and possible to subset stuff
    2. StoryChain as "graphviz", but it is subsettable and you can easily follow the chain and expand every Story with more information.
