## Idea

### Problem

* The Generated Actor document is unreadable for everyone and it sucks
    * Irx has to copy-paste-change a lot
    * T doesn't use it at all

### Solution

Two files - generated-short and generated-verbose.

Current format:

* **Name**: Leonidas
* Wartości
  * Moje
    * Hedonism: pleasure above anything else
    * Face: prestige; having a specific reputation and upholding it
    * Power: more influence upon reality, amass wealth and influence
  * Nieistotne
    * Stimulation: feel alive, feel more, new things, experiment, explore
    * Self-direction: autonomy, ability to control your own way and direction
* Osobowość
  * Ocean
    * ENCAO:  -+--0
  * Jak się objawia osobowość
    * Nie znosi być w centrum uwagi
    * Zakłóca spokój; nie toleruje spokoju i nudy
    * Zniechęca innych, demotywujący
    * Experiences dramatic shifts in mood
* Potencjalna praca:
  * Szklarz
  * Inżynier systemów i sieci komputerowych
* Potencjalne silniki motywacyjne:
  * Moje silniki:
    * Rule of Cool: nieważne jak to skuteczne, ważne, że oczy wszystkich będą na nas! Reckless, dangerous but oh-how glorious!
    * Władca marionetek: wszyscy będą grali tak jak im zagrasz. Informacje, plotki, manipulacja, paranoja. To Ty władasz światem każdego z nich, z cienia.
  * Silniki WROGA lub anty-silnik:
    * Supremacja: Moja grupa jest LEPSZA, tamta grupa jest GORSZA. Wzmocnienie moich lub osłabienie tamtych jest moralnie właściwe.

Desired short format:

* Lycoris Kidiron, 57 lat (ze wspomaganiami drakolickimi dającymi jej efektywność 3x-latki) (objęta przez Wiki)
    * Wartości
        * TAK: Bezpieczeństwo, Osiągnięcia ("dzięki mnie ta arkologia będzie bezpieczna i osiągniemy najwyższy poziom eksportu żywności")
        * NIE: Konformizm ("nikt, kto próbował się dopasować do innych nie doprowadził do postępu")
    * Ocean
        * ENCAO: +-0+0 (optymistyczna, nieustraszona, lubi się dogadywać z innymi, nie patrzy na ryzyko)
    * Silniki:
        * TAK: Kapitan Ahab: polowanie na białego wieloryba do końca świata i jeszcze dalej, nieważne co przy tym stracimy. (tu: "zrobię najlepszą arkologię, lepszą dla ludzi niż planeta macierzysta")
    * Rola:
        * ekspert od bioinżynierii i terraformacji, pionier (zdolna do przeżycia w trudnym terenie)

### How to do it

Add new formatter ("short"). After working with T, add new "verbose" formatter.

### Possible expansion

N/A for now
