## Idea

Look at Threads and Motives. Modify the Stories. Create and display.

### Domain Explanation

EZ does not really contain the concept of ‘campaign’. One story can be connected to more than one ‘thread’, as we call them.

However, the concept of a `Thread` (or `StoryThread`) is not enough. There are also stories which do not have anything in common with each other, but they have similar motives.

To illustrate the issue let's look at a city called Czółenko.

Thread:

* There is an overarching story thread which deals with Czółenko, a small city in Szczeliniec. It deals with themes:
    * How Esuriit - the energy of hunger and hatred - came to be in Czółenko
    * How it has managed to corrupt a lot of people 
    * How it has been kind of contained by the local population 
    * How one unfortunate terminus tries to potentially shut it down.

Those are the motives. A single story can be linked with:

* Esuriit corrupts all
* salvagers in the spaceships
* 'Aurelion’s syndicate' encroaching in space

So if we have a Story, say... `230524-ekologia-jaszczurow-w-arachnoziemie` .

There are linked **Threads**:

* `reformacja-arachnoziem`
    * conflict between conservation and better tomorrow and conformity in that city
* `wojna-domowa-verlenów`
    * ever-escalating conflicts which lead towards an inevitable civil war

And linked **Motives**:

* `butterfly-effect`
    * a minor action escalated into something unpredictable
* `legendy-verlenlandu`
    * powerful aristocrats aiming for legendary status do awesome and epic stuff
* `mroczna-strona-verlenow`
    * powerful aristocrats aiming for legendary status have their own dark sides, hurting their own

The current state is slightly unfortunate - the Threads are completely not managed and there is chaos in the data. There is no such thing as a motives linked to the stories, so we’d have to write and update every single story there is.

And by ‘we’ I mean ‘I’.

### Solution

1. Display ThreadBriefs and create SingleThread from the data we have.
2. Cry.
3. Create a structure like 'generated actor' and 'read actor', as in, part data to SingleThread comes from stuff we read.
    * Some stuff will need Spoilers - we will use <Collapse /> component from ant.design which is being currently used anyway.
4. Add Motives to existing Stories. Will be imperfect, but will help somewhat.
5. Display MotiveBriefs and SingleMotives. Replicate 'generated actor' and 'read actor' in Motives.
    * Some stuff will need Spoilers - we will use <Collapse /> component from ant.design which is being currently used anyway.
6. Clean data.
    * having subsetter and application we don't need some 'threads' like 'XXX's story' anymore

### Possible expansion

Generator - if we have enough good Threads and Motives, then we add StoryComponents, we can build a **true** RPG story generator:

* Select StoryStructure
* Select Motives and Threads
* Based on the above, add 2-3 valuable StoryComponents
* --> Story has been constructed as a base structure with agendas, pacing etc.
