# Architecture Design Record
## Descriptor

* Number: 0001
* Title: Exporter shall be implemented as a stop-gap.

## Context - Value neutral, describe forces at play

Concerns: The Exporter solution.

* I am technical, so is my wife. But sometimes we play with non-technical people.
    * Those people want to see the aggregations of their Actors too and they aren't too keen on browsing SQLite.
* My knowledge of frontend technologies is less-than-stellar. Creation of proper frontend over webapi will take a while.
    * And I have no time at all.
* Therefore the Exporter, like the tool used in 2015 shall be used.
    * Read data, aggregate it and output it in files.
    * Those files shall be separated from the main input files. They will be in human-readable format (markdown or png/svg)
    * Stuff shall be committed to Git
        * Therefore, Gitlab shall become UI for others, buying me time to do it "right".

## Decision - 1 sentence, “We will ....”

We will implement Exporter as stated above.

## Status - Proposed, Accepted, Deprecated, Superseded

Accepted.

## Consequences

* High levels of happiness in other players. 
* Better decisions during Stories. 
* More context while designing stories. 
* Also, other players found some bugs in chronology between stories I was not aware of.
