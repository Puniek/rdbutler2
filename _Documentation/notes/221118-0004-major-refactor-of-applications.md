# Architecture Design Record
## Descriptor

* Number: 0004
* Title: Major refactor of applications

## Context - Value neutral, describe forces at play

Concerns: the code becomes harder to work with, there are too many data structures, most data structures are useless. Useless stuff in database. And there are new features in C#10 I can use.

## Decision 1 - 1 sentence, “We will ....”

I shall refactor the code to the new structure.

Base (struktury organizacyjne): https://edu.pjwstk.edu.pl/wyklady/poz/scb/main44.html 

### Role of particular applications

* Commons
    * common stuff between all the application; be it entities and tools
* Importer
    * Assuming we ever want to make a 'UI' of sorts...
    * ...Importer is then responsible for all heavy calculations and for filling the Database with all useful stuff
* Exporter
    * just creates views of the data in the DB - UI would do the same
* Generator
    * generates stuff from DB or its own stuff

### Current state

Current state:

* Commons
    * Config                        (config provider, json...)
    * Data                          (overall data converter)
        * Domain                    (always-useful data structure which are independent of everything)
            * AggregatedActor
            * StoryInfo
            * ...
        * DtoConverters             (factories converting stuff from 'Models' to 'Domain' / 'Dtos')
            * ActorInfoDtoFactory
            * AggregatedActorFactory
            * ...
        * Dtos                      (the... same as Domain?)
            * AcdeorDto
            * ActorInfoDto      
            * AggregatedMeritDto    
            * ...
        * Models                    (stuff touching the database; read / write)
            * ActorFactionModel
            * ActorInfoDataModel
            * ...
    * Repos                         (database stuff)
        * SqliteRepository
    * Tools                         (purely technical stuff, stateless)
        * Rngs
        * FileOps                   (read / write to files)
        * MarkdownFormatters
        * ...
* Importer                          
    * Actions
        * TimeCalculator            (calculating time)
        * ParseDomainText           (stuff related to regex and extracting particular data)
    * DtoConverters                 (extract from text -> build Domain / DTO entity from Commons)
        * AcdeorDtoFactory
        * ActorDtoFactory
        * ...
    * EntitiesExtracted             (stuff extracted from text which is not really useful outside of Importer but atm goes to DB)
        * AcdeorRecord
        * ActorMerit
        * ...
    * EntitiesRead                  (stuff read from HDD)
        * ActorSheet
        * GlobalTime
        * Story
        * ...
    * Infrastructure                (persistors unique for this subapplication; stuff worked on during Importer -> models)
        * Models
            * ActorFactionModelFactory
            * ActorInfoDtoModelFactory
            * ...
        * Database                  (persistor)
* Exporter                          (**struktura sztabowo-liniowa**; sztab: Commands, linie: poszczególne CmdSupport)
    * Commands                      (high level stuff which takes place during exporting)
        * ActorstoryThreadAsMkdn
        * AggregatedActorAsMkdn
        * ...
    * CommandSupport                (all stuff supporting main command; master - support; Division level structure + CommandHQ)
        * AggregatedActorAsMkdn
            * ...Converter
        * StoryChainAsDotFile
            * ChangeNodeLabel
            * StoryInfoGraphvizNode
    * Infrastructure                (persistors unique for this subapplication)
        * FileSaver
* Generator
    * FOR NOW, DIFFERENT WORLD; less reliant on non-commons

### New state

* Commons                       (**struktura liniowa**; podział na obszary)
    * Config                        (config provider, json...)
    * CoreEntities                  (common entities: AggregatedActor etc. Only this shall be persisted in the DB.)
    * Infrastructure
        * DbEntityFactories         (stuff from DB -> Domain entities)
        * DbModels                  (stuff read from DB)
        * Repos
    * Tools                         (all technical common stuff)
* Importer                      (**Struktura funkcjonalna typu U**; funkcje -> FAZY) (**batch sequential architecture**)
    * Program                       (headquarters, place which runs all of that)
    * P1_ReadParsing                (all the code responsible for reading entities and all first-level entities)
        * Entities
        * Factories
        * ...
    * P2_Extraction                 (all the code responsible for extracted entities and second-level entities)
        * Entities
        * Factories
        * ...
    * P3_Aggregation                (all the code which aggregates previous stuff)
        * THERE WILL BE NO Entities (Entities by definition need to be in Common; this goes to DB and is used by Exporter / WebAPI)
        * Hydrators?
        * ...
    * P4_Persisting                 (persistence; contains DB stuff etc)
        * Models                    (THIS TIME only those which are absolutely needed will go to database)
        * EntityDbFactories
        * ...
* Exporter                      (**struktura sztabowo-liniowa**; sztab: Commands, linie: poszczególne CmdSupport)
    * Commands                      (high level stuff which takes place during exporting)
        * ActorstoryThreadAsMkdn
        * AggregatedActorAsMkdn
        * ...
    * CommandSupport                (all stuff supporting main command; master - support; Division level structure + CommandHQ)
        * AggregatedActorAsMkdn
            * ...Converter
        * StoryChainAsDotFile
            * ChangeNodeLabel
            * StoryInfoGraphvizNode
    * Infrastructure                (persistors unique for this subapplication)
        * FileSaver

### How to get there

1. Correct 'Domain / DTO' stuff in Commons                                              DONE
    1. Eliminate 'DTO', move stuff to Domain                                            DONE
    2. Make sure all factories are present                                              DONE
2. Restructure Commons project to the proposed structure                                DONE
3. Restructure Importer to Functional-U structure                                       DONE
    1. Slight setback - AcdeorDtoFactory seems to be too undefined.                      INFO
    2. Eliminate WorldMap; this is done on Exporter level now                           DONE
4. Move 'common' stuff to Domain in Commons                                             DONE
    1. Split factories between 'model -> entity' and 'parse -> entity' factories        DONE
    2. Domain entities should be in Commons in proper places                            DONE
    3. Deduplicate responsibilities between Commons/Domain and Importer/PhaseX          DONE
        1. Look at AcdeorFactory - might have to make AggregatedProgression / Plan      SOLVED, WONTDO
            1. Reasoning: we don't NEED anything like that. -> AcdeorPlus as domain object.
5. Clean responsibilities between Entities                                              DONE
6. Cull pointless things from being saved to the database                               DONE
    1. We need the substrate (ReadStories, ReadActors)                                  DONE
    2. We need the final things (phase 4)                                               DONE
7. Clean NAMES - "StoryInfo -> AggregatedStory" and other stuff hidden in methods       DONE
8. Clean Tests and test structure                                                       DONE

## Decision 2 - 1 sentence, “We will ....”

I will NOT use records in place of classes after a research.

Sadly, C# records do 99% of what I need from classes. The one thing needed is equality. Consider this example:

```
    public record Person(string FirstName, string LastName, string[] PhoneNumbers);

    public static void Main()
    {
        Person person1 = new("Nancy", "Davolio", new string[] {"A", "B"});
        Person person2 = new("Nancy", "Davolio", new string[] {"A", "B"});
        Console.WriteLine(person1 == person2); // output: False  // BECAUSE string[] is a reference equality object
    }
```

In normal code I do this:

```
    public class AcdeorDto
    {
        public AcdeorDto(string originUid, string actor, string deed, string endDate)
        {
            OriginUid = originUid;
            Actor = actor;
            Deed = deed;
            EndDate = endDate;
        }

        public string OriginUid { get; }
        public string Actor { get; }
        public string Deed { get; }
        public string EndDate { get; }

        public override int GetHashCode() => HashCode.Combine(OriginUid, Actor, Deed, EndDate);
        public override bool Equals(object obj)
        {

            if (!(obj is AcdeorDto other)) return false;

            if (OriginUid != other.OriginUid) return false;
            if (Actor != other.Actor) return false;
            if (Deed != other.Deed) return false;
            if (EndDate != other.EndDate) return false;

            return true;
        }
    }
```

But in records I cannot override Equals. And even if I could - I still have to do all the work I do for classes. So what's the point?

## Decision 3 - 1 sentence, “We will ....”

I will not change Acdeor as domain object into AggregatedProgression, AggregatePlan...

REASONING: for now I am using them in a very anemic way. They are not important for me yet, nor difficult to use.
Cost of making this now exceeds the gains.

For now I shall denote it as AcdeorPlus (because it is an aggregate of Acdeor and EndDate). If I need something specific, I will extract that specific entity as I did with AggregatedMerit.

## Status - Proposed, Accepted, Deprecated, Superseded

Accepted.

## Consequences

* Technical
    * Code shall be more readable and it will be much easier to find one's way 
    * Database shall be more readable - it is easy to see what is where
    * After updating the tests they shall be more readable using """" xxx """" variant
    * Rule of Proximity shall be enforced more ("stuff similar to each other should be next to each other")
    * Factories will be separated
    * Amount of Entities will be much smaller
* Business
    * Faster to add new features ("threads" etc)
    * Reduction of clutter -> faster to navigate the code -> overall faster dev speed
* Risks
    * None
        * bugs shall be detected by tests which exist
        * in case of refactor fail, we can always revert it (thanks git <3)
