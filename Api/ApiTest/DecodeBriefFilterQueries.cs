using Api.Tools;
using NUnit.Framework;
using System.Net;

namespace ApiTest
{
    public class DecodeBriefFilterQueries
    {

        [Test]
        public void For_query_involving_only_1_actor_returns_dict_with_1_kv_pair()
        {
            // Given
            string query = "a: Arianna Verlen";
            string encoded = WebUtility.UrlEncode(query);

            // When
            Dictionary<string, string> actual = DecodeBriefFilter.Decode(encoded);

            // Expected
            Dictionary<string, string> expected = new() { { "actors", "Arianna Verlen"} };

            // Then
            CollectionAssert.AreEquivalent(expected, actual);

        }

        [Test]
        public void For_query_involving_2_actors_returns_dict_with_1_kv_pair()
        {
            // Given
            string query = "a: Arianna Verlen and Elena Verlen";
            string encoded = WebUtility.UrlEncode(query);

            // When
            Dictionary<string, string> actual = DecodeBriefFilter.Decode(encoded);

            // Expected
            Dictionary<string, string> expected = new() { { "actors", "Arianna Verlen and Elena Verlen" } };

            // Then
            CollectionAssert.AreEquivalent(expected, actual);

        }

        [Test]
        public void For_query_involving_1_param_each_from_area_returns_dict_with_2_kv_pairs()
        {
            // Given
            string query = "a: Arianna Verlen; l: Verlenland;";
            string encoded = WebUtility.UrlEncode(query);

            // When
            Dictionary<string, string> actual = DecodeBriefFilter.Decode(encoded);

            // Expected
            Dictionary<string, string> expected = new() { { "actors", "Arianna Verlen" }, { "locations", "Verlenland" } };

            // Then
            CollectionAssert.AreEquivalent(expected, actual);

        }

        [Test]
        public void No_matter_the_amount_of_terminators_only_valid_and_supported_queries_will_be_captured()
        {
            // Given
            string query = "sd: 0111-05;;;;xx: irrelevant;; ed: 0112-01;;;;";
            string encoded = WebUtility.UrlEncode(query);

            // When
            Dictionary<string, string> actual = DecodeBriefFilter.Decode(encoded);

            // Expected
            Dictionary<string, string> expected = new() { { "startDate", "0111-05" }, { "endDate", "0112-01" } };

            // Then
            CollectionAssert.AreEquivalent(expected, actual);

        }

        [TestCase("a: Arianna", "actors", "Arianna")]
        [TestCase("actors: Arianna", "actors", "Arianna")]
        [TestCase("l: Verlenland", "locations", "Verlenland")]
        [TestCase("locations: Verlenland", "locations", "Verlenland")]
        [TestCase("th: nemesis", "threads", "nemesis")]
        [TestCase("threads: nemesis", "threads", "nemesis")]
        [TestCase("mt: nemesis", "motives", "nemesis")]
        [TestCase("motives: nemesis", "motives", "nemesis")]
        [TestCase("p: T", "players", "T")]
        [TestCase("players: T", "players", "T")]
        [TestCase("gm: Irx", "gm", "Irx")]
        [TestCase("sd: 0112-", "startDate", "0112-")]
        [TestCase("startDate: 0112-", "startDate", "0112-")]
        [TestCase("ed: 0112-", "endDate", "0112-")]
        [TestCase("endDate: 0112-", "endDate", "0112-")]
        public void Check_all_supported_combination_of_params_in_isolation(string query, string key, string value)
        {
            // Given
            string encoded = WebUtility.UrlEncode(query);

            // When
            Dictionary<string, string> actual = DecodeBriefFilter.Decode(encoded);

            // Expected
            Dictionary<string, string> expected = new() { { key, value } };

            // Then
            CollectionAssert.AreEquivalent(expected, actual);
        }
    }
}