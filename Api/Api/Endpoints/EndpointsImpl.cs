﻿using Api.Tools;
using Commons.CoreEntities;
using Commons.Infrastructure.Repos;
using System.Net;

namespace Api.Endpoints
{
    public class EndpointsImpl
    {
        public static async Task<ActorBriefRecord[]> GetActorBriefsAsync(string? filteredBy, IRepository repo)
        {
            Dictionary<string, string> filters = DecodeBriefFilter.Decode(filteredBy);
            var allActorBriefs = await repo.GetActorBriefs(filters);
            return allActorBriefs.OrderByDescending(a => a.Intensity).ToArray();
        }

        public static async Task<FactionBriefRecord[]> GetFactionBriefsAsync(string? filteredBy, IRepository repo)
        {
            Dictionary<string, string> filters = DecodeBriefFilter.Decode(filteredBy);
            FactionBriefRecord[] allFactionBriefs = await repo.GetFactionBriefs(filters);
            return allFactionBriefs.OrderByDescending(a => a.Intensity).ToArray();
        }

        public static async Task<StoryBriefRecord[]> GetStoryBriefsAsync(string? filteredBy, IRepository repo)
        {
            Dictionary<string, string> filters = DecodeBriefFilter.Decode(filteredBy);
            StoryBriefRecord[] storyBriefs = await repo.GetStoryBriefs(filters);
            return storyBriefs.Reverse().ToArray();
        }

        public static async Task<ActorThreadStoryBlock[]> GetActorThreadStoryBlocksByActorNameAsync(string actorUid, IRepository repo)
        {
            ActorThreadStoryBlock[] actorThreadStoryBlocks = await repo.GetActorThreadStoryBlockByUid(actorUid);
            return actorThreadStoryBlocks.OrderByDescending(a => a.EndDate).ToArray();
        }

        /// <summary>
        /// This one returns a single AggregatedActor by giving an ID on input: '2010-triana-porzecznik'
        /// </summary>
        public static Func<string, IRepository, Task<AggregatedActor>> GetActorByUidAsync =
            async (string actorUid, IRepository repo) =>
            {
                AggregatedActor actor = await repo.GetActorByUid(actorUid);
                return actor;
            };

        /// <summary>
        /// This one returns a single AggregatedStory by giving an ID on input: '230412-dzwiedzie-poluja-na-ser'
        /// </summary>
        /// <returns></returns>
        public static async Task<AggregatedStory> GetStoryByUidAsync(string storyUid, IRepository repo)
        {
            AggregatedStory story = await repo.GetStoryByUid(storyUid);
            return story;
        }

        public static async Task<ThreadBriefRecord[]> GetThreadBriefsAsync(string? filteredBy, IRepository repo)
        {
            Dictionary<string, string> filters = DecodeBriefFilter.Decode(filteredBy);
            var allThreadBriefs = await repo.GetThreadBriefs(filters);
            return allThreadBriefs;
        }

        public static async Task<MotiveBriefRecord[]> GetMotiveBriefsAsync(string? filteredBy, IRepository repo)
        {
            Dictionary<string, string> filters = DecodeBriefFilter.Decode(filteredBy);
            var allMotiveBriefs = await repo.GetMotiveBriefs(filters);
            return allMotiveBriefs;
        }

        public static async Task<AggregatedThread> GetThreadByUidAsync(string threadUid, IRepository repo)
        {
            AggregatedThread thread = await repo.GetThreadByUid(threadUid);
            return thread;
        }

        public static async Task<AggregatedMotive> GetMotiveByUidAsync(string motiveUid, IRepository repo)
        {
            AggregatedMotive thread = await repo.GetMotiveByUid(motiveUid);
            return thread;
        }

        public static async Task<LocationTreeNode> GetUniqueLocationRecordAsync(IRepository repo)
        {
            string recordTreeString = await repo.GetUniqueLocationRecordTree();
            LocationTreeNode recordTree = LocationTreeNode.CreateFromJson(recordTreeString);
            return recordTree;
        }

        public static async Task<AggregatedSingleLocationDetails> GetSingleLocationDetailsByPathAsync(string locationPath, IRepository repo)
        {
            AggregatedSingleLocationDetails locationDetails = await repo.GetSingleLocationDetails(locationPath);
            return locationDetails;
        }
    }
}
