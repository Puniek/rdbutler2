﻿using Commons.Config;
using Commons.CoreEntities;
using Commons.Infrastructure.Repos;

namespace Api.Endpoints
{
    public class EndpointsMapping
    {
        public static void MapEndpoints(WebApplication app)
        {
            app.MapGet("/actors", EndpointsImpl.GetActorBriefsAsync)
                .WithName("GetActorBriefs")
                .WithOpenApi()
                .WithTags("BrowseActors");

            app.MapGet("/factions", EndpointsImpl.GetFactionBriefsAsync)
                .WithName("GetFactionBriefs")
                .WithOpenApi()
                .WithTags("BrowseFactions");

            app.MapGet("/threads", EndpointsImpl.GetThreadBriefsAsync)
                .WithName("GetThreadBriefs")
                .WithOpenApi()
                .WithTags("BrowseStoryTags");

            app.MapGet("/motives", EndpointsImpl.GetMotiveBriefsAsync)
                .WithName("GetMotiveBriefs")
                .WithOpenApi()
                .WithTags("BrowseStoryTags");

            app.MapGet("/stories", EndpointsImpl.GetStoryBriefsAsync)
                .WithName("GetStoryBriefs")
                .WithOpenApi()
                .WithTags("BrowseStories");

            app.MapGet("/locations", EndpointsImpl.GetUniqueLocationRecordAsync)
                .WithName("GetUniqueLocationRecords")
                .WithOpenApi()
                .WithTags("BrowseLocations");


            app.MapGet("/stories/{storyUid}", EndpointsImpl.GetStoryByUidAsync)
                .WithName("GetSingleAggregatedStoryByUid")
                .WithOpenApi()
                .WithTags("BrowseStories");

            app.MapGet("/actors/{actorUid}", EndpointsImpl.GetActorByUidAsync)
                .WithName("GetSingleAggregatedActorByUid")
                .WithOpenApi()
                .WithTags("BrowseActors");

            app.MapGet("/threads/{threadUid}", EndpointsImpl.GetThreadByUidAsync)
                .WithName("GetSingleThreadByUid")
                .WithOpenApi()
                .WithTags("BrowseStoryTags");

            app.MapGet("/motives/{motiveUid}", EndpointsImpl.GetMotiveByUidAsync)
                .WithName("GetSingleMotiveByUid")
                .WithOpenApi()
                .WithTags("BrowseStoryTags");

            app.MapGet("/actorthreadstoryblocks/{actorUid}", EndpointsImpl.GetActorThreadStoryBlocksByActorNameAsync)
                .WithName("GetActorThreadStoryBlocksByActorName")
                .WithOpenApi()
                .WithTags("BrowseActors");

            app.MapGet("/locations/{locationPath}", EndpointsImpl.GetSingleLocationDetailsByPathAsync)
                .WithName("GetSingleLocationRecordDetails")
                .WithOpenApi()
                .WithTags("BrowseLocations");

            MapDiagnosticEndpoints(app);
        }

        private static void MapDiagnosticEndpoints(WebApplication app)
        {
            // checks if application responds at all
            app.MapGet("/ping", () => { return "ping " + DateTime.Now; })
                .WithName("Ping")
                .WithOpenApi()
                .WithTags("DIAGNOSTICS");

            // checks if there is an access to the directory with DB
            app.MapGet("/cat", () => { return System.IO.File.ReadLines(new AppConfigProvider().GetPathToDatabaseFolder() + "/cat.txt").FirstOrDefault(); })
                .WithName("Cat")
                .WithOpenApi()
                .WithTags("DIAGNOSTICS");
        }
    }

}
