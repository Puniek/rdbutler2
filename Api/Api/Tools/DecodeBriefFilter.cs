﻿using System.Net;

namespace Api.Tools
{
    public static class DecodeBriefFilter
    {
        public static Dictionary<string, string> Decode(string? filteredBy)
        {
            string decodedQuery = filteredBy == null ? string.Empty : WebUtility.UrlDecode(filteredBy);
            string[] decodedAreas = decodedQuery.Split(';').Select(s => s.Trim()).ToArray();

            Dictionary<string, string> filters = new() {};

            foreach (string area in decodedAreas)
            {
                if (area.StartsWith("l:") || area.StartsWith("locations:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("locations", value.Trim());
                }
                else if (area.StartsWith("a:") || area.StartsWith("actors:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("actors", value.Trim());
                }
                else if (area.StartsWith("th:") || area.StartsWith("threads:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("threads", value.Trim());
                }
                else if (area.StartsWith("mt:") || area.StartsWith("motives:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("motives", value.Trim());
                }
                else if (area.StartsWith("p:") || area.StartsWith("players:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("players", value.Trim());
                }
                else if (area.StartsWith("gm:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("gm", value.Trim());
                }
                else if (area.StartsWith("sd:") || area.StartsWith("startDate:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("startDate", value.Trim());
                }
                else if (area.StartsWith("ed:") || area.StartsWith("endDate:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("endDate", value.Trim());
                }
                else if (area.StartsWith("mi:") || area.StartsWith("minIntensity:"))
                {
                    string value = area.Substring(area.IndexOf(":") + 1);
                    filters.Add("minIntensity", value.Trim());
                }
            }

            
            return filters;
        }
    }
}
