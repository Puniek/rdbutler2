---
layout: cybermagic-karta-postaci
categories: archetype
title: "Inspirator"
---

# {{ page.title }}

## Archetyp

### Co robi? (3)

* zauracza i porywa za sobą tłum
* kontroluje uwagę i emocje
* manipuluje urokiem i aktorstwem

### Czego chce? (3)

* TAK: łączyć ludzi wokół Sprawy
* TAK: dawać siłę i nadzieję
* TAK: długoterminowych efektów
* NIE: śmieszność i utrata twarzy
* NIE: działać samotnie, bez wsparcia

### Jak działa? (3)

* TAK: przekonywanie i wpływ
* TAK: naturalnie przejmuje dowodzenie
* TAK: dziełami sztuki czy osobiście
* NIE: działanie w ukryciu, dyskretne
* NIE: działanie wbrew swej Sprawie

### Zasoby i otoczenie (3)

* akcesoria do występów
* grupa głęboko oddanych osób
* sprawa, której jest oddany
* reputacja i pozycja; wolno więcej

### Co to za archetyp

Kontroluje tłumy i przyciąga uwagę, niezależnie od sytuacji. Za nim podążają inni. Z szerokimi uprawnieniami, walczy o Sprawę.

### Tagi i przykłady

* SILNY: Ludzie, Agenci, Magia, Uprawnienia
* SŁABY: Ukrycie, Walka, Technologia
* Artysta. Manipulator. Arystokrata.
* Używa: Tłumy, Charyzma, Uprawnienia

### Przykładowe działania

* zatrzyma lincz/zamieszki swoim rozkazem
* dowie się wszystkiego z plotek i rozmowy
* steruje Pryzmatem używając rozgrzanego tłumu
* spowolni lub skrzywdzi plotką czy agentami
* pozyska coś bardzo trudnego uprawnieniami
