using Archdeck.Actions;
using Archdeck.Entities;
using Commons.Tools;
using NUnit.Framework;

namespace ArchdeckTests
{
    public class TestNandeckProfileConverter
    {
        [Test]
        public void TestHappyPathForSingleProfileFront()
        {
            // Expected
            string expected = @"""Inspirator"",""- zauracza i porywa za sob� t�um\13\- kontroluje uwag� i emocje\13\- manipuluje urokiem i aktorstwem"",""TAK: ��czy� ludzi wok� Sprawy\13\TAK: dawa� si�� i nadziej�\13\TAK: d�ugoterminowych efekt�w\13\NIE: �mieszno�� i utrata twarzy\13\NIE: dzia�a� samotnie, bez wsparcia"",""TAK: przekonywanie i wp�yw\13\TAK: naturalnie przejmuje dowodzenie\13\TAK: dzie�ami sztuki czy osobi�cie\13\NIE: dzia�anie w ukryciu, dyskretne\13\NIE: dzia�anie wbrew swej Sprawie"",""- akcesoria do wyst�p�w\13\- grupa g��boko oddanych os�b\13\- sprawa, kt�rej jest oddany\13\- reputacja i pozycja; wolno wi�cej""";

            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\1911-inspirator.md");
            ProfileArchetype inspirator = ProfileArchetypeFactory.Create(record);
            ProfileCardFront front = ProfileCardFrontFactory.Create(inspirator);

            // When
            var actual = NandeckProfileConverter.Convert(front);

            // Then
            Assert.True(expected == actual);
        }
    }
}