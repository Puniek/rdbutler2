using Archdeck.Entities;
using Commons.Tools;
using NUnit.Framework;

namespace ArchdeckTests
{
    public class TestProfileArchetypeFactory
    {
        [Test]
        public void TestHappyPathForSingleProfileArchetype_CheckingName()
        {
            // Given
            FileRecord record = FileOps.ReadFile(@"TestData\1911-inspirator.md");

            // When
            ProfileArchetype archetype = ProfileArchetypeFactory.Create(record);

            // Then
            Assert.True("Inspirator" == archetype.Name);
        }
    }
}