using Archdeck.Actions;
using Archdeck.Entities;
using NUnit.Framework;

namespace ArchdeckTests
{
    public class TestNandeckConversionEngine
    {
        [Test]
        public void TestMkdnListToNandeckList()
        {
            // Given
            string input = @"* zauracza i porywa za sob� t�um
* kontroluje uwag� i emocje
* manipuluje urokiem i aktorstwem";

            // Expecting
            string expected = @"- zauracza i porywa za sob� t�um\13\- kontroluje uwag� i emocje\13\- manipuluje urokiem i aktorstwem";

            // When
            var actual = NandeckConversionEngine.MkdnListToNandeckList(input);

            // Then
            Assert.True(expected == actual);
        }

        [Test]
        public void TestMkdnListToFlatNewLines()
        {
            // Given
            string input = @"* zauracza i porywa za sob� t�um
* kontroluje uwag� i emocje
* manipuluje urokiem i aktorstwem";

            // Expecting
            string expected = @"zauracza i porywa za sob� t�um\13\kontroluje uwag� i emocje\13\manipuluje urokiem i aktorstwem";

            // When
            var actual = NandeckConversionEngine.MkdnListToFlatNewLines(input);

            // Then
            Assert.True(expected == actual);
        }
    }
}