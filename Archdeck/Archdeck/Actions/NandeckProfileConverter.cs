﻿using System;
using System.Collections.Generic;
using System.Text;
using Archdeck.Entities;

namespace Archdeck.Actions
{
    public static class NandeckProfileConverter
    {
        public static string Convert(ProfileCardFront[] cfs)
        {
            string header = @"Nazwa,Pomysl,Motywacja,Dzialanie,Zasoby";
            StringBuilder sb = new StringBuilder();
            sb.Append(header).Append("\n");
            foreach (var cf in cfs)
            {
                sb.Append(Convert(cf)).Append("\n");
            }
            return sb.ToString();
        }

        public static string Convert(ProfileCardBack[] cbs)
        {
            string header = @"Nazwa,CoTo,Tagi,Akcje";
            StringBuilder sb = new StringBuilder();
            sb.Append(header).Append("\n");
            foreach (var cb in cbs)
            {
                sb.Append(Convert(cb)).Append("\n");
            }
            return sb.ToString();
        }

        public static string Convert(ProfileCardFront front)
        {
            string actions = NandeckConversionEngine.MkdnListToNandeckList(front.Actions);
            string hunger = NandeckConversionEngine.MkdnListToFlatNewLines(front.Hunger);
            string howActs = NandeckConversionEngine.MkdnListToFlatNewLines(front.HowActs);
            string resources = NandeckConversionEngine.MkdnListToNandeckList(front.Resources);

            return $"\"{front.Name}\",\"{actions}\",\"{hunger}\",\"{howActs}\",\"{resources}\"";
        }

        public static string Convert(ProfileCardBack back)
        {
            string whatis = NandeckConversionEngine.MkdnListToFlatNewLines(back.Whatis);
            string tags = NandeckConversionEngine.MkdnListToFlatNewLines(back.Tags);
            string examples = NandeckConversionEngine.MkdnListToFlatNewLines(back.Examples);

            return $"\"{back.Name}\",\"{whatis}\",\"{tags}\",\"{examples}\"";
        }
    }
}
