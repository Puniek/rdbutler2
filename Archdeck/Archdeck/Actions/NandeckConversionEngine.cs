﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Archdeck.Actions
{
    public static class NandeckConversionEngine
    {
        public static string MkdnListToNandeckList(string input)
        {
            return input.Trim().Replace("\r\n", "\\13\\").Replace("\n", "\\13\\").Replace("*", "-");
        }

        public static string MkdnListToFlatNewLines(string input)
        {
            return input.Trim().Replace("\r\n", "\\13\\").Replace("\n", "\\13\\").Replace("* ", "");
        }
    }
}
