﻿using Archdeck.Actions;
using Archdeck.Entities;
using Commons.Tools;

namespace Archdeck
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputPath = @"D:\Data\RiggedDice\rpg\cybermagic\archetypy\postacie";
            string outputFrontFilePath = @"D:\Data\RiggedDice\rpg\cybermagic\nandeck\archetypy-profil-gen-front.txt";
            string outputBackFilePath = @"D:\Data\RiggedDice\rpg\cybermagic\nandeck\archetypy-profil-gen-back.txt";

            FileRecord[] fileRecords = FileOps.ReadFiles(inputPath);

            ProfileArchetype[] pas = ProfileArchetypeFactory.Create(fileRecords);

            ProfileCardFront[] cfs = ProfileCardFrontFactory.Create(pas);
            ProfileCardBack[] cbs = ProfileCardBackFactory.Create(pas);

            
            string nandeckCardFronts = NandeckProfileConverter.Convert(cfs);
            string nandeckCardBacks = NandeckProfileConverter.Convert(cbs);
            
            FileOps.SaveFile(nandeckCardFronts, outputFrontFilePath);
            FileOps.SaveFile(nandeckCardBacks, outputBackFilePath);
            /**/

        }
    }
}
